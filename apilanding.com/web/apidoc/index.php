<?php
    include '../functions/db.php';
    include_once '../functions/language.php';


	$tituloapi = "<-- Seleccione una API para ver su documentaci�n.";
	$idapi = "";

	$sql = "SELECT * from web_apis A left join web_apis_desc B ON A.id = B.idapi WHERE A.id = " . $_GET['idapi'];
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$id = $_GET['idapi'];
			$idapi = $_GET['idapi'];
			$nombre = $row["nombre"];
			$descripcion = $row["descripcion"];
			$proveedor = $row["proveedor"];
			$contexto = $row["contexto"];
			$linkapicenter = $row["linkapicenter"];
			$linkdoc = $row["linkdoc"];
			$imagen1 = $row["imagen1"];
			$imagen2 = $row["imagen2"];
			$tiposuscripcion = $row["tiposuscripcion"];
			$idestado = $row["idestado"];
			$categorias = $row["categorias"];
			$industrias = $row["industrias"];
			$fechavigencia = $row["fechavigencia"];
			$auditor = $row["auditor"];
			$fechabaja = $row["fechabaja"];
			$idioma = $row["idioma"];
			$descripcioncomercial = $row["descripcioncomercial"];
			$descripciontecnica = $row["descripciontecnica"];
			$casoexito = $row["casoexito"];
			$metodo = $row["metodo"];
			$tipoapi = $row["tipoapi"];

			if ($tipoapi == "1"){
				$contexto = "database/apidata";
			}

			$tituloapi = $nombre . " - " . $descripcion . "(" . $contexto . ")";
		}
	}




	$serverapi = 'https://cont1-virtual1.certisend.com/web/container/api/v1/';


	$sql = "SELECT * from web_apis_parametros WHERE idapi = " . $_GET['idapi'];
	$resultparametros = $conn->query($sql);
	$urlget = '';
	if ($tipoapi == "1"){
		$urlget = 'apicode=YOUR_API_CODE&';
	}
	if ($resultparametros->num_rows > 0) {
		while($row = $resultparametros->fetch_assoc()) {
			$urlget = $urlget . $row['parametro'] . '=VALUE_OF_' . strtoupper($row['parametro']) . '&';
		}
	}
	$urlget = $urlget . "internalid=YOUR_INTERNAL_ID";



	$sql = "SELECT * from web_apis_parametros WHERE idapi = " . $_GET['idapi'];
	$resultparametros = $conn->query($sql);


	$sql = "SELECT * from web_apis_campos_respuesta WHERE idapi = " . $_GET['idapi'];
	$resultrespuestas = $conn->query($sql);






?>

    <!DOCTYPE html>
    <html lang="en">

    <!-- Mirrored from pixxett.com/htmldemos/api/v-2/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 06 May 2019 01:28:38 GMT -->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <!--[if IE]>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <![endif]-->

        <meta charset="iso-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="keywords" content="<?php echo $lang['SITE_keywords'];?>">
        <meta name="description" content="<?php echo $lang['SITE_desc'];?>">
        <meta name="author" content="Sysworld Servicios S.A.">

        <title>
            <?php echo $lang['SITE_title'];?>
        </title>

        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900">
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900">

        <link rel="stylesheet" type="text/css" href="bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="css/jquery.mobile-menu.css">
        <link rel="stylesheet" type="text/css" href="css/style.css" media="all">
        <link rel="stylesheet" type="text/css" href="css/responsive.css">
        <link rel="icon" type="image/x-icon" href="images/favicon.ico" />
    </head>

    <body id="pixxett-api">
        <div id="page">

            <header class="header" id="header">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-1 col-sm-2">
                            <div class="mm-toggle-wrap">
                                <div class="mm-toggle"><i class="fa fa-align-justify"></i><span class="mm-label">Menu</span> </div>
                            </div>

                            <a class="header__block header__brand" href="#">
                                <img src="images/logo.png" width="154px" height="38px">
                            </a>

                        </div>
                        <div class="col-lg-11 col-sm-10 hidden-xs">
                            <div class="header__nav">
                                <div class="header__nav--left">
                                    <ul class="dx-nav-0 dx-nav-0-docs">
                                        <li class="dx-nav-0-item ">
                                            <a class="dx-nav-0-link" href="javascript:void(0);">
                                                <- Volver</a>
                                        </li>
                                    </ul>
                                    <form class="header__search dx-form-search" id="siteSearch" method="get" action="index.php">
                                        <label class="sr-only" for="siteQ">API Docs</label>
                                        <select class="dx-search-input" id="siteQ" name="idapi" type="search" value="" placeholder="API Docs" onchange="this.form.submit()">
                                            <option>SELECCIONE</option>
											<?php
											$sql = "SELECT A.id, A.nombre, A.descripcion, A.proveedor, A.contexto, A.imagen1, A.tiposuscripcion from web_apis A left join web_apis_desc B ON A.id = B.idapi WHERE A.idestado = 1";
											$result = $conn->query($sql);
											if ($result->num_rows > 0) {
												while($row = $result->fetch_assoc()) {
													if ($idapi == $row["id"]){
														echo '<option value="' . $row["id"] . '" data-tokens="' . $row["descripcion"] . '" selected>' . $row["contexto"] . ' (' .$row["descripcion"] . ')</option>';
													}else{
														echo '<option value="' . $row["id"] . '" data-tokens="' . $row["descripcion"] . '">' . $row["contexto"] . ' (' .$row["descripcion"] . ')</option>';
													}
												}
											}
											?>
                                        </select>
                                        <span class="button-search fa fa-search"></span>
                                    </form>
                                    <div>&nbsp;&nbsp;&nbsp;<b><u><?php echo $tituloapi;?></u></b></div>
                                </div>
                                <div class="header__nav--right">
                                    <ul class="dx-nav-0 dx-nav-0-tools">
                                        <li class="dx-nav-0-item ">
                                            <a class="dx-nav-0-link" href="../index.php">Sitio Comercial</a>
                                        </li>
                                        <li class="dx-nav-0-item ">
                                            <a class="dx-nav-0-link" href="../apitest.php">Probar APIS</a>
                                        </li>
                                        <li class="dx-nav-0-item ">
                                            <a class="dx-nav-0-link" href="../support.php">Soporte</a>
                                        </li>
                                    </ul>
                                    <div class="dx-auth-block">
                                        <div class="dx-auth-logged-out">
                                            <a class="dx-auth-login dx-btn dx-btn-primary" data-js="auth-btn" href="../login.php" data-dxa="login,nav-click,nav-login">API Center</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <div class="header-section-wrapper">
                <div class="header-section header-section-example">
                    <div id="language">
                        <ul class="language-toggle">
                            <li>
                                <input type="radio" class="language-toggle-source" name="language-toggle" id="toggle-lang-curl" data-language="curl" checked="checked">
                                <label for="toggle-lang-curl" class="language-toggle-button language-toggle-button--curl">curl</label>
                            </li>
                            <li>
                                <input type="radio" class="language-toggle-source" name="language-toggle" id="toggle-lang-ruby" data-language="ruby">
                                <label for="toggle-lang-ruby" class="language-toggle-button language-toggle-button--ruby">Ruby</label>
                            </li>
                            <li>
                                <input type="radio" class="language-toggle-source" name="language-toggle" id="toggle-lang-python" data-language="python">
                                <label for="toggle-lang-python" class="language-toggle-button language-toggle-button--python">Python</label>
                            </li>
                            <li>
                                <input type="radio" class="language-toggle-source" name="language-toggle" id="toggle-lang-php" data-language="php">
                                <label for="toggle-lang-php" class="language-toggle-button language-toggle-button--php">PHP</label>
                            </li>
                            <li>
                                <input type="radio" class="language-toggle-source" name="language-toggle" id="toggle-lang-java" data-language="java">
                                <label for="toggle-lang-java" class="language-toggle-button language-toggle-button--java">Java</label>
                            </li>
                            <li>
                                <input type="radio" class="language-toggle-source" name="language-toggle" id="toggle-lang-net" data-language="net">
                                <label for="toggle-lang-net" class="language-toggle-button language-toggle-button--net">#C</label>
                            </li>
                            <li>
                                <input type="radio" class="language-toggle-source" name="language-toggle" id="toggle-lang-asp" data-language="asp">
                                <label for="toggle-lang-asp" class="language-toggle-button language-toggle-button--asp">ASP</label>
                            </li>
                            <li>
                                <input type="radio" class="language-toggle-source" name="language-toggle" id="toggle-lang-vb6" data-language="vb6">
                                <label for="toggle-lang-vb6" class="language-toggle-button language-toggle-button--vb6">VB6</label>
                            </li>
                            <li>
                                <input type="radio" class="language-toggle-source" name="language-toggle" id="toggle-lang-node" data-language="node">
                                <label for="toggle-lang-node" class="language-toggle-button language-toggle-button--node">Node</label>
                            </li>
                            <li>
                                <input type="radio" class="language-toggle-source" name="language-toggle" id="toggle-lang-go" data-language="go">
                                <label for="toggle-lang-go" class="language-toggle-button language-toggle-button--go">Go</label>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div id="documenter_sidebar">
                <div id="scrollholder" class="scrollholder">
                    <div id="scroll" class="scroll">
                        <ol id="documenter_nav">
                            <li><a class="current" href="#documenter-1"><i class="fa fa-file-text-o"></i> Referencias API</a></li>
                            <li><a href="#documenter-2"><i class="fa fa-key"></i> Autenticaci�n </a>
                            </li>
                            <li>
                                <a href="#documenter-4"><i class="fa fa-table"></i> Par�metros</a>
                                <ol>
                                    <li><a href="#documenter-4-1">Respuestas</a></li>
                                </ol>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>

            <div id="background">
                <div class="background-actual"></div>
            </div>

            <div id="documenter_content" class="method-area-wrapper">
                <section id="documenter-1" class="method">
                    <div class="method-area">
                        <div class="method-copy">
                            <div class="method-copy-padding">
                                <h3>Referencias API</h3>
                                <p>
                                    API significa "Application Programming Interface". Una API proporciona un conjunto de comandos, funciones y protocolos para facilitar la programaci�n de software. Estas funciones predefinidas simplifican la interacci�n del programador con el sistema operativo, ya que el hardware (monitor, datos en el disco duro, etc.) no tiene que ser direccionado directamente. En lo que se refiere a Internet, las APIs Web est�n a la vanguardia y tambi�n sirven como interfaz para permitir el uso de funciones existentes de terceros.
                                </p>
                                <p> <img src="images/place-holder.png" alt="Image"> </p>
                            </div>
                        </div>
                        <div class="method-example">
                            <div class="method-example-part">
                                <h5>API Reference</h5>
                                <p>
                                    Curabitur lacinia convallis nibh, <a href="#">non cursus augue</a>. Quisque id sem id lorem porttitor efficitur in quis libero. Nullam turpis ante auctor <a href="#">purus sed</a>.
                                </p>
                            </div>
                            <div class="method-example-part">
                                <div class="method-example-endpoint">
                                    <pre class=" language-none"><code class=" language-none">https://examples.com</code></pre>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="documenter-2" class="method">
                    <div class="method-area">
                        <div class="method-copy">
                            <div class="method-copy-padding">
                                <h3> Authentication </h3>
                                <p>
                                    Ante la imparable evoluci�n de aplicaciones, frameworks, lenguajes y alternativas para facilitarnos
                                    las tareas de Desarrollo Web en diferentes �mbitos, hablaremos a continuaci�n sobre el modelo
                                    de autenticaci�n para API REST de nuestra infraestructura. Las API REST que nuestro cloud entrega
                                    son uno de los mejores recursos actuales para cualquier desarrollador y en esta ocasi�n,
                                    queremos que conozcas mejor las caracter�sticas y el flujo de este modelo.
                                </p>
                                <p>
                                    En el mundo de las API REST, se deben implementar flujos distintos de los habituales en
                                    cuanto a la autenticaci�n de usuarios, ya que su arquitectura nos ofrece otro tipo de
                                    recursos que los tradicionales.
                                    Por lo general, en las aplicaciones web tradicionales, se utilizan las variables de
                                    sesi�n como soporte para memorizar el usuario.
                                    Esto viene a decir que en el momento de autenticar un usuario, el servidor genera las
                                    correspondientes variables de sesi�n y en las siguientes p�ginas consultadas por ese
                                    mismo cliente, es capaz de recordar al usuario.
                                </p>
                                <p>
                                    Sin embargo, en las aplicaciones REST, esto no es posible, ya que una de las limitaciones
                                    de este sistema es que no se dispone de variables de sesi�n. Esto es debido a motivos
                                    de escalabilidad, ya que las API REST suelen estar alojadas en un cluster de servidores.
                                    La soluci�n habitual para salvar este problema es la autenticaci�n por token.
                                </p>
                                <p>
                                    Flujo de autenticaci�n por usuario y contrase�a</br>
                                    El hecho de usar un token como mecanismo involucrado en la autenticaci�n no produce
                                    efectos visibles para los usuarios, ya que estos no tienen que lidiar con �l directamente.
                                    Dicho de otra manera, todas las diferencias en el flujo son transparentes para el usuario.
                                </p>
                                <p>
                                    Nuestro API Cloud Center trabaja de forma standarizada y por defecto con autenticaci�n de token basado en parametros querys:<br><br>
                                    <b>Token de Suscripci�n (token-susc):</b> Este token permite en cada llamado a la api identificar la suscripci�n del usuario.<br>
                                    <b>Token de API (token-api):</b> Este token permite identificar la key de la API que se requiere consumir.<br>
                                </p>
                                <p>
									Otros Mecanismos de Seguridad: En caso que el usuario as� lo requiera pueda proteger sus tokens y keys utilizando diferentes mecanismos de seguridad como:<br>
									- Authentication by Header<br>
									- API EndPoint to Generate Tokens Dynamics.<br>
									- oauth2<br>
                                </p>
                            </div>
                            <div class="method-list attributes">
                                <h5 class="method-list-title"> Parametros Seguridad</h5>
                                <ul class="method-list-group">
                                    <li class="method-list-item">
                                        <h6 class="method-list-item-label"><a href="#" class="header-anchor"></a>
											token-susc
										</h6>
                                        <p class="method-list-item-description">
                                            Mediante el parametro token-susc el sistema debera recibir la key seteada a su suscripci�n y que se encuentra disponible en su Panel Comercial.<br>
                                            Ejemplo: <code class=" language-undefined">"token-susc":"20190d614196076fb924df8dd392c5as"</code>
                                        </p>
                                    </li>
                                    <li class="method-list-item">
                                        <h6 class="method-list-item-label"><a href="#" class="header-anchor"></a>
											token-api
										</h6>
                                        <p class="method-list-item-description">
                                            Mediante el parametro token-api el sistema debera recibir la key seteada a la API que esta queriendo consumir. Dicha API puede ser solicitada desde su Panel Comercial.<br>
                                            Ejemplo: <code class=" language-undefined">"token-api":"9f9f0fe087e970b8c21c4577e1cd95af"</code>
                                        </p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="method-example">
                            <div class="method-example-part">
                                <h5> Errores de Autenticaci�n: </h5>
                                <div class="table-responsive">
                                    <div class="table">
                                        <table class="table-container">
                                            <colgroup>
                                                <col class="col-xs-3">
                                                    <col class="col-xs-9">
                                            </colgroup>
                                            <tbody>
                                                <tr>
                                                    <th class="table-row-property">200 - OK</th>
                                                    <td class="table-row-definition">OK.</td>
                                                </tr>
                                                <tr>
                                                    <th class="table-row-property">401 - Security</th>
                                                    <td class="table-row-definition">Security tokens not found.</td>
                                                </tr>
                                                <tr>
                                                    <th class="table-row-property">401 - Security</th>
                                                    <td class="table-row-definition">Security tokens not defined.</td>
                                                </tr>
                                                <tr>
                                                    <th class="table-row-property">401 - Security</th>
                                                    <td class="table-row-definition">Security tokens error.</td>
                                                </tr>
                                                <tr>
                                                    <th class="table-row-property">409 - Unauthorized</th>
                                                    <td class="table-row-definition">Suscription stoped.</td>
                                                </tr>
                                                <tr>
                                                    <th class="table-row-property">409 - Unauthorized</th>
                                                    <td class="table-row-definition">Api offline or discontinued.</td>
                                                </tr>

                                                <tr>
                                                    <th class="table-row-property">429 - Limits</th>
                                                    <td class="table-row-definition">Limit exceeded.</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="documenter-4" class="method">
                    <div class="method-area">
                        <div class="method-copy">
                            <div class="method-copy-padding">
                                <h3 id="handling-errors">Parametros de Entrada</h3>
                                <p>
                                    Mediante el metodo de llamado <code class=" language-undefined"><?php echo $metodo;?></code> la API <strong><?php echo $nombre;?> (<?php echo $descripcion;?>)</strong> puede recibir los siguientes parametros de entrada:<br>
                                </p>
                            </div>
                            <div class="method-list attributes">
                                <h5 class="method-list-title"> Parametros Entrada</h5>
                                <ul class="method-list-group">
									<?php
										if ($tipoapi == "1"){
									?>
												<li class="method-list-item">
													<div><b><u>Code of your API Data.</u></b></div><br>
													<h6 class="method-list-item-label"><a href="#" class="header-anchor"></a>
														apicode
													</h6>
													<p class="method-list-item-description">
														<b>Tipo:</b> <code class=" language-undefined">string</code><br>
														<b>Requerido:</b> <code class=" language-undefined">true</code><br>
														<b>Formato:</b> <code class=" language-undefined">query</code><br>
													</p>
												</li>
									<?php
										}
									?>


									<?php
										if ($resultparametros->num_rows > 0) {
											while($row = $resultparametros->fetch_assoc()) {
									?>
												<li class="method-list-item">
													<div><b><u><?php echo $row['descripcion'];?></u></b></div><br>
													<h6 class="method-list-item-label"><a href="#" class="header-anchor"></a>
														<?php echo $row['parametro'];?>
													</h6>
													<p class="method-list-item-description">
														<b>Tipo:</b> <code class=" language-undefined"><?php echo $row['tipo'];?></code><br>
														<b>Requerido:</b> <code class=" language-undefined"><?php echo $row['requerido'];?></code><br>
														<b>Formato:</b> <code class=" language-undefined"><?php echo $row['formato'];?></code><br>
													</p>
												</li>
									<?php
											}
										}
									?>
                                </ul>
                            </div>

                        </div>
                        <div class="method-example">
                            <div class="code-curl code-div active-code">
                                <div class="method-example-part">
                                    <div class=" language-undefined">
                                        <pre class="language-php"><code class="language-php">
											curl -X GET "<?php echo $serverapi;?><?php echo $contexto;?>?token-susc=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX&token-api=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX&<?php echo $urlget;?>" -H "accept: application/json"
										</code></pre>
                                    </div>
                                </div>
                            </div>
                            <div class="code-python code-div">
                                <div class="method-example-part">
                                    <div class=" language-undefined">
                                        <pre class="language-php"><code class="language-php">
											import requests
											r = requests.get('<?php echo $serverapi;?><?php echo $contexto;?>?token-susc=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX&token-api=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX&<?php echo $urlget;?>')
											r.json()
										</code></pre>
                                    </div>
                                </div>
                            </div>
                            <div class="code-ruby code-div">
                                <div class="method-example-part">
                                    <div class=" language-undefined">
                                        <pre class="language-php"><code class="language-php">
											require 'net/http'
											require 'json'

											url = '<?php echo $serverapi;?><?php echo $contexto;?>?token-susc=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX&token-api=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX&<?php echo $urlget;?>'
											uri = URI(url)
											response = Net::HTTP.get(uri)
											JSON.parse(response)
										</code></pre>
                                    </div>
                                </div>
                            </div>
                            <div class="code-php code-div">
                                <div class="method-example-part">
                                    <div class=" language-undefined">
                                        <pre class="language-php"><code class="language-php">
											$json = file_get_contents('<?php echo $serverapi;?><?php echo $contexto;?>?token-susc=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX&token-api=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX&<?php echo $urlget;?>');
											echo $json;
											$obj = json_decode($json);
										</code></pre>
                                    </div>
                                </div>
                            </div>
                            <div class="code-java code-div">
                                <div class="method-example-part">
                                    <div class=" language-undefined">
                                        <pre class="language-php"><code class="language-php">
											import java.io.BufferedReader;
											import java.io.IOException;
											import java.io.InputStream;
											import java.io.InputStreamReader;
											import java.io.Reader;
											import java.net.URL;
											import java.nio.charset.Charset;

											import org.json.JSONException;
											import org.json.JSONObject;

											public class JsonReader {

											  private static String readAll(Reader rd) throws IOException {
												StringBuilder sb = new StringBuilder();
												int cp;
												while ((cp = rd.read()) != -1) {
												  sb.append((char) cp);
												}
												return sb.toString();
											  }

											  public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
												InputStream is = new URL(url).openStream();
												try {
												  BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
												  String jsonText = readAll(rd);
												  JSONObject json = new JSONObject(jsonText);
												  return json;
												} finally {
												  is.close();
												}
											  }

											  public static void main(String[] args) throws IOException, JSONException {
												JSONObject json = readJsonFromUrl("<?php echo $serverapi;?><?php echo $contexto;?>?token-susc=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX&token-api=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX&<?php echo $urlget;?>");
												System.out.println(json.toString());
												System.out.println(json.get("id"));
											  }
											}
										</code></pre>
                                    </div>
                                </div>
                            </div>
                            <div class="code-vb6 code-div">
                                <div class="method-example-part">
                                    <div class=" language-undefined">
                                        <pre class="language-php"><code class="language-php">
											Private Function GetAPIDATA() As String
												Dim xmlHttp As Object
												Dim strTokenSusc as String
												Dim strTokenAPI as String
												Dim strUrl as String

												strTokenSusc = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
												strTokenAPI = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

												strUrl = "<?php echo $serverapi;?><?php echo $contexto;?>?token-susc=" & strTokenSusc & "&token-api=" & strTokenAPI & "&<?php echo $urlget;?>"


												Set xmlHttp = CreateObject("MSXML2.XmlHttp")
												xmlHttp.Open "GET", sURL, False
												xmlHttp.send
												GetAPIDATA = xmlHttp.responseText
												Set xmlHttp = Nothing
											End Function
										</code></pre>
                                    </div>
                                </div>
                            </div>
                            <div class="code-asp code-div">
                                <div class="method-example-part">
                                    <div class=" language-undefined">
                                        <pre class="language-php"><code class="language-php">
											<%
												dim url
												url = "<?php echo $serverapi;?><?php echo $contexto;?>?token-susc=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX&token-api=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX&<?php echo $urlget;?>"
												Set HttpReq = Server.CreateObject("MSXML2.ServerXMLHTTP")
												HttpReq.open "GET", url, false
												HttpReq.setRequestHeader "Content-Type", "application/json"
												HttpReq.Send()
											%>
										</code></pre>
                                    </div>
                                </div>
                            </div>
                            <div class="code-net code-div">
                                <div class="method-example-part">
                                    <div class="language-php">
                                        <pre class="language-php"><code class="language-php">
											using (var httpClient = new System.Net.Http.HttpClient())
											{
												var json = await httpClient.GetStringAsync("<?php echo $serverapi;?><?php echo $contexto;?>?token-susc=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX&token-api=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX&<?php echo $urlget;?>");
											}
										</code></pre>
                                    </div>
                                </div>
                            </div>
                            <div class="code-node code-div">
                                <div class="method-example-part">
                                    <div class=" language-undefined">
                                        <pre class="language-php"><code class="language-php">
											var https = require('https');

											var options = {
												host: '<?php echo $serverapi;?>',
												path: '<?php echo $contexto;?>?token-susc=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX&token-api=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX&<?php echo $urlget;?>',
												headers: {
													'Accept': 'application/json'
												}
											};
											https.get(options, function (res) {
												var json = '';

												res.on('data', function (chunk) {
													json += chunk;
												});

												res.on('end', function () {
													if (res.statusCode === 200) {
														try {
															var data = JSON.parse(json);
															// data is available here:
															console.log(json);
														} catch (e) {
															console.log('Error parsing JSON!');
														}
													} else {
														console.log('Status:', res.statusCode);
													}
												});
											}).on('error', function (err) {
												console.log('Error:', err);
											});
										</code></pre>
                                    </div>
                                </div>
                            </div>
                            <div class="code-go code-div">
                                <div class="method-example-part">
                                    <div class=" language-undefined">
                                        <pre class="language-php"><code class="language-php">
											package main

											import (
												"fmt"
												"net/http"
												"io/ioutil"
												"os"
												)

											func main() {
												response, err := http.Get("<?php echo $serverapi;?><?php echo $contexto;?>?token-susc=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX&token-api=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX&<?php echo $urlget;?>")
												if err != nil {
													fmt.Printf("%s", err)
													os.Exit(1)
												} else {
													defer response.Body.Close()
													contents, err := ioutil.ReadAll(response.Body)
													if err != nil {
														fmt.Printf("%s", err)
														os.Exit(1)
													}
													fmt.Printf("%s\n", string(contents))
												}
											}
										</code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="documenter-4-1" class="method">
                    <div class="method-area">
                        <div class="method-copy">
                            <div class="method-copy-padding">
                                <h3>Parametros de Salida</h3>
                                <p>Una vez consumida la API y siempre y cuando la respuesta de la misma y el contro de autenticaci�n retorne un 200-OK, la misma arrojara los siguientes campos de salida en formato JSON:</p>
                                <div class="table-responsive">
                                    <table class="table table-exampled">
                                        <thead>
                                            <tr>
                                                <td class="text-center"><i class="fa fa-trash"></i></td>
                                                <td>ID</td>
                                                <td>Campo</td>
                                                <td>Tipo</td>
                                                <td>Longitud</td>
                                                <td>Descripci�n</td>
                                            </tr>
                                        </thead>
                                        <tbody>
											<?php
												if ($resultrespuestas->num_rows > 0) {
													while($row = $resultrespuestas->fetch_assoc()) {
											?>
														<tr>
															<td class="text-center">
																<div class="checkbox margin-t-0">
																	<input id="checkbox6" type="checkbox">
																	<label for="checkbox6"></label>
																</div>
															</td>
															<td># <b><?php echo $row['id'];?></b></td>
															<td><?php echo $row['campo'];?></td>
															<td><?php echo $row['tipo'];?></td>
															<td><?php echo $row['longitud'];?></td>
															<td><?php echo $row['descripcion'];?></td>
														</tr>
											<?php
													}
												}
											?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="method-example">
                            <div class="method-example-part">
                                <h5>Otros C�digos HTTP</h5>
                                <div class="table">
                                    <table class="table-container">
                                        <colgroup>
                                            <col class="col-xs-3">
                                                <col class="col-xs-9">
                                        </colgroup>
                                        <tbody>
                                            <tr>
                                                <th class="table-row-property">200 - OK</th>
                                                <td class="table-row-definition">Respuesta est�ndar para peticiones correctas.</td>
                                            </tr>
                                            <tr>
                                                <th class="table-row-property">400 - Bad Request</th>
                                                <td class="table-row-definition">El servidor no procesar� la solicitud, porque no puede, o no debe, debido a algo que es percibido como un error del cliente (ej: solicitud malformada, sintaxis err�nea, etc). La solicitud contiene sintaxis err�nea y no deber�a repetirse.</td>
                                            </tr>
                                            <tr>
                                                <th class="table-row-property">401 - Unauthorized</th>
                                                <td class="table-row-definition">Similar al 403 Forbidden, pero espec�ficamente para su uso cuando la autentificaci�n es posible pero ha fallado o a�n no ha sido provista.</td>
                                            </tr>
                                            <tr>
                                                <th class="table-row-property">404 - Not Found</th>
                                                <td class="table-row-definition">Una petici�n fue hecha a una URI utilizando un m�todo de solicitud no soportado por dicha URI; por ejemplo, cuando se utiliza GET en un formulario que requiere que los datos sean presentados v�a POST, o utilizando PUT en un recurso de solo lectura.</td>
                                            </tr>
                                            <tr>
                                                <th class="table-row-property">409 - Conflict</th>
                                                <td class="table-row-definition">Indica que la solicitud no pudo ser procesada debido a un conflicto con el estado actual del recurso que esta identifica.</td>
                                            </tr>
                                            <tr>
                                                <th class="table-row-property">429 - Too Many Requests</th>
                                                <td class="table-row-definition">Hay muchas conexiones desde esta direcci�n de internet.</td>
                                            </tr>
                                            <tr>
                                                <th class="table-row-property">500, 502, 503, 504 - Server Errors</th>
                                                <td class="table-row-definition">El servidor fall� al completar una solicitud aparentemente v�lida.</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>

        <div id="mobile-menu">
            <div class="mobile-menu-inner">
                <ul>
                    <li>
                        <div class="mm-search">
                            <form id="search1" name="search">
                                <div class="input-group">
                                    <input type="text" class="form-control simple" placeholder="Search ..." name="srch-term" id="srch-term">
                                    <div class="input-group-btn">
                                        <button class="btn btn-default" type="submit"><i class="fa fa-search"></i> </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </li>
                    <li>
                        <a href="#documenter-1">Page links</a>
                        <ul>
                            <li><a href="#documenter-1">API Reference</a> </li>
                            <li><a href="#documenter-2">Authentication </a> </li>
                            <li>
                                <a href="#documenter-3">Errors </a>
                                <ul>
                                    <li><a href="#documenter-3-1">Handling errors</a></li>
                                </ul>
                            </li>
                            <li><a href="#documenter-4">Expanding Objects</a> </li>
                            <li><a href="#documenter-5">Idempotent Requests</a> </li>
                        </ul>
                    </li>
                </ul>
                <div class="top-links">
                    <ul class="links">
                        <li><a title="Docs" href="../testapi.php">Probar APIS</a> </li>
                        <li><a title="Support" href="https://cloud.sysworld.com.ar/clients/login" target="_blank">Tickets</a> </li>
                        <li><a title="Dashboard" href="../index.php">Sitio Comercial</a> </li>
                        <li class="last"><a title="../login.php" href="javascript:void(0)">API Center</a> </li>
                    </ul>
                </div>
            </div>
        </div>
        <script src="js/prism.js"></script>
        <script src="js/jquery.1.6.4.js"></script>
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.scrollTo-1.4.2-min.js"></script>
        <script src="js/jquery.easing.js"></script>
        <script>
            document.createElement('section');
            var duration = 500,
                easing = 'swing';
        </script>
        <script src="js/slides.min.jquery.js"></script>
        <script src="js/script.js"></script>
        <script src="js/scroll.js"></script>
        <script src="js/common.js"></script>
        <script src="js/jquery.mobile-menu.min.js"></script>
        <script>
            ScrollLoad("scrollholder", "scroll", false);
        </script>
    </body>

    <!-- Mirrored from pixxett.com/htmldemos/api/v-2/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 06 May 2019 01:28:44 GMT -->

    </html>