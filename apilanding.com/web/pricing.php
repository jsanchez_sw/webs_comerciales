<?php
    include 'functions/db.php';

    include_once 'functions/language.php';
?>
<!DOCTYPE html>
<html lang="<?php echo $lang['SITE_lang'];?>">

<!-- Mirrored from protechtheme.com/saas/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:17:32 GMT -->
<head>
        <!-- Required meta tags -->
        <meta charset="iso-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="keywords" content="<?php echo $lang['SITE_keywords'];?>">
		<meta name="description" content="<?php echo $lang['SITE_desc'];?>">
		<meta name="author" content="Sysworld Servicios S.A.">
        <title><?php echo $lang['SITE_title'];?></title>

        <!-- Bootstrap core CSS -->
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Select2 -->
        <link href="assets/select2/css/select2.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- Iconmoon -->
        <link href="assets/iconmoon/css/iconmoon.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="css/custom.css" rel="stylesheet">
        <link rel="icon" type="image/x-icon" href="images/favicon.ico" />
    </head>
    <body>

<!-- ==============================================
**Preloader**
=================================================== -->
        <div id="loader">
            <div id="element">
                <div class="circ-one"></div>
                <div class="circ-two"></div>
            </div>
        </div>

<!-- ==============================================
**Header**
=================================================== -->
        <header>
			<?php include 'menu.php';?>
        </header>

<!-- ==============================================
**Inner Banner**
=================================================== -->
        <section class="inner-banner pricing-banner">
            <div class="container">
                <div class="contents">
                    <h1><?php echo $lang['PRICES_Prices'];?></h1>
                    <p><?php echo $lang['PRICES_Header'];?></span></p>
                </div>
            </div>
        </section>

<!-- ==============================================
**Select Sutable Pricing**
=================================================== -->
        <section class="select-pricing-plan padding-lg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5">
                        <h2><?php echo $lang['PRICES_section1_title'];?></h2>
                        <p><?php echo $lang['PRICES_section1_desc'];?></p>
                    </div>
                    <div class="col-lg-7">
                        <figure> <img src="images/select-pricing-img.png" class="img-fluid" alt=""> </figure>
                    </div>
                </div>
            </div>
        </section>

<!-- ==============================================
**Choose Pack**
=================================================== -->
        <section class="choose-pack padding-lg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <h2><?php echo $lang['PRICES_section2_title'];?></h2>
                        <p><?php echo $lang['PRICES_section2_desc'];?></p>
                    </div>
                </div>
                <ul class="row">
                    <li class="col-md">
                        <div class="inner">
                            <div class="head-block">
                                <div class="plan-title"> <span><?php echo $lang['PRICES_Plan1_Top'];?></span>
                                    <h3><?php echo $lang['PRICES_Plan1_Title'];?></h3>
                                </div>
                                <div class="price">
                                    <div class="graph"><span class="org"></span><span></span><span></span></div>
                                    <div class="right">
                                        <div class="amt"><?php echo $lang['PRICES_Plan1_Price'];?><sup>$</sup></div>
                                        <div class="month"><?php echo $lang['PRICES_Plan1_Price_Freq'];?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="cnt-block">
                                <ul>
                                    <?php echo $lang['PRICES_Plan1_Items'];?>
                                </ul>
                                <span class="you-choose"><?php echo $lang['PRICES_Plan1_Footer'];?></span> <a href="register.php?p=bronze" class="btn get-started"><?php echo $lang['PRICES_Plan_Button'];?></a> </div>
                        </div>
                    </li>
                    <li class="col-md">
                        <div class="inner">
                            <div class="head-block">
                                <div class="plan-title"> <span><?php echo $lang['PRICES_Plan2_Top'];?></span>
                                    <h3><?php echo $lang['PRICES_Plan2_Title'];?></h3>
                                </div>
                                <div class="price">
                                    <div class="graph"><span class="org"></span><span class="org"></span><span></span></div>
                                    <div class="right">
                                        <div class="amt"><?php echo $lang['PRICES_Plan2_Price'];?><sup>$</sup></div>
                                        <div class="month"><?php echo $lang['PRICES_Plan2_Price_Freq'];?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="cnt-block">
                                <ul>
                                    <?php echo $lang['PRICES_Plan2_Items'];?>
                                </ul>
                                <span class="you-choose"><?php echo $lang['PRICES_Plan2_Footer'];?></span> <a href="register.php?p=silver" class="btn get-started"><?php echo $lang['PRICES_Plan_Button'];?></a> </div>
                        </div>
                    </li>
                    <li class="col-md active">
                        <div class="inner">
                            <div class="head-block">
                                <div class="plan-title"> <span><?php echo $lang['PRICES_Plan3_Top'];?></span>
                                    <h3><?php echo $lang['PRICES_Plan3_Title'];?></h3>
                                </div>
                                <div class="price">
                                    <div class="graph"><span class="org"></span><span class="org"></span><span class="org"></span></div>
                                    <div class="right">
                                        <div class="amt"><?php echo $lang['PRICES_Plan3_Price'];?><sup>$</sup></div>
                                        <div class="month"><?php echo $lang['PRICES_Plan3_Price_Freq'];?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="cnt-block">
                                <ul>
                                    <?php echo $lang['PRICES_Plan3_Items'];?>
                                </ul>
                                <span class="you-choose"><?php echo $lang['PRICES_Plan3_Footer'];?></span> <a href="register.php?p=gold" class="btn get-started"><?php echo $lang['PRICES_Plan_Button'];?></a> </div>
                        </div>
                    </li>
                </ul>
				<div>
					<br><br>Precios expresados en la moneda seleccionada en el encabezado del sitio.
				</div>

            </div>
        </section>

<!-- ==============================================
**Our Features**
=================================================== -->
        <section class="client-speak our-features padding-lg">
            <div class="container">
                <div class="row justify-content-center head-block">
                    <div class="col-md-10"> <span><?php echo $lang['PRICES_section3_title'];?></span>
                        <h2><?php echo $lang['PRICES_section3_subtitle'];?></h2>
                        <p class="hidden-xs"><?php echo $lang['PRICES_section3_desc'];?></p>
                    </div>
                </div>
                <ul class="row features-listing">
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><img src="images/features-ico1.png" alt=""></span>
                            <h3><?php echo $lang['PRICES_section3_item1_title'];?></h3>
                            <p><?php echo $lang['PRICES_section3_item1_desc'];?></p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><img src="images/features-ico2.png" alt=""></span>
                            <h3><?php echo $lang['PRICES_section3_item2_title'];?></h3>
                            <p><?php echo $lang['PRICES_section3_item2_desc'];?></p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><img src="images/features-ico3.png" alt=""></span>
                            <h3><?php echo $lang['PRICES_section3_item3_title'];?></h3>
                            <p><?php echo $lang['PRICES_section3_item3_desc'];?></p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><img src="images/features-ico4.png" alt=""></span>
                            <h3><?php echo $lang['PRICES_section3_item4_title'];?></h3>
                            <p><?php echo $lang['PRICES_section3_item4_desc'];?></p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><img src="images/features-ico5.png" alt=""></span>
                            <h3><?php echo $lang['PRICES_section3_item5_title'];?></h3>
                            <p><?php echo $lang['PRICES_section3_item5_desc'];?></p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><img src="images/features-ico6.png" alt=""></span>
                            <h3><?php echo $lang['PRICES_section3_item6_title'];?></h3>
                            <p><?php echo $lang['PRICES_section3_item6_desc'];?></p>
                        </div>
                    </li>
                </ul>
            </div>
        </section>

<!-- ==============================================
**Need to Discuss Sec**
=================================================== -->
        <section class="need-to-discuss padding-lg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10 head-block">
                        <h2><?php echo $lang['PRICES_section4_title'];?></h2>
                        <p><?php echo $lang['PRICES_section4_desc'];?></p>
                    </div>
                </div>
                <div class="submit-form row">
                    <div class="col-md-6">
                        <input name="First Name" placeholder="First Name" type="text">
                    </div>
                    <div class="col-md-6">
                        <input name="Last Name" placeholder="Last Name" type="text">
                    </div>
                    <div class="col-md-6">
                        <input name="Company" placeholder="Company" type="text">
                    </div>
                    <div class="col-md-6">
                        <input name="Phone Number" placeholder="Phone Number" type="text">
                    </div>
                    <div class="col-md-6">
                        <input name="Business Mail" placeholder="Business Mail" type="text">
                    </div>
                    <div class="col-md-6">
                        <input name="Job Title" placeholder="Job Title" type="text">
                    </div>
                    <div class="col-md-12">
                        <button class="submit-btn"><?php echo $lang['PRICES_section4_button'];?></button>
                    </div>
                </div>
            </div>
        </section>

<!-- ==============================================
**Still Have Questains Sec**
=================================================== -->
        <section class="still-hav-qtns-outer padding-lg">
            <div class="container">
                <h2><?php echo $lang['PRICES_section5_title'];?></h2>
                <ul class="row features-listing">
                    <li class="col-md-4 equal-hight">
                        <div class="info-content"> <span class="icon-holder"><img src="images/stil-hav-sec-icon-1.png" alt=""></span>
                            <h3><?php echo $lang['PRICES_section5_item1_tittle'];?></h3>
                            <p><?php echo $lang['PRICES_section5_item1_desc'];?></p>
                            <a href="support.php" class="know-more">Saber m�s</a> </div>
                    </li>
                    <li class="col-md-4 equal-hight">
                        <div class="info-content"> <span class="icon-holder"><img src="images/stil-hav-sec-icon-2.png" alt=""></span>
                            <h3><?php echo $lang['PRICES_section5_item2_tittle'];?></h3>
                            <p><?php echo $lang['PRICES_section5_item2_desc'];?></p>
                            <a href="faq.php" class="know-more">Saber m�s</a> </div>
                    </li>
                    <li class="col-md-4 equal-hight">
                        <div class="info-content"> <span class="icon-holder"><img src="images/stil-hav-sec-icon-3.png" alt=""></span>
                            <h3><?php echo $lang['PRICES_section5_item3_tittle'];?></h3>
                            <p><?php echo $lang['PRICES_section5_item3_desc'];?></p>
                            <a href="aboutus.php" class="know-more">Saber m�s</a> </div>
                    </li>
                </ul>
            </div>
        </section>

<!-- ==============================================
**Signup Section**
=================================================== -->

<!-- ==============================================
**Footer opt1**
=================================================== -->

	<?php include 'footer.php';?>



        <!-- Scroll to top -->
        <a href="#" class="scroll-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>

        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery.min.js"></script>
        <!-- Popper JS -->
        <script src="js/popper.min.js"></script>
        <!-- Bootsrap JS -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- Select2 JS -->
        <script src="assets/select2/js/select2.min.js"></script>
        <!-- Custom JS -->
        <script src="js/custom.js"></script>
    </body>

<!-- Mirrored from protechtheme.com/saas/pricing.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:17:42 GMT -->
</html>