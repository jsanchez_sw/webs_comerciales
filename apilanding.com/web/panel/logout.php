<?php
    include '../functions/db.php';
    include_once '../functions/language.php';

	$_SESSION['apilanding_user_id'] = '';
	$_SESSION['apilanding_user_email'] = '';
	$_SESSION['apilanding_user_name'] = '';

	header('Location: ../login.php');
?>