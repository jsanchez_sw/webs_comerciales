<?php
    include 'functions/db.php';

    include_once 'functions/language.php';

?>

<!DOCTYPE html>
<html lang="<?php echo $lang['SITE_lang'];?>">

<!-- Mirrored from protechtheme.com/saas/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:17:32 GMT -->
<head>
        <!-- Required meta tags -->
        <meta charset="iso-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="keywords" content="<?php echo $lang['SITE_keywords'];?>">
		<meta name="description" content="<?php echo $lang['SITE_desc'];?>">
		<meta name="author" content="Sysworld Servicios S.A.">
        <title><?php echo $lang['SITE_title'];?></title>
        <!-- Bootstrap core CSS -->
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="css/custom.css" rel="stylesheet">
        <link rel="icon" type="image/x-icon" href="images/favicon.ico" />
    </head>
    <body>

<!-- ==============================================
**Preloader**
=================================================== -->
        <div id="loader">
            <div id="element">
                <div class="circ-one"></div>
                <div class="circ-two"></div>
            </div>
        </div>

<!-- ==============================================
**Register**
=================================================== -->
        <section class="login-outer">
            <div class="content-area">
            <center><img src="images/Apilanding_01.png"></center>
                <div class="login-form-holder">
                    <div class="inner">
                        <div class="login-form">
                            <h3><?php echo $lang['REGISTER_header_title'];?></h3>
                            <form method="post" action="panel/register.php">
                                <div class="name">
                                    <label><?php echo $lang['REGISTER_form_name'];?></label>
                                    <input name="name" placeholder="" type="text">
                                </div>
                                <div class="mail">
                                    <label><?php echo $lang['REGISTER_form_email'];?></label>
                                    <input name="email" placeholder="" type="email" value="<?php echo $_POST['email'];?>">
                                </div>
                                <div class="company">
                                    <label><?php echo $lang['REGISTER_form_company'];?></label>
                                    <input name="company" placeholder="" type="text">
                                </div>
                                <div class="password">
                                    <label><?php echo $lang['REGISTER_form_pass'];?></label>
                                    <input name="password" placeholder="" type="password" value="<?php echo $_POST['password'];?>">
                                </div>
                                <div class="confirm-password">
                                    <label><?php echo $lang['REGISTER_form_pass2'];?></label>
                                    <input name="password2" placeholder="" type="password">
                                </div>
                                <div class="terms">
                                    <label>
                                        <input value="" type="checkbox">
                                        <span><?php echo $lang['REGISTER_form_terms'];?></label>
                                </div>
                                <div class="login-btn-sec">
                                    <button class=" btn login-btn"><?php echo $lang['REGISTER_form_button'];?></button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="box-hav-accnt">
                        <p><?php echo $lang['REGISTER_form_login'];?></p>
                    </div>
                </div>
            </div>
        </section>

        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery.min.js"></script>
        <!-- Popper JS -->
        <script src="js/popper.min.js"></script>
        <!-- Bootsrap JS -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- Select2 JS -->
        <script src="assets/select2/js/select2.min.js"></script>
        <!-- Custom JS -->
        <script src="js/custom.js"></script>
    </body>

<!-- Mirrored from protechtheme.com/saas/register.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:18:11 GMT -->
</html>