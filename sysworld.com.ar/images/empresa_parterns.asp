



<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-66387809-1', 'auto');
  ga('send', 'pageview');

</script>




<!DOCTYPE HTML>
<html dir="ltr" lang="es">
   <head>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
      <meta name="description" content="SysWorld Servicios S.A - es una empresa dedicada a prestar servicios de comunicaciones electrónicas masivas con certificaciones notariales y garantia de ingreso a bandeja de entrada en mÃ¡s de 3.2 billones de cuentas de correo electronico." />
      <meta name="keywords" content="envio certificado,correo certificado,email certificado,e-mail certificado,mail certificado,envio con respaldo notarial,correo con respaldo notarial,email con respaldo notarial,e-mail con respaldo notarial,mail con respaldo notarial,archivo certificado,archivo con respaldo notarial,documento con respaldo notarial,documento certificado,mx backup certificado,respaldo notarial,servicio smtp,envio masivo,comunicaciones masivas,deliverability,antispam,spam,emails masivos,emails transaccionales" />


      <title>Partners - Sysworld Servicios S.A.</title>
      <link rel="stylesheet" type="text/css" href="css/_style.css" />
      <link rel="stylesheet" type="text/css" href="css/_mobile.css" />
      <link rel="stylesheet" type="text/css" href="css/primary-blue.css" />
      <script  type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
      <!--[if lt IE 9]>
      <link rel="stylesheet" href="css/IE.css" />
      <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->
      <!--[if lte IE 8]>
      <script type="text/javascript" src="js/IE.js"></script>
      <![endif]-->
		<script type="text/javascript">
			var LHCChatOptions = {};
			LHCChatOptions.opt = {widget_height:340,widget_width:300,popup_height:520,popup_width:500};
			(function() {
			var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			var refferer = (document.referrer) ? encodeURIComponent(document.referrer.substr(document.referrer.indexOf('://')+1)) : '';
			var location  = (document.location) ? encodeURIComponent(window.location.href.substring(window.location.protocol.length)) : '';
			po.src = '//chat.enviocertificado.com/web/index.php/esp/chat/getstatus/(click)/internal/(position)/bottom_right/(ma)/br/(top)/350/(units)/pixels/(leaveamessage)/true/(department)/1/3?r='+refferer+'&l='+location;
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			})();
		</script>

   </head>
   <body>
      <!-- START Top-Toolbar -->
      <aside class="top-aside clearfix">
         <div class="center-wrap">
            <div class="one_half">
               <div class="sidebar-widget">
                  <ul class="custom-menu">
                     <li><a href="login.asp">Login</a></li>
                     <li><a href="http://www.sysworld.com.ar/portal" target="_blank">Registrarse</a></li>
                     <li><a href="http://www.sysworld.com.ar/portal" target="_blank">Portal Clientes</a></li>
                     <li><a href="legal.asp">Legal</a></li>
                  </ul>
               </div>
            </div>
            <!-- END top-toolbar-left -->
            <div class="one_half">
               <div class="sidebar-widget">
                  <ul class="social_icons">
                     <li><a href="http://help.enviocertificado.com/web/category/novedades/feed/" class="rss">RSS</a></li>
                     <li><a href="https://twitter.com/Sysworldsa" class="twitter">Twitter</a></li>
                     <li><a href="https://www.facebook.com/SysWorld-Servicios-SA-323344231089747" class="facebook">Facebook</a></li>
                     <li><a href="mailto:info@sysworld.com.ar" class="email">Email</a></li>
                  </ul>
               </div>
            </div>
            <!-- END top-toolbar-right -->
         </div>
         <!-- END center-wrap -->
         <div class="top-aside-shadow"></div>
      </aside>
      <!-- END Top-Toolbar -->




      <!-- START Header -->
      <header>
         <div class="center-wrap">
            <div class="companyIdentity">
               <a href="index.asp"><img src="images/Logo_SW_encabezado.png" alt="clientes" /></a>
            </div>
            <!-- END companyIdentity -->




            <!-- START Main Navigation -->
            <nav>
               <ul>
                  <li class="current-menu-item"><a href="index.asp">Inicio</a></li>


                  <li>
                     <a href="empresa.asp">Empresa</a>
                     <ul class="sub-menu">
                        <li><a href="empresa_datacenter.asp">Datacenter</a></li>
                        <li><a href="empresa_calidad.asp">Calidad y Seguridad</a></li>
                        <li><a href="empresa_clientes.asp">Clientes</a></li>
                        <li><a href="empresa_distribuidores.asp">Distribuidores / Agentes</a></li>
                        <li><a href="empresa_parterns.asp">Partners</a></li>
                     </ul>
                  </li>
                  <li>
                     <a href="productos.asp">Productos</a>
                     <ul class="sub-menu">
                        <li>
                           <a href="ern-template.asp">ERN</a>
                           <ul class="sub-menu">
                              <li><a href="ern-funcionamiento.asp">Funcionamiento</a></li>
                              <li><a href="ern-servicios.asp">Servicios</a></li>
                              <li><a href="ern-aplicaciones.asp">Aplicaciones</a></li>
                              <li><a href="ern-ventajas.asp">Ventajas</a></li>
                              <li><a href="ern-preguntas.asp">Preguntas frecuentes</a></li>
                              <li><a href="ern-precios.asp">Precios</a></li>
                           </ul>
                        </li>
                        <li>
                           <a href="arn-template.asp">ARN</a>
                           <ul class="sub-menu">
                              <li><a href="arn-funcionamiento.asp">Funcionamiento</a></li>
                              <li><a href="arn-servicios.asp">Servicios</a></li>
                              <li><a href="arn-aplicaciones.asp">Aplicaciones</a></li>
                              <li><a href="arn-ventajas.asp">Ventajas</a></li>
                              <li><a href="arn-preguntas.asp">Preguntas frecuentes</a></li>
                              <li><a href="arn-precios.asp">Precios</a></li>
                           </ul>

                        </li>
                        <li>
                           <a href="xrn-template.asp">XRN</a>
                           <ul class="sub-menu">
                              <li><a href="xrn-funcionamiento.asp">Funcionamiento</a></li>
                              <li><a href="xrn-servicios.asp">Servicios</a></li>
                              <li><a href="xrn-aplicaciones.asp">Aplicaciones</a></li>
                              <li><a href="xrn-ventajas.asp">Ventajas</a></li>
                              <li><a href="xrn-preguntas.asp">Preguntas frecuentes</a></li>
                              <li><a href="xrn-precios.asp">Precios</a></li>
                           </ul>
                        </li>
                        <li>
                           <a href="tuscomprobantes-template.asp">Tus comprobantes.com</a>
                           <ul class="sub-menu">
                              <li><a href="tuscomprobantes-funcionamiento.asp">Funcionamiento</a></li>
                              <li><a href="tuscomprobantes-servicios.asp">Servicios</a></li>
                              <li><a href="tuscomprobantes-aplicaciones.asp">Aplicaciones</a></li>
                              <li><a href="tuscomprobantes-ventajas.asp">Ventajas</a></li>
                              <li><a href="tuscomprobantes-preguntas.asp">Preguntas frecuentes</a></li>
                              <li><a href="tuscomprobantes-precios.asp">Precios</a></li>
                           </ul>
                        </li>

                     </ul>
                  </li>

                  <li>
                     <a href="soporte.asp">Soporte</a>
                     <ul class="sub-menu">
                    <li><a href="http://help.enviocertificado.com/web/category/novedades/" target="_blank">Novedades</a></li>
                    <li><a href="http://app.enviocertificado.com/DinSite/v2/svs/services/analisisern/estado.asp" target="_blank">Estado del Servicio </a></li>
                    <li><a href="http://help.enviocertificado.com/web/" target="_blank">Ayuda</a></li>
                    <li><a href="chat.asp">Chat</a></li>
                     </ul>
                  </li>
                  <li>
                     <a href="contacto.asp">Contacto</a>
                  </li>

               </ul>
            </nav>
            <!-- END Main Navigation -->
         </div>
         <!-- END center-wrap -->
      </header>
      <!-- END Header -->




      <!-- START small_banner -->
      <section class="small_banner">
         <div class="center-wrap">
            <p class="page-banner-heading"> Partners</p>
            <p class="page-banner-description"></p>
            <div class="breadcrumbs">
               <a href="index.asp">Inicio</a> &rarr; <a href="empresa.asp">Empresa</a> &rarr; <span class="current_crumb">Partners</span>
            </div>
            <!-- END breadcrumbs -->
         </div>
         <!-- END center-wrap -->

         <div class="shadow top"></div>
         <div class="shadow bottom"></div>
         <div class="tt-overlay"></div>
      </section>






      <section id="content-container" class="clearfix">






         <div id="gallery-outer-wrap" class="clearfix">
            <div id="main-wrap" class="clearfix">
               <div id="iso-wrap" class="clearfix iso-space">

               	  <!-- item relacionado a clientes de seguro term9 -->
                  <div data-id="id-1" class="one_fourth term-9">
                     <div class="img-frame full-fourth">
                        <div class="lightbox-zoom">
 <img src="images/partners/MP.jpg" alt="" width="197" height="133" />
                           </a>
                        </div>
                        <!-- END lightbox-zoom -->
                     </div>
                     <!-- END img-frame -->
                     <a data-gal="prettyPhoto[group0]" href="content-images/sample-bio-4.png">
                     <img src="content-images/sample-bio-4.png" alt="" width="197" height="133" style="display:none" />
                     </a>
                     <h4>Mercado Pago</h4>
                      <p>Mercado Pago es reconocida como la plataforma líder de Latinoamérica respecto a cobranzas.</p>

                  </div>
                  <!-- END item relacionado a clientes de seguro term9 -->


                   <!-- item relacionado a clientes de seguro term9 -->
                  <div data-id="id-1" class="one_fourth term-9">
                     <div class="img-frame full-fourth">
                        <div class="lightbox-zoom">
                           <a href="http://www.emailreg.org">  <img src="images/partners/emailreg.jpg" alt="" width="197" height="133" /></a>
                           </a>
                        </div>
                        <!-- END lightbox-zoom -->
                     </div>
                     <!-- END img-frame -->
                     <a data-gal="prettyPhoto[group0]" href="content-images/sample-bio-4.png">
                     <img src="content-images/sample-bio-4.png" alt="" width="197" height="133" style="display:none" />
                     </a>
                     <h4>Email Reg </h4>
                      <p>Email Reg provee una whitelist de servidores de email y dominios para evitar falsos positivos en los filtros de spam.</p>
                  </div>
                  <!-- END item relacionado a clientes de seguro term9 -->


                   <!-- item relacionado a clientes de seguro term9 -->
                  <div data-id="id-1" class="one_fourth term-9">
                     <div class="img-frame full-fourth">
                        <div class="lightbox-zoom">
                           <a href="https://returnpath.com/"> <img src="images/partners/ReturnPatch.jpg" alt="" width="197" height="133" /> </a>
                           </a>
                        </div>
                        <!-- END lightbox-zoom -->
                     </div>
                     <!-- END img-frame -->
                     <a data-gal="prettyPhoto[group0]" href="content-images/sample-bio-4.png">
                     <img src="content-images/sample-bio-4.png" alt="" width="197" height="133" style="display:none" />
                     </a>
                     <h4>Return Path</h4>
                      <p>Return Path provee soluciones a empresas para construir conecciones más cercanas con los clientes y defender su marca.</p>
                  </div>
                  <!-- END item relacionado a clientes de seguro term9 -->



                  <!-- item relacion a clientes de automotrices term8 -->
                  <div data-id="id-2" class="one_fourth term-8">
                     <div class="img-frame full-fourth">
                        <div class="lightbox-zoom">
                           <a href="https://iplan.com.ar"> <img src="images/partners/iplan.jpg" alt="" width="197" height="133" /> </a>
                           </a>
                        </div>
                        <!-- END lightbox-zoom -->
                     </div>
                     <!-- END img-frame -->
                     <h4>IPlan</h4>
                      <p>IPLAN es la compañía líder en la provisión de Conectividad, Datacenter y Servicios Cloud para empresas.</p>
                  </div>
                  <!-- END item relacionado a clientes de automotrices -->















                </div>
               <!-- END iso-wrap -->
            </div>
            <!-- END main-wrap -->
         </div>
         <!-- END gallery-outer-wrap -->
      </section>
      <!-- END content-container -->




      <!-- START Footer Callout -->
     <div class="footer-callout clearfix">
         <div class="center-wrap tt-relative">
            <div class="footer-callout-content">
               <p class="callout-heading">Prueba Gratis</p>
               <p class="callout-text">Obtenga una cuenta de DEMO Gratis para toda la gama de servicios.</p>
            </div>
            <!-- END footer-callout-content -->
            <div class="footer-callout-button">
               <a href="demo.asp" class="large green button">Registrar DEMO &rarr;</a>
            </div>
            <!-- END footer-callout-button -->
         </div>
         <!-- END center-wrap -->
      </div>
      <!-- END Footer Callout -->




      <!-- START Footer -->
      <footer>
         <div class="center-wrap tt-relative">
            <div class="footer-content clearfix">
               <div class="footer-default-one">
                  <div class="sidebar-widget">
                     <div class="textwidget">
                        <p><img src="images/Logo_SW_pie.png" alt="" class="footer-logo" /><br />
                           <a href="http://www.enviocertificado.com">EnvioCertificado.com</a> | <a href="http://www.enviolegal.com">EnvioLegal.com</a> | <a href="http://www.archivocertificado.com">ArchivoCertificado.com</a> | <a href="http://www.enviocertificado.org">EnvioCertificado.org</a> |<a href="http://www.certificar.email">Certificar.Email</a> | <a href="http://www.certificado.website">Certificado.Website</a> | <a href="http://www.envio.me">Envio.Me</a>
                        </p>
                     </div>
                     <!-- END textwidget -->
                  </div>
                  <!-- END sidebar-widget -->
               </div>
               <!-- END footer-default-one -->


               <div class="footer-default-two">
                  <div class="sidebar-widget">

                     <div id="mc_embed_signup">
                        <form action="http://mail1.dynsite.com.ar/appmail/index.php/lists/fz562kb0fh279/subscribe" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank">
                           <label for="mce-EMAIL">Suscripción a Novedades:</label>
                           <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Su Email" required />
                           <div class="clear"><input type="submit" value="Suscribirse" name="subscribe" id="mc-embedded-subscribe" class="button" /></div>
                        </form>
                     </div>
                     <!-- END mc_embed_signup -->
                  </div>
                  <!-- END sidebar-widget -->
               </div>
               <!-- END footer-default-two -->


               <div class="footer-default-three">
                  <div class="sidebar-widget">
                     <p class="foot-heading">Contactese con Nosotros</p>
                     <ul class="social_icons">
                        <li><a href="http://help.enviocertificado.com/web/category/novedades/feed/" class="rss">RSS</a></li>
                        <li><a href="https://twitter.com/Sysworldsa" class="twitter">Twitter</a></li>
                        <li><a href="https://www.facebook.com/SysWorld-Servicios-SA-323344231089747" class="facebook">Facebook</a></li>
                     </ul>
                  </div>
                  <!-- END sidebar-widget -->
               </div>
               <!-- END footer-default-three -->
            </div>
            <!-- END footer-content -->
         </div>
         <!-- END center-wrap -->
         <div class="footer-copyright clearfix">
            <div class="center-wrap clearfix">
               <div class="foot-copy">
                  <p>Copyright &copy; 2010 - 2015 SysWorld Servicios S.A - Todos los derechos reservados. All rights reserved.</p>
               </div>
               <!-- END foot-copy -->
               <a href="#" id="scroll_to_top" class="link-top">Ir Arriba</a>
               <ul class="footer-nav">
                  <li><a href="abusos.asp">Abusos</a></li>
                  <li><a href="contacto.asp">Contacto</a></li>
                  <li><a href="login.asp">Login</a></li>
                  <li><a href="http://help.enviocertificado.com/web/category/novedades/" target="_blank">Novedades</a></li>
                  <li><a href="sitemap.asp">Sitemap</a></li>
               </ul>
            </div>
            <!-- END center-wrap -->
         </div>
         <!-- END footer-copyright -->
         <div class="shadow top"></div>
         <div class="tt-overlay"></div>
      </footer>

      <script type="text/javascript" src="js/custom-main.js"></script>
      <script type="text/javascript" src="js/custom-gallery.js"></script>
      <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
      <script type="text/javascript" src="js/jquery.isotope.js"></script>
   </body>
</html>