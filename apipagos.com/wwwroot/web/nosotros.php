<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="iso-8859-1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
<link href="images/favicon.png" rel="icon" />
<title>APIPAGOS - El centro de los Pagos Online.</title>
<meta name="description" content="">
<meta name="author" content="">

<!-- Web Fonts
============================================= -->
<link rel='stylesheet' href='../../../fonts.googleapis.com/cssab2a.css?family=Rubik:300,300i,400,400i,500,500i,700,700i,900,900i' type='text/css'>

<!-- Stylesheet
============================================= -->
<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="vendor/font-awesome/css/all.min.css" />
<link rel="stylesheet" type="text/css" href="vendor/owl.carousel/assets/owl.carousel.min.css" />
<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
</head>
<body>

<!-- Preloader -->
<div id="preloader">
  <div data-loader="dual-ring"></div>
</div>
<!-- Preloader End -->

<!-- Document Wrapper
============================================= -->
<div id="main-wrapper">

  <!-- Header
  ============================================= -->
  <header id="header">
    <div class="container">
      <div class="header-row">
        <div class="header-column justify-content-start">
          <!-- Logo
          ============================= -->
          <div class="logo"> <a class="d-flex" href="index.php" title="Inicio"><img src="images/logo.png" alt="APIPagos" /></a> </div>
          <!-- Logo end -->
          <!-- Collapse Button
          ============================== -->
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header-nav"> <span></span> <span></span> <span></span> </button>
          <!-- Collapse Button end -->

          <!-- Primary Navigation
          ============================== -->
          <nav class="primary-menu navbar navbar-expand-lg">
            <div id="header-nav" class="collapse navbar-collapse">
              <ul class="navbar-nav mr-auto">
                <li><a href="../pays/paysimple.php">Pagar</a></li>
                <li><a href="cobrar.php">Cobrar</a></li>
                <li><a href="nosotros.php">Nosotros</a></li>
                <li><a href="contacto.php">Contacto</a></li>
                <li class="dropdown"> <a class="dropdown-toggle" href="#">API</a>
				  <ul class="dropdown-menu">
					<li><a class="dropdown-item" href="http://www.apilanding.com">Documentaci�n</a></li>
				  </ul>
                </li>
              </ul>
            </div>
          </nav>
          <!-- Primary Navigation end -->
        </div>
        <div class="header-column justify-content-end">
          <!-- Login & Signup Link
          ============================== -->
          <nav class="login-signup navbar navbar-expand">
            <ul class="navbar-nav">
              <li><a href="login.php" target="_blank">Login</a> </li>
              <li class="align-items-center h-auto ml-sm-3"><a class="btn btn-primary d-none d-sm-block" href="registro.php" target="_blank">Contratar</a></li>
            </ul>
          </nav>
          <!-- Login & Signup Link end -->
        </div>
      </div>
    </div>
  </header>
  <!-- Header End -->

  <!-- Page Header
  ============================================= -->
  <section class="page-header page-header-text-light py-0 mb-0">
	<section class="hero-wrap section">
    <div class="hero-mask opacity-7 bg-dark"></div>
    <div class="hero-bg hero-bg-scroll" style="background-image:url('images/bg/image-2.jpg');"></div>
    <div class="hero-content">
      <div class="container">
        <div class="row">
          <div class="col-12 text-center">
            <h1 class="text-11 font-weight-500 text-white mb-4">Acerca de APIPagos</h1>
            <p class="text-5 text-white line-height-4 mb-4">Una aliado estrategico para su negocio.</p>
            <a href="#" class="btn btn-primary m-2">Contactenos</a> <a class="btn btn-outline-light video-btn m-2" href="#" data-src="https://www.youtube.com/embed/7e90gBu4pas" data-toggle="modal" data-target="#videoModal"><span class="mr-2"><i class="fas fa-play-circle"></i></span>Conozca nuestras herramientas</a> </div>
        </div>
      </div>
    </div>
  </section>
    </section>
  <!-- Page Header end -->

  <!-- Content
  ============================================= -->
  <div id="content">

    <!-- Who we are
    ============================================= -->
    <section class="section">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 d-flex">
            <div class="my-auto px-0 px-lg-5 mx-2">
              <h2 class="text-9">Nosotros</h2>
              <p class="text-4">SysWorld Servicios S.A - es una empresa Argentina dedicada a servicios de tecnolog�a, comunicaciones y datacenter. Forma parte de un grupo de negocios conjuntamente con Worldsys S.A., una empresa dedicada al desarrollo de software de aplicaci�n, implantaci�n y servicios de consultor�a relacionados con las �reas administrativas y contables de entidades financieras, industriales, comerciales y de servicios l�der en el mercado nacional.<br><br>
			  </p>
            </div>
          </div>
          <div class="col-lg-6 my-auto text-center"> <img class="img-fluid shadow-lg rounded-lg" src="images/who-we-are.jpg" alt=""> </div>
        </div>
      </div>
    </section>
    <!-- Who we are end -->

    <!-- Our Values
    ============================================= -->
    <section class="section bg-white">
      <div class="container">
        <div class="row no-gutters">
          <div class="col-lg-6 order-2 order-lg-1">
            <div class="row">
              <div class="col-6 col-lg-7 ml-auto mb-lg-n5"> <img class="img-fluid rounded-lg shadow-lg" src="images/our-values-vision.jpg" alt="banner"> </div>
              <div class="col-6 col-lg-8 mt-lg-n5"> <img class="img-fluid rounded-lg shadow-lg" src="images/our-values-mission.jpg" alt="banner"> </div>
            </div>
          </div>
          <div class="col-lg-6 d-flex order-1 order-lg-2">
            <div class="my-auto px-0 px-lg-5">
              <h2 class="text-9 mb-4">Nuestros Valores</h2>
              <h4 class="text-4 font-weight-500">Creencias</h4>
              <p class="tex-3" >Un equipo de profesionales altamente capacitados trabaja d�a a d�a creyendo que lo que hacemos le simplifica la vida a cientos de miles de personas y cuida el medioambiente reduciendo la huella de carbono.</p>
              <h4 class="text-4 font-weight-500 mb-2">�Con Qu�?</h4>
              <p class="tex-3" >Simplemente desarrollamos software como servicios para gesti�n, securizaci�n y automatizaci�n de procesos de comunicaciones, pagos y cobranzas entre una organizaci�n y sus clientes.</p>
              <h4 class="text-4 font-weight-500 mb-2">�Porqu�?</h4>
              <p class="tex-3">Innovaci�n: En todo lo que hacemos estamos pensando 4 a�os m�s adelante con el fin de anticiparnos a los cambios y poder acompa�ar a nuestros clientes en los cambios culturales.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Our Values end -->


    <!-- Our Investors
    ============================================= -->
    <section class="section bg-white">
      <div class="container">
        <h2 class="text-9 text-center">Nuestros Partners</h2>
        <p class="text-4 text-center mb-5">Todas estas empresas nos acompa�an con el fin de brindar el mejor servicio.</p>
        <div class="brands-grid separator-border">
          <div class="row align-items-center">
            <div class="col-6 col-sm-4 col-lg-2 text-center"><a href="#"><img class="img-fluid" src="images/partner/partner-1.png" alt="Brands"></a></div>
            <div class="col-6 col-sm-4 col-lg-2 text-center"><a href="#"><img class="img-fluid" src="images/partner/partner-2.png" alt="Brands"></a></div>
            <div class="col-6 col-sm-4 col-lg-2 text-center"><a href="#"><img class="img-fluid" src="images/partner/partner-3.png" alt="Brands"></a></div>
            <div class="col-6 col-sm-4 col-lg-2 text-center"><a href="#"><img class="img-fluid" src="images/partner/partner-4.png" alt="Brands"></a></div>
          </div>
        </div>
      </div>
    </section>
    <!-- Our Investors end -->

    <!-- Testimonial
    ============================================= -->
    <section class="section bg-light">
      <div class="container">
        <h2 class="text-9 text-center">Casos de �xito</h2>
        <div class="owl-carousel owl-theme" data-autoplay="true" data-nav="true" data-loop="true" data-margin="30" data-slideby="2" data-stagepadding="5" data-items-xs="1" data-items-sm="1" data-items-md="2" data-items-lg="2">
          <div class="item">
            <div class="testimonial rounded text-center p-4">
              <p class="text-4">�Hemos implementado procesos de cobranza con el fin de automatizar los pagos �nicos y recurrentes de nuestros clientes dando la posiblidad de pagas a clientes nacionales e internacionales.�</p>
              <strong class="d-block font-weight-500">APILanding</strong> <span class="text-muted">Portal de Servicios de APIS</span> </div>
          </div>
          <div class="item">
            <div class="testimonial rounded text-center p-4">
              <p class="text-4">�Se ha generado un proceso automatizado para cobranzas de mora temprana integrando soluciones automatizadas de campa�as de env�os de SMS, Email y WhatsAPP negociando en linea el pago de la deuda.�</p>
              <strong class="d-block font-weight-500">CertiSend</strong> <span class="text-muted">Servicio Saas IT.</span> </div>
          </div>
          <div class="item">
            <div class="testimonial rounded text-center p-4">
              <p class="text-4">�Hemos integrado en nuestro sistema de booking y reservas online la capacidad no solo de pago de nuestros clientes de forma-online sino que toda la automatizaci�n de contracargos e imputaciones.�</p>
              <strong class="d-block font-weight-500">Bosque del Nahuel</strong> <span class="text-muted">Hotel Boutique Bariloche</span> </div>
          </div>
        </div>
        <div class="text-center mt-4"><a href="#" class="btn-link text-4">Conozca m�s de nuestros casos de �xito.<i class="fas fa-chevron-right text-2 ml-2"></i></a></div>
      </div>
    </section>
    <!-- Testimonial end -->

    <!-- JOIN US
    ============================================= -->
    <section class="section bg-primary py-5">
      <div class="container text-center">
        <div class="row">
          <div class="col-sm-6 col-md-3">
            <div class="featured-box text-center">
              <div class="featured-box-icon text-light mb-2"> <i class="fas fa-globe"></i> </div>
              <h4 class="text-12 text-white mb-0">75+</h4>
              <p class="text-4 text-white mb-0">Herramientas</p>
            </div>
          </div>
          <div class="col-sm-6 col-md-3">
            <div class="featured-box text-center">
              <div class="featured-box-icon text-light mb-2"> <i class="fas fa-dollar-sign"></i> </div>
              <h4 class="text-12 text-white mb-0">+20</h4>
              <p class="text-4 text-white mb-0">Medios de Pago</p>
            </div>
          </div>
          <div class="col-sm-6 col-md-3 mt-4 mt-md-0">
            <div class="featured-box text-center">
              <div class="featured-box-icon text-light mb-2"> <i class="fas fa-users"></i> </div>
              <h4 class="text-12 text-white mb-0">+3.5MM</h4>
              <p class="text-4 text-white mb-0">Transacciones</p>
            </div>
          </div>
          <div class="col-sm-6 col-md-3 mt-4 mt-md-0">
            <div class="featured-box text-center">
              <div class="featured-box-icon text-light mb-2"> <i class="far fa-life-ring"></i> </div>
              <h4 class="text-12 text-white mb-0">24X7</h4>
              <p class="text-4 text-white mb-0">Soporte</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- JOIN US end -->

  </div>
  <!-- Content end -->

  <!-- Footer
  ============================================= -->
  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg d-lg-flex align-items-center">
          <ul class="nav justify-content-center justify-content-lg-start text-3">
            <li class="nav-item"> <a class="nav-link active" href="nosotros.php">Nosotros</a></li>
            <li class="nav-item"> <a class="nav-link" href="soporte.php">Soporte</a></li>
            <li class="nav-item"> <a class="nav-link" href="ayuda.php">Ayuda</a></li>
            <li class="nav-item"> <a class="nav-link" href="contacto.php">Contacto</a></li>
            <li class="nav-item"> <a class="nav-link" href="nosotros.php?#partners">Partners</a></li>
            <li class="nav-item"> <a class="nav-link" href="precios.php">Precios</a></li>
          </ul>
        </div>
        <div class="col-lg d-lg-flex justify-content-lg-end mt-3 mt-lg-0">
          <ul class="social-icons justify-content-center">
            <li class="social-icons-facebook"><a data-toggle="tooltip" href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
            <li class="social-icons-twitter"><a data-toggle="tooltip" href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
            <li class="social-icons-google"><a data-toggle="tooltip" href="http://www.google.com/" target="_blank" title="Google"><i class="fab fa-google"></i></a></li>
            <li class="social-icons-youtube"><a data-toggle="tooltip" href="http://www.youtube.com/" target="_blank" title="Youtube"><i class="fab fa-youtube"></i></a></li>
          </ul>
        </div>
      </div>
      <div class="footer-copyright pt-3 pt-lg-2 mt-2">
        <div class="row">
          <div class="col-lg">
            <p class="text-center text-lg-left mb-2 mb-lg-0">Copyright &copy; 2019 <a href="index.php">APIPagos</a>. All Rights Reserved.</p>
          </div>
          <div class="col-lg d-lg-flex align-items-center justify-content-lg-end">
            <ul class="nav justify-content-center">
              <li class="nav-item"> <a class="nav-link active" href="seguridad.php">Seguridad</a></li>
              <li class="nav-item"> <a class="nav-link" href="terminos.php">T�rminos y Condiciones</a></li>
              <li class="nav-item"> <a class="nav-link" href="privacidad.php">Privacidad</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- Footer end -->


</div>
<!-- Document Wrapper end -->

<!-- Back to Top
============================================= -->
<a id="back-to-top" data-toggle="tooltip" title="Back to Top" href="javascript:void(0)"><i class="fa fa-chevron-up"></i></a>

<!-- Video Modal
============================================= -->
<div class="modal fade" id="videoModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content bg-transparent border-0">
      <button type="button" class="close text-white opacity-10 ml-auto mr-n3 font-weight-400" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <div class="modal-body p-0">
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" id="video" allow="autoplay"></iframe>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Video Modal end -->

<!-- Script -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="js/theme.js"></script>
</body>
</html>