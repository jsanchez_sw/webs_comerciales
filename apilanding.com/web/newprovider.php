<?php
    include 'functions/db.php';

    include_once 'functions/language.php';
?>

<!DOCTYPE html>
<html lang="<?php echo $lang['SITE_lang'];?>">

<!-- Mirrored from protechtheme.com/saas/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:17:32 GMT -->
<head>
        <!-- Required meta tags -->
        <meta charset="iso-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="keywords" content="<?php echo $lang['SITE_keywords'];?>">
		<meta name="description" content="<?php echo $lang['SITE_desc'];?>">
		<meta name="author" content="Sysworld Servicios S.A.">
        <title><?php echo $lang['SITE_title'];?></title>
        <!-- Bootstrap core CSS -->
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Select2 -->
        <link href="assets/select2/css/select2.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- Iconmoon -->
        <link href="assets/iconmoon/css/iconmoon.css" rel="stylesheet">
        <!-- Magnific Popup -->
        <link href="assets/magnific-popup/css/magnific-popup.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="css/custom.css" rel="stylesheet">
        <link rel="icon" type="image/x-icon" href="images/favicon.ico" />
    </head>
    <body>

<!-- ==============================================
**Preloader**
=================================================== -->
        <div id="loader">
            <div id="element">
                <div class="circ-one"></div>
                <div class="circ-two"></div>
            </div>
        </div>

<!-- ==============================================
**Header**
=================================================== -->
        <header>
			<?php include 'menu.php';?>
        </header>

<!-- ==============================================
**Inner Banner**
=================================================== -->
        <section class="inner-banner how-it-works-banner">
            <div class="container">
                <div class="contents">
                    <h1>Sea Proveedor de APIS</h1>
                    <p>Integre sus productos / servicios con valor agregado en nuestro catalogo de APIS y comience a disfrutar de las regalias generadas mediante el uso y consumo que generen nuestros clientes. </p>
                </div>
            </div>
        </section>

<!-- ==============================================
**How it works**
=================================================== -->
        <section class="how-it-work-items padding-lg">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="content-area">
                            <div class="icon"><span>01</span></div>
                            <div class="text-area">
                                <h2>Catalogo y MarketPlace Funcionando</h2>
                                <p>Acceda a publicar sus servicios de valor agregado en el catalogo market-place de APIS de valor agregado m�s importante de la regi�n. Publicite y visibilice sus servicios a empresas que se encuentran en instancias de contratataci�n.</p>
                                <br>
                                <p>Resuelva sus procesos con una herramienta automatizada de publicaci�n, informaci�n comercial, documentaci�n t�cnica, acounting, control y facturaci�n de todos los servicios que desee publicar en nuestro catalogo.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <figure class="right"><img src="images/how-it-works-img-1.png" class="img-fluid" alt=""></figure>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 order-2 order-md-12">
                        <figure><img src="images/how-it-works-img-2.png" class="img-fluid" alt=""></figure>
                    </div>
                    <div class="col-md-6 order-1 order-md-12">
                        <div class="content-area">
                            <div class="icon"><span>02</span></div>
                            <div class="text-area">
                                <h2>Publique sus Servicios</h2>
                                <p>Disponibilice sus servicios de valor agregado de forma automatizada utilizando nuestro portal de Proveedores. Acceda a integrar sus soluciones mediante APIS de integraci�n o simplemente gestione su integraci�n por medio de archivos o bases de datos.</p>
                                <br>
                                <p>Mediante nuestro portal de proveedores administrar sus servicios y disponibilizar los mismos le llevar� muy pocos minutos.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="content-area">
                            <div class="icon"><span>03</span></div>
                            <div class="text-area">
                                <h2>Administre sus Clientes</h2>
                                <p>Gracias a nuestra aplicaci�n de proveedores podr� administrar los consumos que realicen los usuarios en sus servicios y controlar los mismos desde cualquier disposivio y de forma online.</p>
                                <br>
                                <p>Mensualmente recibir� en su cuenta bancaria o mediante una cuenta 2checkout / paypal las regalias generadas producto de los servicios brindados en nuestro Market-API.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <figure class="right"><img src="images/how-it-works-img-3.png" class="img-fluid" alt=""></figure>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 order-2 order-md-12">
                        <figure><img src="images/how-it-works-img-4.png" class="img-fluid" alt=""></figure>
                    </div>
                    <div class="col-md-6 order-1 order-md-12">
                        <div class="content-area">
                            <div class="icon"><span>04</span></div>
                            <div class="text-area">
                                <h2>Olvidese del Soporte</h2>
                                <p>Reduzca sus costos operativos de soporte y atenci�n al cliente tanto para la etapa de integraci�n a sus servicios como para la administraci�n de soporte postventa.<p>
                                <br>
                                <p>Un equipo especializado de soporte de niveles de se�ority asignados automaticamente mediante nuestro workflow de soporte le garantizar�n respuestas en tiempo y forma a los usuarios que deseen consumir sus servicios.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
		<section class="choose-pack padding-lg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <h2>Pol�ticas Transparentes</h2>
                        <p>Nuestras politicas de acuerdos con proveedores y partners son transparentes y se basan en un acuerdo entre partes dependiendo las responsabilidadades de las partes. As� mismo el precio de venta de los servicios del proveedor son definidos dinamicamente por el provedor desde el panel de administraci�n.</p>
                    </div>
                </div>
                <ul class="row">
                    <li class="col-md">
                        <div class="inner">
                            <div class="head-block">
                                <div class="plan-title"> <span>Bronze</span>
                                    <h3>Simple</h3>
                                </div>
                                <div class="price">
                                    <div class="graph"><span class="org"></span><span></span><span></span></div>
                                    <div class="right">
                                        <div class="amt">15<sup>%</sup></div>
                                        <div class="month">Sobre Ventas</div>
                                    </div>
                                </div>
                            </div>
                            <div class="cnt-block">
                                <ul>
                                    <li>Panel de Proveedor</li>
                                    <li>APIS Alta Disponibilidad</li>
                                    <li>Integrese por Archivos y BD</li>
                                    <li>Soporte al Proveedor</li>
                                </ul>
                                <br><br><br><br><br><br>
                                <span class="you-choose">Soporte para implementaci�n.</span> <a href="contact.php" class="btn get-started">Contactenos</a> </div>
                        </div>
                    </li>
                    <li class="col-md">
                        <div class="inner">
                            <div class="head-block">
                                <div class="plan-title"> <span>Silver</span>
                                    <h3>Intermedio</h3>
                                </div>
                                <div class="price">
                                    <div class="graph"><span class="org"></span><span class="org"></span><span></span></div>
                                    <div class="right">
                                        <div class="amt">25<sup>%</sup></div>
                                        <div class="month">Sobre Ventas</div>
                                    </div>
                                </div>
                            </div>
                            <div class="cnt-block">
                                <ul>
                                    <li>Panel de Proveedor</li>
                                    <li>APIS Alta Disponibilidad</li>
                                    <li>Integrese por Archivos y BD</li>
                                    <li>Soporte al Proveedor</li>
                                    <li>Soporte al Cliente</li>
                                </ul>
                                <br><br><br><br>
                                <span class="you-choose">Soporte para implementaci�n.</span> <a href="contact.php" class="btn get-started">Contactenos</a> </div>
                        </div>
                    </li>
                    <li class="col-md active">
                        <div class="inner">
                            <div class="head-block">
                                <div class="plan-title"> <span>Gold</span>
                                    <h3>Delegado</h3>
                                </div>
                                <div class="price">
                                    <div class="graph"><span class="org"></span><span class="org"></span><span class="org"></span></div>
                                    <div class="right">
                                        <div class="amt">30<sup>%</sup></div>
                                        <div class="month">Sobre Ventas</div>
                                    </div>
                                </div>
                            </div>
                            <div class="cnt-block">
                                <ul>
                                    <li>Panel de Proveedor</li>
                                    <li>APIS Alta Disponibilidad</li>
                                    <li>Integrese por Archivos y BD</li>
                                    <li>Integrese por API y WS</li>
                                    <li>Soporte al Proveedor</li>
                                    <li>Soporte al Cliente</li>
                                    <li>Publicidad de su servicio</li>
                                </ul>
                                <span class="you-choose">Soporte para implementaci�n.</span> <a href="contact.php" class="btn get-started">Contactenos</a> </div>
                        </div>
                    </li>
                </ul>
            </div>
        </section>

        <section class="call-to-action">
            <!-- Start Block 1 -->
            <div class="call-action-box-large">
                <div class="container">
                    <h2>Sea proveedor de nuestro <span><strong>Catalogo</strong> </span>Gr�tis</h2>
                    <p>Acceda a publicar sus servicios en nuestro catalogo de forma gratuita. Solamente el sistema cobrar� una comisi�n de venta sobre lo contratado por los usuarios que utilicen sus servicios.</p>
                    <a class="btn orange" href="contact.php">Suscribase Ahora</a> </div>
            </div>
            <!-- End Block 1 -->
		</section>


<!-- ==============================================
**Signup Section**
=================================================== -->
<?php
    include 'footer.php';
?>


        <!-- Scroll to top -->
        <a href="#" class="scroll-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>

        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery.min.js"></script>
        <!-- Popper JS -->
        <script src="js/popper.min.js"></script>
        <!-- Bootsrap JS -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- Select2 JS -->
        <script src="assets/select2/js/select2.min.js"></script>
        <!-- Custom JS -->
        <script src="js/custom.js"></script>
    </body>

<!-- Mirrored from protechtheme.com/saas/how-it-works.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:17:41 GMT -->
</html>