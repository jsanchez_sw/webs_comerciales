<?php
    include 'functions/db.php';

    include_once 'functions/language.php';
?>


<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from protechtheme.com/saas/blog-grid.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:17:18 GMT -->
<head>
        <!-- Required meta tags -->
        <meta charset="iso-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="keywords" content="<?php echo $lang['SITE_keywords'];?>">
		<meta name="description" content="<?php echo $lang['SITE_desc'];?>">
		<meta name="author" content="Sysworld Servicios S.A.">
        <title><?php echo $lang['SITE_title'];?></title>
        <!-- Bootstrap core CSS -->
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Select2 -->
        <link href="assets/select2/css/select2.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- Iconmoon -->
        <link href="assets/iconmoon/css/iconmoon.css" rel="stylesheet">
        <!-- Magnific Popup -->
        <link href="assets/magnific-popup/css/magnific-popup.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="css/custom.css" rel="stylesheet">
        <link rel="icon" type="image/x-icon" href="images/favicon.ico" />
    </head>

<!-- ==============================================
**Preloader**
=================================================== -->
        <div id="loader">
            <div id="element">
                <div class="circ-one"></div>
                <div class="circ-two"></div>
            </div>
        </div>

<!-- ==============================================
**Header**
=================================================== -->
        <header>
			<?php include 'menu.php';?>
        </header>

<!-- ==============================================
**Inner Banner**
=================================================== -->
        <section class="inner-banner">
            <div class="container">
                <div class="contents">
                    <h1>Fuentes de APIS</h1>
                    <p>A continuaci�n se listan nuestros proveedores y fuentes que hacen nuestro Catalogo de APIS.<span></span></p>
                </div>
            </div>
        </section>

<!-- ==============================================
**Blog section**
=================================================== -->
        <section class="blog-section">
            <div class="container">
                <div class="blog-content masonry-blog grid">
                    <ul class="clearfix">

						<?php
						$sql = "SELECT * FROM web_apis_proveedores WHERE idestado = 1";
						$result = $conn->query($sql);
						if ($result->num_rows > 0) {
							while($row = $result->fetch_assoc()) {
						?>
							<li>
								<div class="blog-item">
									<p class="time"><?php echo substr($row['fechavigencia'],0,10);?><span></span></p>
									<h5><a href="provider.php?cod=<?php echo $row['codigo'];?>"><?php echo $row['descripcion'];?></a></h5>
									<div class="box">
										<ul class="blog-info">
											<li class="comment"><?php echo $row['cantsusc'];?> Suscripciones</li>
											<li class="comment"><?php echo $row['cantapis'];?> APIS</li>
										</ul>
									</div>
									<p><?php echo substr($row['descripcioncomercial'], 0,300);?>...</p>
									<ul class="blog-tag">
										<li><a href="provider.php?cod=<?php echo $row['codigo'];?>">+ Info</a></li>
										<li><a href="<?php echo $row['url'];?>" target="_blank">Web</a></li>
										<li><a href="apis.php?p=<?php echo $row['codigo'];?>">Ver APIS</a></li>
									</ul>
								</div>
							</li>
						<?php
							}
						}
						?>
                    </ul>

                </div>
            </div>
        </section>
        <section class="call-to-action">
            <!-- Start Block 1 -->
            <div class="call-action-box-large">
                <div class="container">
                    <h2>Sea proveedor de nuestro <span><strong>Catalogo</strong> </span>Gr�tis</h2>
                    <p>Acceda a publicar sus servicios en nuestro catalogo de forma gratuita. Solamente el sistema cobrar� una comisi�n de venta sobre lo contratado por los usuarios que utilicen sus servicios.</p>
                    <a class="btn orange" href="newprovider.php">Conozca M�s</a> </div>
            </div>
            <!-- End Block 1 -->
		</section>

<!-- ==============================================
**Footer opt1**
=================================================== -->
        <?php include 'footer.php';?>

        <!-- Scroll to top -->
        <a href="#" class="scroll-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>

        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery.min.js"></script>
        <!-- Popper JS -->
        <script src="js/popper.min.js"></script>
        <!-- Bootsrap JS -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- Select2 JS -->
        <script src="assets/select2/js/select2.min.js"></script>
        <!-- Masonry JS -->
        <script src="assets/masonry/js/masonry.min.js"></script>
        <!-- Custom JS -->
        <script src="js/custom.js"></script>
    </body>

<!-- Mirrored from protechtheme.com/saas/blog-grid.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:17:19 GMT -->
</html>