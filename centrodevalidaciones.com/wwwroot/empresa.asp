
<!DOCTYPE html>
<html lang="zxx" dir="ltr">

    <head>
        <!-- Standard Meta -->
        <meta charset="utf-8">
        <meta name="format-detection" content="telephone=no" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- Site Properties -->
        <title>Centro de Validaciones - Empresa</title>
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon-precomposed" href="images/apple-touch-icon.png">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Rajdhani:300,400,500,600,700%7CRoboto:300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">

        <!-- CSS -->
        <link rel="stylesheet" href="css/uikit.css">
        <link rel="stylesheet" href="css/components/slider.css">
        <link rel="stylesheet" href="css/components/slideshow.css">
        <link rel="stylesheet" href="css/components/slidenav.css">
        <link rel="stylesheet" href="css/components/dotnav.css">
        <link rel="stylesheet" href="css/small-icon.css">
        <link rel="stylesheet" href="css/elements.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/mediaquery.css">
    </head>

    <body>

        <!-- Header Section -->
        <header id="idz-header">
            <!-- Mobile / Offcanvas Menu -->
        	<div id="mobile-nav" class="uk-offcanvas">
				<div class="uk-offcanvas-bar">
					<ul class="uk-nav uk-nav-offcanvas uk-nav-parent-icon" data-uk-nav>
						<li class="uk-parent"><a href="#">home</a>
							<ul class="uk-nav-sub">
								<li><a href="index.html">Homepage 1</a></li>
						        <li><a href="index2.html">Homepage 2</a></li>
						        <li><a href="index3.html">Homepage 3</a></li>
						        <li><a href="index4.html">Homepage 4</a></li>
					        </ul>
						</li>
			            <li><a href="shared.html">shared</a></li>
			            <li><a href="vps.html">vps</a></li>
			            <li><a href="dedicated.html">Dedicated</a></li>
			            <li><a href="other-services.html">other services </a></li>
			            <li class="uk-parent"><a href="#">blog</a>
						    <ul class="uk-nav-sub">
						    	<li><a href="blog.html">Blog Page</a></li>
						        <li><a href="single.html">Single Post</a></li>
					        </ul>
			            </li>
			            <li><a href="contact.html">Meet Us</a></li>
						<li class="uk-parent uk-active"><a href="#">Pages</a>
						    <ul class="uk-nav-sub">
						        <li><a href="domain-pricing.html">Domain Pricing</a></li>
						        <li><a href="features.html">Features</a></li>
						        <li><a href="about.html">About</a></li>
						        <li><a href="testimonial.html">Testimonial</a></li>
						        <li><a href="team.html">Team</a></li>
						        <li><a href="login.html">Login</a></li>
						        <li><a href="client-area.html">Client Area</a></li>
						        <li><a href="portfolio.html">Portfolio</a></li>
						        <li><a href="faq.html">FAQ</a></li>
						        <li><a href="sitemap.html">Sitemap</a></li>
						        <li><a href="404.html">404 Error</a></li>
						    </ul>
						</li>
						<li class="uk-parent"><a href="#">Elements</a>
						    <ul class="uk-nav-sub">
						        <li><a href="column.html">Column</a></li>
						        <li><a href="table.html">Table</a></li>
						        <li><a href="button-list.html">Button &amp; List</a></li>
						        <li><a href="icon-list.html">Icon List</a></li>
						        <li><a href="icon-use.html">Icon Use</a></li>
						        <li><a href="typography.html">Typography</a></li>
						        <li><a href="panel-promo.html">Panel &amp; Promo Box</a></li>
						        <li><a href="tab-accordion.html">Tab &amp; Accordion</a></li>
						        <li><a href="alert-progress.html">Alert &amp; Progress</a></li>
						    </ul>
						</li>
					</ul>
				</div>
			</div>
			<!-- Mobile / Offcanvas Menu End -->

			<div class="uk-container uk-container-center">
				<div class="uk-grid">

					<!-- Logo -->
					<div class="uk-width-large-1-2 uk-width-medium-1-1 uk-width-small-1-1">
						<div class="idz-logo">
							<a href="index.asp"><img src="images/logo.png" alt="logo" class=""></a>
						</div>
					</div>
					<!-- Logo End -->

					<!-- Domain Search Form -->
					<div class="uk-width-large-1-2  uk-width-medium-1-1 uk-width-small-1-1">
						<div class="idz-search-domain-block">
							<div class="idz-header-search-domain">

								<form class="uk-form idz-search-domain-form">
									<fieldset>
									<input type="text" class="uk-form-large uk-width-1-1 " placeholder="Prueba de Validacion de Email/Telefono/Domicilio/DNI">
									<button class="uk-button-large idz-button blue uppercase">Probar Validador</button>
									</fieldset>
								</form>
							</div>
						</div>
					</div>
					<!-- Domain Search Form End -->

					<!-- Header Info -->
					<div class="uk-width-large-1-1 uk-width-medium-1-1 uk-width-small-1-1">
						<ul class="uk-grid uk-grid-large uk-grid-with-medium-1-4 idz-header-contact">
							<li><i class="uk-icon-globe"></i>Español</li>
							<li><i class="uk-icon-phone"></i>(+54 11) 5263-2919</li>
							<li><i class="uk-icon-headphones"></i><a target="blank" href="http://www.sysworld.com.ar/soporte.asp">Live Chat</li></a>
							<li><i class="uk-icon-life-bouy "></i><a href="#client-area" data-uk-modal>Acceso Clientes</a>
						</ul>
					</div>
					<!-- Header End -->

				</div>
			</div>

			<!-- Header Nav -->
			<div class="idz-header-nav" data-uk-sticky="{top:-400}">

				<div class="uk-container uk-container-center">
					<div class="uk-grid">
						<div class="uk-width-medium-1-1">

							<div class="idz-menu-wrapper">
								<div class="uk-grid">
									<div class="uk-width-large-8-10 uk-width-medium-3-10 uk-width-small-3-10">

										<!-- Menu Items -->
										<div class="idz-nav">
											<nav class="uk-navbar">
											    <div class="uk-navbar-flip">
											        <ul class="uk-navbar-nav uk-hidden-medium  uk-hidden-small">
											            <li class="uk-parent uk-active" data-uk-dropdown><a href="index.asp">Inicio</a></li>
											            <li><a href="empresa.asp">Nuestra Empresa</a></li>
											            <li class="uk-parent" data-uk-dropdown="{justify:'.idz-menu-wrapper'}"><a href="funcionamiento.asp">Validadores</a>
					                                        <div class="uk-dropdown uk-dropdown-navbar idz-megamenu">
					                                            <ul class="uk-grid" data-uk-grid-margin>
					                                                <li class="uk-width-large-3-10 uk-width-medium-4-10">
					                                                    <ul class="uk-list uk-list-space">
					                                                        <li>
					                                                            <a href="validadoremail.asp">
					                                                                <div class="icon-wrap circle left small blue">
					                                                                    <i class="uk-icon-cubes"></i>
					                                                                </div>
					                                                                <h5>Email</h5>
					                                                                <p class="uk-nbfc">Verifique si una cuenta de email existe o no.</p>
					                                                            </a>
					                                                        </li>
					                                                        <li>
					                                                            <a href="validadortelefono.asp">
					                                                                <div class="icon-wrap circle left small blue">
					                                                                    <i class="uk-icon-cube"></i>
					                                                                </div>
					                                                                <h5>Teléfono</h5>
					                                                                <p class="uk-nbfc">Verifique y Normalice un número de teléfono fijo y/o movil.</p>
					                                                            </a>
					                                                        </li>
					                                                        <li>
					                                                            <a href="validadordomicilio.asp">
					                                                                <div class="icon-wrap circle left small blue">
					                                                                    <i class="uk-icon-tasks"></i>
					                                                                </div>
					                                                                <h5>Domicilio</h5>
					                                                                <p class="uk-nbfc">Valide, Normalice y Enriquezca un Domicilio.</p>
					                                                            </a>
					                                                        </li>
					                                                        <li>
					                                                            <a href="validadoridentidad.asp">
					                                                                <div class="icon-wrap circle left small blue">
					                                                                    <i class="uk-icon-tasks"></i>
					                                                                </div>
					                                                                <h5>Identidad</h5>
					                                                                <p class="uk-nbfc">Valide la identidad de una persona física.</p>
					                                                            </a>
					                                                        </li>					                                                    </ul>
					                                                </li>
					                                                <li class="uk-width-large-2-10 uk-width-medium-3-10">
					                                                	<h5>Otros Servicios</h5>
					                                                    <ul class="uk-list uk-list-space idz-list-check">
					                                                        <li><a href="http://www.enviocertificado.com" target="_blank">EnvíoCertificado.com</a></li>
					                                                        <li><a href="http://www.enviocertificado.com" target="_blank">ContactoCertificado.com</a></li>
					                                                        <li><a href="http://www.tuscomprobantes.com" target="_blank">TusComprobantes.com</a></li>
					                                                    </ul>
					                                                </li>
					                                                <li class="uk-width-large-2-10 uk-width-medium-3-10">
					                                                	<h5>Soporte</h5>
					                                                    <ul class="uk-list  uk-list-space idz-list-custom">
					                                                        <li><a href="contacto.asp"><i class="uk-icon-envelope-o"></i>Contactenos</a></li>
					                                                        <li><a href="http://help.enviocertificado.com" target="_blank"><i class="uk-icon-book"></i>Tutoriales</a></li>
					                                                        <li><a href="http://help.enviocertificado.com" target="_blank"><i class="uk-icon-comments-o"></i>Foro de Debate</a></li>
					                                                        <li><a href="http://www.sysworld.com.ar/portal" target="_blank"><i class="uk-icon-exclamation-triangle"></i>Portal de Clientes</a></li>
					                                                    </ul>
					                                                </li>
					                                                <li class="uk-width-large-3-10 uk-hidden-medium">
					                                                    <h5>¿Porqué usar validadores?</h5>
					                                                    <p>Integrar sistemas automáticos de validacion en sus procesos le garantiza poder contactar a sus clientes y reducir costos en la comunicación.</p>
					                                                </li>
					                                            </ul>
					                                        </div>
					                                    </li>
											            <li><a href="funcionamiento.asp">Funcionamiento</a></li>
											            <li><a href="contacto.asp">Contactenos</a></li>
											        </ul>
											    </div>
											</nav>
											<!-- Mobile Menu Toggle  -->
                                            <a href="#mobile-nav" class="uk-navbar-toggle uk-visible-small uk-visible-medium" data-uk-offcanvas></a>
                                            <!-- Mobile Menu Toggle End-->
										</div>
										<!-- Menu Items End -->
									</div>


								</div>
							</div>

						</div>
					</div>
				</div>

			</div>
			<!-- Header Nav End -->

		</header>
		<!-- Header Section End -->

        <!-- Page Header -->
        <section id="idz-header-inner">
            <div class="uk-container uk-container-center">

                <div class="idz-page-title idz-padding-medium">
                    <div class="uk-grid">
                    	<!-- Page Header Title -->
                        <div class="uk-width-large-1-2 uk-width-medium-1-3">
                            <div class="idz-title">
                                <h1 class="">Nuestra<span>Empresa</span></h1>
                                <i class="uk-icon-server"></i>
                            </div>
                        </div>
                        <!-- Page Header Title End -->

						<!-- Page Description -->
                        <div class="uk-width-large-1-2 uk-width-medium-2-3">
                            <div class="uk-grid">
                                <div class="uk-width-medium-3-4 uk-push-1-4">
                                    <div class="idz-page-promo uk-margin-top">
                                        <p>Más de 30 años creando soluciones jovenes.</p>
                                    </div>
                                </div>
                                <div class="uk-width-medium-1-4 uk-pull-3-4"></div>
                          </div>
                       </div>
                       <!-- Page Description End -->

                    </div>
                </div>

            </div>
        </section>
        <!-- Page Header End -->

       	<!-- Content Section -->
        <section class="idz-padding">
            <div class="uk-container uk-container-center">
                <div class="uk-grid uk-margin-medium-bottom">
                    <div class="uk-width-large-6-10 uk-width-medium-7-10 uk-width-small-1-1">
                        <h1 class="uk-margin-small-bottom">Nuestra Historia</h1>
                        <p class="idz-text-18px idz-text-blue  uk-margin-small-top uk-margin-medium-bottom">Un socio estratégico para su negocio.</p>
                        <p>
							SysWorld Servicios S.A - es una empresa Argentina dedicada a servicios de tecnología, comunicaciones y datacenter. Forma parte de un grupo de negocios conjuntamente con Worldsys S.A. una empresa dedicada al desarrollo de software de aplicación, implantación y servicios de consultoría relacionados con las áreas administrativas y contables de entidades financieras, industriales, comerciales y de servicios líder en el mercado nacional.
						</p>
						<p>
							SysWorld Servicios S.A - cuenta con profesionales de amplia experiencia en el segmento de servicios informáticos, redes y telecomunicaciones.
						</p>
						<p>
							Alianzas estratégicas con proveedores de hardware y software líderes en el mundo le permiten a nuestra compañía poder dar un servicio de calidad garantizada
                        </p>
                        <h3 class="uk-margin-large-top uk-margin-medium-bottom">¿Porqué Elegirnos?</h3>
                        <div class="icon-wrap small left">
                            <i class="smico-graph"></i>
                        </div>
                        <h4 class="uk-margin-bottom-remove">Nivel de Servicios</h4>
                        <p class="uk-nbfc uk-margin-top-remove uk-margin-medium-bottom">Todos nuestros productos y servicios fueron gestados con el fin de garantizarles a nuestros clientes la mejor calidad y el mejor uptime de servicio. Procedimientos bajo normas ISO27001 y ISO9001 garantizan la eficiencia y seguridad de nuestros procesos.</p>
                        <div class="icon-wrap small left">
                            <i class="smico-box"></i>
                        </div>
                        <h4 class="uk-margin-bottom-remove">Medio Ambiente</h4>
                        <p class="uk-nbfc uk-margin-top-remove uk-margin-medium-bottom">Todos nuestros productos y servicios tienen como objetivo principal reducir la huella de carbono que la empresas generan.</p>
                        <div class="icon-wrap small left">
                            <i class="smico-wallet"></i>
                        </div>
                        <h4 class="uk-margin-bottom-remove">Valor Agregado</h4>
                        <p class="uk-nbfc uk-margin-top-remove uk-margin-medium-bottom">A la hora de desarrollar un nuevo producto o servicio el foco principal de nuestro equipo se centra en que los mismos agreguen valor a los procesos de nuestros clientes.</p>
                        <div class="icon-wrap small left">
                            <i class="smico-chemistry"></i>
                        </div>
                        <h4 class="uk-margin-bottom-remove">Innovación Permanente</h4>
                        <p class="uk-nbfc uk-margin-top-remove">Nuestro equipo de innovación trabaja permanentemente investigando y aprendiendo de las mejoras y avances de la tecnología con el fin de actualizar permanentemente nuestros servicios a las últimas tendencias del mundo.</p>
                    </div>
                    <div class="uk-width-large-4-10 uk-width-medium-3-10 uk-hidden-small">
                        <img src="images/sample/sample_object12.jpg" alt="">
                    </div>
                </div>
            </div>
        </section>
        <!-- Content Section End -->



		<!-- Footer -->
		<footer id="idz-footer" class="idz-padding">
			<div class="uk-container uk-container-center">
				<div class="uk-grid">

					<!-- Footer Column 1 -->
					<div class="uk-width-large-1-4 uk-width-medium-1-4 uk-width-small-1-2">
						<div class="idz-footer-widget">
							<h3 class="uppercase idz-text-white">Compañía</h3>
							<ul class="uk-list uk-list-space idz-footer-menu">
								<li><a href="contacto.asp">Contactenos</a></li>
								<li><a href="http://www.sysworld.com.ar/legal.asp" target="_blank">Términos y Condiciones</a></li>
								<li><a href="http://www.sysworld.com.ar/legal.asp" target="_blank">Politica de Privacidad</a></li>
								<li><a href="http://www.sysworld.com.ar/" target="_blank">Sitio Coorporativo</a></li>
							</ul>
						</div>
					</div>
					<!-- Footer Column 1 End -->

					<!-- Footer Column 2 -->
					<div class="uk-width-large-1-4 uk-width-medium-1-4 uk-width-small-1-2">
						<div class="idz-footer-widget">
							<h3 class="uppercase idz-text-white">Soporte</h3>
							<ul class="uk-list uk-list-space idz-footer-menu">
								<li><a href="http://help.enviocertificado.com" target="_blank">Tutoriales</a></li>
								<li><a href="http://cloud.sysworld.com.ar" target="_blank">Portal de Tickets</a></li>
								<li><a href="http://erp.sysworld.com.ar/app" target="_blank">Portal Administrativo</a></li>
								<li><a href="contacto.asp" target="_blank">Contacto</a></li>
							</ul>
						</div>
					</div>
					<!-- Footer Column 2 End -->

					<!-- Footer Column 3 -->
					<div class="uk-width-large-1-4 uk-width-medium-1-4 uk-width-small-1-2">
						<div class="idz-footer-widget">
							<h3 class="uppercase idz-text-white">Validadores</h3>
							<ul class="uk-list uk-list-space idz-footer-menu">
								<li><a href="validadoremail.asp">Email</a></li>
								<li><a href="validadortelefono.asp">Teléfono</a></li>
								<li><a href="validadordomicilio.asp">Domicilio</a></li>
								<li><a href="validadoridentidad.asp">Identidad</a></li>
							</ul>
						</div>
					</div>
					<!-- Footer Column 3 End -->

					<!-- Footer Column 4 (Social Profile Links) -->
					<div class="uk-width-large-1-4 uk-width-medium-1-4 uk-width-small-1-2">
						<div class="idz-footer-widget idz-footer-social">
							<ul class="uk-list">
								<li>
									<div class="idz-button-group">
										<a target="blank" href="https://www.facebook.com/SysWorld.ar/"><i class="uk-icon-facebook-official"></i>seguinos en nuestro<span class="uppercase">facebook</span></a>
									</div>
								</li>
								<li>
									<div class="idz-button-group">
										<a target="blank" href="https://twitter.com/sysworldsa"><i class="uk-icon-twitter"></i>seguinos en nuestro<span class="uppercase">Twitter</span></a>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<!-- Footer Column 4 (Social Profile Links) -->


				</div>

				<!-- Footer Text -->
				<div class="uk-grid">
					<div class="uk-width-medium-1-1 uk-container-center uk-text-center">
						<p class="idz-footer-note idz-thin idz-text-22px uk-margin-remove">Ya validamos <span><%=FormatNumber(DateDiff("s", "11/08/2006 15:22:12", now),0)%> </span>transacciones</p>
						<p class="idz-site-copyright uk-margin-remove">COPYRIGHT  &copy; © 2006-<%=year(date)%> EnvioCertificado.com - Sysworld Servicios S.A.</p>
					</div>
				</div>
				<!-- Footer Text End -->
			</div>

			<!-- Scroll to Top -->
			<a href="#top" class="to-top uk-icon-chevron-up" data-uk-smooth-scroll></a>
			<!-- Scroll to Top End -->
        </footer>
		<!-- Footer End -->

		<!-- client area modal begin -->
        <div id="client-area" class="uk-modal">
            <div class="uk-modal-dialog">

                <ul id="client-area-tab" class="uk-tab uk-tab-grid" data-uk-tab="{connect:'#account'}">
                    <li class="uk-active uk-width-large-1-2"><a href="">Crear Cuenta</a></li>
                    <li class="uk-width-large-1-2"><a href="#">Registrarse</a></li>
                </ul>

                <ul id="account" class="uk-switcher uk-margin">
                	<!-- Modal Login Form -->
                    <li>
                        <form id="signin-account" class="uk-form">
                            <fieldset>
                                <div class="uk-form-icon">
                                    <i class="uk-icon-envelope"></i>
                                    <input type="text" placeholder="Email">
                                </div>
                                <div class="uk-form-icon">
                                    <i class="uk-icon-key"></i>
                                    <input type="text" placeholder="Password">
                                </div>
                                <label>
                                    <input type="checkbox">Remember me</label>
                                	<a href="http://web.enviocertificado.com" target="_blank" class="uk-button uk-button-large idz-button blue uk-width-large-1-1">Ingresar</a>
                            </fieldset>
                        </form>
                    </li>
                	<!-- Modal Login Form End -->

					<!-- Modal Register Form -->
                    <li>
                        <form id="create-account" class="uk-form" action="http://cloud.sysworld.com.ar/clients/register" target="_blank">
                            <fieldset>
                                <div class="uk-form-icon">
                                    <i class="uk-icon-user"></i>
                                    <input type="text" placeholder="Usuario">
                                </div>
                                <div class="uk-form-icon">
                                    <i class="uk-icon-envelope"></i>
                                    <input type="text" placeholder="Email">
                                </div>
                                <div class="uk-form-icon">
                                    <i class="uk-icon-key"></i>
                                    <input type="text" placeholder="Password">
                                </div>
                                <label>
                                    <input type="checkbox">Acepto los términos y condiciones.</label>
                                <a href="http://cloud.sysworld.com.ar/clients/register" target="_blank" class="uk-button uk-button-large idz-button blue uk-width-large-1-1">Registrarse</a>
                            </fieldset>
                        </form>
                    </li>
                    <!-- Modal Register Form End -->
                </ul>
                <div class="uk-modal-caption">
                    <a href="#">Forgot your password?</a>
                </div>
            </div>
        </div>
        <!-- client area modal end -->

        <!-- Javascript -->
        <script src="js/jquery.js"></script>
        <script src="js/uikit.min.js"></script>
        <script src="js/components/slider.js"></script>
        <script src="js/components/slideshow.js"></script>
        <script src="js/components/slideshow-fx.js"></script>
        <script src="js/components/parallax.js"></script>
        <script src="js/components/sticky.js"></script>
        <script src="js/jquery.countTo.js"></script>
        <script src="js/template-config.js"></script>
    </body>
</html>