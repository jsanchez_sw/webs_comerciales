<?php
    include '../functions/db.php';
    include_once '../functions/language.php';
	include 'session.php';



	$usuario = $_SESSION['apilanding_user_email'];


	$consumomesapisincluidas = "0";
	$limitesusc = "100";

	//CONSUMIO MES APIS INCLUIDAS
	$sql = " SELECT SUM(A.consumiomes) as consumomes "
	. " FROM web_suscripciones_keys A LEFT JOIN web_suscripciones B "
	. " ON A.idsuscripcion = B.id "
	. " LEFT JOIN web_apis C "
	. " ON A.idapi = C.id "
	. " WHERE A.usuario = '" . $usuario . "'"
	. " AND A.idapi in(SELECT id FROM web_apis WHERE tiposuscripcion = 1)";


	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$consumomesapisincluidas = $row['consumomes'];
		}
	}

	//CONSUMIO MES APIS NO INCLUIDAS
	$sql = " SELECT SUM(A.consumiomes) as consumomes "
	. " FROM web_suscripciones_keys A LEFT JOIN web_suscripciones B "
	. " ON A.idsuscripcion = B.id "
	. " LEFT JOIN web_apis C "
	. " ON A.idapi = C.id "
	. " WHERE A.usuario = '" . $usuario . "'"
	. " AND A.idapi in(SELECT id FROM web_apis WHERE tiposuscripcion = 2)";

	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$consumomesapisnoincluidas = $row['consumomes'];
		}
	}

	//CONSUMIO MES TOTAL
	$sql = " SELECT SUM(A.consumiomes) as consumomes "
	. " FROM web_suscripciones_keys A LEFT JOIN web_suscripciones B "
	. " ON A.idsuscripcion = B.id "
	. " LEFT JOIN web_apis C "
	. " ON A.idapi = C.id "
	. " WHERE A.usuario = '" . $usuario . "'";

	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$consumomesapistotal = $row['consumomes'];
		}
	}


	//LIMITE CONSUMO SUSCRIPCION
	 $sql = "SELECT * FROM web_suscripciones "
	. " WHERE usuario = '" . $usuario . "' ";

	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			if ($row['idestado'] == "0"){
				$limitesusc = "100";
			}else{
				if ($row['suscripcion'] == "1"){
					$limitesusc = "10000";
				}
				if ($row['suscripcion'] == "2"){
					$limitesusc = "100000";
				}
				if ($row['suscripcion'] == "3"){
					$limitesusc = "1000000";
				}
			}
		}
	}

	$consumomesapisincluidaspercent = round(($consumomesapisincluidas * 100) / $limitesusc);
	$consumomesapisnoincluidaspercent = round(($consumomesapisnoincluidas * 100) / $limitesusc);
	$consumomesapistotalpercent = round(($consumomesapistotal * 100) / $limitesusc);



?>






<!DOCTYPE html>
<html lang="<?php echo $lang['SITE_lang'];?>">

<head>
    <meta charset="iso-8859-1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="<?php echo $lang['SITE_keywords'];?>">
    <meta name="description" content="<?php echo $lang['SITE_desc'];?>">
    <meta name="author" content="Sysworld Servicios S.A.">
    <link rel="icon" type="image/x-icon" href="../images/favicon.ico" />
    <title><?php echo $lang['SITE_title'];?></title>
    <!-- ===== Bootstrap CSS ===== -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- ===== Plugin CSS ===== -->
    <link href="../plugins/components/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="../plugins/components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <link href="../plugins/components/css-chart/css-chart.css" rel="stylesheet">
    <!-- ===== Animation CSS ===== -->
    <link href="css/animate.css" rel="stylesheet">
    <!-- ===== Custom CSS ===== -->
    <link href="css/style.css" rel="stylesheet">
    <!-- ===== Color CSS ===== -->
    <link href="css/colors/default.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="mini-sidebar">
    <!-- ===== Main-Wrapper ===== -->
    <div id="wrapper">
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <!-- ===== Top-Navigation ===== -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
                <a class="navbar-toggle font-20 hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse">
                    <i class="fa fa-bars"></i>
                </a>
                <div class="top-left-part">
                    <a class="logo" href="index.php">
                        <span>&nbsp;</span>
                        <b>
                             <img src="../images/panel_api.png" width="240px" height="55px" />
                        </b>
                        <span>&nbsp;</span>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-left hidden-xs">
                    <li>
                        <a href="javascript:void(0)" class="sidebartoggler font-20 waves-effect waves-light"><i class="icon-arrow-left-circle"></i></a>
                    </li>
                    <li>
                        <form role="search" class="app-search hidden-xs" action="../apis.php" method="get">
                            <i class="icon-magnifier"></i>
                            <input type="text" name="s" placeholder="<?php echo $lang['INDEX_page_search'];?>" class="form-control">
                        </form>
                    </li>
                </ul>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle waves-effect waves-light font-20" data-toggle="dropdown" href="javascript:void(0);">
                            <i class="icon-speech"></i>
                            <span class="badge badge-xs badge-danger"></span>
                        </a>
                        <ul class="dropdown-menu mailbox animated bounceInDown">
                            <li>
                                <div class="drop-title"><?php echo $lang['INDEX_page_messages'];?></div>
                            </li>
                            <li>
                                <div class="message-center">
                                </div>
                            </li>
                            <li>
                                <a class="text-center" href="javascript:void(0);">
                                    <strong><?php echo $lang['INDEX_page_notifications'];?></strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle waves-effect waves-light font-20" data-toggle="dropdown" href="javascript:void(0);">
                            <i class="icon-calender"></i>
                            <span class="badge badge-xs badge-danger"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-tasks animated slideInUp">
                        </ul>
                    </li>
                    <li class="right-side-toggle">
                    </li>
                </ul>
            </div>
        </nav>
        <!-- ===== Top-Navigation-End ===== -->
        <!-- ===== Left-Sidebar ===== -->
        <aside class="sidebar">
            <div class="scroll-sidebar">
                <div class="user-profile">
                    <div class="dropdown user-pro-body">
                        <p class="profile-text m-t-15 font-16"><a href="javascript:void(0);"> Panel</a></p>
                    </div>
                </div>
                <nav class="sidebar-nav">
					<?php include 'menu.php';?>
                </nav>
                <div class="p-30">
                    <span class="hide-menu">
                        <a href="suscriptions.php" class="btn btn-success"><?php echo $lang['MENU_menu_bottom1'];?></a>
                        <a href="../index.php" class="btn btn-default m-t-15"><?php echo $lang['MENU_menu_bottom2'];?></a>
                    </span>
                </div>
            </div>
        </aside>
        <!-- ===== Left-Sidebar-End ===== -->
        <!-- ===== Page-Content ===== -->
        <div class="page-wrapper">
            <!-- ===== Page-Container ===== -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8 col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="white-box small-box-widget">
                                    <ul class="list-inline row">
                                        <li class="col-xs-3 p-t-10">
                                            <div class="icon-box bg-primary">
                                                <i class="icon-bag"></i>
                                            </div>
                                        </li>
                                        <li class="col-xs-9 p-l-20">
                                            <h4><?php echo $lang['INDEX_page_item1'];?></h4>
                                            <div class="ct-sales-chart"></div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
							<div class="col-sm-6">
                                <div class="white-box small-box-widget">
                                    <ul class="list-inline row">
                                        <li class="col-xs-3 p-t-10">
                                            <div class="icon-box bg-success">
                                                <i class="icon-user"></i>
                                            </div>
                                        </li>
                                        <li class="col-xs-9 p-l-20">
                                            <h4><?php echo $lang['INDEX_page_item2'];?></h4>
                                            <div class="ct-uq-chart chart-pos"></div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="white-box small-box-widget">
                                    <div class="p-t-10 p-b-10">
                                        <div class="icon-box bg-warning">
                                            <i class="icon-refresh"></i>
                                        </div>
                                        <div class="detail-box">
                                            <h4><?php echo $lang['INDEX_page_item3'];?><span class="pull-right text-warning font-22 font-normal"><?php echo $consumomes;?></span></h4>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="<?php echo $consumomesapisincluidas;?>" aria-valuemin="0" aria-valuemax="<?php echo $limitesusc;?>" style="width: <?php echo $consumomesapisincluidaspercent;?>%">
                                                    <span class="sr-only"><?php echo $consumomesapisincluidaspercent;?>% Complete</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="white-box small-box-widget">
                                    <div class="p-t-10 p-b-10">
                                        <div class="icon-box bg-danger">
                                            <i class="icon-cloud-download"></i>
                                        </div>
                                        <div class="detail-box">
                                            <h4><?php echo $lang['INDEX_page_item4'];?><span class="pull-right text-danger font-22 font-normal"><?php echo $consumomesapisnoincluidas;?></span></h4>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="<?php echo $consumomesapisnoincluidas;?>" aria-valuemin="0" aria-valuemax="<?php echo $limitesusc;?>" style="width: <?php echo $consumomesapisnoincluidaspercent;?>%">
                                                    <span class="sr-only"><?php echo $consumomesapisnoincluidaspercent;?>% Complete</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="white-box circle-chart-widget">
                            <div class="circle-chart">
                                <div class="c1">
                                    <input class="knob" data-min="0" data-max="100" data-bgColor="#e4edef" data-fgColor="#0283cc" data-displayInput=false data-width="182" data-height="182" data-thickness=".05" data-linecap=round value="<?php echo $consumomesapisnoincluidaspercent;?>" readonly>
                                </div>
                                <div class="c2">
                                    <input class="knob" data-min="0" data-max="100" data-bgColor="#e4edef" data-fgColor="#e74a25" data-displayInput=false data-width="154" data-height="154" data-thickness=".05" data-linecap=round value="<?php echo $consumomesapisincluidaspercent;?>" readonly>
                                </div>
                                <div class="c3">
                                    <input class="knob" data-min="0" data-max="100" data-bgColor="#e4edef" data-fgColor="#00bbd9" data-displayInput=false data-width="125" data-height="125" data-thickness=".05" data-linecap=round value="<?php echo $consumomesapistotalpercent;?>" readonly>
                                </div>
                                <div class="chart-overlap"><i class="icon-trophy"></i></div>
                            </div>
                            <ul class="list-inline m-b-0 m-t-30 t-a-c">
                                <li>
                                    <h6 class="font-15"><i class="fa fa-circle m-r-5 text-primary"></i><?php echo $consumomesapisnoincluidaspercent;?>%<br>Extras</h6>
                                </li>
                                <li>
                                    <h6 class="font-15"><i class="fa fa-circle m-r-5 text-danger"></i><?php echo $consumomesapisincluidaspercent;?>%<br>Inc.</h6>
                                </li>
                                <li>
                                    <h6 class="font-15"><i class="fa fa-circle m-r-5 text-info"></i><?php echo $consumomesapistotalpercent;?>%<br>Total</h6>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box stat-widget">
                            <div class="row">
                                <div class="col-sm-3">
                                    <h4 class="box-title"><?php echo $lang['INDEX_page_item5_statistics1'];?></h4>
                                </div>
                                <div class="col-sm-9">
                                    <select class="custom-select">
                                        <option selected value="0">Feb 04 - Mar 03</option>
                                        <option value="1">Mar 04 - Apr 03</option>
                                        <option value="2">Apr 04 - May 03</option>
                                        <option value="3">May 04 - Jun 03</option>
                                    </select>
                                    <ul class="list-inline">
                                        <li>
                                            <h6 class="font-15"><i class="fa fa-circle m-r-5 text-success"></i><?php echo $lang['INDEX_page_item5_statistics2'];?></h6>
                                        </li>
                                        <li>
                                            <h6 class="font-15"><i class="fa fa-circle m-r-5 text-primary"></i><?php echo $lang['INDEX_page_item5_statistics3'];?></h6>
                                        </li>
                                    </ul>
                                </div>
                                <div class="stat chart-pos"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ===== Right-Sidebar ===== -->
                <div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"> <?php echo $lang['INDEX_page_panel_service'];?> <span><i class="icon-close right-side-toggler"></i></span> </div>
                        <div class="r-panel-body">
                            <ul class="hidden-xs">
                                <li><b><?php echo $lang['INDEX_page_panel_service_item1'];?></b></li>
                                <li>
                                    <div class="checkbox checkbox-danger">
                                        <input id="headcheck" type="checkbox" class="fxhdr">
                                        <label for="headcheck"> Fix Header </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox checkbox-warning">
                                        <input id="sidecheck" type="checkbox" class="fxsdr">
                                        <label for="sidecheck"> Fix Sidebar </label>
                                    </div>
                                </li>
                            </ul>
                            <ul id="themecolors" class="m-t-20">
                                <li><b><?php echo $lang['INDEX_page_panel_service_item2'];?></b></li>
                                <li><a href="javascript:void(0)" data-theme="default" class="default-theme working">1</a></li>
                                <li><a href="javascript:void(0)" data-theme="green" class="green-theme">2</a></li>
                                <li><a href="javascript:void(0)" data-theme="yellow" class="yellow-theme">3</a></li>
                                <li><a href="javascript:void(0)" data-theme="red" class="red-theme">4</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple" class="purple-theme">5</a></li>
                                <li><a href="javascript:void(0)" data-theme="black" class="black-theme">6</a></li>
                                <li class="db"><b><?php echo $lang['INDEX_page_panel_service_item3'];?></b></li>
                                <li><a href="javascript:void(0)" data-theme="default-dark" class="default-dark-theme">7</a></li>
                                <li><a href="javascript:void(0)" data-theme="green-dark" class="green-dark-theme">8</a></li>
                                <li><a href="javascript:void(0)" data-theme="yellow-dark" class="yellow-dark-theme">9</a></li>
                                <li><a href="javascript:void(0)" data-theme="red-dark" class="red-dark-theme">10</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple-dark" class="purple-dark-theme">11</a></li>
                                <li><a href="javascript:void(0)" data-theme="black-dark" class="black-dark-theme">12</a></li>
                            </ul>
                            <ul class="m-t-20 chatonline">
                                <li><b><?php echo $lang['INDEX_page_panel_service_item4'];?></b></li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/1.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/2.jpg" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/3.jpg" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/4.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/5.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/6.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- ===== Right-Sidebar-End ===== -->
            </div>
            <!-- ===== Page-Container-End ===== -->
            <footer class="footer t-a-c">
                APILanding.com - The Center of the APIS.<br>Powered by <a href="http://www.sysworld.com.ar">Sysworld Servicios S.A.</a>
            </footer>
        </div>
        <!-- ===== Page-Content-End ===== -->
    </div>
    <!-- ===== Main-Wrapper-End ===== -->
    <!-- ==============================
        Required JS Files
        =============================== -->
    <!-- ===== jQuery ===== -->
    <script src="../plugins/components/jquery/dist/jquery.min.js"></script>
    <!-- ===== Bootstrap JavaScript ===== -->
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- ===== Slimscroll JavaScript ===== -->
    <script src="js/jquery.slimscroll.js"></script>
    <!-- ===== Wave Effects JavaScript ===== -->
    <script src="js/waves.js"></script>
    <!-- ===== Menu Plugin JavaScript ===== -->
    <script src="js/sidebarmenu.js"></script>
    <!-- ===== Custom JavaScript ===== -->
    <script src="js/custom.js"></script>
    <!-- ===== Plugin JS ===== -->
    <script src="../plugins/components/chartist-js/dist/chartist.min.js"></script>
    <script src="../plugins/components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
    <script src="../plugins/components/knob/jquery.knob.js"></script>
    <script src="../plugins/components/custom-chart/chart.js"></script>
    <!-- ===== Style Switcher JS ===== -->
    <script src="../plugins/components/styleswitcher/jQuery.style.switcher.js"></script>


	<script>
		$(function() {
			"use strict";

			$(function() {

				$('.knob').each(function() {

					var elm = $(this);
					var perc = elm.attr("value");

					elm.knob();

					$({ value: 0 }).animate({ value: perc }, {
						duration: 1000,
						easing: 'swing',
						progress: function() {
							elm.val(Math.ceil(this.value)).trigger('change')
						}
					});
				});
			});

			// ==============================================================
			// Statistics Chart
			// ==============================================================

			var chart1 = new Chartist.Line('.stat', {
				labels: [0, 5, 10, 15, 20, 25],
				series: [
					[40, 10, 33, 18, 27, 45],
					[10, 24, 37, 11, 30, 25]
				]
			}, {
				high: 50,
				low: 0,
				height: '278px',
				showArea: false,
				fullWidth: true,
				axisY: {
					onlyInteger: true,
					showGrid: false,
					offset: 20,
				},
				plugins: [
					Chartist.plugins.tooltip()
				]
			});

			// ==============================================================
			// New Sales Chart
			// ==============================================================

			var chart2 = new Chartist.Line('.ct-sales-chart', {
				series: [
					[2, 3, 4, 4, 3, 2, 2, 3, 4, 4.9, 5.5, 6, 6, 5, 4, 4, 5, 6, 7]
				]
			}, {
				low: 0,
				showArea: true,
				fullWidth: true,
				height: '80px',
				plugins: [
					Chartist.plugins.tooltip()
				]
			});

			// ==============================================================
			// User quota Chart
			// ==============================================================

			var chart3 = new Chartist.Bar('.ct-uq-chart', {
				series: [
					[10, 10, 10, 10, 10, 10, 10],
					[5, 3, 7, 6, 8, 2, 4]
				]
			}, {
				high: 10,
				low: 0,
				stackBars: true,
				fullWidth: true,
				height: '80px',
				plugins: [
					Chartist.plugins.tooltip()
				]
			}).on('draw', function(data) {
				if (data.type === 'bar') {
					data.element.attr({
						style: 'stroke-width: 7px'
					});
				}
			});

			var chart = [chart1, chart2, chart3];

			for (var i = 0; i < chart.length; i++) {
				chart[i].on('draw', function(data) {
					if (data.type === 'line' || data.type === 'area') {
						data.element.animate({
							d: {
								begin: 500 * data.index,
								dur: 500,
								from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
								to: data.path.clone().stringify(),
								easing: Chartist.Svg.Easing.easeInOutElastic
							}
						});
					}
					if (data.type === 'bar') {
						data.element.animate({
							y2: {
								dur: 500,
								from: data.y1,
								to: data.y2,
								easing: Chartist.Svg.Easing.easeInOutElastic
							},
							opacity: {
								dur: 500,
								from: 0,
								to: 1,
								easing: Chartist.Svg.Easing.easeInOutElastic
							}
						});
					}
				});
			}
		});
	</script>


	<script src="../plugins/components/session-timeout/jquery.sessionTimeout.min.js"></script>
	<script>
		var SessionTimeout=function(){var i=function(){$.sessionTimeout({title:"Session Timeout",message:"Su sesi�n ha llegado a un tiempo de inactividad que se cerrar�.",redirUrl:"logout.php",logoutUrl:"logout.php",warnAfter:900000,redirAfter:902000,ignoreUserActivity:!0,countdownMessage:"Cerrando en {timer} segundos.",countdownBar:!0})};return{init:function(){i()}}}();jQuery(document).ready(function(){SessionTimeout.init()});
	</script>


</body>

</html>
