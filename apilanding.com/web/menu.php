<?php
	$selenglish = '';
	$selspanish = '';
	$selcurrars = '';
	$selcurrusd = '';

	if ($_SESSION['lang'] == "es"){
		$selspanish = 'selected="selected"';
	}
	if ($_SESSION['lang'] == "en"){
		$selenglish = 'selected="selected"';
	}

?>



            <!-- Start Header top Bar -->
            <div class="header-top">
                <div class="container clearfix">
                    <div class="lang-wrapper">
                        <div class="select-lang">
                            <form action="index.php" method="GET">
                            	<input type="hidden" value="<?php echo md5(date('dMYhis'));?>">
								<select name="curr" class="currency_select" onchange="this.form.submit();">
									<option value="usd" <?php echo $selcurrusd;?>>USD</option>
								</select>
							</form>
                        </div>
                        <div class="select-lang2">
                            <form action="index.php" method="GET">
								<input type="hidden" value="<?php echo md5(date('dMYhis'));?>">
								<select class="custom_select" name="lang" onchange="this.form.submit();">
									<option value="es" <?php echo $selspanish;?>>Espa�ol</option>
								</select>
							</form>
                        </div>
                    </div>
                    <div class="right-block clearfix">
                        <ul class="top-nav hidden-xs">
                            <li><a href="index.php"><?php echo $lang['MENU_HOME'];?></a></li>
                            <li><a href="http://www.sysworld.com.ar" target="_blank"><?php echo $lang['MENU_Header_CoorpSite'];?></a></li>
                            <li><a href="support.php"><?php echo $lang['MENU_Header_Support'];?></a></li>
                            <li><a href="pricing.php"><?php echo $lang['MENU_Header_Prices'];?></a></li>
                            <li><a href="http://estado.sysworld.com.ar" target="_blank"><?php echo $lang['MENU_Header_Status'];?></a></li>
                        </ul>
                        <ul class="follow-us hidden-xs">
                            <li><a href="https://www.facebook.com/SysWorld.ar"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="https://twitter.com/sysworldsa"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="https://ar.linkedin.com/company/sysworld"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- End Header top Bar -->
            <!-- Start Navigation -->
            <nav class="navbar navbar-expand-lg navbar-light">
                <div class="container"> <a class="navbar-brand" href="index.php"><img src="images/Apilanding_01.png" class="img-fluid" alt=""></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                        <ul class="navbar-nav mr-auto">
							<li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" id="dropdown3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $lang['MENU_PROD'];?></a>
                                <div class="dropdown-menu megamenu" aria-labelledby="dropdown3">
                                    <div class="inner">
                                        <ul>
                                            <li><a class="dropdown-item" href="prod_cart.php"><?php echo $lang['MENU_PROD_CART'];?></a></li>
                                            <li><a class="dropdown-item" href="prod_file.php"><?php echo $lang['MENU_PROD_FILE'];?></a></li>
                                            <li><a class="dropdown-item" href="prod_db.php"><?php echo $lang['MENU_PROD_DB'];?></a></li>
                                        </ul>
                                        <ul>
                                            <li><a class="dropdown-item" href="prod_api_center.php"><?php echo $lang['MENU_PROD_ADMIN'];?></a></li>
                                            <li><a class="dropdown-item" href="prod_api_test.php"><?php echo $lang['MENU_PROD_TEST'];?></a></li>
                                            <li><a class="dropdown-item" href="prod_api_doc.php"><?php echo $lang['MENU_DOC'];?></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" id="dropdown2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">APIS</a>
                                <div class="dropdown-menu" aria-labelledby="dropdown2">
                                    <div class="inner">
                                        <a class="dropdown-item" href="apis.php"><?php echo $lang['MENU_APIS_All'];?></a>
										<a class="dropdown-item" href="providers.php"><?php echo $lang['MENU_APIS_Prov'];?></a>
										<a class="dropdown-item" href="support.php?t=need"><?php echo $lang['MENU_APIS_Need'];?></a>
										<a class="dropdown-item" href="newprovider.php"><?php echo $lang['MENU_APIS_ADDProv'];?></a>
										<a class="dropdown-item" href="prod_file.php"><?php echo $lang['MENU_APIS_File'];?></a>
										<a class="dropdown-item" href="prod_db.php"><?php echo $lang['MENU_APIS_DB'];?></a>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" id="dropdown3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $lang['MENU_AboutUs'];?></a>
                                <div class="dropdown-menu megamenu" aria-labelledby="dropdown3">
                                    <div class="inner">
                                        <ul>
                                            <li><a class="dropdown-item" href="features.php"><?php echo $lang['MENU_AboutUsFeatures'];?></a></li>
                                            <li><a class="dropdown-item" href="pricing.php"><?php echo $lang['MENU_AboutUsPrices'];?></a></li>
                                            <li><a class="dropdown-item" href="aboutus.php"><?php echo $lang['MENU_AboutUsCompany'];?></a></li>
                                            <li><a class="dropdown-item" href="network.php"><?php echo $lang['MENU_AboutUsNetwork'];?></a></li>
                                            <li><a class="dropdown-item" href="quality.php"><?php echo $lang['MENU_AboutUsQuality'];?></a></li>
                                            <li><a class="dropdown-item" href="http://www.sysworld.com.ar/v2/sw/empresa_clientes.asp" target="_blank"><?php echo $lang['MENU_AboutUsClients'];?></a></li>
                                            <li><a class="dropdown-item" href="legal.php"><?php echo $lang['MENU_AboutUsLegal'];?></a></li>
                                            <li><a class="dropdown-item" href="contact.php"><?php echo $lang['MENU_AboutUsContact'];?></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" id="dropdown4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $lang['MENU_Developers'];?></a>
                                <div class="dropdown-menu" aria-labelledby="dropdown4">
                                    <div class="inner">
                                        <a class="dropdown-item" href="apidoc/index.php" target="_blank"><?php echo $lang['MENU_DevelopersDocumentation'];?></a>
                                        <a class="dropdown-item" href="login.php" target="_blank"><?php echo $lang['MENU_DevelopersApiAdmin'];?></a>
                                        <a class="dropdown-item" href="apitest.php"><?php echo $lang['MENU_DevelopersTest'];?></a>
                                    </div>
                                </div>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" id="dropdown4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $lang['MENU_Support'];?></a>
                                <div class="dropdown-menu" aria-labelledby="dropdown4">
                                    <div class="inner">
                                        <a class="dropdown-item" href="support.php"><?php echo $lang['MENU_SupportContact'];?></a>
                                        <a class="dropdown-item" href="apidoc/index.php" target="_blank"><?php echo $lang['MENU_SupportTutorials'];?></a>
                                        <a class="dropdown-item" href="login.php" target="_blank"><?php echo $lang['MENU_SupportApiAdmin'];?></a>
                                        <a class="dropdown-item" href="https://cloud.sysworld.com.ar/clients/login" target="_blank"><?php echo $lang['MENU_SupportTickets'];?></a>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <ul class="navbar-right d-flex">
                        	<?php
                        		if(isset($_SESSION['apilanding_user_id'])){
                        			if(strlen($_SESSION['apilanding_user_id']) > 0){
										echo '<a href="panel/index.php"><button class="btn login-btn">Panel API Center</button></a>';
                        			}else{
										echo '<li><a href="register.php">' . $lang['MENU_Signup'] . '</a></li>';
										echo '<li><a href="login.php">' . $lang['MENU_Login'] . '</a></li>';
                        			}
                        		}else{
									echo '<li><a href="register.php">' . $lang['MENU_Signup'] . '</a></li>';
									echo '<li><a href="login.php">' . $lang['MENU_Login'] . '</a></li>';
                        		}
                        	?>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- End Navigation -->