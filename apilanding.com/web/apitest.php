<?php
    include 'functions/db.php';

    include_once 'functions/language.php';

?>

<!DOCTYPE html>
<html lang="<?php echo $lang['SITE_lang'];?>">

<!-- Mirrored from protechtheme.com/saas/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:17:32 GMT -->
<head>
        <!-- Required meta tags -->
        <meta charset="iso-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="keywords" content="<?php echo $lang['SITE_keywords'];?>">
		<meta name="description" content="<?php echo $lang['SITE_desc'];?>">
		<meta name="author" content="Sysworld Servicios S.A.">
        <title><?php echo $lang['SITE_title_apitest'];?></title>

        <!-- Bootstrap core CSS -->
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Select2 -->
        <link href="assets/select2/css/select2.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- Iconmoon -->
        <link href="assets/iconmoon/css/iconmoon.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="css/custom.css" rel="stylesheet">
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/css/select2.min.css" rel="stylesheet" />
		<link rel="icon" type="image/x-icon" href="images/favicon.ico" />
    </head>
    <body>

<!-- ==============================================
**Preloader**
=================================================== -->
        <div id="loader">
            <div id="element">
                <div class="circ-one"></div>
                <div class="circ-two"></div>
            </div>
        </div>

<!-- ==============================================
**Header**
=================================================== -->
        <header>
			<?php include 'menu.php';?>
        </header>

<!-- ==============================================
**Inner Banner**
=================================================== -->
        <section class="inner-banner">
            <div class="container">
                <div class="contents">
                    <h1><?php echo $lang['APITEST_header_tittle'];?></h1>
                    <p><?php echo $lang['APITEST_header_subtittle'];?></p>
                </div>
            </div>
        </section>

<!-- ==============================================
**Contact Info**
=================================================== -->
        <section class="contact-outer padding-lg">
            <div class="container">
                <!-- Start Support Request -->
                <div class="row justify-content-center">
                    <div class="col-md-10 col-lg-8">
                        <div class="support-request">
                            <h2><?php echo $lang['APITEST_test_tittle'];?></h2>
                            <p><?php echo $lang['APITEST_test_subtittle'];?></p>
                            <form action="http://www.apilanding.com/web/apiswagger.php" method="get" class="support-form" target="_blank">
                                <div class="row">
                                    <div class="col-md-12">
										<select class="js-example-basic-single" name="idapi">
											<option>Seleccionar API para Probar</option>
										<?php
										$sql = "SELECT A.id, A.nombre, A.descripcion, A.proveedor, A.contexto, A.imagen1, A.tiposuscripcion from web_apis A left join web_apis_desc B ON A.id = B.idapi WHERE A.idestado = 1 AND A.tiposuscripcion > 0 ";


										$result = $conn->query($sql);
										if ($result->num_rows > 0) {
											while($row = $result->fetch_assoc()) {
												echo '<option value="' . $row["id"] . '" data-tokens="' . $row["descripcion"] . '">' . $row["contexto"] . ' (' .$row["descripcion"] . ')</option>';
											}
										}
										?>
										</select>

                                    </div>
                                    <div class="col-md-12">
                                        <button class="submit-btn"><?php echo $lang['APITEST_item4_bottom'];?></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <br><hr><br>




                <div class="row justify-content-center">
                    <div class="col-md-12 col-lg-10">
                        <ul class="row contact-list">
                            <li class="col-md-4">
                                <div class="inner equal-hight">
                                    <div class="icon"><span class="icon-contact"></span></div>
                                    <h3><?php echo $lang['APITEST_item1_tittle'];?></h3>
                                    <p><?php echo $lang['APITEST_item1_subtittle'];?></p>
                                    <div class="call"><span class="icon-phone"></span> (54) 11 5263-2919</div>
                                    <a href="contact.php" class="mail-to"><span class="icon-mail"></span> info@sysworld.com.ar</a> </div>
                            </li>
                            <li class="col-md-4">
                                <div class="inner equal-hight">
                                    <div class="icon"><span class="icon-live-chat"></span></div>
                                    <h3><?php echo $lang['APITEST_item2_tittle'];?></h3>
                                    <p><?php echo $lang['APITEST_item2_subtittle'];?></p>
                                    <a href="#" class="live-chat"><span class="icon-chat-bubble"></span> <?php echo $lang['APITEST_item2_bottom'];?></a> </div>
                            </li>
                            <li class="col-md-4">
                                <div class="inner equal-hight">
                                    <div class="icon"><span class="icon-support-ticket"></span></div>
                                    <h3><?php echo $lang['APITEST_item3_tittle'];?></h3>
                                    <p><?php echo $lang['APITEST_item3_subtittle'];?></p>
                                    <a href="https://cloud.sysworld.com.ar/clients/login" target="_blank" class="live-chat support"><?php echo $lang['APITEST_item3_bottom'];?></a> </div>
                            </li>
                        </ul>
                    </div>
                </div>



            </div>
        </section>

<!-- ==============================================
**Advices and Answers**
=================================================== -->

<!-- ==============================================
**Signup Section**
=================================================== -->



	<?php include 'footer.php';?>



        <!-- Scroll to top -->
        <a href="#" class="scroll-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>

        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery.min.js"></script>
        <!-- Popper JS -->
        <script src="js/popper.min.js"></script>
        <!-- Bootsrap JS -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- Select2 JS -->
        <script src="assets/select2/js/select2.min.js"></script>
        <!-- Match Height JS -->
        <script src="assets/matchHeight/js/matchHeight-min.js"></script>
        <!-- Custom JS -->
        <script src="js/custom.js"></script>

		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/js/select2.min.js"></script>
		<script>
			$(document).ready(function() {
				$('.js-example-basic-single').select2();
			});
		</script>

    </body>

<!-- Mirrored from protechtheme.com/saas/support.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:18:09 GMT -->
</html>

