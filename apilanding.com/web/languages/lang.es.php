<?php

$lang = array();


$lang['SITE_lang'] = 'es';
$lang['SITE_title'] = 'API-Web | ApiLanding.com - El Centro de las APIS';
$lang['SITE_title_apitest'] = 'API-Test | ApiLanding.com - El Centro de las APIS';
$lang['SITE_desc'] = 'Sitio y store especializado en APIS e integraciones para empresas y negocios orientados a consumir servicios de tercera parte con valor agregado.';
$lang['SITE_keywords'] = 'api, apis, marketplace apis,';

//MENU
$lang['MENU_Header_CoorpSite'] = 'Sitio Coorporativo';
$lang['MENU_Header_Support'] = 'Soporte';
$lang['MENU_Header_Prices'] = 'Precios';
$lang['MENU_Header_Status'] = 'Estado de Servicios';
$lang['MENU_HOME'] = 'Inicio';

$lang['MENU_PROD'] = 'Productos';
$lang['MENU_PROD_CART'] = 'API Cart';
$lang['MENU_PROD_FILE'] = 'API File';
$lang['MENU_PROD_DB'] = 'API Database';
$lang['MENU_PROD_ADMIN'] = 'API Center';
$lang['MENU_PROD_TEST'] = 'API Test';
$lang['MENU_DOC'] = 'API Doc';

$lang['MENU_APIS_All'] = 'Ver el Catalogo';
$lang['MENU_APIS_Need'] = '�Nos Falta una API?';
$lang['MENU_APIS_Prov'] = 'Proveedores de APIS';
$lang['MENU_APIS_ADDProv'] = 'Ser Proveedor';
$lang['MENU_APIS_File'] = 'Tus Datos en APIS';
$lang['MENU_APIS_DB'] = 'Tus BD en APIS';
$lang['MENU_APIS_FintechFinance'] = 'Fintech y Financieras';
$lang['MENU_APIS_Identity'] = 'Identidad';
$lang['MENU_APIS_EmailNotification'] = 'Email y Notificaciones';
$lang['MENU_APIS_Security'] = 'Seguridad';
$lang['MENU_APIS_DeveloperTools'] = 'Herramientas de Desarrollo';
$lang['MENU_APIS_Localisation'] = 'Localizaci�n';
$lang['MENU_APIS_Productivity'] = 'Productividad';
$lang['MENU_APIS_SocialNetworks'] = 'Redes Sociales';
$lang['MENU_APIS_Utilities'] = 'Utilidades';
$lang['MENU_AboutUs'] = 'Nosotros';
$lang['MENU_AboutUsPrices'] = 'Precios';
$lang['MENU_AboutUsFeatures'] = 'Funcionalidades';
$lang['MENU_AboutUsCompany'] = 'Empresa';
$lang['MENU_AboutUsNetwork'] = 'Redes y Datacenter';
$lang['MENU_AboutUsQuality'] = 'Normas de Calidad';
$lang['MENU_AboutUsClients'] = 'Clientes';
$lang['MENU_AboutUsLegal'] = 'Legales';
$lang['MENU_AboutUsContact'] = 'Contacto';
$lang['MENU_Support'] = 'Soporte';
$lang['MENU_SupportContact'] = 'Contacto';
$lang['MENU_SupportTutorials'] = 'Tutoriales';
$lang['MENU_SupportApiAdmin'] = 'API Admin';
$lang['MENU_SupportTickets'] = 'Gestionar Tickets';
$lang['MENU_Developers'] = 'Probar';
$lang['MENU_DevelopersDocumentation'] = 'Documentaci�n APIS';
$lang['MENU_DevelopersApiAdmin'] = 'API Admin';
$lang['MENU_DevelopersTest'] = 'Probar APIS';
$lang['MENU_Signup'] = 'Registrarse';
$lang['MENU_Login'] = 'Acceder';

$lang['GRAL_section_prueba_item1'] = 'Crear una <br>Cuenta Gratis';
$lang['GRAL_section_prueba_item2'] = 'Probar las APIS<br>sin creditos';
$lang['GRAL_section_prueba_item3'] = 'Aprobar la <br>suscripci�n y pagar';
$lang['GRAL_section_prueba_button'] = 'Registrese Gratis';


///////////////////////////////////////////////////////////////////////////////////
//PRICES.PHP
///////////////////////////////////////////////////////////////////////////////////

$lang['PRICES_Prices'] = 'Nuestros Precios';
$lang['PRICES_Header'] = 'Una politica de precios clara y precisa para que pueda preveer sus consumos basada en una suscripci�n acorde a las necesidades de su negocio.';
$lang['PRICES_section1_title'] = 'Gestione sus consumos y suscripciones.';
$lang['PRICES_section1_desc'] = 'Gracias a nuestro panel de control de suscripciones y consumos, podr� gestionar los mismos de una manera segura y sencilla. <br><br>As� mismo podr� asignar limites y creditos a cada una de las aplicaciones / tokens que de de alta en su cuenta.<br><br>Adicionalmente podr� optar por el servicio de alertas y notificaciones en base a condiciones de consumos.';
$lang['PRICES_section2_title'] = 'Una suscripci�n para cada negocio..';
$lang['PRICES_section2_desc'] = 'Elija la suscripci�n que mejor se adapta a su negocio. Todas las suscripciones, addons y funcionalidades pueden cambiarse desde su panel de control.';

$lang['PRICES_Plan_Button'] = 'Empezar Ahora !';

$lang['PRICES_Plan1_Top'] = 'BRONZE';
$lang['PRICES_Plan1_Title'] = '10.000 Llamadas';
$lang['PRICES_Plan1_Price'] = '80';
$lang['PRICES_Plan1_Price_Freq'] = 'POR MES';
$lang['PRICES_Plan1_Items'] = '<li>Panel Administrador.</li><li>Facturaci�n Mensual.</li><li>Soporte Cloud.</li><li>Alta Disponibilidad</li><li>500 Llamadas Diarias.</li><li>+ 100 APIS Incluidas.</li>';
$lang['PRICES_Plan1_Footer'] = '<a href="apis.php?f=susc">VER APIS INCLUIDAS</a><br><u>Registro Gratis con 100 Creditos</u>';

$lang['PRICES_Plan2_Top'] = 'SILVER';
$lang['PRICES_Plan2_Title'] = '100.000 Llamadas';
$lang['PRICES_Plan2_Price'] = '230';
$lang['PRICES_Plan2_Price_Freq'] = 'POR MES';
$lang['PRICES_Plan2_Items'] = '<li>Panel Administrador.</li><li>Facturaci�n Mensual.</li><li>Soporte Cloud.</li><li>Alta Disponibilidad</li><li>5.000 Llamadas Diarias.</li><li>+ 100 APIS Incluidas.</li>';
$lang['PRICES_Plan2_Footer'] = '<a href="apis.php?f=susc">VER APIS INCLUIDAS</a><br><u>Registro Gratis con 500 Creditos</u>';

$lang['PRICES_Plan3_Top'] = 'GOLD';
$lang['PRICES_Plan3_Title'] = '1.000.000 Llamadas';
$lang['PRICES_Plan3_Price'] = '570';
$lang['PRICES_Plan3_Price_Freq'] = 'POR MES';
$lang['PRICES_Plan3_Items'] = '<li>Panel Administrador.</li><li>Facturaci�n Mensual.</li><li>Soporte Preferencial.</li><li>Alta Disponibilidad</li><li>50.000 Llamadas Diarias.</li><li>+ 100 APIS Incluidas.</li>';
$lang['PRICES_Plan3_Footer'] = '<a href="apis.php?f=susc">VER APIS INCLUIDAS</a><br><u>Registro Gratis con 1000 Creditos</u>';

$lang['PRICES_section3_title'] = 'Funcionalidades Incluidas';
$lang['PRICES_section3_subtitle'] = 'EL Valor Agregado para <br>Mejorar Su Productividad.';
$lang['PRICES_section3_desc'] = 'Una API para cada necesidad le van a permitir agregarle valor a sus procesos y reducir sus costos aumentando la productividad y reduciendo sus costos operativos.';


$lang['PRICES_section3_item1_title'] = 'Panel de Control';
$lang['PRICES_section3_item1_desc'] = 'Una panel de control pensado para la administraci�n y el control de sus suscripciones y la seguridad del consumo de sus recursos.';
$lang['PRICES_section3_item2_title'] = 'Multi Plataforma';
$lang['PRICES_section3_item2_desc'] = 'Un servicio de API REST apto para ser conectado m�ltiples lenguajes de programaci�n y diferentes tipos de dispositivos.';
$lang['PRICES_section3_item3_title'] = 'Soporte a Medida';
$lang['PRICES_section3_item3_desc'] = 'Un equipo especializado en diferentes negocios lo acompa�ara en la implementaci�n y mantenimiento de los servicios contratados.';
$lang['PRICES_section3_item4_title'] = 'Soluciones a Medida';
$lang['PRICES_section3_item4_desc'] = 'Un equipo de profesionales lo asistir� en brindarle soluciones de valor agregado con un enfoque orientado a su negocio';
$lang['PRICES_section3_item5_title'] = 'Alta Disponibilidad';
$lang['PRICES_section3_item5_desc'] = 'Toda la infraestructura de nuestros servicios est� pensada para darle continuidad al negocio y alta disponibilidad de servicios.';
$lang['PRICES_section3_item6_title'] = 'Crecimiento Horizontal';
$lang['PRICES_section3_item6_desc'] = 'Multiples servidores en m�ltiples centros de procesamiento garantizan el servicio a sus procesos y el crecimiento de su empresa.';

$lang['PRICES_section4_title'] = 'Registrese Gratis Sin Tarjeta de Cr�dito.';
$lang['PRICES_section4_desc'] = 'Seleccione el plan que mejor se adapta a sus necesidades y registrese totalmente gratis. Pruebe las apis que mejor se adaptan a su negocio y cuando lo pasa a producci�n comience a pagar el plan seleccionado.';
$lang['PRICES_section4_button'] = 'Crear Cuenta Gratis.';

$lang['PRICES_section5_title'] = '�Tienes Alguna Pregunta?';
$lang['PRICES_section5_item1_tittle'] = 'Centro de Ayuda';
$lang['PRICES_section5_item1_desc'] = 'Pod�s encontrar toda la informaci�n necesaria para comunicarte con nosotros a trav�s de nuestro departamento comercial.';
$lang['PRICES_section5_item2_tittle'] = 'Faq';
$lang['PRICES_section5_item2_desc'] = 'Pod�s ver todas las interrogantes mas frecuentes y resaltantes de nuestros clientes y usuarios sobre todo nuestro servicio';
$lang['PRICES_section5_item3_tittle'] = 'Documentos T�cnicos';
$lang['PRICES_section5_item3_desc'] = 'Pod�s Acceder a una gama de documentos que describen el funcionamiento asi como el comportamiento de enuestro servicio';


///////////////////////////////////////////////////////////////////////////////////
//APIS.PHP
///////////////////////////////////////////////////////////////////////////////////

$lang['APIS_header_title'] = 'Conozca nuestro catalogo.';
$lang['APIS_header_desc'] = 'Navegue en todo nuestro catalogo y entienda cada una de nuestras apis ingresando a su informaci�n detallada y costos. <br>Pruebe gratuitamente cada una de nuestra APIS registrandose y obteniendo un FREE TRIAL.';

$lang['APIS_search_title'] = 'Buscar API ...';
$lang['APIS_search_category_title'] = 'Filtrar por Categorias:';
$lang['APIS_search_category_industries'] = 'Filtrar por Industrias:';

$lang['APIS_category_title'] = 'Categorias:';
$lang['APIS_category_industries'] = 'Industrias:';


$lang['APIS_search_category_industries_pymes'] = 'Pymes';
$lang['APIS_search_category_industries_coorp'] = 'Coorporaciones';
$lang['APIS_search_category_industries_automotive'] = 'Automotriz';
$lang['APIS_search_category_industries_finance'] = 'Financiera';
$lang['APIS_search_category_insurance'] = 'Seguros';
$lang['APIS_search_category_retail'] = 'Retail';
$lang['APIS_search_category_tech'] = 'Tecnolog�a';
$lang['APIS_search_category_government'] = 'Gobierno';
$lang['APIS_list_link_button'] = 'Detalles';

///////////////////////////////////////////////////////////////////////////////////
//REGISTER.PHP
///////////////////////////////////////////////////////////////////////////////////

$lang['REGISTER_header_title'] = 'Registrese Gratis';
$lang['REGISTER_form_company'] = 'Empresa';
$lang['REGISTER_form_email'] = 'Email';
$lang['REGISTER_form_name'] = 'Nombre';
$lang['REGISTER_form_pass'] = 'Contrase�a';
$lang['REGISTER_form_pass2'] = 'Repetir Contrase�a';
$lang['REGISTER_form_suscription'] = 'Suscripci�n';
$lang['REGISTER_form_terms'] = 'Acepto los t�rminos y condiciones';
$lang['REGISTER_form_button'] = 'Crear Cuenta';
$lang['REGISTER_form_login'] ='�Ya tiene una cuenta? <a href="login.php">Acceder</a>';


///////////////////////////////////////////////////////////////////////////////////
//LOGIN.PHP
///////////////////////////////////////////////////////////////////////////////////

$lang['LOGIN_header_title'] = 'Acceder al Sistema';
$lang['LOGIN_form_company'] = 'Empresa';
$lang['LOGIN_form_email'] = 'Email';
$lang['LOGIN_form_pass'] = 'Contrase�a';
$lang['LOGIN_form_login'] = 'Acceder';
$lang['LOGIN_form_remember'] = 'Recordarme';
$lang['LOGIN_form_recovery'] = '�Olvido su Contrase�a?';
$lang['LOGIN_form_register'] = '�No tiene una cuenta? <a href="register.php">Registrese Gratis</a>';
$lang['LOGIN_error_1'] = 'Usuario o Contrase�a inv�lidos.';

///////////////////////////////////////////////////////////////////////////////////
//FOOTER.PHP
///////////////////////////////////////////////////////////////////////////////////

$lang['FOOTER_Copyright'] = 'Copyright 2006 - 2019 � Sysworld Servicios S.A.';
$lang['FOOTER_Powered'] = 'Todos los derechos reservados.Powered by Sysworld Servicios S.A.';
$lang['FOOTER_item1_tittle'] = 'Crear una Cuenta<br> Gratis';
$lang['FOOTER_item2_tittle'] = 'Probar las APIS<br> sin creditos';
$lang['FOOTER_item3_tittle'] = 'Aprobar la<br> suscripci�n y pagar';
$lang['FOOTER_bottom'] = 'Registr�te Gratis';
$lang['FOOTER_qlinks'] = 'Enlaces R�pidos';
$lang['FOOTER_link1'] = 'Inicio';
$lang['FOOTER_link2'] = 'Contacto';
$lang['FOOTER_link3'] = 'Calidad';
$lang['FOOTER_link4'] = 'Legales';
$lang['FOOTER_link5'] = 'Tickets';
$lang['FOOTER_link6'] = 'Funcionalidades';
$lang['FOOTER_link7'] = 'Precios';
$lang['FOOTER_link8'] = 'Redes';
$lang['FOOTER_link9'] = 'Clientes';
$lang['FOOTER_link10'] = 'API Center';
$lang['FOOTER_link11'] = 'FAQs';
$lang['FOOTER_link12'] = 'Soporte';
$lang['FOOTER_social_red'] = 'Conecta con Nosotros';
$lang['FOOTER_sub_tittle'] = 'Suscribete a Nosotros';
$lang['FOOTER_sub_subtittle'] = 'Se el primero en saber sobre nuestras novedades y m�s.';
$lang['FOOTER_sub_email'] = 'Email';
$lang['FOOTER_lastpost'] = 'Un equipo de profesionales altamente capacitados trabaja d�a a d�a creyendo que lo que hacemos le simplifica la vida a cientos de miles de personas y cuida el medioambiente reduciendo la huella de carbono.';
$lang['FOOTER_twitter_profile'] = 'Sysworld Servicios';
$lang['FOOTER_twitter_profile2'] = '@sysworldsa';
$lang['FOOTER_post_tittle'] = 'Otros Productos';
$lang['FOOTER_post1'] = '<b>CentroDeValidaciones.com</b><br>Normalice y Valide sus datos.<br><a href="http://www.centrodevalidaciones.com">Acceso Web</a>';
$lang['FOOTER_post2'] = '<b>CasillaPostal.com</b><br>El Centro de las Comunicaciones.<br><a href="http://www.casillapostal.com/web">Acceso Web</a>';
$lang['FOOTER_post3'] = '<b>EnvioCertificado.com</b><br>Comunique Fehacientemente.<br><a href="http://www.enviocertificado.com">Acceso Web</a>';


///////////////////////////////////////////////////////////////////////////////////
//INDEX.PHP
///////////////////////////////////////////////////////////////////////////////////

$lang['INDEX_header_tittle'] = 'Un centro de integraciones para mejorar sus procesos.';
$lang['INDEX_header_subtittle'] = 'Integrese de forma r�pida, sencilla y a bajo costo!';
$lang['INDEX_header_bottom'] = 'Explorar';
$lang['INDEX_header_bottom_info'] = 'Conozca nuestro catalogo y realice pruebas gratuitas.';

$lang['INDEX_section2_tittle'] = 'El desembarco de las APIS.';
$lang['INDEX_section2_desc'] = 'Desde nuestra empresa creemos que la tecnolog�a cloud y las integraciones con servicios de terceros ha llegado para quedarse. Es por eso que hemos abierto toda nuestra suite de herramientas para que nuestros clientes las puedan consumir a demanda.';
$lang['INDEX_section2_item1_tittle'] = 'Alta Disponibilidad';
$lang['INDEX_section2_item1_desc'] = 'Toda la infraestructura de nuestros servicios est� pensada para darle continuidad al negocio y alta disponibilidad de servicios.';
$lang['INDEX_section2_item2_tittle'] = 'Crecimiento';
$lang['INDEX_section2_item2_desc'] = 'Una infraestructura balanceada y resistente a fallos esta a su disposici�n para que su negocio pueda crecer independientemente de los recursos..';
$lang['INDEX_section2_item3_tittle'] = 'Soporte Dedicado';
$lang['INDEX_section2_item3_desc'] = 'Un equipo especializado en diferentes negocios lo acompa�ara en la implementaci�n y mantenimiento de los servicios para brindarle valor agregado.';
$lang['INDEX_section2_bottom_items'] = 'Saber M�s';

$lang['INDEX_section3_tittle'] = 'Acceda a Prueba En Linea';
$lang['INDEX_section3_desc'] = 'Todas nuestras apis cuentan con un API-Testing en linea. De esta forma usted con tan solo crear una cuenta gratuita, ser� capaz de probar todas las funcionalidades y respuestas de nuestras APIS sin tener que realizar desarrollos.';
$lang['INDEX_section3_bottom'] = 'Saber M�s';

$lang['INDEX_section4_tittle'] = 'Obtenga Documentaci�n Completa';
$lang['INDEX_section4_desc'] = 'Gracias a nuestro portal API-Doc usted podr� consultar en linea y de forma actualizada y centralizada toda la documentaci�n de nuestras APIS. As� mismo acceder� a c�digos fuente de ejemplos para m�s de 10 lenguajes de programaci�n.';

$lang['INDEX_section5_tittle'] = 'Simple & Powerful editor';
$lang['INDEX_section5_desc'] = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrystandard dummy text ever since beenLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrystandard dummy text ever since been';
$lang['INDEX_section5_bottom'] = 'Saber M�s';

$lang['INDEX_section6_tittle'] = 'Casos de Exito<br>de nuestros clientes.';
$lang['INDEX_section6_desc'] = 'A continuaci�n presentamos algunos de los casos de �xito implementados en los �ltimos tiempos en sistemas productivos de nuestros clientes.';
$lang['INDEX_section6_item1_name'] = 'Autoahorro VW.';
$lang['INDEX_section6_item1_tittle'] = 'Automotriz';
$lang['INDEX_section6_item1_desc'] = 'Se implemento un proceso de validadores y normalizadores con el fin de realizar filtros y scoring de ventas en el inicio de la venta al cliente.';
$lang['INDEX_section6_item2_name'] = 'Banco Frances';
$lang['INDEX_section6_item2_tittle'] = 'Financiera';
$lang['INDEX_section6_item2_desc'] = 'Mediante nuestras herramientas de integraciones se implemento la gestion de documentaci�n con respaldo notarial a compradores del Banco.';
$lang['INDEX_section6_item3_name'] = 'Banco Formosa';
$lang['INDEX_section6_item3_tittle'] = 'Financiera';
$lang['INDEX_section6_item3_desc'] = 'Gracias a las herramientas de validaci�n y normalizaci�n se logro optimizar el proceso de puesta a disposici�n de documentaci�n digital.';
$lang['INDEX_section6_item4_name'] = 'Toyota Plan';
$lang['INDEX_section6_item4_tittle'] = 'Automotriz';
$lang['INDEX_section6_item4_desc'] = 'Diferentes sistemas de validaci�n, normalizaci�n y scoring le permitieron al cliente mejorar el proceso de digitalizaci�n y comunicaci�n electronica.';

///////////////////////////////////////////////////////////////////////////////////
//APITEST.PHP
///////////////////////////////////////////////////////////////////////////////////

$lang['APITEST_header_tittle'] = 'Acceda a Probar las APIS';
$lang['APITEST_header_subtittle'] = 'Pruebe mediante nuestro panel automatizado las APIS de su inter�s.';
$lang['APITEST_item1_tittle'] = 'Contacto';
$lang['APITEST_item1_subtittle'] = 'Env�enos un mensaje y a la brevedad le responderemos.';
$lang['APITEST_item2_tittle'] = 'Chat en vivo';
$lang['APITEST_item2_subtittle'] = 'Habla con nosotros en cualquier momento';
$lang['APITEST_item3_tittle'] = 'Cargar Tickets';
$lang['APITEST_item3_subtittle'] = 'Enviamos un ticket para atender tu solicitud';
$lang['APITEST_item2_bottom'] = 'Chat en vivo';
$lang['APITEST_item3_bottom'] = 'Cargar Ticket';
$lang['APITEST_test_tittle'] = 'Prueba y Documentaci�n al instante';
$lang['APITEST_test_subtittle'] = 'Seleccione la API de su inter�s y acceda al sitio de prueba online:';
$lang['APITEST_item4_bottom'] = 'ACCEDER';

///////////////////////////////////////////////////////////////////////////////////
//SUPPORT.PHP
///////////////////////////////////////////////////////////////////////////////////

$lang['SUPPORT_header_tittle'] = 'Contacto';
$lang['SUPPORT_header_subtittle'] = 'Un equipo de especialistas est� para asesorarlo en sus inquietudes. No dude en contactarnos.';
$lang['SUPPORT_item1_tittle'] = 'Informaci�n de contacto';
$lang['SUPPORT_item1_subtittle'] = 'Env�enos un mensaje y a la brevedad le responderemos.';
$lang['SUPPORT_item2_tittle'] = 'Chat en vivo';
$lang['SUPPORT_item2_subtittle'] = 'Habla con nosotros en cualquier momento';
$lang['SUPPORT_item3_tittle'] = 'Cargar Tickets';
$lang['SUPPORT_item3_subtittle'] = 'Enviamos un ticket para atender tu solicitud';
$lang['SUPPORT_item2_bottom'] = 'Chat en vivo';
$lang['SUPPORT_item3_bottom'] = 'Cargar Ticket';
$lang['SUPPORT_item4_tittle'] = 'Envianos un mensaje';
$lang['SUPPORT_item4_subtittle'] = 'Env�anos un mensaje con tu correo y un comentario y uno de nuestros operadores te responder� lo m�s pronto posible.';
$lang['SUPPORT_item4_bottom'] = 'Enviar';

///////////////////////////////////////////////////////////////////////////////////
//ABOUTUS.PHP
///////////////////////////////////////////////////////////////////////////////////

$lang['ABOUT_header_tittle'] = 'Empresa';
$lang['ABOUT_header_subtittle'] = 'SysWorld Servicios S.A - es una empresa Argentina dedicada a servicios de tecnolog�a, comunicaciones y datacenter. Forma parte de un grupo de negocios conjuntamente con Worldsys S.A., una empresa dedicada al desarrollo de software de aplicaci�n, implantaci�n y servicios de consultor�a relacionados con las �reas administrativas y contables de entidades financieras, industriales, comerciales y de servicios l�der en el mercado nacional.';
$lang['ABOUT_section1_tittle'] = 'Nuestros Valores';
$lang['ABOUT_section1_subtittle'] = 'Trabajamos d�a a d�a para ser una de las empresas l�deres en la prestaci�n de servicios profesionales de comunicaciones y certificaciones electr�nicas de valor agregado gracias al compromiso y talento de nuestros profesionales. En cada proyecto, nos ponemos a prueba, entregamos lo mejor de nosotros para comprender los desaf�os que enfrentan nuestros clientes para desarrollar soluciones que le agreguen valor a su negocio.';
$lang['ABOUT_section1_item1'] = 'Datacenter Propio';
$lang['ABOUT_section1_item2'] = 'Calidad y Seguridad';
$lang['ABOUT_section1_item3'] = 'Socios Estat�gicos';
$lang['ABOUT_section1_item4'] = 'Pol�ticas de Calidad';
$lang['ABOUT_section1_item5'] = 'FUNDADO EN';
$lang['ABOUT_section1_item6'] = 'CLIENTES EN TODO EL MUNDO';
$lang['ABOUT_section1_item7'] = 'PROYECTOS COMPLETADOS';
$lang['ABOUT_section1_item8'] = 'MIEMBROS DE EQUIPO';

$lang['ABOUT_section2_tittle'] = 'Ve algunas Funcionalidades';
$lang['ABOUT_section2_subtittle'] = 'Un equipo de especialistas analizan constantemente los cambios tecnol�gicos para adaptar nuestras soluciones r�pidamente.';

$lang['ABOUT_section3_tittle'] = 'Equipo de Apilanding';
$lang['ABOUT_section3_subtittle'] = 'Hemos sido pioneros en el concepto de un centro de datos distribuida globalmente y con certificaciones de calidad y seguridad.';
$lang['ABOUT_section3_item1_tittle'] = 'Desarrollador';
$lang['ABOUT_section3_item1_subtittle'] = 'Lorem Ipsum is simply dummy text of the printing and typesetting Lorem Ipsum is simply dummy text of the printing';
$lang['ABOUT_section3_item2_tittle'] = 'Marketing Digital';
$lang['ABOUT_section3_item2_subtittle'] = 'Lorem Ipsum is simply dummy text of the printing and typesetting Lorem Ipsum is simply dummy text of the printing';
$lang['ABOUT_section3_item3_tittle'] = 'Fundador';
$lang['ABOUT_section3_item3_subtittle'] = 'Lorem Ipsum is simply dummy text of the printing and typesetting Lorem Ipsum is simply dummy text of the printing';
$lang['ABOUT_section3_item4_tittle'] = 'Director de tecnolog�a';
$lang['ABOUT_section3_item4_subtittle'] = 'Lorem Ipsum is simply dummy text of the printing and typesetting Lorem Ipsum is simply dummy text of the printing';

///////////////////////////////////////////////////////////////////////////////////
//NETWORK.PHP
///////////////////////////////////////////////////////////////////////////////////

$lang['NETWORK_header_tittle'] = 'Redes y Datacenter';
$lang['NETWORK_header_subtittle'] = 'Hemos sido pioneros en el concepto de un centro de datos distribuida globalmente y con certificaciones de calidad y seguridad. ';

$lang['NETWORK_section1_item1_title'] = 'Monitoreo';
$lang['NETWORK_section1_item1_subtitle'] = 'Un software centralizado interno/externo se encarga de monitorear continuamente todas las redes y servidores enlaces, servidores, servicios, accesos, proveedores y dem�s controles para obtener alertas inmediatas ante el anormal funcionamiento de cualquier eslab�n de la cadena de servicio.';
$lang['NETWORK_section1_item2_title'] = 'Almacenamiento';
$lang['NETWORK_section1_item2_subtitle'] = 'Servidores de almacenamiento de alta disponibilidad con discos r�gidos de alta velocidad brindan almacenamiento suficiente para la red de productos y servicios.';
$lang['NETWORK_section1_item3_title'] = 'Infraestructura';
$lang['NETWORK_section1_item3_subtitle'] = 'Simplemente desarrollamos software como servicios para gesti�n, securizaci�n y automatizaci�n de procesos de comunicaciones entre una organizaci�n y sus clientes.';
$lang['NETWORK_section1_item4_title'] = 'Seguridad';
$lang['NETWORK_section1_item4_subtitle'] = 'Un sistema de c�maras de monitoreo ubicadas estrat�gicamente en los accesos y dentro de los centros de procesamiento son las encargadas de registrar todos los accesos y movimientos entorno a los servidores. Controles de acceso por tarjetas de proximidad y huella digital adem�s de controles de acceso f�sico por personal seguridad son las tres barreras de seguridad previas a pasar para tener acceso al �rea de Servidores. Sensores de movimiento y alarmas dentro del CPD registran y emiten alertas de seguridad al personal de prevenci�n para controlar y registrar cambios f�sicos y/o ambientales.';

///////////////////////////////////////////////////////////////////////////////////
//QUALITY.PHP
///////////////////////////////////////////////////////////////////////////////////

$lang['QUALITY_header_tittle'] = 'Normas de Calidad';
$lang['QUALITY_header_subtittle'] = 'La necesidad de alcanzar niveles competitivos internacionales, llev� a la direcci�n de nuestra empresa a fijar estrategias espec�ficas hacia los sistemas de gesti�n basados en el control total de la calidad.';

$lang['QUALITY_section1_item1_tittle'] = 'Pol�ticas de Calidad';
$lang['QUALITY_section1_item1_subtittle'] = 'La calidad y la satisfacci�n de nuestros clientes es nuestro principal foco en todo los procesos de nuestra empresa.';
$lang['QUALITY_section1_item2_tittle'] = 'Seguridad de Informaci�n';
$lang['QUALITY_section1_item2_subtittle'] = 'Todos nuestros procesos y normas internas se centran en garantizar la seguridad de los datos de nuestros clientes.';
$lang['QUALITY_section1_item3_tittle'] = 'Integraci�n';
$lang['QUALITY_section1_item3_subtittle'] = 'El proceso de integraci�n es f�cil y r�pido. Nuestros asistentes y tutoriales en linea lo acompa�aran en todo el proceso.';
$lang['QUALITY_section1_item4_tittle'] = 'Innovaci�n Permanente';
$lang['QUALITY_section1_item4_subtittle'] = 'Un equipo de especialistas analizan constantemente los cambios tecnol�gicos para adaptar nuestras soluciones r�pidamente.';
$lang['QUALITY_section1_item5_tittle'] = '24 Hs Online';
$lang['QUALITY_section1_item5_subtittle'] = 'Diferentes centros de datos globalmente distribuidos garantizan el uptime de nuestros servicios y la continuidad del negocio.';
$lang['QUALITY_section1_item6_tittle'] = 'Pol�ticas de Calidad';
$lang['QUALITY_section1_item6_subtittle'] = 'La calidad y la satisfacci�n de nuestros clientes es nuestro principal foco en todo los procesos de nuestra empresa.';

///////////////////////////////////////////////////////////////////////////////////
//LEGAL.PHP
///////////////////////////////////////////////////////////////////////////////////

$lang['LEGAL_header_tittle'] = 'Pol�ticas de Privacidad';
$lang['LEGAL_header_subtittle'] = 'Nuestras Pol�ticas de Privacidad cuentan con las certificaciones necesarias para garantizar el buen funcionamiento de todos nuestros servicios y productos.';

$lang['LEGAL_section1_tittle'] = 'Privacidad y Confidencialidad';
$lang['LEGAL_section1_subtittle'] = 'El titular de los datos personales tiene la facultad de ejercer el derecho de acceso a los mismos en forma gratuita a intervalos no inferiores a seis meses, salvo que se acredite un inter�s leg�timo al efecto conforme lo establecido en el art�culo 14, inciso 3 de la Ley N� 25.326 DIRECCION NACIONAL DE PROTECCION DE DATOS PERSONALES, Organo de Control de la Ley N� 25.326, tiene la atribuci�n de atender las denuncias y reclamos que se interpongan con relaci�n al incumplimiento de las normas sobre protecci�n de datos personales.';
$lang['LEGAL_section1_item1'] = 'USO DE COOKIES: LA EMPRESA utiliza cookies cuando un Usuario navega por los sitios y p�ginas web del Sitio. Las cookies se asocian �nicamente con un Usuario an�nimo y su computadora, y no proporcionan por s� el nombre y apellidos del usuario. Las cookies de LA EMPRESA no pueden leer datos de su disco duro ni leer los archivos cookies creados por otros proveedores. ';
$lang['LEGAL_section1_item2'] = 'SEGURIDAD DE LOS DATOS: LA EMPRESA previene de manera l�gica y en las medidas de sus posibilidades, sin que esto implique responsabilidad alguna para ella, los accesos no autorizados. Todas las transacciones se procesan en servidores que garantizan la protecci�n de la informaci�n confidencial, conforme as� lo establece el Art.9 LEY 25326 El responsable o usuario del archivo de datos debe adoptar las medidas t�cnicas y organizativas que resulten necesarias para garantizar la seguridad y confidencialidad de los datos personales, de modo de evitar su adulteraci�n, p�rdida, consulta o tratamiento no autorizado, y que permitan detectar desviaciones, intencionales o no, de informaci�n, ya sea que los riesgos provengan de la acci�n humana o del medio t�cnico utilizado. Queda prohibido registrar datos personales en archivos, registros o bancos que no re�nan condiciones t�cnicas de integridad y seguridad.';

$lang['LEGAL_section2_item3_tittle'] = 'Utilizaci�n de los Servicios';
$lang['LEGAL_section2_item3_subtittle'] = 'Los usuarios deber�n utilizar los servicios, y acceder a los contenidos del sitio de conformidad con las disposiciones establecidas en estos t�rminos y condiciones; con el ordenamiento jur�dico al que se encuentren sometidos en raz�n del lugar, de las personas, o de la materia de la cual se trate, considerado en su conjunto; y seg�n las pautas de conducta impuestas por la moral, las buenas costumbres y el debido respeto a los derechos de terceros.';
$lang['LEGAL_section2_item3_subtittle2'] = 'Cualquier uso de los productos y/o servicios que tenga por objeto, lesionar los derechos de terceros, contravenir el orden jur�dico o constituya una pr�ctica ofensiva.
- Infrinjan los derechos de propiedad intelectual de terceros. - Posea contenido inapropiado. - Tenga por objeto vulnerar la seguridad, y o normal funcionamiento de los sistemas inform�ticos de Apilanding o de terceros. - Induzca, instigue o promueva acciones delictivas, il�citas, disfuncionales o moralmente reprochables, o constituya una violaci�n de derechos de propiedad intelectual de terceras personas. - Tenga por objeto recolectar informaci�n de terceros con el objeto de remitirles publicidad o propaganda de cualquier tipo o especie, sin que esta fuera expresamente solicitada.';
$lang['LEGAL_section2_item3_subtittle3'] = 'El titular de los datos personales tiene la facultad de ejercer el derecho de acceso a los mismos en forma gratuita a intervalos no inferiores a seis meses, salvo que se acredite un inter�s leg�timo al efecto conforme lo establecido en el art�culo 14, inciso 3 de la Ley N� 25.326 DIRECCION NACIONAL DE PROTECCION DE DATOS PERSONALES, Organo de Control de la Ley N� 25.326, tiene la atribuci�n de atender las denuncias y reclamos que se interpongan con relaci�n al incumplimiento de las normas sobre protecci�n de datos personales.';
$lang['LEGAL_section_item4_tittle'] = 'T�rminos y Condiciones';
$lang['LEGAL_section_item4_subtittle'] = 'Los usuarios deber�n utilizar los servicios, y acceder a los contenidos del sitio de conformidad con las disposiciones establecidas en estos t�rminos y condiciones; con el ordenamiento jur�dico al que se encuentren sometidos en raz�n del lugar, de las personas, o de la materia de la cual se trate, considerado en su conjunto; y seg�n las pautas de conducta impuestas por la moral, las buenas costumbres y el debido respeto a los derechos de terceros
USO PROHIBIDO Cualquier uso de los productos y/o servicios que tenga por objeto, lesionar los derechos de terceros, contravenir el orden jur�dico o constituya una pr�ctica ofensiva.';
$lang['LEGAL_section_item4_subtittle2'] = 'Infrinjan los derechos de propiedad intelectual de terceros. - Posea contenido inapropiado. - Tenga por objeto vulnerar la seguridad, y o normal funcionamiento de los sistemas inform�ticos de Apilanding o de terceros. - Induzca, instigue o promueva acciones delictivas, il�citas, disfuncionales o moralmente reprochables, o constituya una violaci�n de derechos de propiedad intelectual de terceras personas. - Tenga por objeto recolectar informaci�n de terceros con el objeto de remitirles publicidad o propaganda de cualquier tipo o especie, sin que esta fuera expresamente solicitada.';
$lang['LEGAL_section2_item5_tittle'] = 'Uso Indebido';
$lang['LEGAL_section2_item5_subtittle'] = 'Cualquier tipo de manipulaci�n y/o publicaci�n de p�ginas, c�digo, logotipo e informaci�n del Sitio en p�ginas de Internet del Usuario o un tercero est� estrictamente prohibido. Esto incluye, pero no se limita a, la publicaci�n de cualquier p�gina o parte de p�gina del Sitio en un frame o cuadro de otra p�gina.';
$lang['LEGAL_section2_item6_tittle'] = 'Legislaci�n y Jurisdicci�n';
$lang['LEGAL_section2_item6_subtittle'] = 'A todos los efectos legales en relaci�n a los productos y/o servicios brindados por Apilanding, ser� aplicable la legislaci�n vigente en la Rep�blica Argentina, y ser� competente la justicia ordinaria con jurisdicci�n en la Ciudad de Buenos Aires.';

///////////////////////////////////////////////////////////////////////////////////
//CONTACT.PHP
///////////////////////////////////////////////////////////////////////////////////

$lang['CONTACT_header_tittle'] = 'Cont�ctenos';
$lang['CONTACT_header_subtittle'] = 'Un equipo de especialistas est� para asesorarlo en sus inquietudes. No dude en contactarnos.';
$lang['CONTACT_info_tittle'] = 'Informaci�n de Contacto';
$lang['CONTACT_info_item_bottom'] = 'Conecta con';
$lang['CONTACT_info_item1'] = 'Nombre';
$lang['CONTACT_info_item2'] = 'Email';
$lang['CONTACT_info_item3'] = 'Tel�fono';
$lang['CONTACT_info_item4'] = 'Compa��a';
$lang['CONTACT_info_item5'] = 'Mensaje';
$lang['CONTACT_info_item6'] = 'Enviar';

///////////////////////////////////////////////////////////////////////////////////
//FAQ.PHP
///////////////////////////////////////////////////////////////////////////////////

$lang['FAQ_header_tittle'] = 'Preguntas Frecuentes';
$lang['FAQ_header_subtittle'] = 'Conoce las preguntas mas solicitadas por nuestros clientes y usuarios';

$lang['FAQ_item1'] = 'NORMAS DE CALIDAD';
$lang['FAQ_item1_desc'] = 'Conoce nuestras normas de calidad';
$lang['FAQ_item2'] = 'EMPRESA';
$lang['FAQ_item2_desc'] = 'Conoce nuestra empresa';
$lang['FAQ_item3'] = 'lEGALES';
$lang['FAQ_item3_desc'] = 'Conoce nuestras polit�cas de privacidad';
$lang['FAQ_item4'] = 'PRECIOS Y PLANES';
$lang['FAQ_item4_desc'] = 'Conoce nuestros precios y planes';

$lang['FAQ_question1'] = '�?';
$lang['FAQ_answer1'] = 'respuesta1';
$lang['FAQ_question2'] = '�?';
$lang['FAQ_answer2'] = 'respuesta2';
$lang['FAQ_question3'] = '�?';
$lang['FAQ_answer3'] = 'respuesta3';
$lang['FAQ_question4'] = '�?';
$lang['FAQ_answer4'] = 'respuesta4';
$lang['FAQ_question5'] = '�?';
$lang['FAQ_answer5'] = 'respuesta5';
$lang['FAQ_question6'] = '�?';
$lang['FAQ_answer6'] = 'respuesta6';
$lang['FAQ_question7'] = '�?';
$lang['FAQ_answer7'] = 'respuesta7';
$lang['FAQ_question8'] = '�?';
$lang['FAQ_answer8'] = 'respuesta8';
$lang['FAQ_question9'] = '�?';
$lang['FAQ_answer9'] = 'respuesta9';
$lang['FAQ_question10'] = '�?';
$lang['FAQ_answer10'] = 'respuesta10';
$lang['FAQ_question11'] = '�?';
$lang['FAQ_answer11'] = 'respuesta11';





?>