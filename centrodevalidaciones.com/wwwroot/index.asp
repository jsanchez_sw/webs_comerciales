﻿<!DOCTYPE html>
<html lang="en-gb" dir="ltr">

    <head>
        <!-- Standard Meta -->
        <meta charset="utf-8">
        <meta name="format-detection" content="telephone=no" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- Site Properties -->
        <title>Centro de Validaciones - Inicio</title>
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon-precomposed" href="images/apple-touch-icon.png">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Rajdhani:300,400,500,600,700%7CRoboto:300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">

        <!-- CSS -->
        <link rel="stylesheet" href="css/uikit.css">
        <link rel="stylesheet" href="css/components/slider.css">
        <link rel="stylesheet" href="css/components/slideshow.css">
		<link rel="stylesheet" href="css/components/slidenav.css">
        <link rel="stylesheet" href="css/components/dotnav.css">
        <link rel="stylesheet" href="css/small-icon.css">
        <link rel="stylesheet" href="css/elements.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/mediaquery.css">
    </head>

    <body>

		<!-- Header Section -->
		<header id="idz-header">
			<!-- Mobile / Offcanvas Menu -->
        	<div id="mobile-nav" class="uk-offcanvas">
				<div class="uk-offcanvas-bar">
					<ul class="uk-nav uk-nav-offcanvas uk-nav-parent-icon" data-uk-nav>
						<li class="uk-parent uk-active"><a href="index.asp">Inicio</a>
						</li>
			            <li><a href="shared.html">shared</a></li>
			            <li><a href="vps.html">vps</a></li>
			            <li><a href="dedicated.html">Dedicated</a></li>
			            <li><a href="other-services.html">other services </a></li>
			            <li class="uk-parent"><a href="#">blog</a>
						    <ul class="uk-nav-sub">
						    	<li><a href="blog.html">Blog Page</a></li>
						        <li><a href="single.html">Single Post</a></li>
					        </ul>
			            </li>
			            <li><a href="contact.html">Meet Us</a></li>
						<li class="uk-parent"><a href="#">Pages</a>
						    <ul class="uk-nav-sub">
						        <li><a href="domain-pricing.html">Domain Pricing</a></li>
						        <li><a href="features.html">Features</a></li>
						        <li><a href="about.html">About</a></li>
						        <li><a href="testimonial.html">Testimonial</a></li>
						        <li><a href="team.html">Team</a></li>
						        <li><a href="login.html">Login</a></li>
						        <li><a href="client-area.html">Client Area</a></li>
						        <li><a href="portfolio.html">Portfolio</a></li>
						        <li><a href="faq.html">FAQ</a></li>
						        <li><a href="sitemap.html">Sitemap</a></li>
						        <li><a href="404.html">404 Error</a></li>
						    </ul>
						</li>
						<li class="uk-parent"><a href="#">Elements</a>
						    <ul class="uk-nav-sub">
						        <li><a href="column.html">Column</a></li>
						        <li><a href="table.html">Table</a></li>
						        <li><a href="button-list.html">Button &amp; List</a></li>
						        <li><a href="icon-list.html">Icon List</a></li>
						        <li><a href="icon-use.html">Icon Use</a></li>
						        <li><a href="typography.html">Typography</a></li>
						        <li><a href="panel-promo.html">Panel &amp; Promo Box</a></li>
						        <li><a href="tab-accordion.html">Tab &amp; Accordion</a></li>
						        <li><a href="alert-progress.html">Alert &amp; Progress</a></li>
						    </ul>
						</li>
					</ul>
				</div>
			</div>
			<!-- Mobile / Offcanvas Menu End -->

			<div class="uk-container uk-container-center">
				<div class="uk-grid">

					<!-- Logo -->
					<div class="uk-width-large-1-2 uk-width-medium-1-1 uk-width-small-1-1">
						<div class="idz-logo">
							<a href="index.asp"><img src="images/logo.png" alt="logo" class=""></a>
						</div>
					</div>
					<!-- Logo End -->

					<!-- Domain Search Form -->
					<div class="uk-width-large-1-2  uk-width-medium-1-1 uk-width-small-1-1">
						<div class="idz-search-domain-block">
							<div class="idz-header-search-domain">

								<form class="uk-form idz-search-domain-form">
									<fieldset>
									<input type="text" class="uk-form-large uk-width-1-1 " placeholder="Prueba de Validación de Email/Teléfono/Domicilio/DNI">
									<button class="uk-button-large idz-button blue uppercase">Probar Validador</button>
									</fieldset>
								</form>
							</div>
						</div>
					</div>
					<!-- Domain Search Form End -->

					<!-- Header Info -->
					<div class="uk-width-large-1-1 uk-width-medium-1-1 uk-width-small-1-1">
						<ul class="uk-grid uk-grid-large uk-grid-with-medium-1-4 idz-header-contact">
							<li><i class="uk-icon-globe"></i>Español</li>
							<li><i class="uk-icon-phone"></i>(+54 11) 5263-2919</li>
							<li><i class="uk-icon-headphones"></i><a target="blank" href="http://www.sysworld.com.ar/soporte.asp">Live Chat</li></a>
							<li><i class="uk-icon-life-bouy "></i><a href="#client-area" data-uk-modal>Acceso Clientes</a>
						</ul>
					</div>
					<!-- Header End -->

				</div>
			</div>

			<!-- Header Nav -->
			<div class="idz-header-nav" data-uk-sticky="{top:-400}">

				<div class="uk-container uk-container-center">
					<div class="uk-grid">
						<div class="uk-width-medium-1-1">

							<div class="idz-menu-wrapper">
								<div class="uk-grid">
									<div class="uk-width-large-8-10 uk-width-medium-3-10 uk-width-small-3-10">

										<!-- Menu Items -->
										<div class="idz-nav">
											<nav class="uk-navbar">
											    <div class="uk-navbar-flip">
											        <ul class="uk-navbar-nav uk-hidden-medium  uk-hidden-small">
											            <li class="uk-parent uk-active" data-uk-dropdown><a href="index.asp">Inicio</a></li>
											            <li><a href="empresa.asp">Nuestra Empresa</a></li>
											            <li class="uk-parent" data-uk-dropdown="{justify:'.idz-menu-wrapper'}"><a href="funcionamiento.asp">Validadores</a>
					                                        <div class="uk-dropdown uk-dropdown-navbar idz-megamenu">
					                                            <ul class="uk-grid" data-uk-grid-margin>
					                                                <li class="uk-width-large-3-10 uk-width-medium-4-10">
					                                                    <ul class="uk-list uk-list-space">
					                                                        <li>
					                                                            <a href="validadoremail.asp">
					                                                                <div class="icon-wrap circle left small blue">
					                                                                    <i class="uk-icon-cubes"></i>
					                                                                </div>
					                                                                <h5>Email</h5>
					                                                                <p class="uk-nbfc">Verifique si una cuenta de email existe o no.</p>
					                                                            </a>
					                                                        </li>
					                                                        <li>
					                                                            <a href="validadortelefono.asp">
					                                                                <div class="icon-wrap circle left small blue">
					                                                                    <i class="uk-icon-cube"></i>
					                                                                </div>
					                                                                <h5>Teléfono</h5>
					                                                                <p class="uk-nbfc">Verifique y Normalice un número de teléfono fijo y/o movil.</p>
					                                                            </a>
					                                                        </li>
					                                                        <li>
					                                                            <a href="validadordomicilio.asp">
					                                                                <div class="icon-wrap circle left small blue">
					                                                                    <i class="uk-icon-tasks"></i>
					                                                                </div>
					                                                                <h5>Domicilio</h5>
					                                                                <p class="uk-nbfc">Valide, Normalice y Enriquezca un Domicilio.</p>
					                                                            </a>
					                                                        </li>
					                                                        <li>
					                                                            <a href="validadoridentidad.asp">
					                                                                <div class="icon-wrap circle left small blue">
					                                                                    <i class="uk-icon-tasks"></i>
					                                                                </div>
					                                                                <h5>Identidad</h5>
					                                                                <p class="uk-nbfc">Valide la identidad de una persona física.</p>
					                                                            </a>
					                                                        </li>					                                                    </ul>
					                                                </li>
					                                                <li class="uk-width-large-2-10 uk-width-medium-3-10">
					                                                	<h5>Otros Servicios</h5>
					                                                    <ul class="uk-list uk-list-space idz-list-check">
					                                                        <li><a href="http://www.enviocertificado.com" target="_blank">EnvíoCertificado.com</a></li>
					                                                        <li><a href="http://www.enviocertificado.com" target="_blank">ContactoCertificado.com</a></li>
					                                                        <li><a href="http://www.tuscomprobantes.com" target="_blank">TusComprobantes.com</a></li>
					                                                    </ul>
					                                                </li>
					                                                <li class="uk-width-large-2-10 uk-width-medium-3-10">
					                                                	<h5>Soporte</h5>
					                                                    <ul class="uk-list  uk-list-space idz-list-custom">
					                                                        <li><a href="contacto.asp"><i class="uk-icon-envelope-o"></i>Contactenos</a></li>
					                                                        <li><a href="http://help.enviocertificado.com" target="_blank"><i class="uk-icon-book"></i>Tutoriales</a></li>
					                                                        <li><a href="http://help.enviocertificado.com" target="_blank"><i class="uk-icon-comments-o"></i>Foro de Debate</a></li>
					                                                        <li><a href="http://www.sysworld.com.ar/portal" target="_blank"><i class="uk-icon-exclamation-triangle"></i>Portal de Clientes</a></li>
					                                                    </ul>
					                                                </li>
					                                                <li class="uk-width-large-3-10 uk-hidden-medium">
					                                                    <h5>¿Porqué usar validadores?</h5>
					                                                    <p>Integrar sistemas automáticos de validación en sus procesos le garantiza poder contactar a sus clientes y reducir costos en la comunicación.</p>
					                                                </li>
					                                            </ul>
					                                        </div>
					                                    </li>
											            <li><a href="funcionamiento.asp">Funcionamiento</a></li>
											            <li><a href="contacto.asp">Contactenos</a></li>
											        </ul>
											    </div>
											</nav>
											<!-- Mobile Menu Toggle  -->
                                            <a href="#mobile-nav" class="uk-navbar-toggle uk-visible-small uk-visible-medium" data-uk-offcanvas></a>
                                            <!-- Mobile Menu Toggle End-->
										</div>
										<!-- Menu Items End -->
									</div>


								</div>
							</div>

						</div>
					</div>
				</div>

			</div>
			<!-- Header Nav End -->

		</header>
		<!-- Header Section End -->

		<!-- Slideshow -->
		<section id="idz-slideshow">
			<div class="uk-slidenav-position" data-uk-slideshow="{autplay: true,animation:'scroll',height: 480}">
			    <ul class="uk-slideshow">
			    	<!-- Slideshow Item 1 -->
			        <li class="idz-padding slide-bg-1">
			        	<div class="uk-container uk-container-center">
				        	<div class="uk-grid">
								<div class="uk-width-large-1-2 uk-width-medium-1-2">
									<div class="idz-slideshow-content">
										<h1>Integre un validador online de email en sus sistemas. </h1>
										<h3 class="idz-text-22px">Verifique dentro de sus procesos si una dirección de correo electrónico es válida o no.</h3>
										<div class="idz-price-tag">
											<p>Desde: </p>
											<h1>$450/mes</h1>
										</div>
										<a href="#" class="uk-button idz-button outline red xlarge uppercase">Pruebelo Ahora!</a>
									</div>
								</div>

								<div class="uk-width-large-1-2 uk-width-medium-1-2">
									<img src="images/sample/slideshow/cloud.png" alt="cloud" class="uk-img-preserve idz-slides-img">
								</div>
				        	</div>
			        	</div>
			        </li>
			        <!-- Slideshow Item 1 End -->

			        <!-- Slideshow Item 2 -->
			        <li class="slide-bg-2">
			        	<div class="uk-container uk-container-center">
                            <div class="uk-grid">
                                <div class="uk-width-large-8-10 uk-width-medium-8-10">
                                    <img src="images/sample/slideshow/sld_object2.png" alt="" class="uk-align-left sld3-img">
                                    <div class="slide-content3">
                                        <p class="uk-text-large uk-margin-bottom-remove">Validador de Identidad</p>
                                        <h1 class="uk-margin-top-remove">Conozaca quien acepta<br/>sus transacciones.</h1>
                                        <p class="uk-text-large uk-margin-medium-bottom">Integrado a buro de datos comerciales.</p>
                                        <a class="uk-button uk-button-large idz-button outline red" href="validadoridentidad.asp">Conozca Más<i class="uk-icon-chevron-circle-right"></i></a>
                                    </div>
                                </div>
                                <div class="uk-width-large-2-10 uk-text-center uk-position-relative">
                                    <div class="ribbon-slide">
                                        <p>Promoción</p>
                                        <h1>20%</h1>
                                        <h2>Dto.!</h2>
                                        <h4>Desde</h4>
                                        <h3>$4,5<span class="small-month">/reg</span></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
			        </li>
			        <!-- Slideshow Item 2 End -->



			        <!-- Slideshow Item 3 -->
			        <li class="slide-bg-3">
			        	<div class="uk-container uk-container-center">
                            <div class="uk-grid">
                                <div class="uk-width-large-7-10 uk-width-medium-7-10 uk-container-center uk-text-center">
                                    <div class="slide-content2">
                                        <div class="icon-wrap large center">
                                            <i class="smico-rocket"></i>
                                        </div>
                                        <h1 class="idz-text-white">Normalice y Valide sus bases</h1>
                                        <h4 class="uk-margin-small-top idz-text-white">Valide de forma batch su base de datos completa y normalice sus datos de contacto.</h4>
                                        <a class="uk-button uk-button-large idz-button red uk-margin-top" href="contacto.asp">Contactenos!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
			        </li>
			        <!-- Slideshow Item 3 End -->
			    </ul>

			    <!-- Slideshow Nav -->
			    <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
			    <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideshow-item="next"></a>
			    <!-- Slideshow Nav End -->

			    <!-- Slideshow Paging -->
			    <ul class="uk-dotnav uk-dotnav-contrast uk-position-bottom uk-flex-center">
			        <li data-uk-slideshow-item="0"><a href=""></a></li>
			        <li data-uk-slideshow-item="1"><a href=""></a></li>
			        <li data-uk-slideshow-item="2"><a href=""></a></li>
			    </ul>
			    <!-- Slideshow End -->
			</div>
		</section>
		<!-- Slideshow End -->


		<!-- Pricing Section -->
		<section id="idz-pricing" class="idz-padding">

			<div class="uk-container uk-container-center">

				<div class="uk-grid">

					<div class="uk-width-medium-2-3 uk-container-center">
						<div class="idz-intro-text uk-text-center">
							<p class="idz-text-blue idz-text-18px">Nuestros Planes</p>
							<h1>Paquetes de validadores completos</h1>
						</div>
					</div>
				</div>

				<ul class="uk-grid uk-grid-width-medium-1-3 uk-grid-width-small-1-1  idz-pricing-list">
					<li>
						<div class="idz-pricing-item default">
							<div class="idz-pricing-badge-icon"><i class="uk-icon-cubes"></i></div>
							<div class="idz-pricing-content">
								<h3 class="idz-">Pequeñas <span>Empresas</span></h3>
								<p>El plan ideal para empresas con menos de 2.000 transacciones mensuales.</p>
								<div class="idz-divider"></div>
								<ul class="uk-list idz-features-list">
									<li><i class="uk-icon-flag"></i>Validador de Email, Teléfono y Domicilio</li>
									<li><i class="uk-icon-database"></i>2.000 créditos/validador</li>
									<li><i class="uk-icon-dashboard "></i>Alta Disponibilidad</li>
								</ul>
								<a href="#" class="uk-button uk-button-large idz-button red uppercase">Probar Ahora<i class="uk-icon-angle-double-right"></i></a>
								<div class="idz-pricing-bg-icon"><i class="uk-icon-cubes"></i></div>
							</div>
						</div>
					</li>
					<li>
						<div class="idz-pricing-item default">
							<div class="idz-pricing-badge-icon featured"><i class="uk-icon-cube"></i></div>
							<div class="idz-pricing-content featured">
								<h3>Medianas <span>Empresas</span></h3>
								<p>El plan ideal para empresas con menos de 10.000 transacciones mensuales.</p>
								<div class="idz-divider red"></div>
								<ul class="uk-list idz-features-list">
									<li><i class="uk-icon-flag"></i>Validador de Email, Teléfono y Domicilio</li>
									<li><i class="uk-icon-database"></i>10.000 créditos/validador</li>
									<li><i class="uk-icon-dashboard "></i>Alta Disponibilidad</li>
								</ul>
								<a href="#" class="uk-button uk-button-large idz-button red uppercase">Probar Ahora<i class="uk-icon-angle-double-right"></i></a>
								<div class="idz-pricing-bg-icon"><i class="uk-icon-cube"></i></div>
							</div>
						</div>
					</li>
					<li>
						<div class="idz-pricing-item default">
							<div class="idz-pricing-badge-icon"><i class="uk-icon-tasks"></i></div>
							<div class="idz-pricing-content">
								<h3>Grandes <span>Empresas</span></h3>
								<p>El plan ideal para empresas con más de 10.000 transacciones mensuales.</p>
								<div class="idz-divider"></div>
								<ul class="uk-list idz-features-list">
									<li><i class="uk-icon-flag"></i>Validador de Email, Teléfono, Domicilio e Identidad</li>
									<li><i class="uk-icon-database"></i>Créditos a demanda</li>
									<li><i class="uk-icon-dashboard "></i>Alta Disponibilidad</li>
								</ul>
								<a href="#" class="uk-button uk-button-large idz-button red uppercase">Probar Ahora<i class="uk-icon-angle-double-right"></i></a>
								<div class="idz-pricing-bg-icon"><i class="uk-icon-tasks"></i></div>
							</div>
						</div>
					</li>
				</ul>

				<div class="idz-spacer large"></div>

				<div class="uk-grid">
					<div class="uk-width-medium-1-2">
						<div class="idz-section-intro uk-margin-top">
							<h1>Ventajas de los validadores</h1>
							<p class="idz-text-18px idz-lineheight-1-8 idz-text-blue idz-margin-bottom-medium">Todos nuestros validadores trabajan con infraestructura de alta disponibilidad y balanceo de carga.</p>

							<div class="uk-grid uk-grid-small">
								<div class="uk-width-large-3-10 uk-width-medium-3-10 uk-width-small-4-10">
									<img src="images/sample/logo/processor.png" class="uk-img-preserve uk-align-left idz-fit-img" alt="processor">
								</div>
								<div class="uk-width-large-7-10 uk-width-medium-7-10 uk-width-small-6-10">
									<ul class="uk-list idz-list-check">
										<li>Webservices de respuesta online</li>
										<li>Validaciones Batch de BD</li>
										<li>Interfaces de salida</li>
										<li>Validación, Normalización y Sugerencias</li>
									</ul>
								</div>
							</div>

						</div>
					</div>

					<div class="uk-width-medium-1-2 idz-tablet-pt-padding">
						<img src="images/sample/slideshow/cloud-server.png" class="uk-img-preserve idz-fit-img" alt="cloud server">
					</div>
				</div>

			</div>

		</section>
		<!-- Pricing Section End -->

		<!-- Features Section -->
		<section id="idz-features" class="uk-position-relative idz-padding bg-parallax1" data-uk-parallax="{bg: -100}">

			<div class="uk-container uk-container-center">
				<div class="uk-grid">

					<div class="uk-width-medium-2-3 uk-container-center">
						<div class="idz-intro-text uk-text-center">
							<p class="idz-text-white">Funcionalidades Generales</p>
							<h1 class="idz-text-white">Validadores</h1>
						</div>
					</div>

				</div>

				<ul class="uk-grid uk-grid-width-large-1-4 uk-grid-width-medium-1-2 uk-grid-width-small-1-2 idz-list-divider">
					<li>
						<div class="idz-list-item">
							<div class="icon-wrap large circle center white">
								<i class="uk-icon-paint-brush"></i>
							</div>
							<h3 class="idz-text-22px idz-text-white uk-text-center">Email</h3>
							<ul class="uk-list idz-list-check white">
								<li>Comprobación de Sintaxis, Dominio, Servidor y Cuenta.</li>
								<li>Errores Tipificados.</li>
								<li>Sugerencias de Tipeo.</li>
								<li>Listas Negras.</li>
								<li>Control de Spam-Trap.</li>
							</ul>
						</div>
					</li>
					<li>
						<div class="idz-list-item">
							<div class="icon-wrap large circle center white">
								<i class="uk-icon-lock"></i>
							</div>
							<h3 class="idz-text-22px idz-text-white uk-text-center">Teléfono</h3>
							<ul class="uk-list idz-list-check white">
								<li>Reconocimiento de número Fijo / Movil + Compañia.</li>
								<li>Normalización</li>
								<li>Descripción del Aréa</li>
								<li>Detecta Portabilidad</li>
								<li>Detecta Movil Apagado</li>
							</ul>
						</div>
					</li>
					<li>
						<div class="idz-list-item">
							<div class="icon-wrap large circle center white">
								<i class="uk-icon-bar-chart"></i>
							</div>
							<h3 class="idz-text-22px idz-text-white uk-text-center">Domicilio</h3>
							<ul class="uk-list idz-list-check white">
								<li>Validación y enriquesimiento del Domicilio Completo.</li>
								<li>Coordenadas Lat. Long.</li>
								<li>Normalización</li>
								<li>Asignación de CP.</li>
								<li>Link Street-View.</li>
							</ul>
						</div>
					</li>
					<li>
						<div class="idz-list-item">
							<div class="icon-wrap large circle center white">
								<i class="uk-icon-life-bouy"></i>
							</div>
							<h3 class="idz-text-22px idz-text-white uk-text-center">Identidad</h3>
							<ul class="uk-list idz-list-check white">
								<li>Validación de Identidad integrado con Buro de Datos.</li>
								<li>Encuestas Multiple-Chise</li>
								<li>Score de Efectividad</li>
								<li>Respaldo Notarial</li>
								<li>Gestión de Legajo</li>
							</ul>
						</div>
					</li>
				</ul>

				<div class="uk-grid">
					<div class="uk-width-large-1-1 uk-width-medium-1-1 uk-width-small-1-1 uk-container-center uk-text-center">
						<a href="funcionamiento.asp" class="uk-button uk-button-large idz-button large outline white uppercase">Ver Funcionamiento General <i class="uk-icon-angle-double-right"></i></a>
						<a href="#" class="uk-button idz-button large red uppercase uk-margin-left">Probar Ahora! <i class="uk-icon-angle-double-right"></i></a>
					</div>
				</div>

			</div>

			<div class="idz-color-overlay"></div>
		</section>
		<!-- Features Section End -->

		<!-- Testimonials Section -->
		<section class="idz-padding">
			<div class="uk-container uk-container-center">
				<div class="uk-grid">

					<div class="uk-width-large-2-3 uk-width-medium-2-3 uk-container-center">
						<div class="idz-intro-text uk-text-center uk-margin-bottom">
							<h1>Casos de éxito</h1>
						</div>
					</div>

					<div class="uk-width-large-8-10 uk-width-medium-1-1 uk-width-small-8-10 uk-container-center">

						<div class="uk-slidenav-position idz-testimonial-slider" data-uk-slideshow="{autoplay: true, animation: 'scroll'}">
						    <ul class="uk-slideshow">
						        <li>
						        	<div class="uk-width-large-3-4 uk-width-medium-3-4  uk-width-small-8-10 uk-container-center">
							        	<div class="idz-testimonial-wrapper">
											<h3 class="idz-text-28 uk-margin-bottom-remove">Sistemas Fase</h3>
											<p class="idz-text-18px idz-testi-subtitle uk-margin-remove idz-text-blue">Alta de Clientes</p>
											<blockquote><p>Se instrumentaron los validadores de email, teléfono e identidad en el sistema ERP Fase para el alta de clientes.</p></blockquote>
											<img src="images/sample/people1.jpg" alt="people1" class="idz-testi-img">
										</div>
									</div>
						        </li>
						        <li>
						        	<div class="uk-width-large-3-4 uk-width-medium-3-4  uk-width-small-8-10 uk-container-center">
							        	<div class="idz-testimonial-wrapper">
											<h3 class="idz-text-28 uk-margin-bottom-remove">Autoahorro VW.</h3>
											<p class="idz-text-18px idz-testi-subtitle uk-margin-remove idz-text-blue">Procesos Internos</p>
											<blockquote><p>Se implemento la integración mediante webservices para realizar la validación del email en el alta de solicitudes de planes de ahorro.</p></blockquote>
											<img src="images/sample/people2.jpg" alt="people2" class="idz-testi-img">
										</div>
									</div>
						        </li>
						        <li>
						        	<div class="uk-width-large-3-4 uk-width-medium-3-4  uk-width-small-8-10 uk-container-center">
							        	<div class="idz-testimonial-wrapper">
											<h3 class="idz-text-28 uk-margin-bottom-remove">Caja de Valores</h3>
											<p class="idz-text-18px idz-testi-subtitle uk-margin-remove idz-text-blue">Validación Batch</p>
											<blockquote><p>Integración Batch con la base de datos del cliente para la validación, normalización y sugerencias de emails y teléfonos fíjos y móviles.</p></blockquote>
											<img src="images/sample/people4.jpg" alt="people2" class="idz-testi-img">
										</div>
									</div>
						        </li>
					        </ul>
					        <a href="" class="uk-slidenav uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
					    	<a href="" class="uk-slidenav uk-slidenav-next" data-uk-slideshow-item="next"></a>
				        </div>

			        </div>

				</div>
			</div>
		</section>
		<!-- Testimonials Section End -->

		<!-- Pre Footer -->
		<section id="pre-footer" class="idz-padding uk-padding-bottom-remove">
			<div class="uk-container uk-container-center">

				<div class="uk-grid">
					<div class="uk-width-large-3-5 uk-width-medium-3-5 uk-width-medium-1-1">
						<h1 class="idz-text-white">Comience a validar sus transacciones.</h1>
					</div>

					<div class="uk-width-large-2-5 uk-width-medium-2-5 uk-width-small-1-1">
						<div class="uk-grid">
							<div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-1">
								<div class="idz-price-badge idz-text-white">
									<div class="idz-price-badge-content">
										<span class="intro">Comienza</span>
										<span class="price">$0,07<span class="cycle">/reg</span></span>
									</div>
								</div>
							</div>
							<div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-1">
								<a href="#" class="uk-button idz-button outline white xlarge uppercase uk-align-right uk-width-1-1">Probar Ahora!</a>
							</div>
						</div>
					</div>
				</div>

			</div>
		</section>
		<!-- Pre Footer End -->

		<!-- Footer -->
		<footer id="idz-footer" class="idz-padding">
			<div class="uk-container uk-container-center">
				<div class="uk-grid">

					<!-- Footer Column 1 -->
					<div class="uk-width-large-1-4 uk-width-medium-1-4 uk-width-small-1-2">
						<div class="idz-footer-widget">
							<h3 class="uppercase idz-text-white">Compañía</h3>
							<ul class="uk-list uk-list-space idz-footer-menu">
								<li><a href="contacto.asp">Contactenos</a></li>
								<li><a href="http://www.sysworld.com.ar/legal.asp" target="_blank">Términos y Condiciones</a></li>
								<li><a href="http://www.sysworld.com.ar/legal.asp" target="_blank">Politica de Privacidad</a></li>
								<li><a href="http://www.sysworld.com.ar/" target="_blank">Sitio Coorporativo</a></li>
							</ul>
						</div>
					</div>
					<!-- Footer Column 1 End -->

					<!-- Footer Column 2 -->
					<div class="uk-width-large-1-4 uk-width-medium-1-4 uk-width-small-1-2">
						<div class="idz-footer-widget">
							<h3 class="uppercase idz-text-white">Soporte</h3>
							<ul class="uk-list uk-list-space idz-footer-menu">
								<li><a href="http://help.enviocertificado.com" target="_blank">Tutoriales</a></li>
								<li><a href="http://cloud.sysworld.com.ar" target="_blank">Portal de Tickets</a></li>
								<li><a href="http://erp.sysworld.com.ar/app" target="_blank">Portal Administrativo</a></li>
								<li><a href="contacto.asp" target="_blank">Contacto</a></li>
							</ul>
						</div>
					</div>
					<!-- Footer Column 2 End -->

					<!-- Footer Column 3 -->
					<div class="uk-width-large-1-4 uk-width-medium-1-4 uk-width-small-1-2">
						<div class="idz-footer-widget">
							<h3 class="uppercase idz-text-white">Validadores</h3>
							<ul class="uk-list uk-list-space idz-footer-menu">
								<li><a href="validadoremail.asp">Email</a></li>
								<li><a href="validadortelefono.asp">Teléfono</a></li>
								<li><a href="validadordomicilio.asp">Domicilio</a></li>
								<li><a href="validadoridentidad.asp">Identidad</a></li>
							</ul>
						</div>
					</div>
					<!-- Footer Column 3 End -->

					<!-- Footer Column 4 (Social Profile Links) -->
					<div class="uk-width-large-1-4 uk-width-medium-1-4 uk-width-small-1-2">
						<div class="idz-footer-widget idz-footer-social">
							<ul class="uk-list">
								<li>
									<div class="idz-button-group">
										<a target="blank" href="https://www.facebook.com/SysWorld.ar/"><i class="uk-icon-facebook-official"></i>seguinos en nuestro<span class="uppercase">facebook</span></a>
									</div>
								</li>
								<li>
									<div class="idz-button-group">
										<a target="blank" href="https://twitter.com/sysworldsa"><i class="uk-icon-twitter"></i>seguinos en nuestro<span class="uppercase">Twitter</span></a>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<!-- Footer Column 4 (Social Profile Links) -->


				</div>

				<!-- Footer Text -->
				<div class="uk-grid">
					<div class="uk-width-medium-1-1 uk-container-center uk-text-center">
						<p class="idz-footer-note idz-thin idz-text-22px uk-margin-remove">Ya validamos <span><%=FormatNumber(DateDiff("s", "11/08/2006 15:22:12", now),0)%> </span>transacciones</p>
						<p class="idz-site-copyright uk-margin-remove">COPYRIGHT  &copy; © 2006-<%=year(date)%> EnvioCertificado.com - Sysworld Servicios S.A.</p>
					</div>
				</div>
				<!-- Footer Text End -->
			</div>

			<!-- Scroll to Top -->
			<a href="#top" class="to-top uk-icon-chevron-up" data-uk-smooth-scroll></a>
			<!-- Scroll to Top End -->
        </footer>
		<!-- Footer End -->

		<!-- client area modal begin -->
        <div id="client-area" class="uk-modal">
            <div class="uk-modal-dialog">

                <ul id="client-area-tab" class="uk-tab uk-tab-grid" data-uk-tab="{connect:'#account'}">
                    <li class="uk-active uk-width-large-1-2"><a href="">Crear Cuenta</a></li>
                    <li class="uk-width-large-1-2"><a href="#">Registrarse</a></li>
                </ul>

                <ul id="account" class="uk-switcher uk-margin">
                	<!-- Modal Login Form -->
                    <li>
                        <form id="signin-account" class="uk-form">
                            <fieldset>
                                <div class="uk-form-icon">
                                    <i class="uk-icon-envelope"></i>
                                    <input type="text" placeholder="Email">
                                </div>
                                <div class="uk-form-icon">
                                    <i class="uk-icon-key"></i>
                                    <input type="text" placeholder="Password">
                                </div>
                                <label>
                                    <input type="checkbox">Recordarme</label>
                                	<a href="http://web.enviocertificado.com" target="_blank" class="uk-button uk-button-large idz-button blue uk-width-large-1-1">Ingresar</a>
                            </fieldset>
                        </form>
                    </li>
                	<!-- Modal Login Form End -->

					<!-- Modal Register Form -->
                    <li>
                        <form id="create-account" class="uk-form" action="http://cloud.sysworld.com.ar/clients/register" target="_blank">
                            <fieldset>
                                <div class="uk-form-icon">
                                    <i class="uk-icon-user"></i>
                                    <input type="text" placeholder="Usuario">
                                </div>
                                <div class="uk-form-icon">
                                    <i class="uk-icon-envelope"></i>
                                    <input type="text" placeholder="Email">
                                </div>
                                <div class="uk-form-icon">
                                    <i class="uk-icon-key"></i>
                                    <input type="text" placeholder="Password">
                                </div>
                                <label>
                                    <input type="checkbox">Acepto los términos y condiciones.</label>
                                <a href="http://cloud.sysworld.com.ar/clients/register" target="_blank" class="uk-button uk-button-large idz-button blue uk-width-large-1-1">Registrarse</a>
                            </fieldset>
                        </form>
                    </li>
                    <!-- Modal Register Form End -->
                </ul>
                <div class="uk-modal-caption">
                    <a href="#">Olvidaste tu contraseña?</a>
                </div>
            </div>
        </div>
        <!-- client area modal end -->


        <!-- Javascript -->
        <script src="js/jquery.js"></script>
        <script src="js/uikit.min.js"></script>
		<script src="js/components/slider.js"></script>
        <script src="js/components/slideshow.js"></script>
        <script src="js/components/slideshow-fx.js"></script>
        <script src="js/components/parallax.js"></script>
        <script src="js/components/sticky.js"></script>
        <script src="js/jquery.countTo.js"></script>
        <script src="js/mediaelement.js"></script>
        <script src="js/instafeed.min.js"></script>
        <script src="js/template-config.js"></script>
    </body>
</html>
