					<li class="menu-item-has-children">
						<a href="index.asp">Inicio</a>
					</li>

					<li class="">
						<a href="empresa.asp">Empresa</a>
						<ul class="sub-menu">
							<li>
								<a href="empresa.asp">
									Nosotros
								</a>
							</li>
							<li>
								<a href="empresa.asp?f=network">
									Network
								</a>
							</li>
							<li>
								<a href="empresa.asp?f=politicas">
									Politicas y Certificaciones
								</a>
							</li>
							<li class="">
								<a href="empresa_clientes.asp">
									Clientes
								</a>
							</li>
							<li>
								<a href="empresa_dist.asp">
									Distribuidores y Agentes
								</a>
							</li>

							<li>
								<a href="empresa.asp?f=partners">
									Partners
								</a>
							</li>
							<li>
								<a href="legales.asp">
									Legales
								</a>
							</li>

						</ul>
					</li>


					<li class="menu-item-has-mega-menu menu-item-has-children">
											<a href="#">Productos</a>
											<div class="megamenu with-products">
												<div class="megamenu-row">

													<div class="col4" data-mh="product-item">
														<div class="product-item">
															<div class="product-item-thumb">

															<img src="img/correo.jpg" alt="product" class="circulo">
															</div>
															<div class="product-item-content">
																<h6 class="title">Env�o Certificados y Con Respaldo Notarial</h6>
																<a href="http://www.enviocertificado.com" target="_blank" class="more-arrow">
																	<span>Ingresar al Sitio</span>
																	<div class="btn-next">
																		<svg class="utouch-icon icon-hover utouch-icon-arrow-right-1"><use xlink:href="#utouch-icon-arrow-right-1"></use></svg>
																		<svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
																	</div>
																</a>
															</div>
														</div>
													</div>

													<div class="col4" data-mh="product-item">
														<div class="product-item">
															<div class="product-item-thumb">

																<img width="250" height="250" src="img/cdv.jpg" alt="product" class="circulo">
															</div>
															<div class="product-item-content">
																<h6 class="title">Centro de Validaciones<br><br></h6>
																<a href="http://www.centrodevalidaciones.com" target="_blank" class="more-arrow">
																	<span>Ingresar al Sitio</span>
																	<div class="btn-next">
																		<svg class="utouch-icon icon-hover utouch-icon-arrow-right-1"><use xlink:href="#utouch-icon-arrow-right-1"></use></svg>
																		<svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
																	</div>
																</a>
															</div>
														</div>
													</div>

													<div class="col4" data-mh="product-item">
														<div class="product-item">
															<div class="product-item-thumb">

																<img width="250" height="250" src="img/crm.jpg" alt="product" class="circulo">
															</div>
															<div class="product-item-content">
																<h6 class="title">Omni CRM - MultiChannel Plataform</h6>
																<a href="http://www.omnicrm.cloud" target="_blank" class="more-arrow">
																	<span>Ingresar al Sitio</span>
																	<div class="btn-next">
																		<svg class="utouch-icon icon-hover utouch-icon-arrow-right-1"><use xlink:href="#utouch-icon-arrow-right-1"></use></svg>
																		<svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
																	</div>
																</a>
															</div>
														</div>
													</div>

													<div class="col4" data-mh="product-item">
														<div class="product-item">
															<div class="product-item-thumb">

																<img width="250" height="250" src="img/smsmoblie.jpg" alt="product" class='circulo'>
															</div>
															<div class="product-item-content">
																<h6 class="title">Omni Portal Web & Mobile</h6><br>
																<a href="http://www.enviocertificado.com" target="_blank" class="more-arrow">
																	<span>Ingresar al Sitio</span>
																	<div class="btn-next">
																		<svg class="utouch-icon icon-hover utouch-icon-arrow-right-1"><use xlink:href="#utouch-icon-arrow-right-1"></use></svg>
																		<svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
																	</div>
																</a>
															</div>
														</div>
													</div>

												</div>
											</div>
					</li>


					<li class="menu-item-has-mega-menu menu-item-has-children">
						<a href="#">Casos</a>

						<div class="megamenu" style="background-image: url('img/Caso_menu.jpg');">
							<div class="megamenu-row">

								<div class="col4">
									<ul>
										<li class="megamenu-item-info">
											<h5 class="megamenu-item-info-title">Gestione</h5>
											<p class="megamenu-item-info-text">Su cartera de clientes.</p>
										</li>
										<li>
											<a href="caso_validaciones.asp"><b>Valide y Normalice</b><br><small>Mantenga su cartera limpia.</small></a>
										</li>
										<li>
											<a href="caso_suscriba.asp"><b>Suscriba y Confirme</b><br><small>Altas y Validaciones de Procesos.</small></a>
										</li>
										<li>
											<a href="caso_enriquezca.asp"><b>Enriquezca sus Bases</b><br><small>DataCleaning y Enriquecimiento.</small></a>
										</li>
										<li>
											<a href="caso_alertas.asp"><b>Alertas Preventivas</b><br><small>Scoring y Alertas Online.</small></a>
										</li>
										<li>
											<a href="caso_portalweb.asp"><b>Portal Web</b><br><small>Disponibilice Informaci�n Web y Mobile.</small></a>
										</li>


									</ul>
								</div>
								<div class="col4">
									<ul>
										<li class="megamenu-item-info">
											<h5 class="megamenu-item-info-title">Comunique</h5>
											<p class="megamenu-item-info-text">R�pido, F�cil y Econ�mico.</p>
										</li>
										<li>
											<a href="caso_ern.asp"><b>Env�os de Email</b><br><small>Simples, Certificados y Con Respaldo Notarial.</small></a>
										</li>
										<li>
											<a href="caso_sms.asp"><b>Env�os de SMSs</b><br><small>Standard y N�meros Corto.</small></a>
										</li>
										<li>
											<a href="caso_whatsapp.asp"><b>Env�os de WhatsApp</b><br><small>Standar y API Oficial WhatsAPP.</small></a>
										</li>
										<li>
											<a href="caso_sociales.asp"><b>Env�os R. Sociales</b><br><small>Twitter, Facebook y Linkedin.</small></a>
										</li>
										<li>
										<a href="caso_casillapostal.asp"><b>Casilla Postal</b><br><small>Push y Mensajes Mobile</small></a>
										</li>
										<li>
											<a href="caso_documentos.asp"><b>Gestor de Documentos</b><br><small>Genere, Hostee y Distribuya Documentos.</small></a>
										</li>

									</ul>
								</div>
								<div class="col4">
									<ul>
										<li class="megamenu-item-info">
											<h5 class="megamenu-item-info-title">Atienda</h5>
											<p class="megamenu-item-info-text">De forma Centralizada y Ordenada.</p>
										</li>
										<li>
											<a href="caso_multichannel.asp"><b>Multichannel Inbound</b><br><small>Todos los canales unificados.</small></a>
										</li>
										<li>
											<a href="caso_omni.asp"><b>CRM Omni</b><br><small>Integraci�n con CRM Omni.</small></a>
										</li>
										<li>
											<a href="caso_bot.asp"><b>Bot de Autorespuestas</b><br><small>Automatice sus respuestas.</small></a>
										</li>
										<li>
											<a href="caso_workflow.asp"><b>Automatizaci�n Workflow</b><br><small>Interconecte sus m�dulos.</small></a>
										</li>
										<li>
											<a href="caso_monitor.asp"><b>Monitor Branding</b><br><small>Detecte clientes insatisfechos.</small></a>
										</li>


									</ul>
								</div>


								<div class="col4">

								</div>

							</div>
						</div>
					</li>




					<li class="">
						<a href="#">Soporte</a>
						<ul class="sub-menu">
							<li>
								<a href="http://help.enviocertificado.com" target="_blank">
									Novedades
								</a>
							</li>
							<li>
								<a href="http://estado.sysworld.com.ar" target="_blank">
									Status
								</a>
							</li>
							<li>
								<a href="http://help.enviocertificado.com" target="_blank">
									Ayuda
								</a>
							</li>
							<li class="">
								<a href="http://cloud.sysworld.com.ar" target="_blank">
									Cargar Ticket
								</a>
							</li>
							<li>
								<a href="http://help.enviocertificado.com" target="_blank">
									Tutoriales
								</a>
							</li>
							<li>
								<a href="legales.asp">
									Legales
								</a>
							</li>
						</ul>
					</li>


					<li class="">
						<a href="contacto.asp">Contacto</a>
					</li>
