<html>
	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<title>EnvioCertificado.com - Envio con Respaldo Notarial.</title>
		<meta name="description" content="Cloudhub is a modern, responsive and easy to customize HTML template, perfectly suited for hosting and technology companies.">
		<meta name="keywords" content="html template, responsive, retina, cloud hosting, technology, startup">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<!-- Icons -->
		<link rel="apple-touch-icon-precomposed" href="img/icons/apple-touch-icon.png">
		<link rel="icon" href="img/icons/favicon.ico">
		<!-- Stylesheets -->
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="css/main.min.css">
	</head>
	<body class="footer-dark">
		<!-- Header -->
		<header id="header" class="header-dynamic header-light header-shadow-scroll">
			<div class="container">
				<a class="logo" href="index.html">
					<img src="img/logos/logo_ern.jpg" alt="">
				</a>
				<nav>
					<ul class="nav-primary">
						<li>
							<a href="index.php">Inicio</a>
						</li>
						<li>
							<a href="empresa.php">Nuestra Empresa</a>
						</li>
						<li>
							<a href="funcionalidades.php">Funcionalidades</a>
						</li>
						<li>
							<a href="faq.php">FAQ</a>
						</li>
						<li>
							<a href="contacto.php">Contacto</a>
						</li>
						<li>
							<a class="button button-secondary" href="panel.php" target="_blank">
								<i class="fa fa-lock icon-left"></i>Acceso al Sistema
							</a>
						</li>
					</ul>
					<ul class="nav-secondary">
						<li>
							<a href="contacto.php"><i class="fa fa-phone icon-left"></i>+54 (011) 5263-2919</a>
						</li>
						<li>
							<a href="contacto.php"><i class="fa fa-comment icon-left"></i>Live Chat</a>
						</li>
						<li>
							<a href="http://help.enviocertificado.com"><i class="fa fa-question-circle icon-left"></i>Tutoriales</a>
						</li>
						<li>
							<a href="http://estado.sysworld.com.ar"><i class="fa fa-check icon-left"></i>Estado de Servicios</a>
						</li>
					</ul>
				</nav>
			</div>
		</header>
		<!-- Notification -->
		<section id="notification" data-dismissible="true" data-title="" data-expires="">
			<div class="container">
				<p>
					Promoción válida por tiempo indeterminado. 30% de descuento en todos los servicios durante el primer año de contratación. <a class="text-margin-left" href="contacto.php">Contratar<i class="fa fa-angle-right icon-right"></i></a>

				</p>
			</div>
		</section>
		<!-- Content -->
		<section id="content">
			<!-- Content Row -->
			<section class="content-row content-row-color content-row-clouds">
				<div class="content-slider animate-container-slide" data-nav="true" data-rotation="5">
					<a class="slide" data-title="EnvioCertificado" href="">
						<div class="container">
							<header class="content-header content-header-large content-header-uppercase">
								<h1>
									<mark>Envío</mark>-Certificado
								</h1>
								<p>
									Certifique sus comunicaciones electrónicas y obtenga respaldo de la BlockChain. <span class="text-color-secondary">Desde $ARS 0,278/Envío</span>
								</p>
							</header>
							<img src="uploads/server-shared.png" alt="">
						</div>
					</a>
					<a class="slide" data-title="ERN" href="">
						<div class="container">
							<header class="content-header content-header-large content-header-uppercase">
								<h1>
									<mark>Envío</mark> con Respaldo Notarial
								</h1>
								<p>
									Obtenga certificaciones con respaldo notarial de sus comunicaciones electrónicas. <span class="text-color-secondary">Desde $ARS 0,692/Envío</span>
								</p>
							</header>
							<img src="uploads/server-virtual.png" alt="">
						</div>
					</a>
					<a class="slide" data-title="TRN" href="">
						<div class="container">
							<header class="content-header content-header-large content-header-uppercase">
								<h1>
									<mark>Transacciones</mark> Certificadas
								</h1>
								<p>
									Certifique sus documentos y transacciones electrónicas mediante la BlockChain + Respaldo Notarial. <span class="text-color-secondary">Desde $ARS 0,177/Tx</span>
								</p>
							</header>
							<img src="uploads/server-dedicated.png" alt="">
						</div>
					</a>
				</div>
			</section>
			<!-- Content Row -->
			<section class="content-row">
				<div class="container">
					<header class="content-header">
						<h2>
							Comunicaciones y Transacciones Certificadas
						</h2>
						<p>
							Gestione sus comunicaciones y transacciones electrónicas de manera segura y certificada.
						</p>
					</header>
					<div class="column-row align-center-bottom text-align-center">
						<div class="column-33">
							<i class="fa fa-rocket icon-feature"></i>
							<h3>
								Envíos Garantizados
							</h3>
							<p>
								Todas nuestros envíos cuentan con garantia de entrega gracias a la reputación de nuestra red y el monitoreo de entrega continuo.
							</p>
							<p>
								<a href="http://www.sysworld.com.ar/v2/ern/funcionalidades.php">Conozca Más<i class="fa fa-angle-right icon-right"></i></a>
							</p>
						</div>
						<div class="column-33">
							<i class="fa fa-cloud icon-feature"></i>
							<h3>
								Alta Disponibilidad
							</h3>
							<p>
								Toda la infraestructura de nuestros servicios está pensada para darle continuidad al negocio y alta disponibilidad de servicios.
							</p>
							<p>
								<a href="http://www.sysworld.com.ar/v2/ern/funcionalidades.php">Conozca Más<i class="fa fa-angle-right icon-right"></i></a>
							</p>
						</div>
						<div class="column-33">
							<i class="fa fa-shield icon-feature"></i>
							<h3>
								Infraestructura Segura
							</h3>
							<p>
								Procesos y auditorias continuas basadas en sistemas de seguridad informática ISO 27001 para garantizar la seguridad de sus transacciones.
							</p>
							<p>
								<a href="http://www.sysworld.com.ar/v2/ern/funcionalidades.php">Conozca Más<i class="fa fa-angle-right icon-right"></i></a>
							</p>
						</div>
					</div>
					<div class="column-row align-center-bottom text-align-center">
						<div class="column-33">
							<i class="fa fa-refresh icon-feature"></i>
							<h3>
								Respuesta Inmediata
							</h3>
							<p>
								Servicios basados en sistemas de procesamiento de estadísticas en tiempo real que garantizan el estado de sus campañas y permiten automatizar procesos inteligentes.
							</p>
							<p>
								<a href="http://www.sysworld.com.ar/v2/ern/funcionalidades.php">Conozca Más<i class="fa fa-angle-right icon-right"></i></a>
							</p>
						</div>
						<div class="column-33">
							<i class="fa fa-support icon-feature"></i>
							<h3>
								Soporte Dedicado
							</h3>
							<p>
								Un equipo especializado en diferentes negocios lo acompañara en la implementación y mantenimiento de los servicios para brindarle valor agregado.
							</p>
							<p>
								<a href="http://www.sysworld.com.ar/v2/ern/contacto.php">Conozca Más<i class="fa fa-angle-right icon-right"></i></a>
							</p>
						</div>
						<div class="column-33">
							<i class="fa fa-gear icon-feature"></i>
							<h3>
								Automatización Poderosa
							</h3>
							<p>
								Poderosos sistemas de automatización e integración mediante sistemas web, interfaces, webservices o APIs para darle una solución a cada necesidad.
							</p>
							<p>
								<a href="http://www.sysworld.com.ar/v2/ern/funcionalidades.php">Conozca Más<i class="fa fa-angle-right icon-right"></i></a>
							</p>
						</div>
					</div>
				</div>
			</section>
			<!-- Content Row -->
			<section class="content-row content-row-gray">
							<div class="container">
								<header class="content-header">
									<h2>
										Nuestros Productos
									</h2>
									<p>
										Un producto pensado para cada necesidad le van a permitir agregarle valor a sus procesos y reducir sus costos aumentando la productividad.
									</p>
								</header>

									<section class="purchase" id="buy-now">
															              <center> <ul>
																					<li>
															                    <a href="http://order.sysworld.com.ar/order/">
															                    <center><div class="fondo"><font size="5" color="white"><i class="material-icons">email</i><br>Envíos Certificado</font><br>
															                    <font size="2" color="white">Comunicaciones certificadas en la BlockChain</font><br></div><br>


															                        <span class="purchase-description"><i class="material-icons">done</i> Remitentes Dedicados</span>
															                        <span class="purchase-description"><i class="material-icons">done</i> Procesamiento de Respuestas</span>
															                        <span class="purchase-description"><i class="material-icons">done</i> Generación de Archivos</span>
															                         <span class="purchase-description"><i class="material-icons">done</i> Detección de estadísticas<br><br></span>
															                        <big class="purchase-price">Desde $0.33<br><br>
															                        <font size="2" color="#a0abbc">Certificación en BlockChain</font><br>
															                        <font size="2" color="#a0abbc">Certificaciones Antispam</font><br>
															                        <font size="2" color="#a0abbc">Soporte Bronce Incluido</font><br><br></big>
															                        <span class="purchase-button"><i class="fa fa-shopping-cart icon-left"></i>Cotizar ahora</span><center>
															                    </a>
															                </li>

															                            <li>
															                    <a href="http://order.sysworld.com.ar/order/">
															                   <center><div class="fondo"><font size="5" color="white"><i class="material-icons">security</i><br>Envíos con Respaldo Notarial</font> <br>
															                   <font size="2" color="white">Notificaciones con validez legal</font><br></div><br>

																					<span class="purchase-description"><i class="material-icons">done</i> Remitentes Dedicados</span>
															                        <span class="purchase-description"><i class="material-icons">done</i> Procesamiento de Respuestas</span>
															                        <span class="purchase-description"><i class="material-icons">done</i> Generación de Archivos</span>
															                        <span class="purchase-description"><i class="material-icons">done</i> Detección de estadísticas<br><br></span>
															                        <big class="purchase-price">Desde $0.68<br><br>
															                              <font size="2" color="#a0abbc">Powered by EnvioCertificado.com</font><br>
																						  <font size="2" color="#a0abbc">Certificaciones Antispam</font><br>
															                              <font size="2" color="#a0abbc">Soporte Bronce Incluido</font><br><br></big>
															                      <span class="purchase-button"><i class="fa fa-shopping-cart icon-left"></i>Cotizar ahora</span><center>
															                    </a>
															                </li>

															                            <li>
															                    <a href="http://order.sysworld.com.ar/order/">
															                     <center> <div class="fondo"><font size="5" color="white"> <i class="material-icons">verified_user</i><br> Transacciones Certificadas </font>   <br>
															                      <font size="2" color="white">Certifique sus transacciones y documentos</font><br></div><br>

															                        <span class="purchase-description"><i class="material-icons">done</i> Respaldo Notarial Incluido</span>
															                        <span class="purchase-description"><i class="material-icons">done</i> Certificación BlockChain</span>
															                        <span class="purchase-description"><i class="material-icons">done</i> Hosting de Archivos</span>
															                        <span class="purchase-description"><i class="material-icons">done</i> DN High Availability<br><br></span>
															                        <big class="purchase-price">Desde $0.21<br><br>
															                         <font size="2" color="#a0abbc">Certificación en BlockChain</font><br>
																					<font size="2" color="#a0abbc">Certificaciones Acceso API</font><br>
															                        <font size="2" color="#a0abbc">Soporte Bronce Incluido</font><br><br></big>
															                        <span class="purchase-button"><i class="fa fa-shopping-cart icon-left"></i>Cotizar ahora</span><center>
															                    </a>
															                	</li>
															                    </ul></center>


												<style>

												.fondo{
												background:#0594F7;
												}


													.purchase-trigger {
													border:1px solid #0594F7;
													border-radius:50px;
													font-size:26px;
													display:inline-block;
													cursor:pointer;
													position:relative;
													border-color:#0594F7;

												}

												.purchase-trigger * {
													-webkit-transition: 0.3s cubic-bezier(0.540, 1.410, 0.540, 0.870);
													-moz-transition: 0.3s cubic-bezier(0.540, 1.410, 0.540, 0.870);
													-o-transition: 0.3s cubic-bezier(0.540, 1.410, 0.540, 0.870);
													transition: 0.3s cubic-bezier(0.540, 1.410, 0.540, 0.870);

													-webkit-transition: 0.4s cubic-bezier(0.165, 0.925, 0.510, 1.005);
													-moz-transition: 0.4s cubic-bezier(0.165, 0.925, 0.510, 1.005);
													-o-transition: 0.4s cubic-bezier(0.165, 0.925, 0.510, 1.005);
													transition: 0.4s cubic-bezier(0.165, 0.925, 0.510, 1.005);

													-webkit-touch-callout: none;
													-webkit-user-select: none;
													-khtml-user-select: none;
													-moz-user-select: none;
													-ms-user-select: none;
													user-select: none;
												}
												.purchase-trigger span {
													padding:17px 80px;
													color:#E85700;
													width:246px;
													display: inline-block;


												}
												.purchase-trigger .bubble {
													position:absolute;
													background:#0594F7;
													top:0;
													border-radius:50px;
													left:auto;
													height:74px;
													width: 246px;
													z-index:-1;
													border-color:#0594F7;

												}
												.purchase-trigger.right .bubble {
													margin-left:246px;
												}

												.purchase-trigger:not(.right) .annual {
													color:#fff;
												}
												.purchase-trigger.right .monthly {
													color:#fff;
												}

												.purchase ul {
													display:table;
													margin:80px 0 80px;
													width:100%;
													max-width:1100px;

												}
												.purchase li {
													display:table-cell;
													width:33.333%;
													padding:30px 0 10px;
													-webkit-transition: 0;
													-webkit-transition: all 0.25s, color 0s !important;
													-moz-transition: all 0.25s, color 0s !important;
													-o-transition: all 0.25s, color 0s !important;
													transition: all 0.25s, color 0s !important;
													position:relative;
													z-index:0;
													border:1px solid #BBB;
													background:white;
													border-color:#0594F7;
													border-radius:15px;
												  	border-width:2px;

												}

												.purchase li:first-child {
													position:relative;
													right: 8px;
												}
												.purchase li:last-child {
													position:relative;
													left: 8px;
												}


												.purchase li:hover {
													box-shadow:13px 13px 30px rgba(0, 0, 0, 0.2);
													z-index:1;
													color:#fff;
													background:#0594F7;
													border-color:#0594F7;
													-webkit-transform:scale(1.05);
												}

												.purchase li * {
													-webkit-transition: 0 !important;
													-moz-transition: 0 !important;
													-o-transition: 0 !important;
													transition: 0 !important;
												}

												.purchase li:hover * {
													color:#fff !important;
													border-color:rgba(255, 255, 255, 0.27) !important;
												}



												.purchase li strong {
													font-size:19px;
													text-transform:uppercase;
													font-family: AvenirLTStd-Heavy;
													color:#0594F7;
													letter-spacing:2.4px;
													line-height:45px;
													font-weight:400;
													margin-bottom: 25px;
													display: inline-block;
												}

												.purchase ul .purchase-description {
													display:block;
													font-size:19px;
													line-height:30px;
												}

												.purchase .purchase-price {
													font-size:40px;
													letter-spacing:2px;
													padding-top:20px;
													display:block;
													font-weight:400;
													padding-bottom:30px;
													color:#0594F7;
												}
												.purchase .purchase-button {
													text-transform:uppercase;
													font-size:15px;
													text-transform:uppercase;
													font-family:arial;
													color:#0594F7;
													text-decoration:none;
													line-height:1;
													padding:28px 0 24px;
													border-top:1px solid #bbb;
													width:100%;
													display:inline-block;
													margin-top:6px;
													display:block;
												}
												.purchase li:hover .purchase-button {

													-webkit-transition: 0;
													-moz-transition: 0;
													-o-transition: 0;
													transition: 0;
												}


												.purchase ul a:hover {
													color:#0594F7;
												}

											.purchase ul a, .purchase ul a:hover .purchase-price, .purchase ul a:hover .purchase-description {
											color: #6C6C6C;
											  text-decoration:none;
											}
											designmodo.com/media="all"
											.purchase li * {
											-webkit-transition: 0 !important;
											-moz-transition: 0 !important;
											-o-transition: 0 !important;
											transition: 0 !important;
			}

							</style>
			<br>


</div>
</section>
			<!-- Content Row -->
			<section class="content-row">
				<div class="container">
					<header class="content-header">
						<h2>
							Red Distribuida de Contenidos y Respaldo.
						</h2>
						<p>
							Infraestructura globalmente distribuida para garantizar la continuidad de los servicios y respaldar las certificaciones de nuestros servicios.
						</p>
					</header>
					<div class="network-map">
						<ul>
							<li style="top: 42%; left: 12.5%;">
							<span class="label-top-left">San Francisco</span>
							</li>
							<li style="top: 44%; left: 20%;">
							<span class="label-bottom-right">Dallas</span>
							</li>
							<li style="top: 40%; left: 26%;">
							<span class="label-top-right">Nueva York</span>
							</li>
							<li style="top: 86%; left: 31%;">
								<span class="label-bottom-right">Buenos Aires</span>
							</li>
							<li style="top: 41%; left: 44.5%;">
							<span class="label-bottom-left">Lisboa</span>
							</li>
							<li style="top: 32%; left: 46.5%;">
							<span class="label-top-left">Londres</span>
							</li>
							<li style="top: 34%; left: 49.5%;">
							<span class="label-bottom-right">Frankfurt</span>
							</li>
							<li style="top: 64%; left: 75%;">
							<span class="label-bottom-left">Singapore</span>
							</li>
							<li style="top: 44%; left: 85%;">
							<span class="label-bottom-right">Tokyo</span>
							</li>
							<li style="top: 83%; left: 89%;">
							<span class="label-top-right">Sydney</span>
							</li>
						</ul>
						<img src="img/layout/map-dark.svg" alt="">
					</div>
				</div>
			</section>
			<!-- Content Row -->
			<section class="content-row content-row-gray">
				<div class="container">
					<header class="content-header">
						<h2>
							Nuestros Clientes
						</h2>
						<p>
							Más de 200 Empresas de diferentes rubros nos confian sus envíos y transacciones.
						</p>
					</header>
					<div class="column-row align-center-bottom">
						<div class="column-33">
							<div class="testimonial">
								<p class="testimonial-content zoom">
									Se implemento un workflow con validaciones, generación de archivos PDFs y envíos certificados para todos los procesos del plan de ahorro.
								</p>
								<p class="testimonial-author">
									Autoahorro VW<br>
									<small>&nbsp;&nbsp;Envío Certificado</small><br>
							</div>
						</div>
						<div class="column-33">
							<div class="testimonial">
								<p class="testimonial-content zoom">
									Se implemento el workflow de certificaciones notariales para los procesos de ordenes de compras de proveedores.
								</p>
								<p class="testimonial-author">
									Banco Frances<br>
									<small>Envío con Respaldo Notarial</small>
								</p>
							</div>
						</div>
						<div class="column-33">
							<div class="testimonial">
								<p class="testimonial-content zoom">
									Se implemento un proceso de notificaciones electrónicas con acuse de recibo y respaldo notarial para las cartas de arribos del negocio.
								</p>
								<p class="testimonial-author">
									Fiat Plan<br>
									<small>&nbsp;&nbsp;&nbsp;ERN + TRN</small><br>
								</p>
							</div>
						</div>
					</div>
				</div>

				<style type="text/css">
				    .zoom{
				        /* Aumentamos la anchura y altura durante 2 segundos */
				        transition: width 1s, height 1s, transform 1s;
				        -moz-transition: width 1s, height 1s, -moz-transform 1s;
				        -webkit-transition: width 1s, height 1s, -webkit-transform 1s;
				        -o-transition: width 1s, height 1s,-o-transform 1s;
				    }
				    .zoom:hover{
				        /* tranformamos el elemento al pasar el mouse por encima al doble de
				           su tamaño con scale(1.2). */
				        transform : scale(1.2);
				        -moz-transform : scale(1.2);      /* Firefox */
				        -webkit-transform : scale(1.2);   /* Chrome - Safari */
				        -o-transform : scale(1.2);        /* Opera */
				    }
					</style>

				</section>
			<!-- Content Row -->
			<section class="content-row content-row-color">
				<div class="container">
					<header class="content-header">
						<h2>
							¿Interesado en nuestra empresa?
						</h2>
						<p>
							Acceda al sitio web corporativo para conocer más detalles de nuestra empresa.<br><br>
							<a class="button button-secondary" href="http://www.sysworld.com.ar" target="_blank">
								<i class="fa fa-globe icon-left"></i>Sitio Web Coorporativo
							</a>
						</p>
					</header>
				</div>
			</section>
		</section>
		<!-- Footer -->
		<footer id="footer">
			<section class="footer-primary">
				<div class="container">
					<div class="column-row">
						<div class="column-33">
							<h5>
								Sysworld Servicios S.A.
							</h5>
							<p>
								EnvioCertificado.com | EnvioLegal.com | CertiSend.com |Envio de Email Certificados | Envios con Respaldo Notarial | Certificaciones en la BlockChain | SMS Certificados | Transacciones Certificadas | Auditoria de Transacciones
							</p>
						</div>
						<div class="column-66">
							<div class="column-row align-right-top">
								<div class="column-25">
									<h5>
										Conectarse
									</h5>
									<ul class="list-style-icon">
										<li>
											<a href="https://www.facebook.com/SysWorld-Servicios-SA-323344231089747" target="_blank"><i class="fa fa-facebook"></i>Facebook</a>
										</li>
										<li>
											<a href="https://twitter.com/Sysworldsa" target="_blank"><i class="fa fa-twitter"></i>Twitter</a>
										</li>
										<li>
											<a href="contacto.php"><i class="fa fa-github"></i>Contacto</a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar/v2/sw/contacto.php" target="_blank"><i class="fa fa-xing"></i>Live Chat</a>
										</li>
									</ul>
								</div>
								<div class="column-25">
									<h5>
										Productos
									</h5>
									<ul>
										<li>
											<a href="http://sysworld.com.ar/v2/sw/index.php">EnvioCertificado</a>
										</li>
										<li>
											<a href="">ERN</a>
										</li>
										<li>
											<a href="">TRN</a>
										</li>
										<li>
											<a href="http://www.centrodevalidaciones.com" target="_blank">Validadores</a>
										</li>
										<li>
											<a href="http://www.omnicrm.cloud" target="_blank">OmniCRM.cloud</a>
										</li>
										<li>
											<a href="http://cloud.sysworld.com.ar/" target="_blank">Portal</a>
										</li>
									</ul>
								</div>
								<div class="column-25">
									<h5>
										Recursos
									</h5>
									<ul>
										<li>
											<a href="funcionalidades.php">Funcionalidades <? echo date("Y"); ?> </a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar/v2/sw/empresa.php?f=network" target="_blank">Network</a>
										</li>
										<li>
											<a href="http://sysworld.com.ar/v2/sw/empresa.php?f=politicas">Respaldo Legales</a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar/v2/sw/empresa.php?f=partners" target="_blank">Partners</a>
										</li>
									</ul>
								</div>
								<div class="column-flex">
									<h5>
										Empresa
									</h5>
									<ul>
										<li>
											<a href="empresa.php">Nosotros</a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar" target="_blank">Sitio Corporativo</a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar/v2/sw/empresa_dist.php" target="_blank">Distribuidores</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="footer-secondary">
				<div class="container">
					<p>
						Copyright 2006 -<?php echo date("Y"); ?> &copy; Sysworld Servicios S.A. Todos los derechos reservados.<br>
						Powered by <a href="http://www.sysworld.com.ar">Sysworld Servicios S.A.</a>
					</p>
				</div>
			</section>
		</footer>
		<!-- Scripts -->
		<script src="js/jquery.min.js"></script>
		<script src="js/headroom.min.js"></script>
		<script src="js/js.cookie.min.js"></script>
		<script src="js/imagesloaded.min.js"></script>
		<script src="js/bricks.min.js"></script>
		<script src="js/main.min.js"></script>

		<script type="text/javascript">
		var LHCChatOptions = {};
		LHCChatOptions.opt = {widget_height:340,widget_width:300,popup_height:520,popup_width:500};
		(function() {
		var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
		var referrer = (document.referrer) ? encodeURIComponent(document.referrer.substr(document.referrer.indexOf('://')+1)) : '';
		var location  = (document.location) ? encodeURIComponent(window.location.href.substring(window.location.protocol.length)) : '';
		po.src = '//livechat.enviocertificado.com/panel/index.php/esp/chat/getstatus/(click)/internal/(position)/bottom_right/(ma)/br/(top)/350/(units)/pixels/(leaveamessage)/true/(department)/2/5/(theme)/4?r='+referrer+'&l='+location;
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
		})();
</script>


	</body>
</html>

