﻿<!-- get_header('Page Name','Title')-->
<!doctype html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>Política de Privacidad</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<link href="https://fonts.googleapis.com/css?family=Karla:400,700,700i%7CRubik:300,400,500,700" rel="stylesheet">

         <link rel="icon" type="image/png" href="favicon.ico">
		<link rel="icon" type="image/png" href="Casilla.ico">
		<!-- Place favicon.ico in the root directory -->
		<link rel="apple-touch-icon" href="apple-touch-icon.png">
		<link rel="stylesheet" href="assets/css/font-awesome.min.css">

		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/css/animate.css">
		<link rel="stylesheet" href="assets/css/iconfont.css">
		<link rel="stylesheet" href="assets/css/magnific-popup.css">
		<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
		<link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
		<link rel="stylesheet" href="assets/css/jquery-ui.structure.min.css">
		<link rel="stylesheet" href="assets/css/jquery-ui.theme.min.css">
		

		<!--For Plugins external css-->
		<link rel="stylesheet" href="assets/css/plugins.css" />

		<!--Theme custom css -->
		<link rel="stylesheet" href="assets/css/style.css">

		<!--Theme Responsive css-->
		<link rel="stylesheet" href="assets/css/responsive.css" />
	</head>
	<body>
	<!--[if lt IE 10]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->

	<!-- prelaoder -->
	<div id="preloader">
    <div class="preloader-wrapper">
        <div class="spinner"></div>
    </div>
    <div class="preloader-cancel-btn">
        <a href="#" class="btn btn-secondary prelaoder-btn">Cancel Preloader</a>
    </div>
</div>	<!-- END prelaoder -->
<div class="header-transparent">
    <!-- topBar section -->
    <div class="xs-top-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <ul class="xs-top-bar-info">
                    <li>
                        <p><i class="icon icon-phone3"></i>+54 (11)5277-8900

</p>
                    </li>
                    <li>
                        <a href="mailto:info@sysworld.com.ar"><i class="icon icon-envelope4"></i>info@sysworld.com.ar</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-6">
                <ul class="top-menu">
                    <li><a href="login.html"><i class="icon icon-key2"></i> Login</a></li>
                    <!-- <li><a href="signup.html">Sign Up</a></li> -->
                    <li><a href="support.html"><i class="icon icon-hours-support"></i> Support</a></li>
                </ul>
            </div>
        </div><!-- .row END -->
    </div><!-- .container END -->
</div>    <!-- End topBar section -->
<header class="xs-header header-v3">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="xs-logo-wraper">
                        <a href="index.html" class="xs-logo">
                            <img src="assets/images/logo.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <nav class="xs-menus">
                        <div class="nav-header">
                            <a class="nav-brand" href="index.html">
                                <img src="assets/images/logo.png" alt="">
                            </a>
                            <div class="nav-toggle"></div>
                        </div>
                        <div class="nav-menus-wrapper">
                            <!--#include file="menu.asp"-->
                        </div>
                    </nav>
                </div>
                <div class="col-lg-2">
                    <ul class="xs-menu-tools">
                        <li>
                            <a href="#modal-popup-1" class="languageSwitcher-button xs-modal-popup"><i class="icon icon-internet"></i></a>
                        </li>
                        <li>
                            <a href="#" class="offset-side-bar"><i class="icon icon-cart2"></i><span class="item-count">2</span></a>
                        </li>
                        <li>
                            <a href="#modal-popup-2" class="navsearch-button xs-modal-popup"><i class="icon icon-search"></i></a>
                        </li>
                        <li>
                            <a href="#" class="navSidebar-button"><i class="icon icon-burger-menu"></i></a>
                        </li>
                    </ul>
                </div>
            </div><!-- .row END -->
        </div><!-- .container END -->
    </header>    <!-- End header section -->
    </div>
    
    
</div>

<!-- banner section -->
<section class="xs-banner inner-banner contet-to-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 align-self-center">
                <div class="xs-banner-content">
                    <h1 class="banner-sub-title wow fadeInLeft">Sysworld Servicios S.A.</h1>
                    <h2 class="banner-title wow fadeInLeft" data-wow-duration="1.5s">Legales</h2>
                    <ul class="breadcrumbs list-inline wow fadeInLeft" data-wow-duration="2s">
                    </ul>
                </div><!-- .xs-banner-content END -->
            </div>
            <div class="col-lg-6 align-self-end">
                <div class="inner-welcome-image-group">
                    <img src="assets/images/innerWelcome/blog.png" alt="hosting image">
                    <img src="assets/images/innerWelcome/info.png" class="banner-ico banner-ico-1" alt="phone icon">
                    <img src="assets/images/innerWelcome/board.png" class="banner-ico banner-ico-2" alt="phone icon">
                </div>
            </div>
        </div><!-- .row END -->
    </div><!-- .container END -->
</section><!-- End banner section -->

<!-- privacy strart -->
<section class="xs-section-padding">
    <div class="container">    
        <div class="row">
            <div class="col-md-12">
                <div class="terms-wraper wow fadeInUp">
                    <center><h4 class="xs-title">Aviso Legal</h4></center>
                    <ol class="order-list">
                        <li>Sysworld Servicios S.A no puede asumir ninguna responsabilidad derivada del uso incorrecto, inapropiado o ilícito de la información aparecida en sus sistemas informáticos, resultados y/o sitios web.</li>
                        <li>Dentro de los límites establecidos en la ley, Sysworld Servicios S.A no asume ninguna responsabilidad derivada de la falta de integridad, actualización y precisión de los datos o informaciones contenidas en sus sistemas informáticos, resultados y/o sitios web.</li>
                        <li>Los sistemas, resultados y/o sitios web de Sysworld Servicios S.A, contienen enlaces a otros sistemas de terceras partes que Sysworld Servicios S.A no puede controlar. Por lo tanto, no puede asumir responsabilidades por el contenido que pueda aparecer los sistemas de terceras partes.</li>
                        <li>Los textos, imágenes, animaciones, software y el resto de contenidos incluidos en este sitio web son propiedad exclusiva de Sysworld Servicios S.A o sus asociados. Cualquier acto de transmisión, distribución, cesión, reproducción, almacenamiento o comunicación pública total o parcial, debe contar con el consentimiento expreso de Sysworld Servicios S.A </li>
                        <li>El presente aviso legal se rige por las leyes vigentes en la República Argentina, quedando sometida a la jurisdicción de los Tribunales Nacionales de la Ciudad Autónoma de Buenos Aires cualquier disputa, reclamación o controversia sobre el mismo</li>
                    </ol><!-- .order-list END -->
                </div><!-- .terms-wraper END -->
                <div class="terms-wraper wow fadeInUp">
                    <center><h4 class="xs-title">Políticas de Cookies</h4></center>
                    <ol class="order-list">Sysworld Servicios S.A
                        <li>Este sitio web, al igual que la mayoría de los sitios en Internet, usa Cookies para mejorar y optimizar la experiencia del usuario. A continuación encontrarás información detallada sobre qué son las “Cookies”, qué tipología utiliza este sitio web, cómo puedes desactivarlas en tu navegador y cómo bloquear específicamente la instalación de Cookies de terceros. </li>
                        <li>¿QUÉ SON LAS COOKIES Y CÓMO LAS UTILIZAN LOS SITIOS WEB DE SYSWORLD SERVICIOS S.A?<br> 
                            Las Cookies son archivos que el sitio web o la aplicación que utilizas instala en tu navegador o en tu dispositivo (Smartphone, tableta o televisión conectada) durante tu recorrido por las páginas o por la aplicación, y sirven para almacenar información sobre tu visita. Como la mayoría de los sitios en internet, los portales web de Sysworld Servicios S.A SAS utilizan Cookies para: 
                            Asegurar que las páginas web pueden funcionar correctamente 
                            Almacenar tus preferencias, como el idioma que has seleccionado o el tamaño de letra. 
                            Conocer tu experiencia de navegación.<br>
                            Recopilar información estadística anónima, como qué páginas has visto o cuánto tiempo has estado en nuestros medios. 
                            El uso de Cookies nos permite optimizar tu navegación, adaptando la información y los servicios ofrecidos a tus intereses, para proporcionarte una mejor experiencia siempre que nos visites; para ello, la información que recabamos a través de las Cookies es analizada tanto por el responsable del Sitio Web como por Comeresa Prensa, S.L.U., que es la sociedad del grupo Sysworld Servicios S.A -grupo al que también pertenece el Sitio Web- encargada de gestionar servicios comunes a todas las demás sociedades. Los sitios web de Sysworld Servicios S.A utilizan Cookies para funcionar, adaptar y facilitar al máximo la navegación del Usuario. 
                        </li>
                        <li>Las Cookies se asocian únicamente a un usuario anónimo y su ordenador/dispositivo y no proporcionan referencias que permitan conocer datos personales. En todo momento podrás acceder a la configuración de tu navegador para modificar y/o bloquear la instalación de las Cookies enviadas por los sitios web de Sysworld Servicios S.A, sin que ello impida al acceso a los contenidos, salvo por lo que respecta a las cookies que son imprescindibles para la prestación del servicio, sin las cuales la web no puede funcionar. Ten en cuenta en cualquier caso que, si modificas y/o bloqueas la instalación de las Cookies, la calidad del funcionamiento de los Servicios o su disponibilidad pueden verse afectadas.  </li>
                        <li>Los Usuarios que completen el proceso de registro o hayan iniciado sesión con sus datos de acceso podrán acceder a servicios personalizados y adaptados a sus preferencias según la información personal suministrada en el momento del registro y la almacenada en la Cookie de su navegador.</li>
                        <li>Las herramientas de email-marketing de Sysworld Servicios S.A utilizan pequeñas imágenes invisibles para los usuarios que son incluidas en los emails. Esta tecnología nos permite saber si un email se ha leído o no, en qué fecha, la dirección IP desde la que ha sido consultado, etc. Con esta información, realizamos estudios estadísticos y analíticos sobre el envío y recepción de los emails para mejorar la oferta de los servicios a los que el usuario está suscrito y ofrecerle información que pueda ser de su interés. </li>
                        <li>¿POR QUÉ SON IMPORTANTES?<br>
                            Desde un punto de vista técnico, permiten que los sitios web funcionen de forma más ágil y adaptada a las preferencias de los usuarios, como por ejemplo almacenar el idioma, la moneda del país o detectar el dispositivo de acceso. 
                            Establecen niveles de protección y seguridad que Impiden o dificultan ciberataques contra el sitio web o sus usuarios. 
                            Permiten que los gestores de los medios puedan conocer datos estadísticos recopilados en las Cookies para mejorar la calidad y experiencia de sus servicios. 
                            Sirven para optimizar la publicidad que mostramos a los usuarios, ofreciendo la que más se ajusta a sus intereses. </li>
                        <li>¿Cuáles son los diferentes tipos de Cookies que utilizamos en Sysworld Servicios S.A ?<br> 
                            Las de sesión expiran cuando el Usuario abandona la página o cierra el navegador, es decir, están activas mientras dura la visita al sitio web y por tanto son borradas de nuestro ordenador al abandonarlo. </li>
                        <li>Las permanentes expiran cuando se cumple el objetivo para el que sirven o bien cuando se borran manualmente, tienen fecha de borrado y se utilizan normalmente en proceso de compra online, personalizaciones o en el registro, para no tener que introducir nuestra contraseña constantemente. 
                            Por otro lado, según quien sea la entidad que gestione el equipo o dominio desde donde se envían las cookies y trate los datos que se obtengan, podemos distinguir entre Cookies propias y de terceros. </li>
                        <li>Las Cookies propias son aquellas Cookies que son enviadas a tu ordenador y gestionadas exclusivamente por nosotros para el mejor funcionamiento del Sitio Web. La información que recabamos se emplea para mejorar la calidad de nuestro servicio y tu experiencia como usuario. </li>
                        <li>Si interactúas con el contenido de nuestro Sitio Web también pueden establecerse cookies de terceros (por ejemplo, al pulsar botones de redes sociales o visionar vídeos alojados en otro sitio web), que son aquellas establecidas por un dominio diferente de nuestro Sitio Web. No podemos acceder a los datos almacenados en las cookies de otros sitios web cuando navegues en los citados sitios web. 
                            Algunos servicios de Sysworld Servicios S.A pueden utilizar conectores con diversas redes sociales: Facebook, Twitter, Google+, Linkedin, etc. Al utilizar el registro social, autorizas a la red social a almacenar una Cookie persistente. Esta Cookie recuerda tu identificación en el servicio, haciendo mucho más rápido el acceso en siguientes visitas. Esta Cookie puede ser eliminada, y además, puedes anular los permisos de acceso de los servicios de Sysworld Servicios S.A desde la configuración de privacidad de la red social concreta. </li>
                        <li>¿CÓMO PUEDO CONFIGURAR MIS COOKIES?<br>
                            Al navegar y continuar en nuestro Sitio Web estará consintiendo el uso de las Cookies en las condiciones contenidas en la presente Política de Cookies. Sysworld Servicios S.A proporciona acceso a esta Política de Cookies en el momento del registro con el objetivo de que el usuario esté informado, y sin perjuicio de que éste pueda ejercer su derecho a bloquear, eliminar y rechazar el uso de Cookies en todo momento. 
                            En cualquier caso le informamos de que, dado que las Cookies no son necesarias para el uso de nuestro Sitio Web, puede bloquearlas o deshabilitarlas activando la configuración de su navegador, que le permite rechazar la instalación de todas las cookies o de algunas de ellas. La práctica mayoría de los navegadores permiten advertir de la presencia de Cookies o rechazarlas automáticamente. Si las rechaza podrá seguir usando nuestro Sitio Web, aunque el uso de algunos de sus servicios podrá ser limitado y por tanto su experiencia en nuestro Sitio Web menos satisfactoria. 
                        </li>
                        <li>¿ACTUALIZAMOS NUESTRA POLÍTICA DE COOKIES?<br> 
                            Es posible que actualicemos la Política de Cookies de nuestro Sitio Web, por ello le recomendamos revisar esta política cada vez que acceda a nuestro Sitio Web con el objetivo de estar adecuadamente informado sobre cómo y para qué usamos las cookies. 
                            La Política de Cookies se actualizó por última vez a fecha 25/05/2018. 
                        </li>
                        <li>¿QUÉ FINALIDADES TIENEN LAS COOKIES QUE UTILIZAMOS PARA PERSONALIZAR TU NAVEGACIÓN EN CUANTO A CONTENIDOS, SERVICIOS Y PUBLICIDAD?<br>
                            Acceso y almacenamiento de datos: Tener acceso a información no sensible de su dispositivo y poder almacenarla, tales como identificadores de publicidad u otros identificadores del dispositivo, incluyendo cookies o tecnologías similares. 
                            Personalización: Conocer los contenidos que te interesan y procesarlo para poder personalizar tu experiencia con nosotros y buscar campañas de publicidad o servicios que puedan estar basadas en lo que te interesa. 
                            Publicidad: Segmentación, resultados y operativa: Registrar tus interacciones con la publicidad con el fin de poder informar de los resultados a los anunciantes y optimizar las campañas en el futuro. Esto incluye poder registrar las campañas que ha visto, con cuales ha interactuado (por ejemplo con un click o terminando en una compra) 
                            Contenidos: Segmentación, resultados y optimización: Registrar tus interacciones con el contenido con el fin de poder hacer un seguimiento de las propuestas de contenidos o servicios que te hacemos y las mejoras de producto. 
                            Medición: Recoger la información del uso de contenido y la combinación con la información previa con la finalidad exclusiva de medir y analizar el consumo de contenidos y servicios.
                         </li>
                    </ol><!-- .order-list END -->
                </div><!-- .terms-wraper END -->
                <div class="terms-wraper wow fadeInUp">
                   <center> <h4 class="xs-title">Política de Privacidad</h4></center>
                    <ol class="order-list">
                        <li>La presente política, que alcanza al directorio y empleados de Sysworld Servicios S.A, tiene por objeto la protección integral de los datos personales asentados en nuestra Base de Datos, sean éstos públicos, o privados destinados a dar informes, para garantizar el derecho al honor y a la intimidad de las personas, así como también el acceso a la información que sobre las mismas tengamos registrada. 
                            Los datos aportados a Sysworld Servicios S.A por las personas físicas o jurídicas, según corresponda, cuentan con su consentimiento para que sean compartidos con terceros específicos, para que puedan ponerse en contacto con Ud., o para que puedan consultar su situación económica o patrimonial. Usted reconoce que la información provista a Sysworld Servicios S.A es verdadera y válida. </li>
                        <li>DEFINICIONES<br>
                            A los fines de la ley 25.326 se entiende por:<br>
                            <b>Datos personales:</b> Información de cualquier tipo referida a personas físicas o de existencia ideal determinadas o determinables.<br>
                            <b>Datos sensibles:</b> Datos personales que revelan origen racial y étnico, opiniones políticas, convicciones religiosas, filosóficas o morales, afiliación sindical e información referente a la salud o a la vida sexual.<br>
                            <b>Archivo, registro, base o banco de datos:</b> Conjunto organizado de datos personales que sean objeto de tratamiento o procesamiento, electrónico o no, cualquiera que fuere la modalidad de su formación, almacenamiento, organización o acceso.<br>
                            <b>Tratamiento de datos:</b> Operaciones y procedimientos sistemáticos, electrónicos o no, que permitan la recolección, conservación, ordenación, almacenamiento, modificación, relacionamiento, evaluación, bloqueo, destrucción, y en general el procesamiento de datos personales, así como también su cesión a terceros a través de comunicaciones, consultas, interconexiones o transferencias.<br>
                            <b>Responsable de archivo, registro, base o banco de datos:</b> Sysworld Servicios S.A<br>
                            <b>Datos informatizados:</b> Datos personales sometidos a tratamiento o procesamiento electrónico o automatizado.<br>
                            <b>Titular de los datos:</b>Toda persona física o persona de existencia ideal con domicilio legal o delegaciones o sucursales en el país, cuyos datos sean objeto del tratamiento al que se refiere la presente ley.<br>
                            <b>Usuario de datos: </b> Toda persona, pública o privada que realice a su arbitrio el tratamiento de datos, ya sea en archivos, registros o bancos de datos propios o a través de conexión con los mismos.<br>
                            <b>Disociación de datos:</b> Todo tratamiento de datos personales de manera que la información obtenida no pueda asociarse a persona determinada o determinable.  
                        </li>
                        <li>
                         PRINCIPIOS DE TRATAMIENTO DE DATOS PERSONALES<br>
                         Calidad: este principio requiere que los datos recabados sean: adecuados, pertinentes y no excesivos en relación al ámbito y finalidad 
                         para la que fueron recogidos; que la recolección no pueda hacerse por medios desleales, fraudulentos o en forma contraria a las disposiciones 
                         de la ley; que no puedan utilizarse los datos para finalidades distintas o incompatibles con las que motivaron su obtención; que los datos 
                         sean ciertos y que puedan actualizarse; que se almacenen de modo que el titular pueda ejercer el derecho de acceso; que sean destruidos cuando
                          hayan dejado de ser necesarios o pertinentes para los fines para los cuales fueron recolectados. La Empresa cuenta con procedimientos internos
                           los cuales detallan la rutina de destrucción diaria de los datos que perdieron vigencia, de acuerdo a lo dictaminado por el art. 26 de la Ley 25.326. 
                        </li>
                        <li><b>Consentimiento:</b> 
                         En la prestación de servicios de información crediticia sólo pueden tratarse datos personales de carácter
                         patrimonial relativos a la solvencia económica y al crédito, obtenidos de fuentes accesibles al público o 
                         procedentes de informaciones facilitadas por el interesado con su consentimiento. Pueden tratarse igualmente
                         datos personales relativos al cumplimiento o incumplimiento de obligaciones de contenido patrimonial, facilitados
                         por el acreedor o por quien actúe por su cuenta o interés. A solicitud del titular de los datos, el responsable o usuario
                         del banco de datos, le comunicará las informaciones, evaluaciones y apreciaciones que sobre el mismo hayan sido comunicadas
                         durante los últimos seis meses y el nombre y domicilio del cesionario en el supuesto de tratarse de datos obtenidos por
                         cesión. Sólo se podrán archivar, registrar o ceder los datos personales que sean significativos para evaluar la solvencia
                         económico-financiera de los afectados durante los últimos cinco años. Dicho plazo se reducirá a dos años cuando el deudor
                         cancele o de otro modo extinga la obligación, debiéndose hacer constar dicho hecho. La prestación de servicios de información
                         crediticia no requerirá el previo consentimiento del titular de los datos a los efectos de su cesión, ni la ulterior comunicación
                         de ésta, cuando estén relacionados con el giro de las actividades comerciales o crediticias de los cesionarios. En el caso de que
                         exista una prestación de servicios deberá suscribirse un contrato y que en el mismo se incluirán los artículos 9, 10 y 25 de la Ley 25.326. 
                        </li>
                        <li>TRATAMIENTOS BÁSICOS REGULADOS<br>
                            Información a proporcionar al titular de los datos: el ciudadano tiene derecho a estar informado 
                            por completo acerca de los usos que se darán a sus datos personales, razón por la cual el responsable
                            o usuario de la base de datos deberá informarle en forma expresa y clara acerca de la existencia del archivo,
                            nombre del responsable y su domicilio; la finalidad de la base de datos y sus destinatarios; el carácter obligatorio
                            u optativo de responder al cuestionario que se le proponga; las consecuencias de brindar datos, su negativa a darlos o
                            la inexactitud de los mismos; la posibilidad de ejercer los derechos de acceso, rectificación o supresión y, en caso
                            de preverse cesiones de los datos, a quién y con qué fin se cederán los mismos. 
                            En todos los casos de cesión de datos personales, el cesionario quedará sujeto a las mismas obligaciones legales y 
                            reglamentarias del cedente y éste responderá solidaria y conjuntamente por la observancia de las mismas ante el organismo
                            de control y el titular de los datos de que se trate, aunque podrá ser eximido total o parcialmente de responsabilidad si
                            demuestra que no se le puede imputar el hecho que ha producido el daño. 
                            Transferencia Internacional: Únicamente se realizará una transferencia internacional de datos personales cuando se garanticen
                            niveles de protección adecuados, salvo que el titular preste su consentimiento expreso o se trate de colaboración judicial
                            internacional, intercambio de datos de carácter médico, cuando así lo exija el tratamiento del afectado, o una investigación
                            epidemiológica previa disociación de los datos que no permita la identificación de sus titulares, transferencias bancarias
                            o bursátiles, en lo relativo a las transacciones respectivas y conforme la legislación que les resulte aplicable, cuando
                            la transferencia se hubiera acordado en el marco de tratados internacionales en los cuales la República Argentina sea 
                            parte, cuando la transferencia tenga por objeto la cooperación internacional entre organismos de inteligencia para la 
                            lucha contra el crimen organizado, el terrorismo y el narcotráfico. 
                        </li>
                        <li>OBLIGACIONES DEL RESPONSABLE DEL BANCO DE DATOS<br>
                            Seguridad de los datos: Sysworld Servicios S.A adopta las medidas de seguridad necesarias para garantizar
                            la seguridad y confidencialidad de los datos personales, a fin de evitar su adulteración, pérdida, consulta
                            o tratamiento no autorizado, y que permitan detectar desviaciones, intencionales o no, de información, ya 
                            sea que los riesgos provengan de la acción humana o del medio técnico utilizado. Secreto: el deber de secreto
                            respecto de los datos personales tratados, es una obligación que corresponde a Sysworld Servicios S.A y a toda
                            persona que consulte su Base, obligación que se mantiene aún finalizada la relación que permitió el acceso
                            al banco de datos.<br>
                            El Administrador de la Empresa. Sr. Sebastián Piñeiro es el responsable de la Política de Privacidad y
                            es quien supervisa la implementación y ejecución, por parte de todo su personal. El obligado por el secreto
                            profesional o normas de confidencialidad sólo puede ser relevado de esa obligación por resolución judicial y
                            cuando medien razones de seguridad y salud públicas, y de defensa nacional.<br>
                            <b>Deber de respuesta:</b> Sysworld Servicios S.A deberá contestar la solicitud que se le dirija,
                            con independencia de que figuren o no datos personales del afectado, debiendo para ello valerse 
                            de cualquiera de los medios autorizados (por escrito, medios electrónicos, telefónicos, de imagen u
                            otro medio idóneo a tal fin), a opción del titular de los datos, o las preferencias que el interesado
                            hubiere expresamente manifestado al interponer el derecho de acceso.<br>
                            Sysworld Servicios S.A realiza, periódicamente, auditorías de sus procesos de tratamiento de datos
                            y capacita permanentemente a su personal en el cumplimiento de la ley de Protección de Datos Personales.
                            La Capacitación de la política se lleva a cabo de acuerdo a dos novedades. La primera novedad es en el 
                            momento de la incorporación de personal en la empresa, la cual al momento de incorporarla se le hace una 
                            inducción sobre el tema y se le hace firmar los acuerdos de confidencialidad y de política de privacidad 
                            de datos y la otra es cuando hay cambios en la normativa y se realiza una capacitación grupal por el sector
                            de legales de la compañía. 
                        </li>
                        <li>AUDITORÍA SOBRE EL CUMPLIMIENTO Y LA CAPACITACIÓN DE LA POLÍTICA DE PRIVACIDAD:<br>
                            El Procedimiento de evaluación del Cumplimiento de la Capacitación de los empleados está a cargo 
                            del Responsable (Sofía Mónaco), quien verifica junto con los Responsables de cada área de la 
                            Compañía, que cada empleado haya sido capacitado en cuanto a la Política de Privacidad, que la 
                            haya firmado y se le haya entregado un ejemplar de la misma. El Ejemplar firmado por el Empleado,
                            es guardado en su legajo de Personal. 
                        </li>
                        <li>DERECHOS DE LAS PERSONAS<br>
                            Derecho de acceso: el titular de los datos tiene derecho a conocer la totalidad de los datos referidos
                            a su persona que pudieran existir en nuestra base de datos. Para ello, puede hacer uso del derecho de 
                            acceso, a fin de conocer si su información se encuentra registrada y, en caso afirmativo, cu les son los
                            datos, las fuentes y los medios a través de los cuales se obtuvieron sus datos, como asimismo conocer las
                            finalidades para las que se recabaron dichos datos y conocer el destino previsto para los mismos y, 
                            finalmente, saber si el archivo está registrado conforme a las exigencias de la LEY N' 25.326.<br>
                            El reclamo se debe formular directamente a Sysworld Servicios S.A, quien deberá proporcionar la información
                            dentro de los DIEZ (10) días corridos de recibida la solicitud. El incumplimiento de esta obligación 
                            habilitará al interesado a promover la acción judicial de Hábeas Data, así como también podrá denunciar
                            el hecho ante la DIRECCIÓN NACIONAL DE PROTECCIÓN DE DATOS PERSONALES.<br>
                            Derechos de rectificación, actualización y supresión: a solicitud del titular del dato, o advertido el
                            error o falsedad, Sysworld Servicios S.A procederá, siempre de manera gratuita, a la rectificación, 
                            supresión o actualización de los datos personales del afectado dentro del plazo de CINCO (5) días hábiles 
                            de requerida. El incumplimiento de esta obligación habilitará al interesado a promover la acción judicial
                            de Hábeas Data.<br>
                            La supresión no procede cuando pudiese causar perjuicios a derechos o intereses legítimos de terceros, o 
                            cuando existiera una obligación legal de conservar los datos. Derecho de consulta al REGISTRO NACIONAL DE BASES
                            DE DATOS: las personas tienen el derecho de consultar al Órgano de Control en forma gratuita sobre la existencia
                            de archivos, registros o bases de datos, sus finalidades y la identidad de sus responsables.<br>
                            Quedan reconocidos, respecto a los datos informados a Sysworld Servicios S.A, sus derechos de acceso, cancelación
                            y rectificación, los cuales podrá ejercitar plenamente, contactándose con Sysworld Servicios S.A, al Centro de 
                            Atención al Cliente mediante el siguiente formulario web:<br>
                            <a href="http://www.informescentrales.com/web/ticketrndp.php">http://www.informescentrales.com/web/ticketrndp.php</a> 
                        </li><br>
                    </ol><!-- .order-list END -->
                    <div class="terms-wraper wow fadeInUp">
                           <center> <h4 class="xs-title">Registro Nacional de Datos Personales Argentina</h4></center>
                            <ol class="order-list">
                                <li>ACTUALIZAR DATOS PERSONALES:<br>
                                    "Conforme el Artículo 43 de la Constitución Nacional, las personas tienen derecho a tomar 
                                    conocimiento de sus datos y a solicitar, en caso que proceda, la rectificación y / o 
                                    actualización de los mismos".<br>
                                    Sysworld Servicios S.A tiene por objeto actuar en el ámbito de la información relacionada con 
                                    la prevención de lavado de activos, la lucha contra el terrorismo, la información comercial y 
                                    bancaria vinculada con la cobertura del riesgo crediticio y sus afines y en cumplimiento del mismo
                                    obtiene información de naturaleza comercial, financiera y judicial de personas físicas y jurídicas,
                                    fundamentalmente de fuentes de acceso público, brindándola a requerimiento de sus abonados para la
                                    concreción de actividades lícitas de orden de cumplimiento normativo, comercial y crediticia.<br>
                                    La información que Sysworld Servicios S.A posee en su Base de Datos se ajusta a lo que establece
                                    el Artículo 43 de la Constitución Nacional por lo que no recaba, trata ni difunde datos llamados 
                                    "sensibles" que son aquellos de carácter personal, íntimos y privados, referidos a la salud, origen
                                    étnico - racial, religión, convicciones políticas, gremiales, preferencias sexuales, y todo 
                                    otro dato inherente a la personalidad que de cualquier modo pudiera afectar el honor y la privacidad
                                    o pudiera generar algún tipo de discriminación. Tampoco le asigna a la información que suministra 
                                    el carácter de un juicio de valor respecto de personas o de su solvencia, no efectuando evaluaciones
                                    sobre su contenido. Todo informe emitido por Sysworld Servicios S.A reviste carácter reservado y 
                                    confidencial, siendo para uso exclusivo de sus clientes; su contenido no puede ser divulgado, 
                                    sirviendo solamente como elemento de ayuda en decisiones comerciales.  
                                </li>
                                <li> OBTENER MIS DATOS PERSONALES:<br>
                                     Para que Ud. pueda conocer qué datos tenemos registrados sobre su persona y -en caso que 
                                     proceda- pueda rectificarlos o actualizarlos, Sysworld Servicios S.A respeta su "DERECHO A 
                                     OBTENER SUS DATOS PERSONALES" conforme lo establece el Artículo 43 de la Constitución 
                                     Nacional. 
                                </li>
                                <li>¿CÓMO OBTENGO MIS DATOS PERSONALES?<br>
                                    a) Deberá contar con su DNI escaneado (frente y dorso). Luego, completar el formulario y subir la imagen
                                     de su documento haciendo click aquí.<br> 
                                    b) Si se trata de persona jurídica, la solicitud deberá ser acompañada por documentación que acredite la 
                                    personería del solicitante.<br>
                                    IMPORTANTE: Los trámites son personales. Si lo hace a través de un tercero, el mismo deberá presentar poder
                                     otorgado ante Escribano Público. En todos los casos se deberá acreditar identidad del titular de los datos. 
                                </li>
                                <li>¿EN QUÉ PLAZO RECIBIRÉ MI INFORME?<br>
                                    Sysworld Servicios S.A le entregará su informe, en un plazo máximo de 10 (diez) días corridos desde completada
                                    adecuadamente su solicitud, remitiéndolo por correo electrónico.</li>
                                <li>¿CUÁL SERÁ EL COSTO DE MI INFORME PERSONAL?<br>
                                    Sysworld Servicios S.A le entregará dicha información sin cargo. 
                                </li>
                                <li>¿CÓMO ACTUALIZO Y/O RECTIFICO MIS DATOS PERSONALES?<br>
                                    En el formulario "Solicitud de actualización de Datos", Ud tiene la posibilidad para 
                                    agregar más información para que ésta sea evaluada y, actualizada o rectificada, en caso
                                     de corresponder.
                                </li>
                                <li>¿CUÁL ES LA DOCUMENTACIÓN REQUERIDA PARA ACTUALIZAR DATOS?<br>
                                    Juicios - Concursos - Quiebras: Escrito, oficio o certificación judicial de los cuales surja la conclusión 
                                    ó cancelación del juicio. Endeudamiento Bancario, Financiero y/o Comercial: Certificado de regularización y/o
                                    cancelación (libre deuda) de la entidad. Deudas con entidades liquidadas por el B.C.R.A.: Constancia de 
                                    cancelación emitida por el B.C.R.A. y/o la entidad o fideicomisaria designada por éste. Actuaciones societarias:
                                    Constancia de renuncia inscripta en el Registro de Sociedades pertinente o edicto con su publicación. Acta de
                                    asamblea con aceptación de la renuncia o designación del nuevo directorio certificada por Escribano Público.
                                    Constancia de la cesión o venta de la cuota parte (en caso de S.R.L.). Divorcios: Sentencia o acta de 
                                    matrimonio con anotación marginal de la sentencia de divorcio. 
                                </li>
                                <li>¿CUÁNTO TARDARÁ EN SER ACTUALIZADA LA BASE DE DATOS?<br>
                                    El plazo máximo es de 5 (cinco) días hábiles de formulado el pedido; pudiendo ser menor cuanto más completa sea la documentación presentada. 
                                </li>
                                <li>¿DEBO ABONAR POR LA ACTUALIZACIÓN DE MIS DATOS?<br>
                                    No; los trámites de actualización se efectúan sin costo alguno para el titular de los datos. 
                                </li>
                </div><!-- .terms-wraper END -->
            </div>
        </div><!-- .row END -->
    </div><!-- .container END -->
</section>


<!-- end privacy -->

<!-- language switcher strart -->
<!-- xs modal -->
<div class="zoom-anim-dialog mfp-hide modal-language" id="modal-popup-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="language-content">
                <p>Switch The Language</p>
                <ul class="flag-lists">
                    <li><a href="#"><img src="assets/images/flags/006-united-states.svg" alt=""><span>English</span></a></li>
                    <li><a href="#"><img src="assets/images/flags/002-canada.svg" alt=""><span>English</span></a></li>
                    <li><a href="#"><img src="assets/images/flags/003-vietnam.svg" alt=""><span>Vietnamese</span></a></li>
                    <li><a href="#"><img src="assets/images/flags/004-france.svg" alt=""><span>French</span></a></li>
                    <li><a href="#"><img src="assets/images/flags/005-germany.svg" alt=""><span>German</span></a></li>
                </ul>
            </div>
        </div>
    </div>
</div><!-- End xs modal --><!-- end language switcher strart -->

<!-- language switcher strart -->
<!-- xs modal -->
<div class="zoom-anim-dialog mfp-hide modal-searchPanel" id="modal-popup-2">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="xs-search-panel">
                <form action="#" method="POST" class="xs-search-group">
                    <input type="search" class="form-control" name="search" id="search" placeholder="Search">
                    <button type="submit" class="search-button"><i class="icon icon-search"></i></button>
                </form>
            </div>
        </div>
    </div>
</div><!-- End xs modal --><!-- end language switcher strart -->

<!-- offset cart strart -->
<!-- sidebar cart item -->
<div class="xs-sidebar-group cart-group">
    <div class="xs-overlay xs-bg-black"></div>
    <div class="xs-sidebar-widget">
        <div class="sidebar-widget-container">
            <div class="widget-heading media">
                <div class="media-body">
                    <a href="#" class="close-side-widget">
                        X
                    </a>
                </div>
            </div>
            <div class="xs-empty-content">
                <h3 class="widget-title">Shopping cart</h3>
                <h4 class="xs-title">No products in the cart.</h4>
                <p class="empty-cart-icon">
                    <i class="icon icon-shopping-cart"></i>
                </p>
                <p class="xs-btn-wraper">
                    <a class="btn btn-primary" href="#">Return To Shop</a>
                </p>
            </div>
        </div>
    </div>
</div>    <!-- END sidebar cart item -->    <!-- END offset cart strart -->

<!-- offset cart strart -->
<!-- sidebar cart item -->
<div class="xs-sidebar-group info-group">
    <div class="xs-overlay xs-bg-black"></div>
    <div class="xs-sidebar-widget">
        <div class="sidebar-widget-container">
            <div class="widget-heading">
                <a href="#" class="close-side-widget">
                    X
                </a>
            </div>
            <div class="sidebar-textwidget">
                <div class="sidebar-logo-wraper">
                    <img src="assets/images/blue_logo.png" alt="sidebar logo">
                </div>
                <p>Far far away, behind the word moun tains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of  </p>
                <ul class="sideabr-list-widget">
                    <li>
                        <div class="media">
                            <div class="d-flex">
                                <img src="assets/images/location.png" alt="">
                            </div>
                            <div class="media-body">
                                <p>759 Pinewood Avenue</p>
                                <span>Marquette, Michigan</span>
                            </div>
                        </div><!-- address 1 -->
                    </li>
                    <li>
                        <div class="media">
                            <div class="d-flex">
                                <img src="assets/images/mail.png" alt="">
                            </div>
                            <div class="media-body">
                                <a href="mailto:info@domain.com">info@domain.com</a>
                                <span>Online Support</span>
                            </div>
                        </div><!-- address 1 -->
                    </li>
                    <li>
                        <div class="media">
                            <div class="d-flex">
                                <img src="assets/images/phone.png" alt="">
                            </div>
                            <div class="media-body">
                                <p>906-624-2565</p>
                                <span>Mon-Fri 8am-5pm</span>
                            </div>
                        </div><!-- address 1 -->
                    </li>
                </ul><!-- .sideabr-list-widget -->
                <div class="subscribe-from">
                    <p>Get Subscribed!</p>
                    <form action="#" method="POST" id="subscribe-form" class="subscribe-form">
                        <label for="sub-input"></label>
                        <div class="form-group">
                            <input type="email" name="email" id="sub-input" placeholder="Enter your mail here" class="form-control">
                            <button class="sub-btn" type="submit"><i class="icon icon-arrow-right"></i></button>
                        </div>
                    </form>
                </div>
                <ul class="social-list version-2">
                    <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#" class="vimeo"><i class="fa fa-vimeo"></i></a></li>
                </ul><!-- .social-list -->
                <div class="text-center">
                    <a href="#" class="btn btn-primary">Purchase Now</a>
                </div>
            </div>
        </div>
    </div>
</div>    <!-- END sidebar widget item -->    <!-- END offset cart strart -->

		<!-- footer section start -->
		<footer class="xs-footer-section">
                <div class="footer-group" style="background-image: url(assets/images/footer-bg.png);">
                    <div class="footer-main">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-3 col-md-6">
                                    <div class="footer-widget wow fadeInUp">
                                        <h3 class="widget-title">CasillaPostal</h3>
                                        <ul class="xs-list">
                                            <li><a href="index.asp">Inicio</a></li>
                                            <li><a href="precios.asp">Precios</a></li>
                                            <li><a href="contacto.asp">Contacto</a></li>
                                            <li><a href="login.asp">Login</a></li>
                                            <li><a href="api.asp">Api Desarrolladores</a></li>
                                            <li><a href="Politica.asp" target="_blank">Legales</a></li>
                                        </ul><!-- .xs-list END -->
                                    </div><!-- .footer-widget END -->
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="footer-widget wow fadeInUp" data-wow-duration="1s">
                                        <h3 class="widget-title">Empresa</h3>
                                        <ul class="xs-list">
                                            <li><a href="empresa.asp">Nosotros</a></li>
                                            <li><a href="http://www.sysworld.com.ar" target="_blank">Sitio Corporativo</a></li>
                                            <li><a href="contacto.asp">Contactenos</a></li>
                                            <li><a href="http://www.sysworld.com.ar/v2/sw/empresa_dist.asp" target="_blank">Distribuidores</a></li>
                                            <li><a href="http://www.sysworld.com.ar/" target="_blank">Politica de Privacidad</a></li>
                                            <li><a href="http://www.sysworld.com.ar/" target="_blank">Datos Personales</a></li>
                                        </ul><!-- .xs-list END -->
                                    </div><!-- .footer-widget END -->
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="footer-widget wow fadeInUp" data-wow-duration="1.3s">
                                        <h3 class="widget-title">Soporte</h3>
                                        <ul class="xs-list">
                                            <li><a href="who-is.html">Tutoriales</a></li>
                                            <li><a href="http://cloud.sysworld.com.ar/" target="_blank">Cargar Ticket</a></li>
                                            <li><a href="http://estado.sysworld.com.ar/" target="_blank">Estado de Servicios</a></li>
                                            <li><a href="faq.asp">FAQ</a></li>
                                        </ul><!-- .xs-list END -->
                                    </div><!-- .footer-widget END -->
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="footer-widget wow fadeInUp" data-wow-duration="1.5s">
                                        <h3 class="widget-title">Contacto</h3>
                                        <ul class="contact-info-widget">
                                            <li class="media">
                                                <img src="assets/images/address-pin.png" class="d-flex" alt="contact icon">
                                                <span class="media-body">Bucarelli 2480, CABA, Argentina</span>
                                            </li><!-- .media END -->
                                            <li class="media">
                                                <img src="assets/images/phone-pin.png" class="d-flex" alt="contact icon">
                                                <span class="media-body">+54 (011) 5269-2919</span>
                                            </li><!-- .media END -->
                                            <li class="media">
                                                <img src="assets/images/email-icon.png" class="d-flex" alt="contact icon">
                                                <span class="media-body">info@sysworld.com.ar</span>
                                            </li><!-- .media END -->
                                        </ul><!-- .contact-info-widget -->
                                    </div><!-- .footer-widget END -->
                                </div>
                            </div><!-- .row END -->
                        </div><!-- .container END -->
                    </div><!-- .footer-main END -->
                    <div class="container">
                        <div class="footer-bottom">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="footer-bottom-info wow fadeInUp">
                                        <p>Un aliado estratègico para su negocio. Desde nuestra empresa creemos en crear soluciones que le apliquen valor a su negocio. <a href="http://www.sysworld.com.ar">Sysworld Servicios S.A.</a></p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <ul class="xs-list list-inline wow fadeInUp" data-wow-duration="1s">
                                        <li><img src="assets/images/security/security-company-images-1.png" alt="security company images"></li>
                                        <li><img src="assets/images/security/security-company-images-2.png" alt="security company images"></li>
                                        <li><img src="assets/images/security/security-company-images-3.png" alt="security company images"></li>
                                        <li><img src="assets/images/security/security-company-images-4.png" alt="security company images"></li>
                                    </ul>
                                </div>
                            </div><!-- .row END -->
                        </div><!-- .footer-bottom end -->
                    </div><!-- .container end -->
                </div><!-- .footer-group END -->
                <div class="footer-copyright">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="copyright-text wow fadeInUp">
                                    <p>&copy; 2018 - <%=year(date)%> Powered by <a href="http://www.sysworld.com.ar">Sysworld Servicios S.A. </a></p>
                                </div><!-- .copyright-text END -->
                            </div>
                            <div class="col-md-4">
                                <div class="footer-logo-wraper wow fadeInUp" data-wow-duration="1s">
                                    <a href="index.asp" class="footer-logo"><img src="assets/images/logo.png" alt="footer logo" width="180" height="45"></a>
                                </div><!-- .footer-logo-wraper END -->
                            </div>
                            <div class="col-md-4">
                                <div class="social-list-wraper wow fadeInUp" data-wow-duration="1.3s">
                                    <ul class="social-list">
                                        <li><a href="https://www.facebook.com/SysWorld-Servicios-SA-323344231089747" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="https://twitter.com/Sysworldsa" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                    </ul>
                                </div><!-- .social-list-wraper END -->
                            </div>
                        </div><!-- .row END -->
                    </div><!-- .container END -->
                </div><!-- .footer-copyright END -->
            </footer>
		<!-- js file start -->
		<script src="assets/js/jquery-3.2.1.min.js"></script>
		<script src="assets/js/jquery-ui.min.js"></script>
		<script src="assets/js/plugins.js"></script>
		<script src="assets/js/Popper.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/jquery.magnific-popup.min.js"></script>
		<script src="assets/js/owl.carousel.min.js"></script>
		<script src="assets/js/tweetie.js"></script>
		<script src="assets/js/jquery.parallax-scroll.js"></script>
		<script src="assets/js/jquery.ajaxchimp.min.js"></script>
		<script src="assets/js/jquery.parallax.js"></script>
		<script src="assets/js/hostslide.js"></script>
		<script src="assets/js/vps-slider.js"></script>
		<script src="assets/js/vps-slider-settings.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyCy7becgYuLwns3uumNm6WdBYkBpLfy44k"></script>
		<script src="assets/js/main.js"></script>		<!-- End js file -->
	</body>
</html>