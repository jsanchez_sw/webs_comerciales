<?php
    include 'functions/db.php';

    include_once 'functions/language.php';
?>
<!DOCTYPE html>
<html lang="<?php echo $lang['SITE_lang'];?>">

<!-- Mirrored from protechtheme.com/saas/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:17:32 GMT -->
<head>
        <!-- Required meta tags -->
        <meta charset="iso-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="keywords" content="<?php echo $lang['SITE_keywords'];?>">
		<meta name="description" content="<?php echo $lang['SITE_desc'];?>">
		<meta name="author" content="Sysworld Servicios S.A.">
        <title><?php echo $lang['SITE_title'];?></title>

        <!-- Bootstrap core CSS -->
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Select2 -->
        <link href="assets/select2/css/select2.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- Iconmoon -->
        <link href="assets/iconmoon/css/iconmoon.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="css/custom.css" rel="stylesheet">
        <link rel="icon" type="image/x-icon" href="images/favicon.ico" />
    </head>
    <body>

<!-- ==============================================
**Preloader**
=================================================== -->
        <div id="loader">
            <div id="element">
                <div class="circ-one"></div>
                <div class="circ-two"></div>
            </div>
        </div>

<!-- ==============================================
**Header**
=================================================== -->
        <header>
			<?php include 'menu.php';?>
        </header>

<!-- ==============================================
**Inner Banner**
=================================================== -->
        <section class="inner-banner">
            <div class="container">
                <div class="contents">
                    <h1><?php echo $lang['LEGAL_header_tittle'];?></h1>
                    <p><?php echo $lang['LEGAL_header_subtittle'];?></p>
                </div>
            </div>
        </section>

<!-- ==============================================
**Privacy Policy**
=================================================== -->
        <section class="privacy-policy padding-lg">
            <div class="container">
                <h4><?php echo $lang['LEGAL_section1_tittle'];?></h4>
                <p><?php echo $lang['LEGAL_section1_subtittle'];?></p>
                <ul class="circle-bordered-list">
                    <li>
                        <p><?php echo $lang['LEGAL_section1_item1'];?></p>
                    </li>
                    <li>
                        <p><?php echo $lang['LEGAL_section1_item2'];?></p>
                    </li>
                </ul>
                <hr>
                <h4><?php echo $lang['LEGAL_section2_item3_tittle'];?></h4>
                <ul class="circle-bordered-list">
                    <li>
                        <p><?php echo $lang['LEGAL_section2_item3_subtittle'];?></p>
                    </li>
                    <li>
                        <p><?php echo $lang['LEGAL_section2_item3_subtittle2'];?></p>
                    </li>
                </ul>
                <ul class="quate-box">
                    <li><?php echo $lang['LEGAL_section2_item3_subtittle3'];?></li>
                </ul>
                <h4><?php echo $lang['LEGAL_section_item4_tittle'];?></h4>
                <p><?php echo $lang['LEGAL_section_item4_subtittle'];?></p>
                <p><?php echo $lang['LEGAL_section_item4_subtittle2'];?></p>
                <hr>
                <h5><?php echo $lang['LEGAL_section2_item5_tittle'];?></h5>
                <p><?php echo $lang['LEGAL_section2_item5_subtittle'];?></p>
                <hr>
                <h5><?php echo $lang['LEGAL_section2_item6_tittle'];?></h5>
                <p><?php echo $lang['LEGAL_section2_item6_subtittle'];?></p>
            </div>
        </section>

<!-- ==============================================
**Signup Section**
=================================================== -->

<!-- ==============================================
**Footer opt1**
=================================================== -->

	<?php include 'footer.php';?>



        <!-- Scroll to top -->
        <a href="#" class="scroll-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>

        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery.min.js"></script>
        <!-- Popper JS -->
        <script src="js/popper.min.js"></script>
        <!-- Bootsrap JS -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- Select2 JS -->
        <script src="assets/select2/js/select2.min.js"></script>
        <!-- Custom JS -->
        <script src="js/custom.js"></script>
    </body>

<!-- Mirrored from protechtheme.com/saas/privacy-policy.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:18:11 GMT -->
</html>