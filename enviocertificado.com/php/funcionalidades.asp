<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="iso-8859-1">
		<title>EnvioCertificado.com - Envio con Respaldo Notarial.</title>
		<meta name="description" content="Cloudhub is a modern, responsive and easy to customize HTML template, perfectly suited for hosting and technology companies.">
		<meta name="keywords" content="html template, responsive, retina, cloud hosting, technology, startup">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<!-- Icons -->
		<link rel="apple-touch-icon-precomposed" href="img/icons/apple-touch-icon.png">
		<link rel="icon" href="img/icons/favicon.ico">
		<!-- Stylesheets -->
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="css/main.min.css">
	</head>
	<body class="footer-dark">
		<!-- Header -->
		<header id="header" class="header-dynamic header-light header-shadow-scroll">
			<div class="container">
				<a class="logo" href="index.html">
					<img src="img/logos/logo_ern.jpg" alt="">
				</a>
				<nav>
					<ul class="nav-primary">
						<li>
							<a href="index.asp">Inicio</a>
						</li>
						<li>
							<a href="empresa.asp">Nuestra Empresa</a>
						</li>
						<li>
							<a href="funcionalidades.asp">Funcionalidades</a>
						</li>
						<li>
							<a href="faq.asp">FAQ</a>
						</li>
						<li>
							<a href="contacto.asp">Contacto</a>
						</li>
						<li>
							<a class="button button-secondary" href="https://web.certisend.com/panel">
								<i class="fa fa-lock icon-left"></i>Acceso al Sistema
							</a>
						</li>
					</ul>
					<ul class="nav-secondary">
						<li>
							<a href="contacto.asp"><i class="fa fa-phone icon-left"></i>+54 (011) 5263-2919</a>
						</li>
						<li>
							<a href="contacto.asp"><i class="fa fa-comment icon-left"></i>Live Chat</a>
						</li>
						<li>
							<a href="http://help.enviocertificado.com"><i class="fa fa-question-circle icon-left"></i>Tutoriales</a>
						</li>
						<li>
							<a href="http://estado.sysworld.com.ar"><i class="fa fa-check icon-left"></i>Estado de Servicios</a>
						</li>
					</ul>
				</nav>
			</div>
		</header>

		<!-- Content -->
		<section id="content">
			<!-- Content Row -->
			<section class="content-row content-row-color content-row-clouds">
				<div class="content-slider animate-container-slide" data-nav="true" data-rotation="5">
					<div class="slide">
						<div class="container">
							<div class="column-row align-center">
								<div class="column-33 text-align-center">
									<i class="fa fa-rocket icon-feature-large text-color-light"></i>
								</div>
								<div class="column-50">
									<header class="content-header content-header-large content-header-align-left">
										<h3>
											Deliverabilidad Garantizada
										</h3>
										<p>
											Gracias a la reputaci�n de nuestra red y a certificaciones anti-spam el 99.9 % de nuestras comunicaciones gozan de llegar a bandeja de entrada.<br><br>
											<a class="button button-secondary" href="funcionalidades.asp">Ver funcionalidades<i class="fa fa-chevron-right icon-right"></i></a>
										</p>
									</header>
								</div>
							</div>
						</div>
					</div>
					<div class="slide">
						<div class="container">
							<div class="column-row align-center">
								<div class="column-33 text-align-center">
									<i class="fa fa-cloud icon-feature-large text-color-light"></i>
								</div>
								<div class="column-50">
									<header class="content-header content-header-large content-header-align-left">
										<h3>
											Alta Disponibilidad
										</h3>
										<p>
											Una infraestructura globalmente distribuida basada en continuidad del negocio permita brindar alta disponibilidad a nuestros servicios m�s cr�ticos.<br><br>
											<a class="button button-secondary" href="funcionalidades.asp">Ver funcionalidades<i class="fa fa-chevron-right icon-right"></i></a>
										</p>
									</header>
								</div>
							</div>
						</div>
					</div>
					<div class="slide">
						<div class="container">
							<div class="column-row align-center">
								<div class="column-33 text-align-center">
									<i class="fa fa-shield icon-feature-large text-color-light"></i>
								</div>
								<div class="column-50">
									<header class="content-header content-header-large content-header-align-left">
										<h3>
											Seguridad de la Informaci�n
										</h3>
										<p>
											Todos nuestros procesos, productos y servicios son gestados bajo estrictas normas de seguridad de la informaci�n para garantizar la confidencialidad y protecci�n de los datos.<br><br>
											<a class="button button-secondary" href="funcionalidades.asp">Ver funcionalidades<i class="fa fa-chevron-right icon-right"></i></a>
										</p>
									</header>
								</div>
							</div>
						</div>
					</div>
					<div class="slide">
						<div class="container">
							<div class="column-row align-center">
								<div class="column-33 text-align-center">
									<i class="fa fa-refresh icon-feature-large text-color-light"></i>
								</div>
								<div class="column-50">
									<header class="content-header content-header-large content-header-align-left">
										<h3>
											Startup inmediato
										</h3>
										<p>
											Gracias a la disponibilidad inmediata de recursos y a que nuestros sistemas son 100% param�tricos, el alta de un nuevo cliente es mur r�pido.<br><br>
											<a class="button button-secondary" href="funcionalidades.asp">Ver funcionalidades<i class="fa fa-chevron-right icon-right"></i></a>
										</p>
									</header>
								</div>
							</div>
						</div>
					</div>
					<div class="slide">
						<div class="container">
							<div class="column-row align-center">
								<div class="column-33 text-align-center">
									<i class="fa fa-support icon-feature-large text-color-light"></i>
								</div>
								<div class="column-50">
									<header class="content-header content-header-large content-header-align-left">
										<h3>
											Soporte Dedicado
										</h3>
										<p>
											Diferentes planes de soporte le garantizan a nuestros clientes una atenci�n a la medida de cada necesidad.<br><br>
											<a class="button button-secondary" href="funcionalidades.asp">Ver funcionalidades<i class="fa fa-chevron-right icon-right"></i></a>
										</p>
									</header>
								</div>
							</div>
						</div>
					</div>
					<div class="slide">
						<div class="container">
							<div class="column-row align-center">
								<div class="column-33 text-align-center">
									<i class="fa fa-gear icon-feature-large text-color-light"></i>
								</div>
								<div class="column-50">
									<header class="content-header content-header-large content-header-align-left">
										<h3>
											Automatizaci�n Poderosa
										</h3>
										<p>
											Gracias a nuestra API y nuestro workflow automatizado podemos crear reglas de negocio a la medida de cada uno de nuestros clientes.<br><br>
											<a class="button button-secondary" href="funcionalidades.asp">Ver funcionalidades<i class="fa fa-chevron-right icon-right"></i></a>
										</p>
									</header>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- Content Row -->
			<section class="content-row">
				<div class="container">
					<div class="column-row">
						<div class="column-66">
							<h3>
								Integrated DDoS Protection
							</h3>
							<p>
								Our network equipment across all locations is backed by enterprise grade DDoS protection gear to ensure your services stay online even in the event of an attack. All products are protected without additional charge, but the protection capacity can vary depending on which product you have ordered. Our dedicated private cloud offers the best protection with the complete 500 Gbit mitigation capacity.
							</p>
							<p>
								You also have the possibility to order a DDoS protected IP address to reroute services outside of our network by setting up a GRE tunnel. Please contact our <a href="contact.html">sales department</a> for more details.
							</p>
							<h4>
								Protected Attack Types
							</h4>
							<div class="column-row">
								<div class="column-33">
									<ul class="list-style-icon">
										<li>
											<i class="fa fa-check"></i>TCP/UDP Floods
										</li>
										<li>
											<i class="fa fa-check"></i>HTTP GET/POST Floods
										</li>
										<li>
											<i class="fa fa-check"></i>UDP Fragmentation
										</li>
										<li>
											<i class="fa fa-check"></i>XOR DDoS
										</li>
									</ul>
								</div>
								<div class="column-33">
									<ul class="list-style-icon">
										<li>
											<i class="fa fa-check"></i>NTP/DNS Amplification
										</li>
										<li>
											<i class="fa fa-check"></i>SSDP/UPNP Responses
										</li>
										<li>
											<i class="fa fa-check"></i>Chargen Responses
										</li>
										<li>
											<i class="fa fa-check"></i>SNMP Responses
										</li>
									</ul>
								</div>
								<div class="column-33">
									<ul class="list-style-icon">
										<li>
											<i class="fa fa-check"></i>Invalid port numbers
										</li>
										<li>
											<i class="fa fa-check"></i>Connection holding
										</li>
										<li>
											<i class="fa fa-check"></i>HOIC and LOIC
										</li>
										<li>
											<i class="fa fa-check"></i>Spoofed SYN
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="column-33">
							<div class="product-box">
								<div class="product-header">
									<h4>
										Remote IP Protection
									</h4>
									<p>
										DDoS Protected GRE Tunnel
									</p>
								</div>
								<div class="product-price">
									$19<span class="term">/ TB</span>
								</div>
								<div class="product-features">
									<ul>
										<li>
											<strong>500 Gbit</strong> Bandwidth Protection
										</li>
										<li>
											<strong>700 Mpps</strong> Packet Protection
										</li>
									</ul>
									<ul>
										<li>
											99.90% Uptime SLA
										</li>
										<li>
											10 Locations
										</li>
									</ul>
								</div>
								<div class="product-order">
									<a class="button button-secondary" href="contact.html">
										<i class="fa fa-shopping-cart icon-left"></i>Contact Sales
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- Content Row -->
			<section class="content-row content-row-gray">
				<div class="container">
					<div class="column-row align-center">
						<div class="column-50">
							<div class="network-map">
								<ul>
									<li style="top: 42%; left: 12.5%;">
										<a href="network.html"></a>
									</li>
									<li style="top: 44%; left: 20%;">
										<a href="network.html"></a>
									</li>
									<li style="top: 40%; left: 26%;">
										<a href="network.html"></a>
									</li>
									<li style="top: 78%; left: 34%;">
										<a href="network.html"></a>
									</li>
									<li style="top: 41%; left: 44.5%;">
										<a href="network.html"></a>
									</li>
									<li style="top: 32%; left: 46.5%;">
										<a href="network.html"></a>
									</li>
									<li style="top: 34%; left: 49.5%;">
										<a href="network.html"></a>
									</li>
									<li style="top: 64%; left: 75%;">
										<a href="network.html"></a>
									</li>
									<li style="top: 44%; left: 85%;">
										<a href="network.html"></a>
									</li>
									<li style="top: 83%; left: 89%;">
										<a href="network.html"></a>
									</li>
								</ul>
								<img src="img/layout/map-dark.svg" alt="">
							</div>
						</div>
						<div class="column-50">
							<h3>
								Instant Worldwide Deployment
							</h3>
							<p>
								Depending on the product you have the choice of up to 10 worldwide locations to deploy your services. Our low latency network has a dedicated primary 100 Gbit capacity and the ability to burst up to 800 Gbit through our peering partners.
							</p>
							<h4>
								Localized Peering
							</h4>
							<p>
								Our network engineers are continuously optimizing the routing to ensure low latency and high bandwidth throughput across all datacenter locations. Customers additionally have the option to request custom routing.
							</p>
						</div>
					</div>
				</div>
			</section>
			<!-- Content Row -->
			<section class="content-row">
				<div class="container">
					<div class="column-row">
						<div class="column-50">
							<h3>
								Service Level Agreement
							</h3>
							<p>
								All products come with an SLA and the resulting availability guarantee as defined in the product description. The percentage defines an effective approximate availability provided by the given product platform to inform every customer of the reliability to be expected from the service in question. Account credit will be issued as hourly price percentage of the product affected by the outage.
							</p>
							<p>
								<small class="text-color-gray">Our uptime guarantee is provided on a best effort basis. Customers will receive account credit should service availability fall below the given guarantee.</small>
							</p>
						</div>
						<div class="column-50">
							<table class="table-layout-fixed">
								<thead>
									<tr>
										<th>Availability</th>
										<th>Outage Timeframe</th>
										<th>Credit Amount</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>100%</td>
										<td>–</td>
										<td>2.50% <small>/ hour</small></td>
									</tr>
									<tr>
										<td>99.99%</td>
										<td>1 hour <small>/ year</small></td>
										<td>1.25% <small>/ hour</small></td>
									</tr>
									<tr>
										<td>99.95%</td>
										<td>4 hours <small>/ year</small></td>
										<td>0.75% <small>/ hour</small></td>
									</tr>
									<tr>
										<td>99.90%</td>
										<td>8 hours <small>/ year</small></td>
										<td>0.25% <small>/ hour</small></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</section>
			<!-- Content Row -->
			<section class="content-row content-row-gray">
				<div class="container">
					<header class="content-header">
						<h2>
							What Our Customers Say
						</h2>
						<p>
							We've helped hundreds of clients with custom server solutions, enabling them to operate much more efficient and secure than they ever did before.
						</p>
					</header>
					<div class="column-row align-center-bottom">
						<div class="column-33">
							<div class="testimonial">
								<p class="testimonial-content">
									My customers didn't experience a single minute of downtime since I moved my services over to Cloudhub.
								</p>
								<p class="testimonial-author">
									<a href="#twitter"><i class="fa fa-twitter icon-left"></i>Peter Miller</a><br>
									<small>Chemical Industries</small>
								</p>
							</div>
						</div>
						<div class="column-33">
							<div class="testimonial">
								<p class="testimonial-content">
									Cloudhub helped me with a professional custom server solution when my business was so rapidly growing my old system couldn't handle the load anymore.
								</p>
								<p class="testimonial-author">
									<a href="#twitter"><i class="fa fa-twitter icon-left"></i>John Smith</a><br>
									<small>HQ Streaming Company</small>
								</p>
							</div>
						</div>
						<div class="column-33">
							<div class="testimonial">
								<p class="testimonial-content">
									By switching to Cloudhub's Anycast DNS system we were able to decrease the worldwide app latency immensely.
								</p>
								<p class="testimonial-author">
									<a href="#twitter"><i class="fa fa-twitter icon-left"></i>Mary Fonda</a><br>
									<small>SAAS Billing Solutions</small>
								</p>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- Content Row -->
			<section class="content-row content-row-color">
				<div class="container">
					<header class="content-header">
						<h2>
							Looking for a custom solution?
						</h2>
						<p>
							Our technicians can provide you with the best custom made solutions on the market, no matter whether you're a small business or large enterprise.<br><br>
							<a class="button button-secondary" href="contact.html">
								<i class="fa fa-envelope icon-left"></i>Get in touch
							</a>
						</p>
					</header>
				</div>
			</section>
		</section>
		<!-- Footer -->
		<footer id="footer">
			<section class="footer-primary">
				<div class="container">
					<div class="column-row">
						<div class="column-33">
							<h5>
								Sysworld Servicios S.A.
							</h5>
							<p>
								EnvioCertificado.com | EnvioLegal.com | CertiSend.com |Envio de Email Certificados | Envios con Respaldo Notarial | Certificaciones en la BlockChain | SMS Certificados | Transacciones Certificadas | Auditoria de Transacciones
							</p>
						</div>
						<div class="column-66">
							<div class="column-row align-right-top">
								<div class="column-25">
									<h5>
										Conectarse
									</h5>
									<ul class="list-style-icon">
										<li>
											<a href="https://www.facebook.com/SysWorld-Servicios-SA-323344231089747" target="_blank"><i class="fa fa-facebook"></i>Facebook</a>
										</li>
										<li>
											<a href="https://twitter.com/Sysworldsa" target="_blank"><i class="fa fa-twitter"></i>Twitter</a>
										</li>
										<li>
											<a href="contacto.asp"><i class="fa fa-github"></i>Contacto</a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar/v2/sw/contacto.asp" target="_blank"><i class="fa fa-xing"></i>Live Chat</a>
										</li>
									</ul>
								</div>
								<div class="column-25">
									<h5>
										Productos
									</h5>
									<ul>
										<li>
											<a href="prodenviocertificado.asp">EnvioCertificado</a>
										</li>
										<li>
											<a href="prodern.asp">ERN</a>
										</li>
										<li>
											<a href="prodtrn.asp">TRN</a>
										</li>
										<li>
											<a href="http://www.centrodevalidaciones.com" target="_blank">Validadores</a>
										</li>
										<li>
											<a href="http://www.omnicrm.cloud" target="_blank">OmniCRM.cloud</a>
										</li>
										<li>
											<a href="http://cloud.sysworld.com.ar/" target="_blank">Customer Portal</a>
										</li>
									</ul>
								</div>
								<div class="column-25">
									<h5>
										Recursos
									</h5>
									<ul>
										<li>
											<a href="funcionalidades.asp">Funcionalidades</a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar/v2/sw/empresa.asp?f=network" target="_blank">Network</a>
										</li>
										<li>
											<a href="legales.asp">Respaldo Legales</a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar/v2/sw/empresa.asp?f=partners" target="_blank">Partners</a>
										</li>
									</ul>
								</div>
								<div class="column-flex">
									<h5>
										Empresa
									</h5>
									<ul>
										<li>
											<a href="empresa.asp">Nosotros</a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar" target="_blank">Sitio Corporativo</a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar/v2/sw/empresa_dist.asp" target="_blank">Distribuidores</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="footer-secondary">
				<div class="container">
					<p>
						Copyright 2006 - <%=year(date)%> &copy; Sysworld Servicios S.A. Todos los derechos reservados.<br>
						Powered by <a href="http://www.sysworld.com.ar">Sysworld Servicios S.A.</a>
					</p>
				</div>
			</section>
		</footer>
		<!-- Scripts -->
		<script src="js/jquery.min.js"></script>
		<script src="js/headroom.min.js"></script>
		<script src="js/js.cookie.min.js"></script>
		<script src="js/imagesloaded.min.js"></script>
		<script src="js/bricks.min.js"></script>
		<script src="js/main.min.js"></script>
	</body>
</html>