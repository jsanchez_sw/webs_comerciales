<?php
    include '../functions/db.php';
    include_once '../functions/language.php';
	include 'session.php';


	$usuario = $_SESSION['apilanding_user_email'];

    $sql = "SELECT * FROM web_suscripciones "
	. " WHERE usuario = '" . $usuario . "' ";

	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$tiposuscripcion = $row['suscripcion'];
			$idestadosuscripcion = $row['idestado'];
		}
	}else{
		$tiposuscripcion = "0";
		$idestadosuscripcion = "0";
	}


	$suscfree = "b-1";
	$suscbronze = "b-1";
	$suscsilver = "b-1";
	$suscgold = "b-1";



	if ($idestadosuscripcion == "0"){
		$suscfree = "featured-plan";
	}else{
		if ($tiposuscripcion == "1"){
			$suscbronze = "featured-plan";
		}
		if ($tiposuscripcion == "2"){
			$suscsilver = "featured-plan";
		}
		if ($tiposuscripcion == "3"){
			$suscgold = "featured-plan";
		}
	}


?>


<!DOCTYPE html>
<html lang="<?php echo $lang['SITE_lang'];?>">

<head>
    <meta charset="iso-8859-1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="<?php echo $lang['SITE_keywords'];?>">
    <meta name="description" content="<?php echo $lang['SITE_desc'];?>">
    <meta name="author" content="Sysworld Servicios S.A.">
    <link rel="icon" type="image/x-icon" href="../images/favicon.ico" />
    <title><?php echo $lang['SITE_title'];?></title>
    <!-- ===== Bootstrap CSS ===== -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- ===== Plugin CSS ===== -->
    <!-- ===== Animation CSS ===== -->
    <link href="css/animate.css" rel="stylesheet">
    <!-- ===== Custom CSS ===== -->
    <link href="css/style.css" rel="stylesheet">
    <!-- ===== Color CSS ===== -->
    <link href="css/colors/default.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="mini-sidebar">
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <!-- ===== Top-Navigation ===== -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
                <a class="navbar-toggle font-20 hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse">
                    <i class="fa fa-bars"></i>
                </a>
                <div class="top-left-part">
                    <a class="logo" href="index.php">
                        <span>&nbsp;</span>
                        <b>
                            <img src="../images/Apilanding_01.png" height="55px" />
                        </b>
                        <span>&nbsp;</span>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-left hidden-xs">
                    <li>
                        <a href="javascript:void(0)" class="sidebartoggler font-20 waves-effect waves-light"><i class="icon-arrow-left-circle"></i></a>
                    </li>
                    <li>
                        <form role="search" class="app-search hidden-xs" action="../apis.php" method="get">
                            <i class="icon-magnifier"></i>
                            <input type="text" placeholder="<?php echo $lang['INDEX_page_search'];?>" class="form-control">
                        </form>
                    </li>
                </ul>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle waves-effect waves-light font-20" data-toggle="dropdown" href="javascript:void(0);">
                            <i class="icon-speech"></i>
                            <span class="badge badge-xs badge-danger"></span>
                        </a>
                        <ul class="dropdown-menu mailbox animated bounceInDown">
                            <li>
                                <div class="drop-title"><?php echo $lang['INDEX_page_messages'];?></div>
                            </li>
                            <li>
                                <div class="message-center">
                                </div>
                            </li>
                            <li>
                                <a class="text-center" href="javascript:void(0);">
                                    <strong><?php echo $lang['INDEX_page_notifications'];?></strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle waves-effect waves-light font-20" data-toggle="dropdown" href="javascript:void(0);">
                            <i class="icon-calender"></i>
                            <span class="badge badge-xs badge-danger"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-tasks animated slideInUp">
                        </ul>
                    </li>
                    <li class="right-side-toggle">
                    </li>
                </ul>
            </div>
        </nav>
        <!-- ===== Top-Navigation-End ===== -->
        <!-- ===== Left-Sidebar ===== -->
        <aside class="sidebar" role="navigation">
            <div class="scroll-sidebar">
                <div class="user-profile">
                    <div class="dropdown user-pro-body">
                        <p class="profile-text m-t-15 font-16"><a href="javascript:void(0);"> Panel</a></p>
                    </div>
                </div>
                <nav class="sidebar-nav">
					<?php include 'menu.php';?>
                </nav>
                <div class="p-30">
                    <span class="hide-menu">
                        <a href="suscriptions.php" class="btn btn-success"><?php echo $lang['MENU_menu_bottom1'];?></a>
                        <a href="../index.php" class="btn btn-default m-t-15"><?php echo $lang['MENU_menu_bottom2'];?></a>
                    </span>
                </div>
            </div>
        </aside>
        <!-- ===== Left-Sidebar-End ===== -->
        <!-- Page Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                <!-- /.row -->
                <!-- .row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <div class="row pricing-plan">
                                <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                    <div class="pricing-box">
                                        <div class="pricing-body <?php echo $suscfree;?>">
                                            <div class="pricing-header">
                                            	<?php if ($idestadosuscripcion == "0"){?>
                                            		<h4 class="price-lable text-white bg-warning"> Activa</h4>
                                            	<?php }?>
                                                <h4 class="text-center">&nbsp;</h4>
                                                <h2 class="text-center"><span class="price-sign"></span>Free</h2>
                                                <p class="uppercase">30 d�as free trial.</p>
                                            </div>
                                            <div class="price-table-content">
                                                <div class="price-row"><i class="icon-user"></i><?php echo $lang['CHANGESUSC_page_silver_item1'];?></div>
                                                <div class="price-row"><i class="icon-screen-smartphone"></i><?php echo $lang['CHANGESUSC_page_silver_item2'];?></div>
                                                <div class="price-row"><i class="icon-drawar"></i><?php echo $lang['CHANGESUSC_page_silver_item3'];?></div>
                                                <div class="price-row"><i class="icon-refresh"></i><?php echo $lang['CHANGESUSC_page_silver_item4'];?></div>
                                                <div class="price-row">
													<?php if ($idestadosuscripcion == "0"){?>
														<a href="suscriptionedit.php"><button class="btn btn-warning waves-effect waves-light m-t-20"><?php echo $lang['CHANGESUSC_page_susc_cancel_bottom'];?></button></a>
													<?php }else{?>
														<a href="suscriptionedit.php"><button class="btn btn-warning waves-effect waves-light m-t-20"><?php echo $lang['CHANGESUSC_page_susc_bottom'];?></button></a>
													<?php }?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                    <div class="pricing-box <?php echo $suscbronze;?>">
                                        <div class="pricing-body">
                                            <div class="pricing-header">
                                            	<?php if ($idestadosuscripcion == "1" and $tiposuscripcion == "1"){?>
                                            		<h4 class="price-lable text-white bg-warning"> Activa</h4>
                                            	<?php }?>
                                                <h4 class="text-center"><?php echo $lang['CHANGESUSC_page_gold'];?></h4>
                                                <h2 class="text-center"><span class="price-sign">$</span>80</h2>
                                                <p class="uppercase"><?php echo $lang['CHANGESUSC_page_month'];?></p>
                                            </div>
                                            <div class="price-table-content">
                                                <div class="price-row"><i class="icon-user"></i><?php echo $lang['CHANGESUSC_page_gold_item1'];?></div>
                                                <div class="price-row"><i class="icon-screen-smartphone"></i><?php echo $lang['CHANGESUSC_page_gold_item2'];?></div>
                                                <div class="price-row"><i class="icon-drawar"></i><?php echo $lang['CHANGESUSC_page_gold_item3'];?></div>
                                                <div class="price-row"><i class="icon-refresh"></i><?php echo $lang['CHANGESUSC_page_gold_item4'];?></div>
                                                <div class="price-row">
													<?php if (($idestadosuscripcion == "0") or ($idestadosuscripcion == "1" and $tiposuscripcion != "1")){?>
														<a href="paycheckout.php?idprod=susc_bronze&codseg=<?php echo md5('susc_bronzedasadssd' . date('dMYH'));?>" target="_blank"><button class="btn btn-success waves-effect waves-light m-t-20"><?php echo $lang['CHANGESUSC_page_susc_bottom'];?></button></a>
													<?php }else{?>
														<a href="paycheckout.php?idprod=susc_bronze&codseg=<?php echo md5('susc_bronzedasadssd' . date('dMYH'));?>" target="_blank"><button class="btn btn-warning waves-effect waves-light m-t-20"><?php echo $lang['CHANGESUSC_page_susc_cancel_bottom'];?></button></a>
													<?php }?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                    <div class="pricing-box <?php echo $suscsilver;?>">
                                        <div class="pricing-body">
                                            <div class="pricing-header">
                                            	<?php if ($idestadosuscripcion == "1" and $tiposuscripcion == "2"){?>
                                            		<h4 class="price-lable text-white bg-warning"> Activa</h4>
                                            	<?php }?>
                                                <h4 class="text-center"><?php echo $lang['CHANGESUSC_page_platinum'];?></h4>
                                                <h2 class="text-center"><span class="price-sign">$</span>230</h2>
                                                <p class="uppercase"><?php echo $lang['CHANGESUSC_page_month'];?></p>
                                            </div>
                                            <div class="price-table-content">
                                                <div class="price-row"><i class="icon-user"></i><?php echo $lang['CHANGESUSC_page_platinum_item1'];?></div>
                                                <div class="price-row"><i class="icon-screen-smartphone"></i><?php echo $lang['CHANGESUSC_page_platinum_item2'];?></div>
                                                <div class="price-row"><i class="icon-drawar"></i><?php echo $lang['CHANGESUSC_page_platinum_item3'];?></div>
                                                <div class="price-row"><i class="icon-refresh"></i><?php echo $lang['CHANGESUSC_page_platinum_item4'];?></div>
                                                <div class="price-row">
													<?php if (($idestadosuscripcion == "0") or ($idestadosuscripcion == "1" and $tiposuscripcion != "2")){?>
														<a href="paycheckout.php?idprod=susc_silver&codseg=<?php echo md5('susc_silverdasadssd' . date('dMYH'));?>" target="_blank"><button class="btn btn-success waves-effect waves-light m-t-20"><?php echo $lang['CHANGESUSC_page_susc_bottom'];?></button></a>
													<?php }?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                    <div class="pricing-box <?php echo $suscgold;?>">
                                        <div class="pricing-body b-r">
                                            <div class="pricing-header">
                                            	<?php if ($idestadosuscripcion == "1" and $tiposuscripcion == "3"){?>
                                            		<h4 class="price-lable text-white bg-warning"> Activa</h4>
                                            	<?php }?>
                                                <h4 class="text-center"><?php echo $lang['CHANGESUSC_page_diamond'];?></h4>
                                                <h2 class="text-center"><span class="price-sign">$</span>570</h2>
                                                <p class="uppercase"><?php echo $lang['CHANGESUSC_page_month'];?></p>
                                            </div>
                                            <div class="price-table-content">
                                                <div class="price-row"><i class="icon-user"></i><?php echo $lang['CHANGESUSC_page_diamond_item1'];?></div>
                                                <div class="price-row"><i class="icon-screen-smartphone"></i><?php echo $lang['CHANGESUSC_page_diamond_item2'];?></div>
                                                <div class="price-row"><i class="icon-drawar"></i><?php echo $lang['CHANGESUSC_page_diamond_item3'];?></div>
                                                <div class="price-row"><i class="icon-refresh"></i><?php echo $lang['CHANGESUSC_page_diamond_item4'];?></div>
                                                <div class="price-row">
													<?php if (($idestadosuscripcion == "0") or ($idestadosuscripcion == "1" and $tiposuscripcion != "3")){?>
														<a href="paycheckout.php?idprod=susc_gold&codseg=<?php echo md5('susc_golddasadssd' . date('dMYH'));?>" target="_blank"><button class="btn btn-success waves-effect waves-light m-t-20"><?php echo $lang['CHANGESUSC_page_susc_bottom'];?></button></a>
													<?php }?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- ===== Right-Sidebar ===== -->
                <div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"> <?php echo $lang['INDEX_page_panel_service'];?><span><i class="icon-close right-side-toggler"></i></span> </div>
                        <div class="r-panel-body">
                            <ul class="hidden-xs">
                                <li><b> <?php echo $lang['INDEX_page_panel_service_item1'];?></b></li>
                                <li>
                                    <div class="checkbox checkbox-danger">
                                        <input id="headcheck" type="checkbox" class="fxhdr">
                                        <label for="headcheck"> Fix Header </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox checkbox-warning">
                                        <input id="sidecheck" type="checkbox" class="fxsdr">
                                        <label for="sidecheck"> Fix Sidebar </label>
                                    </div>
                                </li>
                            </ul>
                            <ul id="themecolors" class="m-t-20">
                                <li><b><?php echo $lang['INDEX_page_panel_service_item2'];?></b></li>
                                <li><a href="javascript:void(0)" data-theme="default" class="default-theme working">1</a></li>
                                <li><a href="javascript:void(0)" data-theme="green" class="green-theme">2</a></li>
                                <li><a href="javascript:void(0)" data-theme="yellow" class="yellow-theme">3</a></li>
                                <li><a href="javascript:void(0)" data-theme="red" class="red-theme">4</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple" class="purple-theme">5</a></li>
                                <li><a href="javascript:void(0)" data-theme="black" class="black-theme">6</a></li>
                                <li class="db"><b><?php echo $lang['INDEX_page_panel_service_item3'];?></b></li>
                                <li><a href="javascript:void(0)" data-theme="default-dark" class="default-dark-theme">7</a></li>
                                <li><a href="javascript:void(0)" data-theme="green-dark" class="green-dark-theme">8</a></li>
                                <li><a href="javascript:void(0)" data-theme="yellow-dark" class="yellow-dark-theme">9</a></li>
                                <li><a href="javascript:void(0)" data-theme="red-dark" class="red-dark-theme">10</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple-dark" class="purple-dark-theme">11</a></li>
                                <li><a href="javascript:void(0)" data-theme="black-dark" class="black-dark-theme">12</a></li>
                            </ul>
                            <ul class="m-t-20 chatonline">
                                <li><b><?php echo $lang['INDEX_page_panel_service_item4'];?></b></li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/1.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/2.jpg" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/3.jpg" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/4.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/5.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/6.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- ===== Right-Sidebar-End ===== -->
            </div>
            <!-- /.container-fluid -->
            <footer class="footer t-a-c">
                APILanding.com - The Center of the APIS.<br>Powered by <a href="http://www.sysworld.com.ar">Sysworld Servicios S.A.</a>
            </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="../plugins/components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="js/sidebarmenu.js"></script>
    <!--slimscroll JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="js/custom.js"></script>
    <!--Style Switcher -->
    <script src="../plugins/components/styleswitcher/jQuery.style.switcher.js"></script>

	<script src="../plugins/components/session-timeout/jquery.sessionTimeout.min.js"></script>
	<script>
		var SessionTimeout=function(){var i=function(){$.sessionTimeout({title:"Session Timeout",message:"Su sesi�n ha llegado a un tiempo de inactividad que se cerrar�.",redirUrl:"logout.php",logoutUrl:"logout.php",warnAfter:900000,redirAfter:902000,ignoreUserActivity:!0,countdownMessage:"Cerrando en {timer} segundos.",countdownBar:!0})};return{init:function(){i()}}}();jQuery(document).ready(function(){SessionTimeout.init()});
	</script>

</body>

</html>
