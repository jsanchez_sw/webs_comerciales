<html>
	<head>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<title>EnvioCertificado.com - Envio con Respaldo Notarial.</title>
		<meta name="description" content="Cloudhub is a modern, responsive and easy to customize HTML template, perfectly suited for hosting and technology companies.">
		<meta name="keywords" content="html template, responsive, retina, cloud hosting, technology, startup">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<!-- Icons -->
		<link rel="apple-touch-icon-precomposed" href="img/icons/apple-touch-icon.png">
		<link rel="icon" href="img/icons/favicon.ico">
		<!-- Stylesheets -->
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="css/main.min.css">
	</head>
	<body class="footer-dark">
		<!-- Header -->
		<header id="header" class="header-dynamic header-light header-shadow-scroll">
			<div class="container">
				<a class="logo" href="index.html">
					<img src="img/logos/logo_ern.jpg" alt="">
				</a>
				<nav>
					<ul class="nav-primary">
						<li>
							<a href="index.php">Inicio</a>
						</li>
						<li>
							<a href="empresa.php">Nuestra Empresa</a>
						</li>
						<li>
							<a href="funcionalidades.php">Funcionalidades</a>
						</li>
						<li>
							<a href="faq.php">FAQ</a>
						</li>
						<li>
							<a href="contacto.php">Contacto</a>
						</li>
						<li>
							<a class="button button-secondary" href="panel.php">
								<i class="fa fa-lock icon-left"></i>Acceso al Sistema
							</a>
						</li>
					</ul>
					<ul class="nav-secondary">
						<li>
							<a href="contacto.php"><i class="fa fa-phone icon-left"></i>+54 (011) 5263-2919</a>
						</li>
						<li>
							<a href="contacto.php"><i class="fa fa-comment icon-left"></i>Live Chat</a>
						</li>
						<li>
							<a href="http://help.enviocertificado.com"><i class="fa fa-question-circle icon-left"></i>Tutoriales</a>
						</li>
						<li>
							<a href="http://estado.sysworld.com.ar"><i class="fa fa-check icon-left"></i>Estado de Servicios</a>
						</li>
					</ul>
				</nav>
			</div>
		</header>

		<!-- Content -->
		<section id="content">
			<!-- Content Row -->
			<section class="content-row content-row-color content-row-clouds">
				<div class="container">
					<header class="content-header content-header-small content-header-uppercase">
						<h1>
							Nuestra Empresa
						</h1>
						<p>
							Un aliado estrategico para su negocio.
						</p>
					</header>
				</div>
			</section>
			<section class="content-row">
				<div class="container">
					<p>
						SysWorld Servicios S.A - es una empresa Argentina dedicada a servicios de tecnología, comunicaciones y datacenter. Forma parte de un grupo de negocios conjuntamente con Worldsys S.A. una empresa dedicada al desarrollo de software de aplicación, implantación y servicios de consultoría relacionados con las áreas administrativas y contables de entidades financieras, industriales, comerciales y de servicios líder en el mercado nacional.

						SysWorld Servicios S.A - cuenta con profesionales de amplia experiencia en el segmento de servicios informáticos, redes y telecomunicaciones.

						Alianzas estratégicas con proveedores de hardware y software líderes en el mundo le permiten a nuestra compañía poder dar un servicio de calidad garantizada.
					</p>
					<h3>
						Nuestras Creencias
					</h3>
					<p>
						Un equipo de profesionales altamente capacitados trabaja día a día creyendo que lo que hacemos le simplifica la vida a cientos de miles de personas y cuida el medioambiente.
					</p>
					<h3>
						Cómo lo Hacemos
					</h3>
					<p>
						Nuestros desarrollos son gestados desde el inicio para ser integrados en diferentes organizaciones con la finalidad de dar valor agregado y reducir los tiempos de los procesos.
					</p>
					<h3>
						Qué Hacemos
					</h3>
					<p>
						Simplemente desarrollamos software como servicios para gestión, securización y automatización de procesos de comunicaciones entre una organización y sus clientes.
					</p>
					<h3>
						Innovación
					</h3>
					<p>
						En todo lo que hacemos estamos pensando 4 años más adelante con el fin de anticiparnos a los cambios y poder acompañar a nuestros clientes en los cambios culturales.
					</p>
				</div>
			</section>


			<section class="content-row content-row-gray">
				<div class="container">
					<header class="content-header">
						<h2>
							Nuestras Oficinas y Centros de Procesamiento
						</h2>
					</header>
					<div class="tab-group">
						<div class="tab-item" data-title="Ventas">
							<div class="column-row align-center-bottom">
								<div class="column-33">
									<h3>
										Oficinas <small class="text-color-gray">Comerciales</small>
									</h3>
									<p>
										Bucarelli 2480<br>
										Buenos Aires - Argentina
									</p>
									<p>
										+54 (011) 5263-2919
									</p>
									<p>
										<a href="#sales-us">info@sysworld.com.ar</a><br>
										<a href="#support-us">soporte@sysworld.com.ar</a>
									</p>
									<p>
										<small class="text-color-gray">Consulte a su ejecutivo comercial para coordinar una visita a nuestras instalaciones.</small>
									</p>
								</div>
								<div class="column-66">
									<div class="network-map">
										<ul>
											<li style="top: 86%; left: 31%;">
												<span class="label-top-right">Buenos Aires</span>
											</li>
										</ul>
										<img src="img/layout/map-dark.svg" alt="">
									</div>
								</div>
							</div>
						</div>
						<div class="tab-item" data-title="Infraestructura">
							<div class="column-row align-center-bottom">
								<div class="column-33">
									<h3>
										Centros de Procesamiento y<small class="text-color-gray"> Respaldo</small>
									</h3>
									<p>
										Datacenter Principal<br>
										Distrito Tecnológico Parque Patricios<br>
										Buenos Aires - Argentina
									</p>
									<p>
										<a href="#sales-us">info@sysworld.com.ar</a><br>
										<a href="#support-us">soporte@sysworld.com.ar</a>
									</p>
									<p>
										<small class="text-color-gray">Consulte a su ejecutivo comercial para coordinar una visita a nuestras instalaciones.</small>
									</p>
								</div>
								<div class="column-66">
									<div class="network-map">
										<ul>
											<li style="top: 42%; left: 12.5%;">
												<span class="label-top-left">San Francisco</span>
											</li>
											<li style="top: 44%; left: 20%;">
												<span class="label-bottom-right">Dallas</span>
											</li>
											<li style="top: 40%; left: 26%;">
												<span class="label-top-right">Nueva York</span>
											</li>
											<li style="top: 86%; left: 31%;">
												<span class="label-bottom-right">Buenos Aires</span>
											</li>
											<li style="top: 41%; left: 44.5%;">
												<span class="label-bottom-left">Lisboa</span>
											</li>
											<li style="top: 32%; left: 46.5%;">
												<span class="label-top-left">Londres</span>
											</li>
											<li style="top: 34%; left: 49.5%;">
												<span class="label-bottom-right">Frankfurt</span>
											</li>
											<li style="top: 64%; left: 75%;">
												<span class="label-bottom-left">Singapore</span>
											</li>
											<li style="top: 44%; left: 85%;">
												<span class="label-bottom-right">Tokyo</span>
											</li>
											<li style="top: 83%; left: 89%;">
												<span class="label-top-right">Sydney</span>
											</li>
										</ul>
										<img src="img/layout/map-dark.svg" alt="">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- Content Row -->
			<section class="content-row">
				<div class="container">
					<header class="content-header">
						<h2>
							Los números hablan de nosotros.
						</h2>
						<p>
							Infraestructura, Equipos de Trabajo y Predisposición puesta al servicio de la necesidad de cada uno de nuestros clientes.
						</p>
					</header>
					<div class="column-row align-center-bottom text-align-center">
						<div class="column-33">
							<i class="fa fa-wrench icon-feature"></i>
							<h3>
								<span class="text-color-primary">50+</span> Ejecutivos.
							</h3>
							<p>
								Un equipo de ejecutivos y especialistas a disposición de nuestros clientes para darle valor agregado a sus procesos.
							</p>
						</div>
						<div class="column-33">
							<i class="fa fa-users icon-feature"></i>
							<h3>
								<span class="text-color-primary">100+</span> Clientes
							</h3>
							<p>
								Cientos de clientes hablan por nosotros al momento de comentar como nuestros servicios aplican valor a sus procesos.
							</p>
						</div>
						<div class="column-33">
							<i class="fa fa-server icon-feature"></i>
							<h3>
								<span class="text-color-primary">9.000 TB</span> Data
							</h3>
							<p>
								Mas de nueve mil TB de información ha sido transaccionada en nombre de nuestros clientes durante el año <%=year(date)-1%>.
							</p>
						</div>
					</div>
				</div>
			</section>
			<!-- Content Row -->
			<section class="content-row content-row-gray">
				<div class="container">
					<header class="content-header">
						<h2>
							Casos de Éxito
						</h2>
						<p>
							Más de 200 Empresas de diferentes rubros nos confian sus enváos y transacciones.
						</p>
					</header>
					<div class="column-row align-center-bottom">
						<div class="column-33">
							<div class="testimonial">
								<p class="testimonial-content zoom">
									Se implemento un workflow con validaciones, generación de archivos PDFs y envíos certificados para todos los procesos del plan de ahorro.
								</p>
								<p class="testimonial-author">
									Autoahorro VW<br>
									<small>&nbsp;&nbsp;Envío Certificado</small><br>

								</p>
							</div>
						</div>
						<div class="column-33">
							<div class="testimonial">
								<p class="testimonial-content zoom">
									Se implemento el workflow de certificaciones notariales para los procesos de ordenes de compras de proveedores.
								</p>
								<p class="testimonial-author">
									Banco Frances<br>
								    <small>Envío con Respaldo Notarial</small>
																	</p>
							</div>
						</div>
						<div class="column-33">
							<div class="testimonial">
								<p class="testimonial-content zoom">
									Se implemento un proceso de notificaciones electrónicas con acuse de recibo y respaldo notarial para las cartas de arribos del negocio.
								</p>
								<p class="testimonial-author">
									Fiat Plan<br>
									<small>&nbsp;&nbsp;&nbsp;ERN + TRN</small><br>
																	</p>
							</div>
						</div>
					</div>
				</div>
					<style type="text/css">
								    .zoom{
								        /* Aumentamos la anchura y altura durante 2 segundos */
								        transition: width 1s, height 1s, transform 1s;
								        -moz-transition: width 1s, height 1s, -moz-transform 1s;
								        -webkit-transition: width 1s, height 1s, -webkit-transform 1s;
								        -o-transition: width 1s, height 1s,-o-transform 1s;
								    }
								    .zoom:hover{
								        /* tranformamos el elemento al pasar el mouse por encima al doble de
								           su tamaño con scale(1.2). */
								        transform : scale(1.2);
								        -moz-transform : scale(1.2);      /* Firefox */
								        -webkit-transform : scale(1.2);   /* Chrome - Safari */
								        -o-transform : scale(1.2);        /* Opera */
								    }
					</style>
			</section>
			<!-- Content Row -->
			<section class="content-row content-row-color">
				<div class="container">
					<header class="content-header">
						<h2>
							¿Interesado en nuestra empresa?
						</h2>
						<p>
							Acceda al sitio web corporativo para conocer mas detalles de nuestra empresa.<br><br>
							<a class="button button-secondary" href="http://www.sysworld.com.ar" target="_blank">
								<i class="fa fa-globe icon-left"></i>Sitio Web Corporativo
							</a>
						</p>
					</header>
				</div>
			</section>
		</section>
		<!-- Footer -->
		<footer id="footer">
			<section class="footer-primary">
				<div class="container">
					<div class="column-row">
						<div class="column-33">
							<h5>
								Sysworld Servicios S.A.
							</h5>
							<p>
								EnvioCertificado.com | EnvioLegal.com | CertiSend.com |Envio de Email Certificados | Envios con Respaldo Notarial | Certificaciones en la BlockChain | SMS Certificados | Transacciones Certificadas | Auditoria de Transacciones
							</p>
						</div>
						<div class="column-66">
							<div class="column-row align-right-top">
								<div class="column-25">
									<h5>
										Conectarse
									</h5>
									<ul class="list-style-icon">
										<li>
											<a href="https://www.facebook.com/SysWorld-Servicios-SA-323344231089747" target="_blank"><i class="fa fa-facebook"></i>Facebook</a>
										</li>
										<li>
											<a href="https://twitter.com/Sysworldsa" target="_blank"><i class="fa fa-twitter"></i>Twitter</a>
										</li>
										<li>
											<a href="contacto.php"><i class="fa fa-github"></i>Contacto</a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar/v2/sw/contacto.php" target="_blank"><i class="fa fa-xing"></i>Live Chat</a>
										</li>
									</ul>
								</div>
								<div class="column-25">
									<h5>
																			Productos
																		</h5>
																		<ul>
																			<li>
																				<a href="http://sysworld.com.ar/v2/sw/index.php">EnvioCertificado</a>
																			</li>
																			<li>
																				<a href="">ERN</a>
																			</li>
																			<li>
																				<a href="">TRN</a>
																			</li>
																			<li>
																				<a href="http://www.centrodevalidaciones.com" target="_blank">Validadores</a>
																			</li>
																			<li>
																				<a href="http://www.omnicrm.cloud" target="_blank">OmniCRM.cloud</a>
																			</li>
																			<li>
																				<a href="http://cloud.sysworld.com.ar/" target="_blank">Portal</a>
										</li>
									</ul>
								</div>
								<div class="column-25">
									<h5>
										Recursos
									</h5>
									<ul>
										<li>
											<a href="funcionalidades.php">Funcionalidades</a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar/v2/sw/empresa.php?f=network" target="_blank">Network</a>
										</li>
										<li>
											<a href="http://sysworld.com.ar/v2/sw/empresa.php?f=politicas">Respaldo Legales</a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar/v2/sw/empresa.php?f=partners" target="_blank">Partners</a>
										</li>
									</ul>
								</div>
								<div class="column-flex">
									<h5>
										Empresa
									</h5>
									<ul>
										<li>
											<a href="empresa.php">Nosotros</a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar" target="_blank">Sitio Corporativo</a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar/v2/sw/empresa_dist.php" target="_blank">Distribuidores</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="footer-secondary">
				<div class="container">
					<p>
						Copyright 2006 -<?php echo date("Y"); ?> &copy; Sysworld Servicios S.A. Todos los derechos reservados.<br>
						Powered by <a href="http://www.sysworld.com.ar">Sysworld Servicios S.A.</a>
					</p>
				</div>
			</section>
		</footer>
		<!-- Scripts -->

		<script type="text/javascript">
				var LHCChatOptions = {};
				LHCChatOptions.opt = {widget_height:340,widget_width:300,popup_height:520,popup_width:500};
				(function() {
				var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
				var referrer = (document.referrer) ? encodeURIComponent(document.referrer.substr(document.referrer.indexOf('://')+1)) : '';
				var location  = (document.location) ? encodeURIComponent(window.location.href.substring(window.location.protocol.length)) : '';
				po.src = '//livechat.enviocertificado.com/panel/index.php/esp/chat/getstatus/(click)/internal/(position)/bottom_right/(ma)/br/(top)/350/(units)/pixels/(leaveamessage)/true/(department)/2/5/(theme)/4?r='+referrer+'&l='+location;
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
				})();
</script>

		<script src="js/jquery.min.js"></script>
		<script src="js/headroom.min.js"></script>
		<script src="js/js.cookie.min.js"></script>
		<script src="js/imagesloaded.min.js"></script>
		<script src="js/bricks.min.js"></script>
		<script src="js/main.min.js"></script>
	</body>
</html>
