
                    <ul id="side-menu">
						<li>
							<a href="index.php" aria-expanded="false"><i class="icon-screen-desktop fa-fw"></i> <span class="hide-menu"><?php echo $lang['MENU_DASHBOARD'];?></span></a>
						</li>
                        <li>
                            <a class="active waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-list fa-fw"></i> <span class="hide-menu"><?php echo $lang['MENU_APIS'];?><span class="label label-rounded label-info pull-right"></span></span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li> <a href="apislist.php?t=cart"><?php echo $lang['MENU_APIS_APILIST'];?></a> </li>
                                <li> <a href="serverlist.php?t=cart"><?php echo $lang['MENU_APIS_SERVERS'];?></a> </li>
                                <li> <a href="apiskey.php?t=cart"><?php echo $lang['MENU_APIS_APISKEY'];?></a> </li>
                                <li> <a href="apirun.php?t=cart"><?php echo $lang['MENU_APIS_RUN'];?></a> </li>
                                <li> <a href="payments.php?t=cart"><?php echo $lang['MENU_APIS_CONSUMPTION'];?></a> </li>
                            </ul>
                        </li>
                        <li>
                            <a class="active waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-folder-alt fa-fw"></i> <span class="hide-menu"><?php echo $lang['MENU_APIS_FILE'];?><span class="label label-rounded label-info pull-right"></span></span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li> <a href="apislist.php?t=file"><?php echo $lang['MENU_APIS_APILIST'];?></a> </li>
                                <li> <a href="apiskey.php?t=file"><?php echo $lang['MENU_APIS_APISKEY'];?></a> </li>
                                <li> <a href="apimix.php"><?php echo $lang['MENU_APIS_APISFILE_MIX'];?></a> </li>
                            </ul>
                        </li>
                        <li>
                            <a class="active waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-layers fa-fw"></i> <span class="hide-menu"><?php echo $lang['MENU_APIS_DB'];?><span class="label label-rounded label-info pull-right"></span></span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li> <a href="apislist.php?t=db"><?php echo $lang['MENU_APIS_APILIST'];?></a> </li>
                                <li> <a href="apiskey.php?t=db"><?php echo $lang['MENU_APIS_APISKEY'];?></a> </li>
                            </ul>
                        </li>
                        <li>
                            <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-basket fa-fw"></i> <span class="hide-menu"><?php echo $lang['MENU_ADMINISTRATION'];?></span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li> <a href="apilog.php"><?php echo $lang['MENU_ADMINISTRATION_LOG'];?></a> </li>
                                <li> <a href="changesusc.php"><?php echo $lang['MENU_ADMINISTRATION_SUSCRIPTION'];?></a> </li>
                                <li> <a href="changesusc.php"><?php echo $lang['MENU_ADMINISTRATION_CHANGEPASS'];?></a> </li>
                                <li> <a href="http://erp.sysworld.com.ar/app/" target="_blank"><?php echo $lang['MENU_ADMINISTRATION_INVOICES'];?></a> </li>
                            </ul>
                        </li>
                        <li>
                            <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-support fa-fw"></i> <span class="hide-menu"><?php echo $lang['MENU_SUPPORT'];?></span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li> <a href="ticketportal.php"><?php echo $lang['MENU_SUPPORT_TUTORIALS'];?></a> </li>
                                <li> <a href="livechat.php"><?php echo $lang['MENU_SUPPORT_CHAT'];?></a> </li>
                                <li> <a href="ticketportal.php"><?php echo $lang['MENU_SUPPORT_TICKET'];?></a> </li>
                            </ul>
                        </li>
                        <li>
                            <a href="../index.php" aria-expanded="false"><i class="icon-eye fa-fw"></i> <span class="hide-menu"><?php echo $lang['MENU_Header_CoorpSite'];?></span></a>
                        </li>
                        <li>
                            <a href="http://www.sysworld.com.ar" target="_blank" aria-expanded="false"><i class="icon-globe fa-fw"></i> <span class="hide-menu"><?php echo $lang['MENU_COMERCIAL'];?></span></a>
                        </li>
                        <li>
                            <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-user fa-fw"></i> <span class="hide-menu"><?php echo $lang['MENU_ACOUNT'];?></span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li> <a href="recoverpw.php"><?php echo $lang['MENU_ACOUNT_CHANGE'];?></a> </li>
                                <li> <a href="logout.php"><?php echo $lang['MENU_ACOUNT_CLOSE'];?></a> </li>
                            </ul>
                        </li>

                    </ul>