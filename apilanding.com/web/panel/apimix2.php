<?php
    include '../functions/db.php';
    include_once '../functions/language.php';
	include 'session.php';


	$usuario = $_SESSION['apilanding_user_email'];


	$codigo = md5(rand(0,9999) . date('d-m-Y h:i:s') . session_id());


	$idapi = $_POST['api'];
	$idfile = $_POST['file'];




?>


<!DOCTYPE html>
<html lang="<?php echo $lang['SITE_lang'];?>">

<head>
    <meta charset="iso-8859-1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="<?php echo $lang['SITE_keywords'];?>">
    <meta name="description" content="<?php echo $lang['SITE_desc'];?>">
    <meta name="author" content="Sysworld Servicios S.A.">
    <link rel="icon" type="image/x-icon" href="../images/favicon.ico" />
    <title><?php echo $lang['SITE_title'];?></title>
    <!-- ===== Bootstrap CSS ===== -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- ===== Plugin CSS ===== -->
    <link href="../plugins/components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <!-- ===== Animation CSS ===== -->
    <link href="css/animate.css" rel="stylesheet">
    <!-- ===== Custom CSS ===== -->
    <link href="css/style.css" rel="stylesheet">
    <!-- ===== Color CSS ===== -->
    <link href="css/colors/default.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="mini-sidebar">
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <!-- ===== Top-Navigation ===== -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
                <a class="navbar-toggle font-20 hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse">
                    <i class="fa fa-bars"></i>
                </a>
                <div class="top-left-part">
                    <a class="logo" href="index.php">
                        <span>&nbsp;</span>
                        <b>
                             <img src="../images/panel_api.png" width="240px" height="55px" />
                        </b>
                        <span>&nbsp;</span>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-left hidden-xs">
                    <li>
                        <a href="javascript:void(0)" class="sidebartoggler font-20 waves-effect waves-light"><i class="icon-arrow-left-circle"></i></a>
                    </li>
                    <li>
                        <form role="search" class="app-search hidden-xs" action="../apis.php" method="get">
                            <i class="icon-magnifier"></i>
                            <input type="text" placeholder="<?php echo $lang['APILIST_page_search'];?>" class="form-control">
                        </form>
                    </li>
                </ul>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle waves-effect waves-light font-20" data-toggle="dropdown" href="javascript:void(0);">
                            <i class="icon-speech"></i>
                            <span class="badge badge-xs badge-danger"></span>
                        </a>
                        <ul class="dropdown-menu mailbox animated bounceInDown">
                            <li>
                                <div class="drop-title"><?php echo $lang['INDEX_page_messages'];?></div>
                            </li>
                            <li>
                                <div class="message-center">
                                </div>
                            </li>
                            <li>
                                <a class="text-center" href="javascript:void(0);">
                                    <strong><?php echo $lang['INDEX_page_notifications'];?></strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle waves-effect waves-light font-20" data-toggle="dropdown" href="javascript:void(0);">
                            <i class="icon-calender"></i>
                            <span class="badge badge-xs badge-danger"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-tasks animated slideInUp">
                        </ul>
                    </li>
                    <li class="right-side-toggle">
                    </li>
                </ul>
            </div>
        </nav>
        <!-- ===== Top-Navigation-End ===== -->
        <!-- ===== Left-Sidebar ===== -->
        <aside class="sidebar" role="navigation">
            <div class="scroll-sidebar">
                <div class="user-profile">
                    <div class="dropdown user-pro-body">
                        <p class="profile-text m-t-15 font-16"><a href="javascript:void(0);"> Panel</a></p>
                    </div>
                </div>
                <nav class="sidebar-nav">
					<?php include 'menu.php';?>
                </nav>
                <div class="p-30">
                    <span class="hide-menu">
                        <a href="suscriptions.php" class="btn btn-success"><?php echo $lang['MENU_menu_bottom1'];?></a>
                        <a href="../index.php" class="btn btn-default m-t-15"><?php echo $lang['MENU_menu_bottom2'];?></a>
                    </span>
                </div>
            </div>
        </aside>
        <!-- ===== Left-Sidebar-End ===== -->
        <!-- Page Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                <!-- /row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">


							<!-- sample modal content -->
                            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                                            <h4 class="modal-title" id="myLargeModalLabel"><div id="divtitlekeys">&nbsp;</div></h4> </div>
                                        <div class="modal-body" id="divkeys">

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

							<div class="row">
								<div class="col-md-12">
									<div class="panel panel-info">
										<div class="panel-heading"> Procesar Archivo contra API</div>
										<div class="panel-wrapper collapse in" aria-expanded="true">
											<div class="panel-body">
												<form action="apimix3.php" method="post">
													<input type="hidden" id="api" name="api" value="<?php echo $idapi;?>">
													<input type="hidden" id="file" name="file" value="<?php echo $idfile;?>">
													<div class="form-body">
														<h3 class="box-title">Configuraci�n de Relaciones:</h3>
														<hr>
															<?php
																$sql = "SELECT * from web_apis_parametros WHERE idapi = " . $idapi;
																$resultparametros = $conn->query($sql);

																if ($resultparametros->num_rows > 0) {
																	while($row = $resultparametros->fetch_assoc()) {
															?>
																		<div class="row">
																			<div class="col-md-6">
																				<div class="form-group">
																					<label class="control-label"><b><?php echo strtoupper($row['parametro']);?>:</b></label>
																					<select class="form-control" id="<?php echo $row['parametro'];?>" name="<?php echo $row['parametro'];?>">
																					<?php
																						$sql = "SELECT * from web_apis_parametros WHERE idapi = " . $idfile;
																						$result = $conn->query($sql);
																						if ($result->num_rows > 0) {
																							while($row2 = $result->fetch_assoc()) {
																								if (strlen($row2['parametro']) > 0){
																					?>
																									<option value="<?php echo $row2['parametro'];?>"><?php echo $row2['parametro'];?></option>
																					<?php
																								}
																							}
																						}

																						$sql = "SELECT * from web_apis_campos_respuesta WHERE idapi = " . $idfile;
																						$resultrespuestas = $conn->query($sql);

																						if ($resultrespuestas->num_rows > 0) {
																							while($row2 = $resultrespuestas->fetch_assoc()) {
																								if (strlen($row2['campo']) > 0){
																					?>
																									<option value="<?php echo $row2['campo'];?>"><?php echo $row2['campo'];?></option>
																					<?php
																								}
																							}
																						}
																					?>
																					</select>
																					<span class="help-block"><?php echo $row['descripcion'];?></span>
																				</div>
																			</div>
																		</div>
															<?php
																	}
																}
															?>
													</div>
													<div class="form-actions">
														<button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Continuar</button>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>

                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- ===== Right-Sidebar ===== -->
                <div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"><?php echo $lang['INDEX_page_panel_service'];?> <span><i class="icon-close right-side-toggler"></i></span> </div>
                        <div class="r-panel-body">
                            <ul class="hidden-xs">
                                <li><b><?php echo $lang['INDEX_page_panel_service_item1'];?></b></li>
                                <li>
                                    <div class="checkbox checkbox-danger">
                                        <input id="headcheck" type="checkbox" class="fxhdr">
                                        <label for="headcheck"> Fix Header </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox checkbox-warning">
                                        <input id="sidecheck" type="checkbox" class="fxsdr">
                                        <label for="sidecheck"> Fix Sidebar </label>
                                    </div>
                                </li>
                            </ul>
                            <ul id="themecolors" class="m-t-20">
                                <li><b><?php echo $lang['INDEX_page_panel_service_item2'];?></b></li>
                                <li><a href="javascript:void(0)" data-theme="default" class="default-theme working">1</a></li>
                                <li><a href="javascript:void(0)" data-theme="green" class="green-theme">2</a></li>
                                <li><a href="javascript:void(0)" data-theme="yellow" class="yellow-theme">3</a></li>
                                <li><a href="javascript:void(0)" data-theme="red" class="red-theme">4</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple" class="purple-theme">5</a></li>
                                <li><a href="javascript:void(0)" data-theme="black" class="black-theme">6</a></li>
                                <li class="db"><b><?php echo $lang['INDEX_page_panel_service_item3'];?></b></li>
                                <li><a href="javascript:void(0)" data-theme="default-dark" class="default-dark-theme">7</a></li>
                                <li><a href="javascript:void(0)" data-theme="green-dark" class="green-dark-theme">8</a></li>
                                <li><a href="javascript:void(0)" data-theme="yellow-dark" class="yellow-dark-theme">9</a></li>
                                <li><a href="javascript:void(0)" data-theme="red-dark" class="red-dark-theme">10</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple-dark" class="purple-dark-theme">11</a></li>
                                <li><a href="javascript:void(0)" data-theme="black-dark" class="black-dark-theme">12</a></li>
                            </ul>
                            <ul class="m-t-20 chatonline">
                                <li><b><?php echo $lang['INDEX_page_panel_service_item4'];?></b></li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/1.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/2.jpg" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/3.jpg" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/4.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/5.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/6.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- ===== Right-Sidebar-End ===== -->
            </div>
            <!-- /.container-fluid -->
            <footer class="footer t-a-c">
                APILanding.com - The Center of the APIS.<br>Powered by <a href="http://www.sysworld.com.ar">Sysworld Servicios S.A.</a>
            </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="../plugins/components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="js/sidebarmenu.js"></script>
    <!--slimscroll JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="js/custom.js"></script>
    <script src="../plugins/components/datatables/jquery.dataTables.min.js"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <!-- end - This is for export functionality only -->
    <script>
    $(function() {
        $('#myTable').DataTable();

        var table = $('#example').DataTable({
            "columnDefs": [{
                "visible": false,
                "targets": 2
            }],
            "order": [
                [2, 'asc']
            ],
            "displayLength": 25,
            "drawCallback": function(settings) {
                var api = this.api();
                var rows = api.rows({
                    page: 'current'
                }).nodes();
                var last = null;
                api.column(2, {
                    page: 'current'
                }).data().each(function(group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                        last = group;
                    }
                });
            }
        });
        // Order by the grouping
        $('#example tbody').on('click', 'tr.group', function() {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                table.order([2, 'desc']).draw();
            } else {
                table.order([2, 'asc']).draw();
            }
        });
    });
    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>

    <script>
    	function obtenerapikeys(context, id){
			$(".bs-example-modal-lg").modal()
    		var divkeys = document.getElementById('divtitlekeys');
    		divkeys.innerHTML = "API: " + context;

			document.getElementById("divkeys").innerHTML = '<h4>Obteniendo Claves... Por favor espere...</h4>';

			var req = new XMLHttpRequest();
			req.open('GET', 'getapikeys.php?idapi=' + id, true);
			req.onreadystatechange = function (aEvt) {
			  if (req.readyState == 4) {
				 if(req.status == 200)
				  document.getElementById("divkeys").innerHTML = req.responseText;
				 else
				  document.getElementById("divkeys").innerHTML = "api error";
			  }
			};
			req.send(null);


    	}
    </script>



    <!--Style Switcher -->
    <script src="../plugins/components/styleswitcher/jQuery.style.switcher.js"></script>
</body>

</html>
