<?php
    include '../functions/db.php';
    include_once '../functions/language.php';
	include 'session.php';

	$usuario = $_SESSION['apilanding_user_email'];


	$idapi = $_POST['api'];
	$idfile = $_POST['file'];

	$encabezado = '';

	$sql = "SELECT * from web_apis_data_campos WHERE idapi = " . $idfile . " order by orden ";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			if (strlen($row['campo']) > 0){
				$encabezado = $encabezado . '<th>FILE_' . $row['campo'] . '</th>';
			}
		}
	}

	$sql = "SELECT * from web_apis_campos_respuesta WHERE idapi = " . $idapi;
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			if (strlen($row['campo']) > 0){
				$encabezado = $encabezado . '<th>API_' . $row['campo'] . '</th>';
			}
		}
	}



	$table = '';

    $sql = "SELECT * FROM web_apis WHERE id = " . $idfile;
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$tabla = 'api_data_sw_';
			if (isset($row['propietario'])){
				if ($row['propietario'] != ''){
					$tabla = 'api_data_cli_';
				}
			}
			$tabla = $tabla . $row['id'] . "_" . $row['codigo'];
			$contexto = $row['contexto'];
		}
	}else{
		echo "Invalid API." . $sql;
		die();
	}



    $sql = "SELECT A.contexto, B.apitoken, C.susctoken FROM web_apis A "
    . " INNER JOIN web_suscripciones_keys B "
    . " ON A.id = B.idapi "
    . " INNER JOIN web_suscripciones C "
    . " ON B.idsuscripcion = C.id "
    . " WHERE A.id = " . $idapi
    . " AND B.usuario = '" . $usuario . "'";


	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$contexto = $row['contexto'];
			$susctoken = $row['susctoken'];
			$apitoken = $row['apitoken'];
		}
	}else{
		echo "Invalid API." . $sql;
		die();
	}

	$urlapi = 'http://cont1-virtual1.certisend.com/web/container/api/v1/' . $contexto . "?token-susc=" . $susctoken . "&token-api=" . $apitoken;

	//Parametros api
	$parametrosapi = '';
	$sql = "SELECT * from web_apis_parametros WHERE idapi = " . $idapi;
	$result2 = $conn->query($sql);
	if ($result2->num_rows > 0) {
		while($row2 = $result2->fetch_assoc()) {
			$parametrosapi = $parametrosapi . $row2['parametro'] . ';';
		}
	}
	$arrparametrosapi = explode(";", $parametrosapi);

	//campos respuestas api
	$camposrtaapi = '';
	$sql = "SELECT * from web_apis_campos_respuesta WHERE idapi = " . $idapi;
	$result2 = $conn->query($sql);
	if ($result2->num_rows > 0) {
		while($row2 = $result2->fetch_assoc()) {
			$camposrtaapi = $camposrtaapi . $row2['campo'] . ';';
		}
	}
	$arrcamposrtaapi = explode(";", $camposrtaapi);



	//Campos file
	$campos = '';
	$sql = "SELECT * from web_apis_data_campos WHERE idapi = " . $idfile . " order by orden ";
	$result2 = $conn->query($sql);
	if ($result2->num_rows > 0) {
		while($row2 = $result2->fetch_assoc()) {
			$campos = $campos . $row2['campo'] . ';';
		}
	}
	$arrcampos = explode(";", $campos);


	$sql = "SELECT * FROM " . $tabla . " limit 10";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$table = $table . '<tr>';
			foreach ($arrcampos as $campo) {
				if (strlen($campo) > 0){
					$table = $table . '<td>' . $row[$campo] . '</td>';
				}
			}

			$urltempapi = $urlapi;
			foreach ($arrparametrosapi as $parametro){
				if (strlen($parametro) > 0){
					$urltempapi = $urltempapi . "&" . $parametro . "=" . $row[$_POST[$parametro]];
				}
			}
			$jsonrta = file_get_contents($urltempapi);
			$arrrta = json_decode($jsonrta, true);

			foreach ($arrcamposrtaapi as $campo) {
				if (strlen($campo) > 0){
					$table = $table . '<td>' . $arrrta[$campo] . '</td>';
				}
			}



			$table = $table . '</tr>';
		}
	}




?>


<!DOCTYPE html>
<html lang="<?php echo $lang['SITE_lang'];?>">

<head>
    <meta charset="iso-8859-1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="<?php echo $lang['SITE_keywords'];?>">
    <meta name="description" content="<?php echo $lang['SITE_desc'];?>">
    <meta name="author" content="Sysworld Servicios S.A.">
    <link rel="icon" type="image/x-icon" href="../images/favicon.ico" />
    <title><?php echo $lang['SITE_title'];?></title>
    <!-- ===== Bootstrap CSS ===== -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- ===== Plugin CSS ===== -->
    <link href="../plugins/components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <!-- ===== Animation CSS ===== -->
    <link href="css/animate.css" rel="stylesheet">
    <!-- ===== Custom CSS ===== -->
    <link href="css/style.css" rel="stylesheet">
    <!-- ===== Color CSS ===== -->
    <link href="css/colors/default.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="mini-sidebar">
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <!-- ===== Top-Navigation ===== -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
                <a class="navbar-toggle font-20 hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse">
                    <i class="fa fa-bars"></i>
                </a>
                <div class="top-left-part">
                    <a class="logo" href="index.php">
                        <span>&nbsp;</span>
                        <b>
                             <img src="../images/panel_api.png" width="240px" height="55px" />
                        </b>
                        <span>&nbsp;</span>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-left hidden-xs">
                    <li>
                        <a href="javascript:void(0)" class="sidebartoggler font-20 waves-effect waves-light"><i class="icon-arrow-left-circle"></i></a>
                    </li>
                    <li>
                        <form role="search" class="app-search hidden-xs" action="../apis.php" method="get">
                            <i class="icon-magnifier"></i>
                            <input type="text" placeholder="<?php echo $lang['APILIST_page_search'];?>" class="form-control">
                        </form>
                    </li>
                </ul>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle waves-effect waves-light font-20" data-toggle="dropdown" href="javascript:void(0);">
                            <i class="icon-speech"></i>
                            <span class="badge badge-xs badge-danger"></span>
                        </a>
                        <ul class="dropdown-menu mailbox animated bounceInDown">
                            <li>
                                <div class="drop-title"><?php echo $lang['INDEX_page_messages'];?></div>
                            </li>
                            <li>
                                <div class="message-center">
                                </div>
                            </li>
                            <li>
                                <a class="text-center" href="javascript:void(0);">
                                    <strong><?php echo $lang['INDEX_page_notifications'];?></strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle waves-effect waves-light font-20" data-toggle="dropdown" href="javascript:void(0);">
                            <i class="icon-calender"></i>
                            <span class="badge badge-xs badge-danger"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-tasks animated slideInUp">
                        </ul>
                    </li>
                    <li class="right-side-toggle">
                    </li>
                </ul>
            </div>
        </nav>
        <!-- ===== Top-Navigation-End ===== -->
        <!-- ===== Left-Sidebar ===== -->
        <aside class="sidebar" role="navigation">
            <div class="scroll-sidebar">
                <div class="user-profile">
                    <div class="dropdown user-pro-body">
                        <p class="profile-text m-t-15 font-16"><a href="javascript:void(0);"> Panel</a></p>
                    </div>
                </div>
                <nav class="sidebar-nav">
					<?php include 'menu.php';?>
                </nav>
                <div class="p-30">
                    <span class="hide-menu">
                        <a href="suscriptions.php" class="btn btn-success"><?php echo $lang['MENU_menu_bottom1'];?></a>
                        <a href="../index.php" class="btn btn-default m-t-15"><?php echo $lang['MENU_menu_bottom2'];?></a>
                    </span>
                </div>
            </div>
        </aside>
        <!-- ===== Left-Sidebar-End ===== -->
        <!-- Page Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                <!-- /row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">


							<!-- sample modal content -->
                            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                                            <h4 class="modal-title" id="myLargeModalLabel"><div id="divtitlekeys">&nbsp;</div></h4> </div>
                                        <div class="modal-body" id="divkeys">

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

							<h3 class="box-title m-b-0">Procesamiento de Archivo con APIS</h3>
                            <div class="table-responsive">
								<table id="example23" class="display nowrap" cellspacing="0" width="100%">
									<thead>
										<tr>
											<?php echo $encabezado;?>
											<?php echo $table;?>
										</tr>
									</thead>
									<tbody id="divtabla">
									</tbody>
								</table>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- ===== Right-Sidebar ===== -->
                <div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"><?php echo $lang['INDEX_page_panel_service'];?> <span><i class="icon-close right-side-toggler"></i></span> </div>
                        <div class="r-panel-body">
                            <ul class="hidden-xs">
                                <li><b><?php echo $lang['INDEX_page_panel_service_item1'];?></b></li>
                                <li>
                                    <div class="checkbox checkbox-danger">
                                        <input id="headcheck" type="checkbox" class="fxhdr">
                                        <label for="headcheck"> Fix Header </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox checkbox-warning">
                                        <input id="sidecheck" type="checkbox" class="fxsdr">
                                        <label for="sidecheck"> Fix Sidebar </label>
                                    </div>
                                </li>
                            </ul>
                            <ul id="themecolors" class="m-t-20">
                                <li><b><?php echo $lang['INDEX_page_panel_service_item2'];?></b></li>
                                <li><a href="javascript:void(0)" data-theme="default" class="default-theme working">1</a></li>
                                <li><a href="javascript:void(0)" data-theme="green" class="green-theme">2</a></li>
                                <li><a href="javascript:void(0)" data-theme="yellow" class="yellow-theme">3</a></li>
                                <li><a href="javascript:void(0)" data-theme="red" class="red-theme">4</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple" class="purple-theme">5</a></li>
                                <li><a href="javascript:void(0)" data-theme="black" class="black-theme">6</a></li>
                                <li class="db"><b><?php echo $lang['INDEX_page_panel_service_item3'];?></b></li>
                                <li><a href="javascript:void(0)" data-theme="default-dark" class="default-dark-theme">7</a></li>
                                <li><a href="javascript:void(0)" data-theme="green-dark" class="green-dark-theme">8</a></li>
                                <li><a href="javascript:void(0)" data-theme="yellow-dark" class="yellow-dark-theme">9</a></li>
                                <li><a href="javascript:void(0)" data-theme="red-dark" class="red-dark-theme">10</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple-dark" class="purple-dark-theme">11</a></li>
                                <li><a href="javascript:void(0)" data-theme="black-dark" class="black-dark-theme">12</a></li>
                            </ul>
                            <ul class="m-t-20 chatonline">
                                <li><b><?php echo $lang['INDEX_page_panel_service_item4'];?></b></li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/1.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/2.jpg" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/3.jpg" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/4.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/5.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/6.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- ===== Right-Sidebar-End ===== -->
            </div>
            <!-- /.container-fluid -->
            <footer class="footer t-a-c">
                APILanding.com - The Center of the APIS.<br>Powered by <a href="http://www.sysworld.com.ar">Sysworld Servicios S.A.</a>
            </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="../plugins/components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="js/sidebarmenu.js"></script>
    <!--slimscroll JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="js/custom.js"></script>
    <script src="../plugins/components/datatables/jquery.dataTables.min.js"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <!-- end - This is for export functionality only -->
    <script>
    $(function() {
        $('#myTable').DataTable();

        var table = $('#example').DataTable({
            "columnDefs": [{
                "visible": false,
                "targets": 2
            }],
            "order": [
                [2, 'asc']
            ],
            "displayLength": 25,
            "drawCallback": function(settings) {
                var api = this.api();
                var rows = api.rows({
                    page: 'current'
                }).nodes();
                var last = null;
                api.column(2, {
                    page: 'current'
                }).data().each(function(group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                        last = group;
                    }
                });
            }
        });
        // Order by the grouping
        $('#example tbody').on('click', 'tr.group', function() {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                table.order([2, 'desc']).draw();
            } else {
                table.order([2, 'asc']).draw();
            }
        });
    });
    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>



    <!--Style Switcher -->
    <script src="../plugins/components/styleswitcher/jQuery.style.switcher.js"></script>

	<script src="../plugins/components/session-timeout/jquery.sessionTimeout.min.js"></script>
	<script>
		var SessionTimeout=function(){var i=function(){$.sessionTimeout({title:"Session Timeout",message:"Su sesi�n ha llegado a un tiempo de inactividad que se cerrar�.",redirUrl:"logout.php",logoutUrl:"logout.php",warnAfter:900000,redirAfter:902000,ignoreUserActivity:!0,countdownMessage:"Cerrando en {timer} segundos.",countdownBar:!0})};return{init:function(){i()}}}();jQuery(document).ready(function(){SessionTimeout.init()});
	</script>



</body>

</html>
