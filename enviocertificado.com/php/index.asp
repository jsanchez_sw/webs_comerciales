<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="iso-8859-1">
		<title>EnvioCertificado.com - Envio con Respaldo Notarial.</title>
		<meta name="description" content="Cloudhub is a modern, responsive and easy to customize HTML template, perfectly suited for hosting and technology companies.">
		<meta name="keywords" content="html template, responsive, retina, cloud hosting, technology, startup">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<!-- Icons -->
		<link rel="apple-touch-icon-precomposed" href="img/icons/apple-touch-icon.png">
		<link rel="icon" href="img/icons/favicon.ico">
		<!-- Stylesheets -->
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="css/main.min.css">
	</head>
	<body class="footer-dark">
		<!-- Header -->
		<header id="header" class="header-dynamic header-light header-shadow-scroll">
			<div class="container">
				<a class="logo" href="index.html">
					<img src="img/logos/logo_ern.jpg" alt="">
				</a>
				<nav>
					<ul class="nav-primary">
						<li>
							<a href="index.asp">Inicio</a>
						</li>
						<li>
							<a href="empresa.asp">Nuestra Empresa</a>
						</li>
						<li>
							<a href="funcionalidades.asp">Funcionalidades</a>
						</li>
						<li>
							<a href="faq.asp">FAQ</a>
						</li>
						<li>
							<a href="contacto.asp">Contacto</a>
						</li>
						<li>
							<a class="button button-secondary" href="https://web.certisend.com/panel">
								<i class="fa fa-lock icon-left"></i>Acceso al Sistema
							</a>
						</li>
					</ul>
					<ul class="nav-secondary">
						<li>
							<a href="contacto.asp"><i class="fa fa-phone icon-left"></i>+54 (011) 5263-2919</a>
						</li>
						<li>
							<a href="contacto.asp"><i class="fa fa-comment icon-left"></i>Live Chat</a>
						</li>
						<li>
							<a href="http://help.enviocertificado.com"><i class="fa fa-question-circle icon-left"></i>Tutoriales</a>
						</li>
						<li>
							<a href="http://estado.sysworld.com.ar"><i class="fa fa-check icon-left"></i>Estado de Servicios</a>
						</li>
					</ul>
				</nav>
			</div>
		</header>
		<!-- Notification -->
		<section id="notification" data-dismissible="true" data-title="" data-expires="">
			<div class="container">
				<p>
					Promoci�n valida por tiempo indeterminado. 30% de descuento en todos los servicios durante el primer a�o de contrataci�n. <a class="text-margin-left" href="contacto.asp">Contratar<i class="fa fa-angle-right icon-right"></i></a>
				</p>
			</div>
		</section>
		<!-- Content -->
		<section id="content">
			<!-- Content Row -->
			<section class="content-row content-row-color content-row-clouds">
				<div class="content-slider animate-container-slide" data-nav="true" data-rotation="5">
					<a class="slide" data-title="EnvioCertificado" href="products-cloud-hosting.html">
						<div class="container">
							<header class="content-header content-header-large content-header-uppercase">
								<h1>
									<mark>Env�o</mark>-Certificado
								</h1>
								<p>
									Certifique sus comunicaciones electr�nicas y obtenga respaldo de la BlockChain. <span class="text-color-secondary">Desde $ARS 0,278/Env�o</span>
								</p>
							</header>
							<img src="uploads/server-shared.png" alt="">
						</div>
					</a>
					<a class="slide" data-title="ERN" href="products-cloud-servers.html">
						<div class="container">
							<header class="content-header content-header-large content-header-uppercase">
								<h1>
									<mark>Env�o</mark> con Respaldo Notarial
								</h1>
								<p>
									Obtenga certificaciones con respaldo notarial de sus comunicaciones electr�nicas. <span class="text-color-secondary">Desde $ARS 0,692/Env�o</span>
								</p>
							</header>
							<img src="uploads/server-virtual.png" alt="">
						</div>
					</a>
					<a class="slide" data-title="TRN" href="products-dedicated-cloud.html">
						<div class="container">
							<header class="content-header content-header-large content-header-uppercase">
								<h1>
									<mark>Transacciones</mark> Certificadas
								</h1>
								<p>
									Certifique sus documentos y transacciones electronicas mediante la BlockChain + Respaldo Notarial. <span class="text-color-secondary">Desde $ARS 0,177/Tx</span>
								</p>
							</header>
							<img src="uploads/server-dedicated.png" alt="">
						</div>
					</a>
				</div>
			</section>
			<!-- Content Row -->
			<section class="content-row">
				<div class="container">
					<header class="content-header">
						<h2>
							Comunicaciones y Transacciones Certificadas
						</h2>
						<p>
							Gestione sus comunicaciones y transacciones electr�nicas de manera segura y certificada.
						</p>
					</header>
					<div class="column-row align-center-bottom text-align-center">
						<div class="column-33">
							<i class="fa fa-rocket icon-feature"></i>
							<h3>
								Env�os Garantizados
							</h3>
							<p>
								Todas nuestros env�os cuentan con garant�a de entrega gracias a la reputaci�n de nuestra red y el monitoreo de entrega continuo.
							</p>
							<p>
								<a href="network.html">Anycast Network<i class="fa fa-angle-right icon-right"></i></a>
							</p>
						</div>
						<div class="column-33">
							<i class="fa fa-cloud icon-feature"></i>
							<h3>
								Alta Disponibilidad
							</h3>
							<p>
								Toda la infraestructura de nuestros servicios esta pensada para darle continuidad al negocio y alta disponibilidad de servicios.
							</p>
							<p>
								<a href="network.html">Cloud Platform<i class="fa fa-angle-right icon-right"></i></a>
							</p>
						</div>
						<div class="column-33">
							<i class="fa fa-shield icon-feature"></i>
							<h3>
								Infraestructura Segura
							</h3>
							<p>
								Procesos y auditorias continuas basadas en sistemas de seguridad inform�tica ISO 27001 para garantizar la seguridad de sus transacciones.
							</p>
							<p>
								<a href="network.html">Global Datacenters<i class="fa fa-angle-right icon-right"></i></a>
							</p>
						</div>
					</div>
					<div class="column-row align-center-bottom text-align-center">
						<div class="column-33">
							<i class="fa fa-refresh icon-feature"></i>
							<h3>
								Respuesta Inmediata
							</h3>
							<p>
								Servicios basados en sistemas de procesamiento de estad�sticas en tiempo real garantizan el estado de sus campa�as y permiten automatizar procesos inteligentes.
							</p>
							<p>
								<a href="home.html">Cloud Products<i class="fa fa-angle-right icon-right"></i></a>
							</p>
						</div>
						<div class="column-33">
							<i class="fa fa-support icon-feature"></i>
							<h3>
								Soporte Dedicado
							</h3>
							<p>
								Un equipo especializado en diferentes negocios lo acompa�ara en la implementaci�n y mantenimiento de los servicios para brindarle valor agregado.
							</p>
							<p>
								<a href="contact.html">Support Portal<i class="fa fa-angle-right icon-right"></i></a>
							</p>
						</div>
						<div class="column-33">
							<i class="fa fa-gear icon-feature"></i>
							<h3>
								Automatizaci�n Poderosa
							</h3>
							<p>
								Poderosos sistemas de automatizaci�n e integraci�n mediante sistemas web, interfaces, webservices o APIs para darle una soluci�n a cada necesidad.
							</p>
							<p>
								<a href="features.html">Developer Guide<i class="fa fa-angle-right icon-right"></i></a>
							</p>
						</div>
					</div>
				</div>
			</section>
			<!-- Content Row -->
			<section class="content-row content-row-gray">
				<div class="container">
					<header class="content-header">
						<h2>
							Nuestros Productos
						</h2>
						<p>
							Un producto pensado para cada necesidad le van a permitir agregarle valor a sus procesos y reducir sus costos aumentando la productividad.
						</p>
					</header>
					<div class="column-row align-center-bottom">
						<div class="column-33">
							<div class="product-box">
								<div class="product-header">
									<h4>
										Env�os Certificado
									</h4>
									<p>
										Comunicaciones certificadas en la BlockChain
									</p>
								</div>
								<div class="product-price">
									$9<span class="term">/ month</span>
								</div>
								<div class="product-features">
									<ul>
										<li>
											<strong>Remitentes Dedicados</strong>
										</li>
										<li>
											<strong>Procesamiento de Respuestas</strong>
										</li>
										<li>
											<strong>Generaci�n de Archivos</strong>
										</li>
										<li>
											<strong>Detecci�n de estad�sticas</strong>
										</li>
									</ul>
									<ul>
										<li>
											Certificaci�n en BlockChain
										</li>
										<li>
											Certificaciones Antispam
										</li>
										<li>
											Soporte Bronze Incluido
										</li>
									</ul>
								</div>
								<div class="product-order">
									<a class="button button-secondary" href="http://order.sysworld.com.ar/order/" target="_blank">
										<i class="fa fa-shopping-cart icon-left"></i>Cotizar Ahora
									</a>
								</div>
							</div>
						</div>
						<div class="column-33">
							<div class="product-box product-box-popular">
								<div class="product-popular">
									Env�os con Respaldo Notarial
								</div>
								<div class="product-header">
									<h4>
										Env�os con Respaldo Notarial
									</h4>
									<p>
										Notificaciones con validez legal
									</p>
								</div>
								<div class="product-price">
									$18<span class="term">/ month</span>
								</div>
								<div class="product-features">
									<ul>
										<li>
											<strong>Remitentes Dedicados</strong>
										</li>
										<li>
											<strong>Procesamiento de Respuestas</strong>
										</li>
										<li>
											<strong>Generaci�n de Archivos</strong>
										</li>
										<li>
											<strong>Detecci�n de estad�sticas</strong>
										</li>
									</ul>
									<ul>
										<li>
											Powered by EnvioCertificado.com
										</li>
										<li>
											Certificaciones Antispam
										</li>
										<li>
											Soporte Bronze Incluido
										</li>
									</ul>
								</div>
								<div class="product-order">
									<a class="button button-secondary" href="http://order.sysworld.com.ar/order/" target="_blank">
										<i class="fa fa-shopping-cart icon-left"></i>Cotizar Ahora
									</a>
								</div>
							</div>
						</div>
						<div class="column-33">
							<div class="product-box">
								<div class="product-header">
									<h4>
										Transacciones Certificadas
									</h4>
									<p>
										Certifique sus transacciones y documentos
									</p>
								</div>
								<div class="product-price">
									$36<span class="term">/ month</span>
								</div>
								<div class="product-features">
									<ul>
										<li>
											<strong>Respaldo Notarial Incluido</strong>
										</li>
										<li>
											<strong>Certificaci�n BlockChain</strong>
										</li>
										<li>
											<strong>Hosting de Archivos</strong>
										</li>
										<li>
											<strong>CDN High Availability</strong>
										</li>
									</ul>
									<ul>
										<li>
											Powered by EnvioCertificado.com
										</li>
										<li>
											Certificaciones Acceso API
										</li>
										<li>
											Soporte Bronze Incluido
										</li>
									</ul>
								</div>
								<div class="product-order">
									<a class="button button-secondary" href="http://order.sysworld.com.ar/order/" target="_blank">
										<i class="fa fa-shopping-cart icon-left"></i>Cotizar Ahora
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- Content Row -->
			<section class="content-row">
				<div class="container">
					<header class="content-header">
						<h2>
							Red Distribuida de Contenidos y Respaldo.
						</h2>
						<p>
							Infraestructura globalmente distribuida para garantizar la continuidad de los servicios y respaldar las certificaciones de nuestros servicios.
						</p>
					</header>
					<div class="network-map">
						<ul>
							<li style="top: 42%; left: 12.5%;">
								<a href="network.html"><span class="label-top-left">San Francisco</span></a>
							</li>
							<li style="top: 44%; left: 20%;">
								<a href="network.html"><span class="label-bottom-right">Dallas</span></a>
							</li>
							<li style="top: 40%; left: 26%;">
								<a href="network.html"><span class="label-top-right">Nueva York</span></a>
							</li>
							<li style="top: 86%; left: 31%;">
								<a href="network.html"><span class="label-bottom-right">Buenos Aires</span></a>
							</li>
							<li style="top: 41%; left: 44.5%;">
								<a href="network.html"><span class="label-bottom-left">Lisboa</span></a>
							</li>
							<li style="top: 32%; left: 46.5%;">
								<a href="network.html"><span class="label-top-left">Londres</span></a>
							</li>
							<li style="top: 34%; left: 49.5%;">
								<a href="network.html"><span class="label-bottom-right">Frankfurt</span></a>
							</li>
							<li style="top: 64%; left: 75%;">
								<a href="network.html"><span class="label-bottom-left">Singapore</span></a>
							</li>
							<li style="top: 44%; left: 85%;">
								<a href="network.html"><span class="label-bottom-right">Tokyo</span></a>
							</li>
							<li style="top: 83%; left: 89%;">
								<a href="network.html"><span class="label-top-right">Sydney</span></a>
							</li>
						</ul>
						<img src="img/layout/map-dark.svg" alt="">
					</div>
				</div>
			</section>
			<!-- Content Row -->
			<section class="content-row content-row-gray">
				<div class="container">
					<header class="content-header">
						<h2>
							Nuestros Clientes
						</h2>
						<p>
							M�s de 200 Empresas de diferentes rubros nos conf�an sus env�os y transacciones.
						</p>
					</header>
					<div class="column-row align-center-bottom">
						<div class="column-33">
							<div class="testimonial">
								<p class="testimonial-content">
									Se implemento un workflow con validaciones, generaci�n de archivos PDFs y env�os certificados para todos los procesos del plan de ahorro.
								</p>
								<p class="testimonial-author">
									Autoahorro VW<br>
									<small>Env�o Certificado</small>
								</p>
							</div>
						</div>
						<div class="column-33">
							<div class="testimonial">
								<p class="testimonial-content">
									Se implemento el workflow de certificaciones notariales para los procesos de ordenes de compras de proveedores.
								</p>
								<p class="testimonial-author">
									Banco Frances<br>
									<small>Env�o con Respaldo Notarial</small>
								</p>
							</div>
						</div>
						<div class="column-33">
							<div class="testimonial">
								<p class="testimonial-content">
									Se implemento un proceso de notificaciones electr�nicas con acuse de recibo y respaldo notarial para las cartas de arribos del negocio.
								</p>
								<p class="testimonial-author">
									Hapag Lloyd<br>
									<small>ERN + TRN</small>
								</p>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- Content Row -->
			<section class="content-row content-row-color">
				<div class="container">
					<header class="content-header">
						<h2>
							�Interesado en nuestra empresa?
						</h2>
						<p>
							Acceda al sitio web corporativo para conocer m�s detalles de nuestra empresa.<br><br>
							<a class="button button-secondary" href="http://www.sysworld.com.ar" target="_blank">
								<i class="fa fa-globe icon-left"></i>Sitio Web Coorporativo
							</a>
						</p>
					</header>
				</div>
			</section>
		</section>
		<!-- Footer -->
		<footer id="footer">
			<section class="footer-primary">
				<div class="container">
					<div class="column-row">
						<div class="column-33">
							<h5>
								Sysworld Servicios S.A.
							</h5>
							<p>
								EnvioCertificado.com | EnvioLegal.com | CertiSend.com |Envio de Email Certificados | Envios con Respaldo Notarial | Certificaciones en la BlockChain | SMS Certificados | Transacciones Certificadas | Auditoria de Transacciones
							</p>
						</div>
						<div class="column-66">
							<div class="column-row align-right-top">
								<div class="column-25">
									<h5>
										Conectarse
									</h5>
									<ul class="list-style-icon">
										<li>
											<a href="https://www.facebook.com/SysWorld-Servicios-SA-323344231089747" target="_blank"><i class="fa fa-facebook"></i>Facebook</a>
										</li>
										<li>
											<a href="https://twitter.com/Sysworldsa" target="_blank"><i class="fa fa-twitter"></i>Twitter</a>
										</li>
										<li>
											<a href="contacto.asp"><i class="fa fa-github"></i>Contacto</a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar/v2/sw/contacto.asp" target="_blank"><i class="fa fa-xing"></i>Live Chat</a>
										</li>
									</ul>
								</div>
								<div class="column-25">
									<h5>
										Productos
									</h5>
									<ul>
										<li>
											<a href="prodenviocertificado.asp">EnvioCertificado</a>
										</li>
										<li>
											<a href="prodern.asp">ERN</a>
										</li>
										<li>
											<a href="prodtrn.asp">TRN</a>
										</li>
										<li>
											<a href="http://www.centrodevalidaciones.com" target="_blank">Validadores</a>
										</li>
										<li>
											<a href="http://www.omnicrm.cloud" target="_blank">OmniCRM.cloud</a>
										</li>
										<li>
											<a href="http://cloud.sysworld.com.ar/" target="_blank">Customer Portal</a>
										</li>
									</ul>
								</div>
								<div class="column-25">
									<h5>
										Recursos
									</h5>
									<ul>
										<li>
											<a href="funcionalidades.asp">Funcionalidades</a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar/v2/sw/empresa.asp?f=network" target="_blank">Network</a>
										</li>
										<li>
											<a href="legales.asp">Respaldo Legales</a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar/v2/sw/empresa.asp?f=partners" target="_blank">Partners</a>
										</li>
									</ul>
								</div>
								<div class="column-flex">
									<h5>
										Empresa
									</h5>
									<ul>
										<li>
											<a href="empresa.asp">Nosotros</a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar" target="_blank">Sitio Corporativo</a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar/v2/sw/empresa_dist.asp" target="_blank">Distribuidores</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="footer-secondary">
				<div class="container">
					<p>
						Copyright 2006 - <%=year(date)%> &copy; Sysworld Servicios S.A. Todos los derechos reservados.<br>
						Powered by <a href="http://www.sysworld.com.ar">Sysworld Servicios S.A.</a>
					</p>
				</div>
			</section>
		</footer>
		<!-- Scripts -->
		<script src="js/jquery.min.js"></script>
		<script src="js/headroom.min.js"></script>
		<script src="js/js.cookie.min.js"></script>
		<script src="js/imagesloaded.min.js"></script>
		<script src="js/bricks.min.js"></script>
		<script src="js/main.min.js"></script>
	</body>
</html>