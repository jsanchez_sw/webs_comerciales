<?php
    include 'functions/db.php';

    include_once 'functions/language.php';
?>
<!DOCTYPE html>
<html lang="<?php echo $lang['SITE_lang'];?>">

<!-- Mirrored from protechtheme.com/saas/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:17:32 GMT -->
<head>
        <!-- Required meta tags -->
        <meta charset="iso-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="keywords" content="<?php echo $lang['SITE_keywords'];?>">
		<meta name="description" content="<?php echo $lang['SITE_desc'];?>">
		<meta name="author" content="Sysworld Servicios S.A.">
        <title><?php echo $lang['SITE_title'];?></title>

        <!-- Bootstrap core CSS -->
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Select2 -->
        <link href="assets/select2/css/select2.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- Iconmoon -->
        <link href="assets/iconmoon/css/iconmoon.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="css/custom.css" rel="stylesheet">
        <link rel="icon" type="image/x-icon" href="images/favicon.ico" />
    </head>
    <body>

<!-- ==============================================
**Preloader**
=================================================== -->
        <div id="loader">
            <div id="element">
                <div class="circ-one"></div>
                <div class="circ-two"></div>
            </div>
        </div>

<!-- ==============================================
**Header**
=================================================== -->
        <header>
			<?php include 'menu.php';?>
        </header>

<!-- ==============================================
**Inner Banner**
=================================================== -->
        <section class="inner-banner">
            <div class="container">
                <div class="contents">
                    <h1><?php echo $lang['FAQ_header_tittle'];?></h1>
                    <p><?php echo $lang['FAQ_header_subtittle'];?></p>
                </div>
            </div>
        </section>

<!-- ==============================================
**FAQ**
=================================================== -->
        <section class="faq-outer grey-bg padding-lg" id="faq-sec">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 faq-left">
                        <ul>
                            <li class="active"> <a href="quality.php">
                                <div class="icon"><span class="icon-general"></span></div>
                                <div class="cnt-block">
                                    <h3><?php echo $lang['FAQ_item1'];?></h3>
                                    <p><?php echo $lang['FAQ_item1_desc'];?></p>
                                </div>
                                </a> </li>
                            <li> <a href="aboutus.php">
                                <div class="icon"><span class="icon-features"></span></div>
                                <div class="cnt-block">
                                    <h3><?php echo $lang['FAQ_item2'];?></h3>
                                    <p><?php echo $lang['FAQ_item2_desc'];?></p>
                                </div>
                                </a> </li>
                            <li> <a href="legal.php">
                                <div class="icon"><span class="icon-privacy"></span></div>
                                <div class="cnt-block">
                                    <h3><?php echo $lang['FAQ_item3'];?></h3>
                                    <p><?php echo $lang['FAQ_item3_desc'];?></p>
                                </div>
                                </a> </li>
                            <li> <a href="pricing.php">
                                <div class="icon"><span class="icon-pricing"></span></div>
                                <div class="cnt-block">
                                    <h3><?php echo $lang['FAQ_item4'];?></h3>
                                    <p><?php echo $lang['FAQ_item4_desc'];?></p>
                                </div>
                                </a> </li>
                        </ul>
                    </div>
                    <div class="col-md-9 faq-right">
                        <div id="accordion" role="tablist">

                            <!-- Start:accordion item 01 -->
                            <div class="card">
                                <div class="card-header" role="tab" id="headingOne">
                                    <h5 class="mb-0"> <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><?php echo $lang['FAQ_question1'];?></a> </h5>
                                </div>
                                <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        <p><?php echo $lang['FAQ_answer1'];?></p>
                                    </div>
                                </div>
                            </div>
                            <!-- End:accordion item 01 -->

                            <!-- Start:accordion item 02 -->
                            <div class="card">
                                <div class="card-header" role="tab" id="headingTwo">
                                    <h5 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><?php echo $lang['FAQ_question2'];?></a> </h5>
                                </div>
                                <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <div class="card-body">
                                        <p><?php echo $lang['FAQ_answer2'];?></p>
                                    </div>
                                </div>
                            </div>
                            <!-- End:accordion item 02 -->

                            <!-- Start:accordion item 03 -->
                            <div class="card">
                                <div class="card-header" role="tab" id="headingThree">
                                    <h5 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> <?php echo $lang['FAQ_question3'];?></a> </h5>
                                </div>
                                <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        <p><?php echo $lang['FAQ_answer3'];?></p>
                                    </div>
                                </div>
                            </div>
                            <!-- End:accordion item 03 -->

                            <!-- Start:accordion item 04 -->
                            <div class="card">
                                <div class="card-header" role="tab" id="headingFour">
                                    <h5 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour"><?php echo $lang['FAQ_question4'];?></a> </h5>
                                </div>
                                <div id="collapseFour" class="collapse" role="tabpanel" aria-labelledby="headingFour" data-parent="#accordion">
                                    <div class="card-body">
                                        <p><?php echo $lang['FAQ_answer4'];?></p>
                                    </div>
                                </div>
                            </div>
                            <!-- End:accordion item 04 -->

                            <!-- Start:accordion item 05 -->
                            <div class="card">
                                <div class="card-header" role="tab" id="headingFive">
                                    <h5 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive"> <?php echo $lang['FAQ_question5'];?></a> </h5>
                                </div>
                                <div id="collapseFive" class="collapse" role="tabpanel" aria-labelledby="headingFive" data-parent="#accordion">
                                    <div class="card-body">
                                        <p><?php echo $lang['FAQ_answer5'];?></p>
                                    </div>
                                </div>
                            </div>
                            <!-- End:accordion item 05 -->

                            <!-- Start:accordion item 06 -->
                            <div class="card">
                                <div class="card-header" role="tab" id="headingSix">
                                    <h5 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix"> <?php echo $lang['FAQ_question6'];?></a> </h5>
                                </div>
                                <div id="collapseSix" class="collapse" role="tabpanel" aria-labelledby="headingSix" data-parent="#accordion">
                                    <div class="card-body">
                                        <p><?php echo $lang['FAQ_answer6'];?></p>
                                    </div>
                                </div>
                            </div>
                            <!-- End:accordion item 06 -->

                            <!-- Start:accordion item 07 -->
                            <div class="card">
                                <div class="card-header" role="tab" id="headingSeven">
                                    <h5 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven"> <?php echo $lang['FAQ_question7'];?></a> </h5>
                                </div>
                                <div id="collapseSeven" class="collapse" role="tabpanel" aria-labelledby="headingSeven" data-parent="#accordion">
                                    <div class="card-body">
                                        <p><?php echo $lang['FAQ_answer7'];?></p>
                                    </div>
                                </div>
                            </div>
                            <!-- End:accordion item 07 -->

                            <!-- Start:accordion item 08 -->
                            <div class="card">
                                <div class="card-header" role="tab" id="headingEight">
                                    <h5 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight"> <?php echo $lang['FAQ_question8'];?></a> </h5>
                                </div>
                                <div id="collapseEight" class="collapse" role="tabpanel" aria-labelledby="headingEight" data-parent="#accordion">
                                    <div class="card-body">
                                        <p><?php echo $lang['FAQ_answer8'];?></p>
                                    </div>
                                </div>
                            </div>
                            <!-- End:accordion item 08 -->

                            <!-- Start:accordion item 09 -->
                            <div class="card">
                                <div class="card-header" role="tab" id="headingNine">
                                    <h5 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine"> <?php echo $lang['FAQ_question9'];?></a> </h5>
                                </div>
                                <div id="collapseNine" class="collapse" role="tabpanel" aria-labelledby="headingNine" data-parent="#accordion">
                                    <div class="card-body">
                                        <p><?php echo $lang['FAQ_answer9'];?></p>
                                    </div>
                                </div>
                            </div>
                            <!-- End:accordion item 09 -->

                            <!-- Start:accordion item 10 -->
                            <div class="card">
                                <div class="card-header" role="tab" id="headingTen">
                                    <h5 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseTen" aria-expanded="false" aria-controls="collapseTen"> <?php echo $lang['FAQ_question10'];?></a> </h5>
                                </div>
                                <div id="collapseTen" class="collapse" role="tabpanel" aria-labelledby="headingTen" data-parent="#accordion">
                                    <div class="card-body">
                                        <p><?php echo $lang['FAQ_answer10'];?></p>
                                    </div>
                                </div>
                            </div>
                            <!-- End:accordion item 10 -->

                            <!-- Start:accordion item 11 -->
                            <div class="card">
                                <div class="card-header" role="tab" id="headingEleven">
                                    <h5 class="mb-0"> <a class="collapsed" data-toggle="collapse" href="#collapseEleven" aria-expanded="false" aria-controls="collapseEleven"> <?php echo $lang['FAQ_question11'];?></a> </h5>
                                </div>
                                <div id="collapseEleven" class="collapse" role="tabpanel" aria-labelledby="headingEleven" data-parent="#accordion">
                                    <div class="card-body">
                                        <p><?php echo $lang['FAQ_answer11'];?></p>
                                    </div>
                                </div>
                            </div>
                            <!-- Start:accordion item 11 -->

                        </div>
                    </div>
                </div>
            </div>
        </section>

<!-- ==============================================
**Signup Section**
=================================================== -->

	<?php include 'footer.php';?>




        <!-- Scroll to top -->
        <a href="#" class="scroll-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>

        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery.min.js"></script>
        <!-- Popper JS -->
        <script src="js/popper.min.js"></script>
        <!-- Bootsrap JS -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- Select2 JS -->
        <script src="assets/select2/js/select2.min.js"></script>
        <!-- Custom JS -->
        <script src="js/custom.js"></script>
    </body>

<!-- Mirrored from protechtheme.com/saas/faq.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:18:11 GMT -->
</html>