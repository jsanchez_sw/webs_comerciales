<?php
    include 'functions/db.php';

    include_once 'functions/language.php';
?>

<!DOCTYPE html>
<html lang="<?php echo $lang['SITE_lang'];?>">

<!-- Mirrored from protechtheme.com/saas/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:17:32 GMT -->
<head>
        <!-- Required meta tags -->
        <meta charset="iso-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="keywords" content="<?php echo $lang['SITE_keywords'];?>">
		<meta name="description" content="<?php echo $lang['SITE_desc'];?>">
		<meta name="author" content="Sysworld Servicios S.A.">
        <title><?php echo $lang['SITE_title'];?></title>
        <!-- Bootstrap core CSS -->
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Select2 -->
        <link href="assets/select2/css/select2.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- Iconmoon -->
        <link href="assets/iconmoon/css/iconmoon.css" rel="stylesheet">
        <!-- Magnific Popup -->
        <link href="assets/magnific-popup/css/magnific-popup.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="css/custom.css" rel="stylesheet">
        <link rel="icon" type="image/x-icon" href="images/favicon.ico" />
    </head>
    <body>

<!-- ==============================================
**Preloader**
=================================================== -->
        <div id="loader">
            <div id="element">
                <div class="circ-one"></div>
                <div class="circ-two"></div>
            </div>
        </div>

<!-- ==============================================
**Header**
=================================================== -->
        <header>
			<?php include 'menu.php';?>
        </header>

<!-- ==============================================
**Inner Banner**
=================================================== -->
        <section class="inner-banner">
            <div class="container">
                <div class="contents">
                    <h1><?php echo $lang['ABOUT_header_tittle'];?></h1>
                    <p><?php echo $lang['ABOUT_header_subtittle'];?></p>
                </div>
            </div>
        </section>

<!-- ==============================================
**Who we Are**
=================================================== -->
        <section class="who-we-are padding-lg">
            <div class="container">
                <div class="row row1">
                    <div class="col-lg-6">
                        <figure><img src="images/who-we-are.jpg" class="img-fluid" alt=""></figure>
                    </div>
                    <div class="col-lg-6">
                        <div class="cnt-block">
                            <h2><?php echo $lang['ABOUT_section1_tittle'];?></h2>
                            <p><?php echo $lang['ABOUT_section1_subtittle'];?></p>
                            <ul class="who-listing">
                                <li><?php echo $lang['ABOUT_section1_item1'];?></li>
                                <li><?php echo $lang['ABOUT_section1_item2'];?></li>
                                <li><?php echo $lang['ABOUT_section1_item3'];?></li>
                                <li><?php echo $lang['ABOUT_section1_item4'];?></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <ul class="counter-listing">
                            <li>
                                <span class="counter" data-num="2007">2007</span>
                                <span class="sub-title"><?php echo $lang['ABOUT_section1_item5'];?></span>
                            </li>
                            <li>
                                <div class="couter-outer"><span class="counter" data-num="350">350</span><span>+</span></div>
                                <span class="sub-title"><?php echo $lang['ABOUT_section1_item6'];?></span>
                            </li>
                            <li>
                                <div class="couter-outer"><span class="counter" data-num="950">950</span><span>+</span></div>
                                <span class="sub-title"><?php echo $lang['ABOUT_section1_item7'];?></span>
                            </li>
                            <li>
                                <div class="couter-outer"><span class="counter" data-num="100">100</span><span>+</span></div>
                                <span class="sub-title"><?php echo $lang['ABOUT_section1_item8'];?></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

<!-- ==============================================
**Take a Tour Section**
=================================================== -->
        <section class="about-video">
            <div class="container">
                <div class="cnt-block"> <a class="play-btn video" href="https://www.youtube.com/watch?v=3xJzYpRVQVA"><span class="icon-know-more-arrow"></span></a>
                    <h2><?php echo $lang['ABOUT_section2_tittle'];?></h2>
                    <p><?php echo $lang['ABOUT_section2_subtittle'];?></p>
                </div>
            </div>
        </section>

<!-- ==============================================
**Our Team Section**
=================================================== -->
        <section class="our-team-outer padding-lg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <h2><?php echo $lang['ABOUT_section3_tittle'];?></h2>
                        <p><?php echo $lang['ABOUT_section3_subtittle'];?></p>
                    </div>
                </div>
                <ul class="row team-listing">
                    <li class="col-md-6 col-lg-3">
                        <figure><img src="images/team-img1.jpg" class="rounded-circle img-fluid" alt=""></figure>
                        <h3>Matt Anderson</h3>
                        <span class="source-title"><?php echo $lang['ABOUT_section3_item1_tittle'];?></span>
                        <p><?php echo $lang['ABOUT_section3_item1_subtittle'];?></p>
                        <ul class="follow-us">
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        </ul>
                    </li>
                    <li class="col-md-6 col-lg-3">
                        <figure><img src="images/team-img2.jpg" class="rounded-circle img-fluid" alt=""></figure>
                        <h3>Pamela Redmond</h3>
                        <span class="source-title"><?php echo $lang['ABOUT_section3_item2_tittle'];?></span>
                        <p><?php echo $lang['ABOUT_section3_item2_subtittle'];?></p>
                        <ul class="follow-us">
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        </ul>
                    </li>
                    <li class="col-md-6 col-lg-3">
                        <figure><img src="images/team-img3.jpg" class="rounded-circle img-fluid" alt=""></figure>
                        <h3>Milan Chudoba</h3>
                        <span class="source-title"><?php echo $lang['ABOUT_section3_item3_tittle'];?></span>
                        <p><?php echo $lang['ABOUT_section3_item3_subtittle'];?></p>
                        <ul class="follow-us">
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        </ul>
                    </li>
                    <li class="col-md-6 col-lg-3">
                        <figure><img src="images/team-img4.jpg" class="rounded-circle img-fluid" alt=""></figure>
                        <h3>Annabel</h3>
                        <span class="source-title"><?php echo $lang['ABOUT_section3_item4_tittle'];?></span>
                        <p><?php echo $lang['ABOUT_section3_item4_subtittle'];?></p>
                        <ul class="follow-us">
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </section>

<!-- ==============================================
**Signup Section**
=================================================== -->
<?php
    include 'footer.php';
?>


        <!-- Scroll to top -->
        <a href="#" class="scroll-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>

        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery.min.js"></script>
        <!-- Popper JS -->
        <script src="js/popper.min.js"></script>
        <!-- Bootsrap JS -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- Select2 JS -->
        <script src="assets/select2/js/select2.min.js"></script>
        <!-- Video Popup JS -->
        <script src="assets/magnific-popup/js/magnific-popup.min.js"></script>
        <!-- Waypoints JS -->
        <script src="assets/waypoints/js/waypoints.min.js"></script>
        <!-- Counter Up JS -->
        <script src="assets/counterup/js/counterup.min.js"></script>
        <!-- Custom JS -->
        <script src="js/custom.js"></script>
    </body>

<!-- Mirrored from protechtheme.com/saas/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:17:33 GMT -->
</html>