<?php
    include '../functions/db.php';
    include_once '../functions/language.php';
	include 'session.php';


	$idapi = $_REQUEST['id'];
	$nombre = $_REQUEST['nombre'];
	$version = $_REQUEST['version'];
	$idestado = $_REQUEST['idestado'];
	$descripcion = $_REQUEST['descripcion'];
	$codigo = $_REQUEST['codigo'];
	$tipoapi = $_REQUEST['tipoapi'];

	$type = $_REQUEST['type'];
	$campo = $_REQUEST['campo'];
	$formato = $_REQUEST['formato'];
	$requerido = $_REQUEST['requerido'];
	$orden = $_REQUEST['orden'];
	$longitud = $_REQUEST['longitud'];
	$separador = $_REQUEST['separador'];
	$encabezado = $_REQUEST['encabezado'];

	if ($separador == "coma"){
		$separador = ",";
	}
	if ($separador == "puntoycoma"){
		$separador = ";";
	}

	$usuario = $_SESSION['apilanding_user_email'];



	if ($_REQUEST['abm'] == "a"){
		$sql = "INSERT INTO web_apis (codigo,nombre,version,descripcion,contexto,tipoapi,tiposuscripcion,idestado, propietario,proveedor,apidataseparador, apidataomitirencabezado, fechavigencia) VALUE("
		. "'" . $codigo . "',"
		. "'" . $nombre . "',"
		. "'" . $version . "',"
		. "'" . $descripcion . "',"
		. "'database/apidata',"
		. $tipoapi . ","
		. "0,"
		. $idestado . ","
		. "'" . $usuario . "',"
		. "'Sysworld',"
		. "'" . $separador . "',"
		. "'" . $encabezado . "',"
		. "now() )";

		if ($conn->query($sql) === TRUE) {
			$last_id = $conn->insert_id;
			header('Location: apiedit.php?e=1&id=' . $last_id . "&codigo=" . $codigo . "&section=config");
		}
	}

	if ($_REQUEST['abm'] == "m"){
		$sql = "UPDATE web_apis SET "
		. "nombre = '" . $nombre . "',"
		. "version = '" . $version . "',"
		. "descripcion = '" . $descripcion . "',"
		. "apidataseparador = '" . $separador . "',"
		. "apidataomitirencabezado = '" . $encabezado . "',"
		. "idestado = " . $idestado . " "
		. "WHERE id = " . $idapi . " AND codigo = '" . $codigo . "'";


		if ($conn->query($sql) === TRUE) {
			header('Location: apiedit.php?e=1&id=' . $idapi . "&codigo=" . $codigo . "&section=config");
		}
	}


	if ($_REQUEST['abm'] == "b"){
		$sql = "DELETE FROM web_apis_parametros "
		. "WHERE idapi IN(SELECT id from web_apis WHERE id = " . $idapi . " AND codigo = '" . $codigo . "')";
		if ($conn->query($sql) === TRUE) {
			$ok = 'ok';
		}

		$sql = "DELETE FROM web_apis_campos_respuesta "
		. "WHERE idapi IN(SELECT id from web_apis WHERE id = " . $idapi . " AND codigo = '" . $codigo . "')";
		if ($conn->query($sql) === TRUE) {
			$ok = 'ok';
		}

		$sql = "DELETE FROM web_apis_data_archivos "
		. "WHERE idapi IN(SELECT id from web_apis WHERE id = " . $idapi . " AND codigo = '" . $codigo . "')";
		if ($conn->query($sql) === TRUE) {
			$ok = 'ok';
		}

		$sql = "DELETE FROM web_apis_data_campos "
		. "WHERE idapi IN(SELECT id from web_apis WHERE id = " . $idapi . " AND codigo = '" . $codigo . "')";
		if ($conn->query($sql) === TRUE) {
			$ok = 'ok';
		}

		$sql = "DELETE FROM web_apis "
		. "WHERE id = " . $idapi . " AND codigo = '" . $codigo . "'";
		if ($conn->query($sql) === TRUE) {
			header('Location: apislist.php?t=' . $type);
		}
	}



	//CREA CAMPO
	if ($_REQUEST['abm'] == "a_fields"){
		if ($type == "entrada"){
			$tipo = 'query';

			$sql = "INSERT INTO web_apis_parametros (idapi,parametro,formato,requerido,tipo,descripcion) VALUE("
			. $idapi . ","
			. "'" . $campo . "',"
			. "'" . $tipo . "',"
			. "'" . $requerido . "',"
			. "'" . $formato . "',"
			. "'" . $descripcion . "')";

			if ($conn->query($sql) === TRUE) {
				$last_id = $conn->insert_id;
			}
		}
		if ($type == "salida"){
			$tipo = 'query';
			$sql = "INSERT INTO web_apis_campos_respuesta (idapi,campo,tipo,longitud,descripcion) VALUE("
			. $idapi . ","
			. "'" . $campo . "',"
			. "'" . $formato . "',"
			. "'" . $longitud . "',"
			. "'" . $descripcion . "')";

			if ($conn->query($sql) === TRUE) {
				$last_id = $conn->insert_id;
			}
		}


		$sql = "INSERT INTO web_apis_data_campos (idapi,orden,campo,formato,requerido,tipo,longitud,descripcion) VALUE("
		. $idapi . ","
		. "'" . $orden . "',"
		. "'" . $campo . "',"
		. "'" . $tipo . "',"
		. "'" . $requerido . "',"
		. "'" . $formato . "',"
		. "'" . $longitud . "',"
		. "'" . $descripcion . "')";

		if ($conn->query($sql) === TRUE) {
			$last_id = $conn->insert_id;
			header('Location: apiedit.php?e=1&id=' . $idapi . "&codigo=" . $codigo . "&section=fields");
		}
	}


	//CREA Archivo
	if ($_REQUEST['abm'] == "u_files"){

        $name = $conn->real_escape_string($_FILES['file']['name']);
        $mime = $conn->real_escape_string($_FILES['file']['type']);
        $data = $conn->real_escape_string(file_get_contents($_FILES  ['file']['tmp_name']));
        $size = intval($_FILES['file']['size']);
echo $size;

		$sql = "INSERT INTO web_apis_data_archivos (idapi,nombrearchivo,tamanoarchivo,archivo,idestado,fechavigencia) VALUE("
		. $idapi . ","
		. "'" . $name . "',"
		. "'" . $size . "',"
		. "'" . $data . "',"
		. "0,"
		. "now() )";
		if ($conn->query($sql) === TRUE) {
			header('Location: apiedit.php?e=1&id=' . $idapi . "&codigo=" . $codigo . "&section=files");
		}else{
			echo "error upload file. ";
			print_r($conn->error);
		}

	}






?>

