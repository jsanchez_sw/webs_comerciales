<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="iso-8859-1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
<link href="images/favicon.png" rel="icon" />
<title>APIPAGOS - El centro de los Pagos Online.</title>
<meta name="description" content="This professional design html template is for build a Money Transfer and online payments website.">
<meta name="author" content="harnishdesign.net">

<!-- Web Fonts
============================================= -->
<link rel='stylesheet' href='../../../fonts.googleapis.com/cssab2a.css?family=Rubik:300,300i,400,400i,500,500i,700,700i,900,900i' type='text/css'>

<!-- Stylesheet
============================================= -->
<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="vendor/font-awesome/css/all.min.css" />
<link rel="stylesheet" type="text/css" href="vendor/owl.carousel/assets/owl.carousel.min.css" />
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
</head>
<body>

<!-- Preloader -->
<div id="preloader">
  <div data-loader="dual-ring"></div>
</div>
<!-- Preloader End -->

<!-- Document Wrapper
============================================= -->
<div id="main-wrapper">

  <!-- Header
  ============================================= -->
  <header id="header">
    <div class="container">
      <div class="header-row">
        <div class="header-column justify-content-start">
          <!-- Logo
          ============================= -->
          <div class="logo"> <a class="d-flex" href="index.php" title="Inicio"><img src="images/logo.png" alt="APIPagos" /></a> </div>
          <!-- Logo end -->
          <!-- Collapse Button
          ============================== -->
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header-nav"> <span></span> <span></span> <span></span> </button><!-- Collapse Button end -->

          <!-- Primary Navigation
          ============================== -->
          <nav class="primary-menu navbar navbar-expand-lg">
            <div id="header-nav" class="collapse navbar-collapse">
              <ul class="navbar-nav mr-auto">
                <li><a href="../pays/paysimple.php">Pagar</a></li>
                <li><a href="cobrar.php">Cobrar</a></li>
                <li><a href="nosotros.php">Nosotros</a></li>
                <li><a href="contacto.php">Contacto</a></li>
                <li class="dropdown"> <a class="dropdown-toggle" href="#">API</a>
				  <ul class="dropdown-menu">
					<li><a class="dropdown-item" href="http://www.apilanding.com">Documentación</a></li>
				  </ul>
                </li>
              </ul>
            </div>
          </nav>
          <!-- Primary Navigation end -->
        </div>

        <div class="header-column justify-content-end">
          <!-- Login & Signup Link
          ============================== -->
          <nav class="login-signup navbar navbar-expand">
            <ul class="navbar-nav">
              <li><a href="login.php" target="_blank">Login</a> </li>
              <li class="align-items-center h-auto ml-sm-3"><a class="btn btn-primary d-none d-sm-block" href="registro.php" target="_blank">Contratar</a></li>
            </ul>
          </nav>
          <!-- Login & Signup Link end -->
        </div>
      </div>
    </div>
  </header>
  <!-- Header End -->

  <!-- Content
  ============================================= -->
  <div id="content">

    <!-- Slideshow
    ============================================= -->
    <div class="owl-carousel owl-theme single-slideshow" data-autoplay="true" data-loop="true" data-autoheight="true" data-nav="true" data-items="1">
      <div class="item">
        <section class="hero-wrap section shadow-md">
          <div class="hero-mask opacity-7 bg-dark"></div>
          <div class="hero-bg" style="background-image:url('images/bg/image-1.jpg');"></div>

          <div class="hero-content py-2 py-lg-5">
            <div class="container text-center">
              <h2 class="text-16 text-white">Cobranzas Digitales</h2>
              <p class="text-5 text-white mb-4">
              Utilice nuestro centro de procesamiento de cobranzas conectado con todos los proveedores de pago sin pagar costos extra por procesamiento.
              </p>
              <a href="contacto.php" class="btn btn-primary m-2">Crear una Cuenta</a> <a class="btn btn-outline-light video-btn m-2" href="" data-src="https://www.youtube.com/embed/7e90gBu4pas" data-toggle="modal" data-target="#videoModal"><span class="text-2 mr-3"><i class="fas fa-play"></i></span>See How it Works</a> </div>
          </div>
        </section>
      </div>
      <div class="item">
        <section class="hero-wrap section shadow-md">
          <div class="hero-bg" style="background-image:url('images/bg/image-3.jpg');"></div>
          <div class="hero-content py-2 py-lg-4">
            <div class="container">
              <div class="row">
                <div class="col-12 col-lg-8 col-xl-7 text-center text-lg-left">
                  <h2 class="text-13 text-white">Más de 3.000.000 de transacciones mensuales.</h2>
                  <p class="text-5 text-white mb-4">Utilizando 20 medios de pago integrados.</p>
                  <a href="contacto.php" class="btn btn-primary mr-3">Contactar un Asesor</a> <a class="btn btn-link text-light video-btn" href="#" data-src="https://www.youtube.com/embed/7e90gBu4pas" data-toggle="modal" data-target="#videoModal"><span class="mr-2"><i class="fas fa-play-circle"></i></span>Watch Demo</a> </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- Slideshow end -->

    <!-- Why choose
    ============================================= -->
    <section class="section bg-light">
      <div class="container">
        <h2 class="text-9 text-center">¿Por qué utilizar APIPagos?</h2>
        <p class="text-4 text-center mb-5">Nuestro modelo de negocio es simple, transparente y cuenta con 4 pilares.</p>
        <div class="row">
          <div class="col-sm-6 col-lg-3 mb-5 mb-lg-0">
            <div class="featured-box">
              <div class="featured-box-icon text-primary"> <i class="fas fa-hand-pointer"></i> </div>
              <h3>Fácil Integracion</h3>
              <p class="text-3">Integrese mediante diferentes tecnologias y herramientas que le permitirán automatizar sus procesos de cobranzas de forma rápida y segura.</p>
              <a href="#" class="btn-link text-3">Ver Más<i class="fas fa-chevron-right text-1 ml-2"></i></a> </div>
          </div>
          <div class="col-sm-6 col-lg-3 mb-5 mb-lg-0">
            <div class="featured-box">
              <div class="featured-box-icon text-primary"> <i class="fas fa-share"></i> </div>
              <h3>Todos Los Medios de Pago</h3>
              <p class="text-3">Unificamos todos los medios de pago nacionales e internacionales en una misma plataforma con integraciones faciles y seguras para optimizar sus procesos.</p>
              <a href="#" class="btn-link text-3">Ver Más<i class="fas fa-chevron-right text-1 ml-2"></i></a> </div>
          </div>
          <div class="col-sm-6 col-lg-3 mb-5 mb-sm-0">
            <div class="featured-box">
              <div class="featured-box-icon text-primary"> <i class="fas fa-dollar-sign"></i> </div>
              <h3>No cobramos comisión extra</h3>
              <p class="text-3">No cobramos comisiones ni costos extra. Se trasladan los costos puestos por las tarjetas y los impuestos municipales, provinciales y/o nacionales.</p>
              <a href="#" class="btn-link text-3">Ver Más<i class="fas fa-chevron-right text-1 ml-2"></i></a> </div>
          </div>
          <div class="col-sm-6 col-lg-3">
            <div class="featured-box">
              <div class="featured-box-icon text-primary"> <i class="fas fa-lock"></i> </div>
              <h3>Dinero a las 72 horas</h3>
              <p class="text-3">Tendrá su dinero disponible para ser transferido a su cuenta bancaria dentro de las 72 horas de acreditado el mismo por el medio de pago cobrador.</p>
              <a href="#" class="btn-link text-3">Ver Más<i class="fas fa-chevron-right text-1 ml-2"></i></a> </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Why choose end -->

    <!-- Payment Solutions
    ============================================= -->
    <section class="section">
      <div class="container overflow-hidden">
        <div class="row">
          <div class="col-lg-5 col-xl-6 d-flex">
            <div class="my-auto pb-5 pb-lg-0">
              <h2 class="text-9">Nuestros Diferencial</h2>
              <p class="text-4">Hemos desarrollado una plataforma tecnologica basada en infraestructura de alta disponibilidad para dar servicios a grandes empresas con extensas carteras de clientes que requieren de herramientas digitales, proactivas y automatizadas para generar cobranzas inteligentes y sin pagar costos de pagos transaccionales basados en escalas de comisión o fees fijos.</p>
              <hr>
              <p class="text-4">Simplemente damos servicios de cobranzas digitales sin cobro de comisiones e integrando valor agregado de toda nuestra suite de productos y servicios.</p>
              <a href="#" class="btn-link text-4">Conozca nuestras herramientas<i class="fas fa-chevron-right text-2 ml-2"></i></a> </div>
          </div>
          <div class="col-lg-7 col-xl-6">
            <div class="row banner style-2 justify-content-center">
              <div class="col-12 col-sm-6 mb-4 text-center">
                <div class="item rounded shadow d-inline-block"> <a href="#">
                  <div class="caption rounded-bottom">
                    <h2 class="text-5 font-weight-400 mb-0">Grandes Industrias</h2>
                  </div>
                  <div class="banner-mask"></div>
                  <img class="img-fluid" src="images/anyone-freelancer.jpg" alt="banner"> </a> </div>
              </div>
              <div class="col-12 col-sm-6 mb-4 text-center">
                <div class="item rounded shadow d-inline-block"> <a href="#">
                  <div class="caption rounded-bottom">
                    <h2 class="text-5 font-weight-400 mb-0">Mayoristas/Retail</h2>
                  </div>
                  <div class="banner-mask"></div>
                  <img class="img-fluid" src="images/anyone-online-shopping.jpg" alt="banner"> </a> </div>
              </div>
              <div class="col-12 col-sm-6 mb-4 mb-sm-0 text-center">
                <div class="item rounded shadow d-inline-block"> <a href="#">
                  <div class="caption rounded-bottom">
                    <h2 class="text-5 font-weight-400 mb-0">Seguros</h2>
                  </div>
                  <div class="banner-mask"></div>
                  <img class="img-fluid" src="images/anyone-online-sellers.jpg" alt="banner"> </a> </div>
              </div>
              <div class="col-12 col-sm-6 text-center">
                <div class="item rounded shadow d-inline-block"> <a href="#">
                  <div class="caption rounded-bottom">
                    <h2 class="text-5 font-weight-400 mb-0">Automotrices</h2>
                  </div>
                  <div class="banner-mask"></div>
                  <img class="img-fluid" src="images/anyone-affiliate-marketing.jpg" alt="banner"> </a> </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Payment Solutions end -->

    <!-- What can you do
    ============================================= -->
    <section class="section bg-light">
      <div class="container">
        <h2 class="text-9 text-center">Medios de Pago Integrados</h2>
        <p class="text-4 text-center mb-5">Hemos realizado el esfuerzo para integrar a todos los jugadores del mercado en una misma plataforma.</p>
        <div class="row">
          <div class="col-sm-6 col-lg-3 mb-4"> <a href="#">
            <div class="featured-box style-5 rounded">
              <div class="featured-box-icon text-primary"><img src="images/medios/amex.png"></div>
              <h3>American Express</h3>
            </div>
            </a> </div>
          <div class="col-sm-6 col-lg-3 mb-4"> <a href="#">
            <div class="featured-box style-5 rounded">
              <div class="featured-box-icon text-primary"><img src="images/medios/visa.png"></div>
              <h3>VISA</h3>
            </div>
            </a> </div>
          <div class="col-sm-6 col-lg-3 mb-4"> <a href="#">
            <div class="featured-box style-5 rounded">
              <div class="featured-box-icon text-primary"><img src="images/medios/mastercard.png"></div>
              <h3>Mastercard</h3>
            </div>
            </a> </div>
          <div class="col-sm-6 col-lg-3 mb-4"> <a href="#">
            <div class="featured-box style-5 rounded">
              <div class="featured-box-icon text-primary"><img src="images/medios/naranja.png"></div>
              <h3>Tarjeta Naranja</h3>
            </div>
            </a> </div>
        </div>
        <div class="row">
          <div class="col-sm-6 col-lg-3 mb-4"> <a href="#">
            <div class="featured-box style-5 rounded">
              <div class="featured-box-icon text-primary"><img src="images/medios/cabal.png"></div>
              <h3>CABAL</h3>
            </div>
            </a> </div>
          <div class="col-sm-6 col-lg-3 mb-4"> <a href="#">
            <div class="featured-box style-5 rounded">
              <div class="featured-box-icon text-primary"><img src="images/medios/mercadopago.png"></div>
              <h3>MercadoPago</h3>
            </div>
            </a> </div>
          <div class="col-sm-6 col-lg-3 mb-4"> <a href="#">
            <div class="featured-box style-5 rounded">
              <div class="featured-box-icon text-primary"><img src="images/medios/pagofacil.png"></div>
              <h3>PagoFacil</h3>
            </div>
            </a> </div>
          <div class="col-sm-6 col-lg-3 mb-4"> <a href="#">
            <div class="featured-box style-5 rounded">
              <div class="featured-box-icon text-primary"><img src="images/medios/rapipago.png"></div>
              <h3>RapiPago</h3>
            </div>
            </a> </div>
        </div>
        <div class="row">
          <div class="col-sm-6 col-lg-3 mb-4"> <a href="#">
            <div class="featured-box style-5 rounded">
              <div class="featured-box-icon text-primary"><img src="images/medios/paypal.png"></div>
              <h3>Paypal</h3>
            </div>
            </a> </div>
          <div class="col-sm-6 col-lg-3 mb-4"> <a href="#">
            <div class="featured-box style-5 rounded">
              <div class="featured-box-icon text-primary"><img src="images/medios/2checkout.png"></div>
              <h3>2CheckOut</h3>
            </div>
            </a> </div>
          <div class="col-sm-6 col-lg-3 mb-4"> <a href="#">
            <div class="featured-box style-5 rounded">
              <div class="featured-box-icon text-primary"><img src="images/medios/cobroexpress.png"></div>
              <h3>Cobro Express</h3>
            </div>
            </a> </div>
          <div class="col-sm-6 col-lg-3 mb-4"> <a href="#">
            <div class="featured-box style-5 rounded">
              <div class="featured-box-icon text-primary"><img src="images/medios/pim.png"></div>
              <h3>PIM</h3>
            </div>
            </a> </div>
        </div>
        <div class="row">
          <div class="col-sm-6 col-lg-3 mb-4"> <a href="#">
            <div class="featured-box style-5 rounded">
              <div class="featured-box-icon text-primary"><img src="images/medios/cvu.png"></div>
              <h3>CVU</h3>
            </div>
            </a> </div>
          <div class="col-sm-6 col-lg-3 mb-4"> <a href="#">
            <div class="featured-box style-5 rounded">
              <div class="featured-box-icon text-primary"><img src="images/medios/cbu.png"></div>
              <h3>CBU</h3>
            </div>
            </a> </div>
          <div class="col-sm-6 col-lg-3 mb-4"> <a href="#">
            <div class="featured-box style-5 rounded">
              <div class="featured-box-icon text-primary"><img src="images/medios/debin.png"></div>
              <h3>Debin</h3>
            </div>
            </a> </div>
        </div>
        <div class="text-center mt-4"><a href="#" class="btn-link text-4">Conocer politicas de precios<i class="fas fa-chevron-right text-2 ml-2"></i></a></div>
      </div>
    </section>
    <!-- What can you do end -->

    <!-- How work
    ============================================= -->
    <section class="section">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <div class="card bg-dark-3 shadow-sm border-0"> <img class="card-img opacity-8" src="images/comohacemos.jpg" alt="banner">
            </div>
          </div>
          <div class="col-lg-6 mt-5 mt-lg-0">
            <div class="ml-4">
              <h2 class="text-9">¿Cómo es que lo Hacemos?</h2>
              <p class="text-4">Gracias a nuestro equipo de profesionales y nuestra trayectoria en el mercado, hemos generado alianzas durante años para poder llevarle a nuestros clientes la solución rápida, segura y más economica del mercado.</p>
              <ul class="list-unstyled text-3 line-height-5">
                <li><i class="fas fa-check mr-2"></i>Seguridad Garantizada.</li>
                <li><i class="fas fa-check mr-2"></i>Herramientas de Valor Agregado.</li>
                <li><i class="fas fa-check mr-2"></i>Politicas de precios trasparentes y sin costos extra.</li>
              </ul>
              <a href="#" class="btn btn-outline-primary shadow-none mt-2">Contacte a un Asesor</a> </div>
          </div>
        </div>
      </div>
    </section>
    <!-- How work end -->

    <!-- Testimonial
    ============================================= -->
    <section class="section bg-light">
      <div class="container">
        <h2 class="text-9 text-center">Casos de éxito</h2>
        <div class="owl-carousel owl-theme" data-autoplay="true" data-nav="true" data-loop="true" data-margin="30" data-slideby="2" data-stagepadding="5" data-items-xs="1" data-items-sm="1" data-items-md="2" data-items-lg="2">
          <div class="item">
            <div class="testimonial rounded text-center p-4">
              <p class="text-4">¡Hemos implementado procesos de cobranza con el fin de automatizar los pagos únicos y recurrentes de nuestros clientes dando la posiblidad de pagas a clientes nacionales e internacionales.!</p>
              <strong class="d-block font-weight-500">APILanding</strong> <span class="text-muted">Portal de Servicios de APIS</span> </div>
          </div>
          <div class="item">
            <div class="testimonial rounded text-center p-4">
              <p class="text-4">¡Se ha generado un proceso automatizado para cobranzas de mora temprana integrando soluciones automatizadas de campañas de envíos de SMS, Email y WhatsAPP negociando en linea el pago de la deuda.!</p>
              <strong class="d-block font-weight-500">CertiSend</strong> <span class="text-muted">Servicio Saas IT.</span> </div>
          </div>
          <div class="item">
            <div class="testimonial rounded text-center p-4">
              <p class="text-4">¡Hemos integrado en nuestro sistema de booking y reservas online la capacidad no solo de pago de nuestros clientes de forma-online sino que toda la automatización de contracargos e imputaciones.!</p>
              <strong class="d-block font-weight-500">Bosque del Nahuel</strong> <span class="text-muted">Hotel Boutique Bariloche</span> </div>
          </div>
        </div>
        <div class="text-center mt-4"><a href="#" class="btn-link text-4">Conozca más de nuestros casos de éxito.<i class="fas fa-chevron-right text-2 ml-2"></i></a></div>
      </div>
    </section>
    <!-- Testimonial end -->

    <!-- Customer Support
    ============================================= -->
    <section class="hero-wrap section shadow-md">
      <div class="hero-mask opacity-9 bg-primary"></div>
      <div class="hero-bg" style="background-image:url('images/bg/image-2.jpg');"></div>
      <div class="hero-content py-5">
        <div class="container text-center">
          <h2 class="text-9 text-white">Contacte un Asesor en Cobranzas</h2>
          <p class="text-4 text-white mb-4">¿Tiene consultas del servicio? Contacte a nuestro asesor de cobranzas online y conozca porque somos la mejor alternativa en el mercado.</p>
          <a href="contacto.php" class="btn btn-light">Contactar Ahora</a> </div>
      </div>
    </section>
    <!-- Customer Support end -->


  <!-- Footer
  ============================================= -->
  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg d-lg-flex align-items-center">
          <ul class="nav justify-content-center justify-content-lg-start text-3">
            <li class="nav-item"> <a class="nav-link active" href="nosotros.php">Nosotros</a></li>
            <li class="nav-item"> <a class="nav-link" href="soporte.php">Soporte</a></li>
            <li class="nav-item"> <a class="nav-link" href="ayuda.php">Ayuda</a></li>
            <li class="nav-item"> <a class="nav-link" href="contacto.php">Contacto</a></li>
            <li class="nav-item"> <a class="nav-link" href="nosotros.php?#partners">Partners</a></li>
            <li class="nav-item"> <a class="nav-link" href="precios.php">Precios</a></li>
          </ul>
        </div>
        <div class="col-lg d-lg-flex justify-content-lg-end mt-3 mt-lg-0">
          <ul class="social-icons justify-content-center">
            <li class="social-icons-facebook"><a data-toggle="tooltip" href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
            <li class="social-icons-twitter"><a data-toggle="tooltip" href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
            <li class="social-icons-google"><a data-toggle="tooltip" href="http://www.google.com/" target="_blank" title="Google"><i class="fab fa-google"></i></a></li>
            <li class="social-icons-youtube"><a data-toggle="tooltip" href="http://www.youtube.com/" target="_blank" title="Youtube"><i class="fab fa-youtube"></i></a></li>
          </ul>
        </div>
      </div>
      <div class="footer-copyright pt-3 pt-lg-2 mt-2">
        <div class="row">
          <div class="col-lg">
            <p class="text-center text-lg-left mb-2 mb-lg-0">Copyright &copy; 2019 <a href="index.php">APIPagos</a>. All Rights Reserved.</p>
          </div>
          <div class="col-lg d-lg-flex align-items-center justify-content-lg-end">
            <ul class="nav justify-content-center">
              <li class="nav-item"> <a class="nav-link active" href="seguridad.php">Seguridad</a></li>
              <li class="nav-item"> <a class="nav-link" href="terminos.php">Términos y Condiciones</a></li>
              <li class="nav-item"> <a class="nav-link" href="privacidad.php">Privacidad</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- Footer end -->

</div>
<!-- Document Wrapper end -->

<!-- Back to Top
============================================= -->
<a id="back-to-top" data-toggle="tooltip" title="Back to Top" href="javascript:void(0)"><i class="fa fa-chevron-up"></i></a>

<!-- Video Modal
============================================= -->
<div class="modal fade" id="videoModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content bg-transparent border-0">
      <button type="button" class="close text-white opacity-10 ml-auto mr-n3 font-weight-400" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <div class="modal-body p-0">
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" id="video" allow="autoplay"></iframe>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Video Modal end -->

<!-- Script -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="js/theme.js"></script>
</body>
</html>