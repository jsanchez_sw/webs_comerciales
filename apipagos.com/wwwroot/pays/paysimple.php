<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="iso-8859-1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
<link href="images/favicon.png" rel="icon" />

<title>APIPAGOS - El centro de los Pagos Online.</title>
<meta name="description" content="">
<meta name="author" content="">

<!-- Web Fonts
============================================= -->
<link rel='stylesheet' href='../../../fonts.googleapis.com/css2a55.css?family=Poppins:100,200,300,400,500,600,700,800,900' type='text/css'>

<!-- Stylesheet
============================================= -->
<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="vendor/font-awesome/css/all.min.css" />
<link rel="stylesheet" type="text/css" href="vendor/owl.carousel/assets/owl.carousel.min.css" />
<link rel="stylesheet" type="text/css" href="vendor/owl.carousel/assets/owl.theme.default.min.css" />
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />

</head>
<body>
<!-- Preloader -->
<div id="preloader"><div data-loader="dual-ring"></div></div><!-- Preloader End -->

<!-- Document Wrapper
============================================= -->
<div id="main-wrapper">

  <!-- Header
  ============================================= -->
  <header id="header">
    <div class="container">
      <div class="header-row">
        <div class="header-column justify-content-start">

          <!-- Logo
          ============================================= -->
          <div class="logo">
          	<a href="../index.php" title="APIPagos"><img src="images/logo.png" alt="APIPagos" width="127" height="29" /></a>
          </div><!-- Logo end -->

        </div>

        <div class="header-column justify-content-end">

          <!-- Primary Navigation
          ============================================= -->
          <nav class="primary-menu navbar navbar-expand-lg">
            <div id="header-nav" class="collapse navbar-collapse">
              <ul class="navbar-nav">
				  <li class="dropdown"> <a class="dropdown-toggle" href="../web/index.php">Sitio Comercial</a></li>
				  <li class="dropdown"> <a class="dropdown-toggle" href="../web/seguridad.php">Seguridad</a></li>
				  <li class="dropdown"> <a class="dropdown-toggle" href="../web/terminos.php">Terminos y Condiciones</a></li>
				  <li class="dropdown"> <a class="dropdown-toggle" href="../web/privacidad.php">Privacidad</a></li>
              </ul>
            </div>
          </nav><!-- Primary Navigation end -->

        </div>

        <!-- Collapse Button
        ============================================= -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header-nav"> <span></span> <span></span> <span></span> </button>
      </div>
    </div>
  </header><!-- Header end -->

  <!-- Content
  ============================================= -->
  <div id="content">

    <!-- Secondary Navigation
    ============================================= -->
    <div class="bg-secondary">
      <div class="container">
        <ul class="nav secondary-nav">
          <li class="nav-item"> <a class="nav-link active" href="recharge-bill-datacard.html"><span><i class="fas fa-credit-card"></i></span> DataCard</a> </li>
        </ul>
      </div>
    </div><!-- Secondary Navigation end -->

    <section class="container">
      <div class="bg-light shadow-md rounded p-4">
        <div class="row">

          <!-- Mobile Recharge
          ============================================= -->
          <div class="col-lg-4 mb-4 mb-lg-0">
            <h2 class="text-4 mb-3">Cobranza Digital con C�digo CIAP</h2>
            <form id="recharge-bill" method="post">
              <div class="mb-3">
                <div class="custom-control custom-radio custom-control-inline">
                  <input id="prepaid" name="rechargeBillpayment" class="custom-control-input" checked="" required type="radio">
                  <label class="custom-control-label" for="prepaid">Pago Online</label>
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" data-bv-field="number" id="mobileNumber" required placeholder="C�digo CIAP">
              </div>
              <div class="form-group">
                <select class="custom-select" id="operator" required="">
                  <option value="">Seleccionar Plan</option>
                  <option>1 Cuota</option>
                </select>
              </div>
              <div class="form-group input-group">
                <div class="input-group-prepend"> <span class="input-group-text">$</span> </div>
                <a href="#" data-target="#view-plans" data-toggle="modal" class="view-plans-link">Ver Planes</a>
                <input class="form-control" id="amount" placeholder="Importe" required type="text">
              </div>
              <button class="btn btn-primary btn-block" type="submit">Continuar con el Pago</button>
            </form>
          </div><!-- Mobile Recharge end -->

          <!-- Slideshow
          ============================================= -->
          <div class="col-lg-8">
            <div class="owl-carousel owl-theme slideshow single-slider">
              <div class="item"><a href="#"><img class="img-fluid" src="images/slider/banner-1.jpg" alt="banner 1" /></a></div>
            </div>
          </div><!-- Slideshow end -->

        </div>
      </div>
    </section>

    <!-- Tabs
    ============================================= -->
    <div class="section pt-4 pb-3">
      <div class="container">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item"> <a class="nav-link active" id="mobile-recharge-tab" data-toggle="tab" href="#mobile-recharge" role="tab" aria-controls="mobile-recharge" aria-selected="true">Pago Online</a> </li>
        </ul>
        <div class="tab-content my-3" id="myTabContent">
          <div class="tab-pane fade show active" id="mobile-recharge" role="tabpanel" aria-labelledby="mobile-recharge-tab">
            <p>API Pagos Online le permitir� no solo realizar el pago del servicio de su proveedor, sino que gracias a nuestras conexiones transparentes con su proveedor notificaremos al mismo instantaneamente reflejando la acreditaci�n inmediantamente en su cuenta.</p>
            <p>Un sistema de transacciones seguras entre todas las partes garantizan al proceso la m�xima seguridad.</p>
          </div>
        </div>
      </div>
    </div><!-- Tabs end -->


  </div><!-- Content end -->

    <!-- Footer
    ============================================= -->
    <footer id="footer">
      <section class="section bg-light shadow-md pt-4 pb-3">
        <div class="container">
          <div class="row">
            <div class="col-sm-6 col-md-3">
              <div class="featured-box text-center">
                <div class="featured-box-icon"> <i class="fas fa-lock"></i> </div>
                <h4>100% Pago Seguro</h4>
                <p>Garantizamos a nuestros clientes y compradores la seguridad.</p>
              </div>
            </div>
            <div class="col-sm-6 col-md-3">
              <div class="featured-box text-center">
                <div class="featured-box-icon"> <i class="fas fa-thumbs-up"></i> </div>
                <h4>Proteccion de Pagos</h4>
                <p>Sistema automatizado de reclamos y contracargos.</p>
              </div>
            </div>
            <div class="col-sm-6 col-md-3">
              <div class="featured-box text-center">
                <div class="featured-box-icon"> <i class="fas fa-envelope"></i> </div>
                <h4>Notificaciones</h4>
                <p>Alertas inmediatas certificadas garantizaran su compra.</p>
              </div>
            </div>
            <div class="col-sm-6 col-md-3">
              <div class="featured-box text-center">
                <div class="featured-box-icon"> <i class="far fa-life-ring"></i> </div>
                <h4>24X7 Soporte</h4>
                <p>Un equipo de soporte esta para ayudarlo en su compra. <a href="soporte.php">Contactarse</a>.</p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <div class="container mt-4">
        <div class="row">
          <div class="col-md-4 mb-3 mb-md-0">
            <p>Payment</p>
            <ul class="payments-types">
              <li><a href="#" target="_blank"> <img data-toggle="tooltip" src="images/payment/visa.png" alt="visa" title="Visa"></a></li>
              <li><a href="#" target="_blank"> <img data-toggle="tooltip" src="images/payment/discover.png" alt="discover" title="Discover"></a></li>
              <li><a href="#" target="_blank"> <img data-toggle="tooltip" src="images/payment/paypal.png" alt="paypal" title="PayPal"></a></li>
              <li><a href="#" target="_blank"> <img data-toggle="tooltip" src="images/payment/american.png" alt="american express" title="American Express"></a></li>
              <li><a href="#" target="_blank"> <img data-toggle="tooltip" src="images/payment/mastercard.png" alt="discover" title="Discover"></a></li>
            </ul>
          </div>
          <div class="col-md-4 mb-3 mb-md-0">
            <p>Suscribirse</p>
            <div class="input-group newsletter">
              <input class="form-control" placeholder="Tu Email" name="newsletterEmail" id="newsletterEmail" type="text">
              <span class="input-group-append">
              <button class="btn btn-secondary" type="submit">Subscribe</button>
              </span> </div>
          </div>
          <div class="col-md-4 d-flex align-items-md-end flex-column">
            <p>Keep in touch</p>
            <ul class="social-icons">
              <li class="social-icons-facebook"><a data-toggle="tooltip" href="https://www.facebook.com/SysWorld-Servicios-SA-323344231089747" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
              <li class="social-icons-twitter"><a data-toggle="tooltip" href="https://twitter.com/Sysworldsa" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
              <li class="social-icons-linkedin"><a data-toggle="tooltip" href="https://www.linkedin.com/company/24428924/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="footer-copyright">
          <ul class="nav justify-content-center">
            <li class="nav-item"> <a class="nav-link active" href="../web/nosotros.php">Nosotros</a> </li>
            <li class="nav-item"> <a class="nav-link" href="../web/contacto.php">Contacto</a> </li>
            <li class="nav-item"> <a class="nav-link" href="soporte.php">Soporte</a> </li>
            <li class="nav-item"> <a class="nav-link" href="../web/seguridad.php">Seguridad</a> </li>
            <li class="nav-item"> <a class="nav-link" href="../web/terminos.php">T�rminos y Condiciones</a> </li>
            <li class="nav-item"> <a class="nav-link" href="../web/privacidad.php">Privacidad</a> </li>
          </ul>
          <p class="copyright-text">Copyright � 2019 <a href="../web/index.php">APIPagos</a>. All Rights Reserved.</p>
        </div>
      </div>
  </footer><!-- Footer end -->


</div><!-- Document Wrapper end -->

<!-- Back to Top
============================================= -->
<a id="back-to-top" data-toggle="tooltip" title="Back to Top" href="javascript:void(0)"><i class="fa fa-chevron-up"></i></a>

<!-- Modal Dialog - View Plans
============================================= -->
<div id="view-plans" class="modal fade" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Browse Plans</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
      </div>
      <div class="modal-body">
        <form class="form-row mb-4 mb-sm-2" method="post">
          <div class="col-12 col-sm-6 col-lg-3">
            <div class="form-group">
              <select class="custom-select" required="">
                <option value="">Select Your Operator</option>
                <option>1st Operator</option>
                <option>2nd Operator</option>
                <option>3rd Operator</option>
                <option>4th Operator</option>
                <option>5th Operator</option>
                <option>6th Operator</option>
                <option>7th Operator</option>
              </select>
            </div>
          </div>
          <div class="col-12 col-sm-6 col-lg-3">
            <div class="form-group">
              <select class="custom-select" required="">
                <option value="">Select Your Circle</option>
                <option>1st Circle</option>
                <option>2nd Circle</option>
                <option>3rd Circle</option>
                <option>4th Circle</option>
                <option>5th Circle</option>
                <option>6th Circle</option>
                <option>7th Circle</option>
              </select>
            </div>
          </div>
          <div class="col-12 col-sm-6 col-lg-3">
            <div class="form-group">
              <select class="custom-select" required="">
                <option value="">All Plans</option>
                <option>Topup</option>
                <option>Full Talktime</option>
                <option>Validity Recharge</option>
                <option>SMS</option>
                <option>Data</option>
                <option>Unlimited Talktime</option>
                <option>STD</option>
              </select>
            </div>
          </div>
          <div class="col-12 col-sm-6 col-lg-3">
            <button class="btn btn-primary btn-block" type="submit">View Plans</button>
          </div>
        </form>
        <div class="plans">
          <div class="table-responsive-md">
            <table class="table table-hover border">
              <tbody>
                <tr>
                  <td class="text-5 text-primary text-center align-middle">$10 <span class="text-1 text-muted d-block">Amount</span></td>
                  <td class="text-3 text-center align-middle">8 <span class="text-1 text-muted d-block">Talktime</span></td>
                  <td class="text-3 text-center align-middle">7 Days <span class="text-1 text-muted d-block">Validity</span></td>
                  <td class="text-1 text-muted align-middle">Talktime $8 & 2 Local & National SMS & Free SMS valid for 2 day(s)</td>
                  <td class="align-middle"><button class="btn btn-sm btn-outline-primary shadow-none text-nowrap" type="submit">Recharge Now</button></td>
                </tr>
                <tr>
                  <td class="text-5 text-primary text-center align-middle">$15 <span class="text-1 text-muted d-block">Amount</span></td>
                  <td class="text-3 text-center align-middle">13 <span class="text-1 text-muted d-block">Talktime</span></td>
                  <td class="text-3 text-center align-middle">15 Days <span class="text-1 text-muted d-block">Validity</span></td>
                  <td class="text-1 text-muted align-middle">Regular Talktime</td>
                  <td class="align-middle"><button class="btn btn-sm btn-outline-primary shadow-none text-nowrap" type="submit">Recharge Now</button></td>
                </tr>
                <tr>
                  <td class="text-5 text-primary text-center align-middle">$50 <span class="text-1 text-muted d-block">Amount</span></td>
                  <td class="text-3 text-center align-middle">47 <span class="text-1 text-muted d-block">Talktime</span></td>
                  <td class="text-3 text-center align-middle">28 Days <span class="text-1 text-muted d-block">Validity</span></td>
                  <td class="text-1 text-muted align-middle">47 Local Vodafone min free </td>
                  <td class="align-middle"><button class="btn btn-sm btn-outline-primary shadow-none text-nowrap" type="submit">Recharge Now</button></td>
                </tr>
                <tr>
                  <td class="text-5 text-primary text-center align-middle">$100 <span class="text-1 text-muted d-block">Amount</span></td>
                  <td class="text-3 text-center align-middle">92 <span class="text-1 text-muted d-block">Talktime</span></td>
                  <td class="text-3 text-center align-middle">28 Days <span class="text-1 text-muted d-block">Validity</span></td>
                  <td class="text-1 text-muted align-middle">Local min 92 & 10 Local & National SMS & Free SMS valid for
                    7 day(s).</td>
                  <td class="align-middle"><button class="btn btn-sm btn-outline-primary shadow-none text-nowrap" type="submit">Recharge Now</button></td>
                </tr>
                <tr>
                  <td class="text-5 text-primary text-center align-middle">$150 <span class="text-1 text-muted d-block">Amount</span></td>
                  <td class="text-3 text-center align-middle">143 <span class="text-1 text-muted d-block">Talktime</span></td>
                  <td class="text-3 text-center align-middle">60 Days <span class="text-1 text-muted d-block">Validity</span></td>
                  <td class="text-1 text-muted align-middle">Talktime $143 & 50 Local & National SMS & Free SMS valid for
                    15 day(s).</td>
                  <td class="align-middle"><button class="btn btn-sm btn-outline-primary shadow-none text-nowrap" type="submit">Recharge Now</button></td>
                </tr>
                <tr>
                  <td class="text-5 text-primary text-center align-middle">$220 <span class="text-1 text-muted d-block">Amount</span></td>
                  <td class="text-3 text-center align-middle">220 <span class="text-1 text-muted d-block">Talktime</span></td>
                  <td class="text-3 text-center align-middle">28 Days <span class="text-1 text-muted d-block">Validity</span></td>
                  <td class="text-1 text-muted align-middle">Full Talktime</td>
                  <td class="align-middle"><button class="btn btn-sm btn-outline-primary shadow-none text-nowrap" type="submit">Recharge Now</button></td>
                </tr>
                <tr>
                  <td class="text-5 text-primary text-center align-middle">$250 <span class="text-1 text-muted d-block">Amount</span></td>
                  <td class="text-3 text-center align-middle">250 <span class="text-1 text-muted d-block">Talktime</span></td>
                  <td class="text-3 text-center align-middle">28 Days <span class="text-1 text-muted d-block">Validity</span></td>
                  <td class="text-1 text-muted align-middle">Full Talktime + 50 SMS per day for 7 days.</td>
                  <td class="align-middle"><button class="btn btn-sm btn-outline-primary shadow-none text-nowrap" type="submit">Recharge Now</button></td>
                </tr>
                <tr>
                  <td class="text-5 text-primary text-center align-middle">$300 <span class="text-1 text-muted d-block">Amount</span></td>
                  <td class="text-3 text-center align-middle">301 <span class="text-1 text-muted d-block">Talktime</span></td>
                  <td class="text-3 text-center align-middle">64 Days <span class="text-1 text-muted d-block">Validity</span></td>
                  <td class="text-1 text-muted align-middle">Full Talktime</td>
                  <td class="align-middle"><button class="btn btn-sm btn-outline-primary shadow-none text-nowrap" type="submit">Recharge Now</button></td>
                </tr>
                <tr>
                  <td class="text-5 text-primary text-center align-middle">$410 <span class="text-1 text-muted d-block">Amount</span></td>
                  <td class="text-3 text-center align-middle">0 <span class="text-1 text-muted d-block">Talktime</span></td>
                  <td class="text-3 text-center align-middle">28 Days <span class="text-1 text-muted d-block">Validity</span></td>
                  <td class="text-1 text-muted align-middle">Unlimited Local,STD & Roaming calls</td>
                  <td class="align-middle"><button class="btn btn-sm btn-outline-primary shadow-none text-nowrap" type="submit">Recharge Now</button></td>
                </tr>
                <tr>
                  <td class="text-5 text-primary text-center align-middle">$501 <span class="text-1 text-muted d-block">Amount</span></td>
                  <td class="text-3 text-center align-middle">510 <span class="text-1 text-muted d-block">Talktime</span></td>
                  <td class="text-3 text-center align-middle">180 Days <span class="text-1 text-muted d-block">Validity</span></td>
                  <td class="text-1 text-muted align-middle">Full Talktime + 100 SMS per day for 28 days.</td>
                  <td class="align-middle"><button class="btn btn-sm btn-outline-primary shadow-none text-nowrap" type="submit">Recharge Now</button></td>
                </tr>
                <tr>
                  <td class="text-5 text-primary text-center align-middle">$799 <span class="text-1 text-muted d-block">Amount</span></td>
                  <td class="text-3 text-center align-middle">820 <span class="text-1 text-muted d-block">Talktime</span></td>
                  <td class="text-3 text-center align-middle">250 Days <span class="text-1 text-muted d-block">Validity</span></td>
                  <td class="text-1 text-muted align-middle">Full Talktime + 100 SMS per day for 84 days.</td>
                  <td class="align-middle"><button class="btn btn-sm btn-outline-primary shadow-none text-nowrap" type="submit">Recharge Now</button></td>
                </tr>
                <tr>
                  <td class="text-5 text-primary text-center align-middle">$999 <span class="text-1 text-muted d-block">Amount</span></td>
                  <td class="text-3 text-center align-middle">1099 <span class="text-1 text-muted d-block">Talktime</span></td>
                  <td class="text-3 text-center align-middle">356 Days <span class="text-1 text-muted d-block">Validity</span></td>
                  <td class="text-1 text-muted align-middle">Full Talktime + 100 SMS per day for 90 days.</td>
                  <td class="align-middle"><button class="btn btn-sm btn-outline-primary shadow-none text-nowrap" type="submit">Recharge Now</button></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div><!-- Modal Dialog - View Plans end -->

<!-- Modal Dialog - Login/Signup
============================================= -->
<div id="login-signup" class="modal fade" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content p-sm-3">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
        <ul class="nav nav-tabs" role="tablist">
          <li class="nav-item"> <a id="login-tab" class="nav-link active text-4" data-toggle="tab" href="#login" role="tab" aria-controls="login" aria-selected="true">Login</a> </li>
          <li class="nav-item"> <a id="signup-tab" class="nav-link text-4" data-toggle="tab" href="#signup" role="tab" aria-controls="signup" aria-selected="false">Sign Up</a> </li>
        </ul>
        <div class="tab-content pt-4">
          <div class="tab-pane fade show active" id="login" role="tabpanel" aria-labelledby="login-tab">
            <form id="loginForm" method="post">
              <div class="form-group">
                <input type="email" class="form-control" id="loginMobile" required placeholder="Mobile or Email ID">
              </div>
              <div class="form-group">
                <input type="password" class="form-control" id="loginPassword" required placeholder="Password">
              </div>
              <div class="row mb-4">
                <div class="col-sm">
                  <div class="form-check custom-control custom-checkbox">
                    <input id="remember-me" name="remember" class="custom-control-input" type="checkbox">
                    <label class="custom-control-label" for="remember-me">Remember Me</label>
                  </div>
                </div>
                <div class="col-sm text-right"> <a class="justify-content-end" href="#">Forgot Password ?</a> </div>
              </div>
              <button class="btn btn-primary btn-block" type="submit">Login</button>
            </form>
          </div>
          <div class="tab-pane fade" id="signup" role="tabpanel" aria-labelledby="signup-tab">
            <form id="signupForm" method="post">
              <div class="form-group">
                <input type="text" class="form-control" data-bv-field="number" id="signupEmail" required placeholder="Email ID">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" id="signupMobile" required placeholder="Mobile Number">
              </div>
              <div class="form-group">
                <input type="password" class="form-control" id="signuploginPassword" required placeholder="Password">
              </div>
              <button class="btn btn-primary btn-block" type="submit">Signup</button>
            </form>
          </div>
          <div class="d-flex align-items-center my-4">
            <hr class="flex-grow-1">
            <span class="mx-2">OR</span>
            <hr class="flex-grow-1">
          </div>
          <div class="row">
            <div class="col-12 mb-3">
              <button type="button" class="btn btn-block btn-outline-primary">Login with Facebook</button>
            </div>
            <div class="col-12">
              <button type="button" class="btn btn-block btn-outline-danger">Login with Google</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div><!-- Modal Dialog - Login/Signup end -->

<!-- Script -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="js/theme.js"></script>

</body>
</html>