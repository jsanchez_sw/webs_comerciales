<?php
    include 'functions/db.php';

    include_once 'functions/language.php';
?>

<!DOCTYPE html>
<html lang="<?php echo $lang['SITE_lang'];?>">

<!-- Mirrored from protechtheme.com/saas/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:17:32 GMT -->
<head>
        <!-- Required meta tags -->
        <meta charset="iso-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="keywords" content="<?php echo $lang['SITE_keywords'];?>">
		<meta name="description" content="<?php echo $lang['SITE_desc'];?>">
		<meta name="author" content="Sysworld Servicios S.A.">
        <title><?php echo $lang['SITE_title'];?></title>
        <!-- Bootstrap core CSS -->
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Select2 -->
        <link href="assets/select2/css/select2.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- Iconmoon -->
        <link href="assets/iconmoon/css/iconmoon.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="css/custom.css" rel="stylesheet">
        <link rel="icon" type="image/x-icon" href="images/favicon.ico" />
    </head>
    <body>

<!-- ==============================================
**Preloader**
=================================================== -->
        <div id="loader">
            <div id="element">
                <div class="circ-one"></div>
                <div class="circ-two"></div>
            </div>
        </div>

<!-- ==============================================
**Header**
=================================================== -->
        <header>
			<?php include 'menu.php';?>
        </header>

<!-- ==============================================
**Inner Banner**
=================================================== -->
        <section class="inner-banner how-it-works-banner">
            <div class="container">
                <div class="contents">
                    <h1><?php echo $lang['NETWORK_header_tittle'];?></h1>
                    <p><?php echo $lang['NETWORK_header_subtittle'];?></p>
                </div>
            </div>
        </section>

<!-- ==============================================
**How it works**
=================================================== -->
        <section class="how-it-work-items padding-lg">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="content-area">
                            <div class="icon"><span>01</span></div>
                            <div class="text-area">
                                <h2><?php echo $lang['NETWORK_section1_item1_title'];?></h2>
                                <p><?php echo $lang['NETWORK_section1_item1_subtitle'];?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <figure class="right"><img src="images/how-it-works-img-1.png" class="img-fluid" alt=""></figure>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 order-2 order-md-12">
                        <figure><img src="images/how-it-works-img-2.png" class="img-fluid" alt=""></figure>
                    </div>
                    <div class="col-md-6 order-1 order-md-12">
                        <div class="content-area">
                            <div class="icon"><span>02</span></div>
                            <div class="text-area">
                                <h2><?php echo $lang['NETWORK_section1_item2_title'];?></h2>
                                <p><?php echo $lang['NETWORK_section1_item2_subtitle'];?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="content-area">
                            <div class="icon"><span>03</span></div>
                            <div class="text-area">
                                <h2><?php echo $lang['NETWORK_section1_item3_title'];?></h2>
                                <p><?php echo $lang['NETWORK_section1_item3_subtitle'];?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <figure class="right"><img src="images/how-it-works-img-3.png" class="img-fluid" alt=""></figure>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 order-2 order-md-12">
                        <figure><img src="images/how-it-works-img-4.png" class="img-fluid" alt=""></figure>
                    </div>
                    <div class="col-md-6 order-1 order-md-12">
                        <div class="content-area">
                            <div class="icon"><span>04</span></div>
                            <div class="text-area">
                                <h2><?php echo $lang['NETWORK_section1_item4_title'];?></h2>
                                <p><?php echo $lang['NETWORK_section1_item4_subtitle'];?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<!-- ==============================================
**Signup Section**
=================================================== -->


<!-- ==============================================
**Footer opt1**
=================================================== -->

	<?php include 'footer.php';?>



        <!-- Scroll to top -->
        <a href="#" class="scroll-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>

        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery.min.js"></script>
        <!-- Popper JS -->
        <script src="js/popper.min.js"></script>
        <!-- Bootsrap JS -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- Select2 JS -->
        <script src="assets/select2/js/select2.min.js"></script>
        <!-- Custom JS -->
        <script src="js/custom.js"></script>
    </body>

<!-- Mirrored from protechtheme.com/saas/how-it-works.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:17:41 GMT -->
</html>