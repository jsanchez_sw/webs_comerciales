<?php
    include 'functions/db.php';

    include_once 'functions/language.php';
?>

<!DOCTYPE html>
<html lang="<?php echo $lang['SITE_lang'];?>">

<!-- Mirrored from protechtheme.com/saas/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:17:32 GMT -->
<head>
        <!-- Required meta tags -->
        <meta charset="iso-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="keywords" content="<?php echo $lang['SITE_keywords'];?>">
		<meta name="description" content="<?php echo $lang['SITE_desc'];?>">
		<meta name="author" content="Sysworld Servicios S.A.">
        <title><?php echo $lang['SITE_title'];?></title>
        <!-- Bootstrap core CSS -->
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Select2 -->
        <link href="assets/select2/css/select2.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- Iconmoon -->
        <link href="assets/iconmoon/css/iconmoon.css" rel="stylesheet">
        <!-- Owl Carousel -->
        <link href="assets/owl-carousel/css/owl.carousel.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="css/custom.css" rel="stylesheet">
        <link rel="icon" type="image/x-icon" href="images/favicon.ico" />
    </head>
    <body>

<!-- ==============================================
**Preloader**
=================================================== -->
        <div id="loader">
            <div id="element">
                <div class="circ-one"></div>
                <div class="circ-two"></div>
            </div>
        </div>

<!-- ==============================================
**Header**
=================================================== -->
        <header>
			<?php include 'menu.php';?>
        </header>

<!-- ==============================================
**Inner Banner**
=================================================== -->
        <section class="inner-banner career-banner">
            <div class="container">
                <div class="contents">
                    <h1><?php echo $lang['QUALITY_header_tittle'];?></h1>
                    <p><?php echo $lang['QUALITY_header_subtittle'];?></p>
            </div>
        </section>

<!-- ==============================================
**Career Our Values**
=================================================== -->
        <section class="career-our-values-sec">
            <div class="container">

                <ul class="row our-values opt2">
                    <li class="col-md-6">
                        <div class="icon"><span class="icon-chair"></span></div>
                        <div class="text-area">
                            <h6><?php echo $lang['QUALITY_section1_item1_tittle'];?></h6>
                            <p><?php echo $lang['QUALITY_section1_item1_subtittle'];?></p>
                        </div>
                    </li>
                    <li class="col-md-6">
                        <div class="icon"><span class="icon-simple"></span></div>
                        <div class="text-area">
                            <h6><?php echo $lang['QUALITY_section1_item2_tittle'];?></h6>
                            <p><?php echo $lang['QUALITY_section1_item2_subtittle'];?></p>
                        </div>
                    </li>
                    <li class="col-md-6">
                        <div class="icon"><span class="icon-focus"></span></div>
                        <div class="text-area">
                            <h6><?php echo $lang['QUALITY_section1_item3_tittle'];?></h6>
                            <p><?php echo $lang['QUALITY_section1_item3_subtittle'];?></p>
                        </div>
                    </li>
                    <li class="col-md-6">
                        <div class="icon"><span class="icon-love"></span></div>
                        <div class="text-area">
                            <h6><?php echo $lang['QUALITY_section1_item4_tittle'];?></h6>
                            <p><?php echo $lang['QUALITY_section1_item4_subtittle'];?></p>
                        </div>
                    </li>
                    <li class="col-md-6">
                        <div class="icon"><span class="icon-feedback"></span></div>
                        <div class="text-area">
                            <h6><?php echo $lang['QUALITY_section1_item5_tittle'];?></h6>
                            <p><?php echo $lang['QUALITY_section1_item5_subtittle'];?></p>
                        </div>
                    </li>
                    <li class="col-md-6">
                        <div class="icon"><span class="icon-healthy"></span></div>
                        <div class="text-area">
                            <h6><?php echo $lang['QUALITY_section1_item6_tittle'];?></h6>
                            <p><?php echo $lang['QUALITY_section1_item6_subtittle'];?></p>
                        </div>
                    </li>
                </ul>
            </div>
        </section>

<!-- ==============================================
**Carousel**
=================================================== -->
        <section class="career-carousel-outer">
            <div class="career-carousel-sec">
                <ul id="owl-career" class="owl-carousel">
                    <li class="item"><img src="images/career-carousel-img-1.jpg" alt=""></li>
                    <li class="item"><img src="images/career-carousel-img-2.jpg" alt=""></li>
                    <li class="item"><img src="images/career-carousel-img-3.jpg" alt=""></li>
                    <li class="item"><img src="images/career-carousel-img-4.jpg" alt=""></li>
                    <li class="item"><img src="images/career-carousel-img-2.jpg" alt=""></li>
                    <li class="item"><img src="images/career-carousel-img-1.jpg" alt=""></li>
                    <li class="item"><img src="images/career-carousel-img-3.jpg" alt=""></li>
                    <li class="item"><img src="images/career-carousel-img-4.jpg" alt=""></li>
                </ul>
            </div>
        </section>

<!-- ==============================================
**What can we do**
=================================================== -->

<!-- ==============================================
**Signup Section**
=================================================== -->

<!-- ==============================================
**Footer opt1**
=================================================== -->
<?php
    include 'footer.php';
?>

        <!-- Scroll to top -->
        <a href="#" class="scroll-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>

        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery.min.js"></script>
        <!-- Popper JS -->
        <script src="js/popper.min.js"></script>
        <!-- Bootsrap JS -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- Select2 JS -->
        <script src="assets/select2/js/select2.min.js"></script>
        <!-- Owl Carousal JS -->
        <script src="assets/owl-carousel/js/owl.carousel.min.js"></script>
        <!-- Custom JS -->
        <script src="js/custom.js"></script>
    </body>

<!-- Mirrored from protechtheme.com/saas/career1.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:18:07 GMT -->
</html>