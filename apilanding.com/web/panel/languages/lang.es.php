<?php

$lang = array();


$lang['SITE_lang'] = 'es';
$lang['SITE_title'] = 'API-Center | ApiLanding.com - El Centro de las APIS';
$lang['SITE_desc'] = 'Sitio y store especializado en APIS e integraciones para empresas y negocios orientados a consumir servicios de tercera parte con valor agregado.';
$lang['SITE_keywords'] = 'api, apis, marketplace apis,';
///////////////////////////////////////////////////////////////////////////////////
//MENU.PHP
///////////////////////////////////////////////////////////////////////////////////

$lang['MENU_Header_CoorpSite'] = 'Sitio Comercial';
$lang['MENU_Header_Support'] = 'Soporte';
$lang['MENU_bottom1'] = 'Activar Suscripci�n';
$lang['MENU_bottom2'] = 'API CENTER';

$lang['MENU_DASHBOARD'] = 'HOME';
$lang['MENU_APIS'] = 'Cat�logo APIS';
$lang['MENU_APIS_APILIST'] = 'Listado de APIS';
$lang['MENU_APIS_RUN'] = 'Ejecutar API';
$lang['MENU_APIS_SERVERS'] = 'Servidores';
$lang['MENU_APIS_APICENTER'] = 'API Center';
$lang['MENU_APIS_APIDOCS'] = 'API Docs';
$lang['MENU_APIS_APISKEY'] = 'Mis Claves';
$lang['MENU_APIS_APISFILE_MIX'] = 'Procesar Datos';
$lang['MENU_APIS_CONSUMPTION'] = 'Consumos';
$lang['MENU_APIS_FILE'] = 'Mis API-File';
$lang['MENU_APIS_DB'] = 'Mis API-DB';


$lang['MENU_ADMINISTRATION_LOG'] = 'Log Transacciones';
$lang['MENU_ADMINISTRATION'] = 'Administraci�n';
$lang['MENU_ADMINISTRATION_SUSCRIPTION'] = 'Suscripci�n';
$lang['MENU_ADMINISTRATION_CHANGEPASS'] = 'Cambiar Suscripci�n';
$lang['MENU_ADMINISTRATION_INVOICES'] = 'Facturas';

$lang['MENU_ACOUNT'] = 'Mi Cuenta';
$lang['MENU_ACOUNT_CHANGE'] = 'Cambiar Contrase�a';
$lang['MENU_ACOUNT_CLOSE'] = 'Cerrar Sesi�n';

$lang['MENU_SUPPORT'] = 'Soporte';
$lang['MENU_SUPPORT_TUTORIALS'] = 'Tutoriales';
$lang['MENU_SUPPORT_CHAT'] = 'Chat en Vivo';
$lang['MENU_SUPPORT_TICKET'] = 'Cargar Ticket';

$lang['MENU_COMERCIAL'] = 'Sitio Coorporativo';

$lang['MENU_menu_bottom1'] = 'Activar Suscripci�n';
$lang['MENU_menu_bottom2'] = 'Sitio Comercial';

///////////////////////////////////////////////////////////////////////////////////
//REGISTER.PHP
///////////////////////////////////////////////////////////////////////////////////

$lang['REGISTER_header_title'] = 'Registrese Gratis';
$lang['REGISTER_form_company'] = 'Empresa';
$lang['REGISTER_form_email'] = 'Email';
$lang['REGISTER_form_pass'] = 'Contrase?a';
$lang['REGISTER_form_pass2'] = 'Repetir Contrase?a';
$lang['REGISTER_form_suscription'] = 'Suscripci?n';
$lang['REGISTER_form_terms'] = 'Acepto los t?rminos y condiciones';
$lang['REGISTER_form_button'] = 'Crear Cuenta';
define('_REGISTER_form_login', '?Ya tiene una cuenta? <a href="login.php">Acceder</a>');


///////////////////////////////////////////////////////////////////////////////////
//LOGIN.PHP
///////////////////////////////////////////////////////////////////////////////////

$lang['LOGIN_header_title'] = 'Acceder al Sistema';
$lang['LOGIN_form_company'] = 'Empresa';
$lang['LOGIN_form_email'] = 'Email';
$lang['LOGIN_form_pass'] = 'Contrase?a';
$lang['LOGIN_form_login'] = 'Acceder';
$lang['LOGIN_form_remember'] = 'Recordarme';
$lang['LOGIN_form_recovery'] = '?Olvido su Contrase?a?';
define('_LOGIN_form_register', '?No tiene una cuenta? <a href="register.php">Registrese Gratis</a>');

///////////////////////////////////////////////////////////////////////////////////
//INDEX.PHP
///////////////////////////////////////////////////////////////////////////////////

$lang['INDEX_page_tittle'] = 'Apilanding';
$lang['INDEX_page_search'] = 'Buscar';
$lang['INDEX_page_messages'] = '&nbsp;';
$lang['INDEX_page_notifications'] = 'Ver todas las notificaciones';
$lang['INDEX_page_task1'] = 'Completo';
$lang['INDEX_page_task2'] = 'Completo';
$lang['INDEX_page_task3'] = 'Completo';
$lang['INDEX_page_task4'] = 'Completo';
$lang['INDEX_page_task_options'] = 'Ver Todas las Estad�sticas';
$lang['INDEX_page_profile_options1'] = 'Perfil';
$lang['INDEX_page_profile_options2'] = 'Bandeja de Entrada';
$lang['INDEX_page_profile_options3'] = 'Configurar Cuenta';
$lang['INDEX_page_profile_options4'] = 'Cerrar sesi�n';

$lang['INDEX_page_panel_service'] = 'PANEL DE SERVICIO';
$lang['INDEX_page_panel_service_item1'] = 'Opciones de dise�o';
$lang['INDEX_page_panel_service_item2'] = 'Color del panel superior';
$lang['INDEX_page_panel_service_item3'] = 'Color del panel lateral';
$lang['INDEX_page_panel_service_item4'] = 'Opciones de chat';

$lang['INDEX_page_item1'] = 'Consumos del Mes';
$lang['INDEX_page_item2'] = 'Limites por API';
$lang['INDEX_page_item3'] = 'Consumo APIS Incluidas';
$lang['INDEX_page_item4'] = 'Consumo APIS Adicionales';
$lang['INDEX_page_item5_statistics1'] = 'Reporte de Consumos';
$lang['INDEX_page_item5_statistics2'] = 'Creditos Bonificados';
$lang['INDEX_page_item5_statistics3'] = 'Creditos Adicionales';

///////////////////////////////////////////////////////////////////////////////////
//SUSCRIPTIONS.PHP
///////////////////////////////////////////////////////////////////////////////////

$lang['SUSCRIPTIONS_page_tittle'] = 'Suscripciones';
$lang['SUSCRIPTIONS_page_search'] = 'Buscar';
$lang['SUSCRIPTIONS_page_activity'] = 'Actividad';
$lang['SUSCRIPTIONS_page_profile'] = 'Perfil';
$lang['SUSCRIPTIONS_page_messages'] = 'Mensajes';
$lang['SUSCRIPTIONS_page_settings'] = 'Configuraci�n';

$lang['SUSCRIPTIONS_page_fullname'] = 'Nombre Completo';
$lang['SUSCRIPTIONS_page_mobile'] = 'Celular';
$lang['SUSCRIPTIONS_page_email'] = 'Email';
$lang['SUSCRIPTIONS_page_location'] = 'locaci�n';
$lang['SUSCRIPTIONS_page_password'] = 'Contrase�a';
$lang['SUSCRIPTIONS_page_phone'] = 'N�mero de Celular';
$lang['SUSCRIPTIONS_page_country'] = 'Seleciona el pa�s';
$lang['SUSCRIPTIONS_page_profile_bottom'] = 'Actualizar Perfil';

///////////////////////////////////////////////////////////////////////////////////
//CHANGESUSC.PHP
///////////////////////////////////////////////////////////////////////////////////

$lang['CHANGESUSC_page_tittle'] = 'Cambiar Suscripci�n';
$lang['CHANGESUSC_page_search'] = 'Buscar';

$lang['CHANGESUSC_page_silver'] = '';
$lang['CHANGESUSC_page_silver_item1'] = 'Soporte Web';
$lang['CHANGESUSC_page_silver_item2'] = '100 Llamadas Diarias';
$lang['CHANGESUSC_page_silver_item3'] = '100 Llamadas Mensuales';
$lang['CHANGESUSC_page_silver_item4'] = 'Alta Disponibilidad';

$lang['CHANGESUSC_page_gold'] = 'BRONZE';
$lang['CHANGESUSC_page_gold_item1'] = 'Soporte Cloud';
$lang['CHANGESUSC_page_gold_item2'] = '500 Llamadas Diarias';
$lang['CHANGESUSC_page_gold_item3'] = '10.000 Llamadas Mensuales';
$lang['CHANGESUSC_page_gold_item4'] = 'Alta Disponibilidad';

$lang['CHANGESUSC_page_platinum'] = 'SILVER';
$lang['CHANGESUSC_page_platinum_item1'] = 'Soporte Cloud';
$lang['CHANGESUSC_page_platinum_item2'] = '5.000 Llamadas Diarias';
$lang['CHANGESUSC_page_platinum_item3'] = '100.000 Llamadas Mensuales';
$lang['CHANGESUSC_page_platinum_item4'] = 'Alta Disponibilidad';

$lang['CHANGESUSC_page_diamond'] = 'GOLD';
$lang['CHANGESUSC_page_diamond_item1'] = 'Soporte Cloud';
$lang['CHANGESUSC_page_diamond_item2'] = '50.000 Llamadas Diarias';
$lang['CHANGESUSC_page_diamond_item3'] = '1.000.000 Llamadas Mensuales';
$lang['CHANGESUSC_page_diamond_item4'] = 'Alta Disponibilidad';

$lang['CHANGESUSC_page_month'] = 'Por Mes';
$lang['CHANGESUSC_page_susc_bottom'] = 'Empezar Ahora!';
$lang['CHANGESUSC_page_susc_cancel_bottom'] = 'Cancelar Suscripci�n!';

///////////////////////////////////////////////////////////////////////////////////
//RECOVERPW.PHP
///////////////////////////////////////////////////////////////////////////////////

$lang['RECOVERPW_page_tittle'] = 'Recuperar Contrase�a';
$lang['RECOVERPW_page_name'] = 'Nombre';
$lang['RECOVERPW_page_email'] = 'Email';
$lang['RECOVERPW_page_bottom'] = 'Recuperar';

///////////////////////////////////////////////////////////////////////////////////
//APILIST.PHP
///////////////////////////////////////////////////////////////////////////////////

$lang['APILIST_page_tittle'] = 'Listado de Apis';
$lang['APILIST_page_search'] = 'Buscar';
$lang['APILIST_page_container_tittle'] = 'Listado de Apis';
$lang['APILIST_page_container_subtittle'] = 'Conozca todo el listado de APIS Disponibles en nuestro Cloud API Center.';

///////////////////////////////////////////////////////////////////////////////////
//PAYMENTS.PHP
///////////////////////////////////////////////////////////////////////////////////

$lang['PAYMENTS_page_tittle'] = 'Consumos';
$lang['PAYMENTS_page_search'] = 'Buscar';
$lang['PAYMENTS_page_container_tittle'] = 'Listado de Consumos';
$lang['PAYMENTS_page_container_subtittle'] = 'Conozca todo el listado de Consumos y Ordenes de Servicio Generadas.';

///////////////////////////////////////////////////////////////////////////////////
//APIKEY.PHP
///////////////////////////////////////////////////////////////////////////////////

$lang['APIKEY_page_tittle'] = 'Api Key';
$lang['APIKEY_page_search'] = 'Buscar';
$lang['APILEY_page_container_tittle'] = 'Listado de APIS Key Token';
$lang['APILEY_page_container_subtittle'] = 'Obtenga los API KEY Token para utilizar las APIS.';



?>