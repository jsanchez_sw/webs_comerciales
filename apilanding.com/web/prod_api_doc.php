<?php
    include 'functions/db.php';

    include_once 'functions/language.php';
?>
<!DOCTYPE html>
<html lang="<?php echo $lang['SITE_lang'];?>">

<!-- Mirrored from protechtheme.com/saas/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:17:32 GMT -->
<head>
        <!-- Required meta tags -->
        <meta charset="iso-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="keywords" content="<?php echo $lang['SITE_keywords'];?>">
		<meta name="description" content="<?php echo $lang['SITE_desc'];?>">
		<meta name="author" content="Sysworld Servicios S.A.">
        <title><?php echo $lang['SITE_title'];?></title>

        <!-- Bootstrap core CSS -->
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Select2 -->
        <link href="assets/select2/css/select2.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- Iconmoon -->
        <link href="assets/iconmoon/css/iconmoon.css" rel="stylesheet">
        <!-- Video Popup -->
        <link href="assets/magnific-popup/css/magnific-popup.css" rel="stylesheet">
        <!-- Owl Carousel -->
        <link href="assets/owl-carousel/css/owl.carousel.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="css/custom.css" rel="stylesheet">
        <link rel="icon" type="image/x-icon" href="images/favicon.ico" />
    </head>
    <body>

<!-- ==============================================
**Preloader**
=================================================== -->
        <div id="loader">
            <div id="element">
                <div class="circ-one"></div>
                <div class="circ-two"></div>
            </div>
        </div>

<!-- ==============================================
**Header**
=================================================== -->
        <header>
			<?php include 'menu.php';?>
        </header>

<!-- ==============================================
**Inner Banner**
=================================================== -->
        <section class="inner-banner features-banner">
            <div class="container">
                <div class="contents">
                    <h1>Funcionalidades</h1>
                    <p>APILanding es el resultado de un cloud basado en servidores de alta disponibilidad que le da alojamiento a cientos de APIS y sistemas de integraci�n.</p>
                </div>
            </div>
        </section>

<!-- ==============================================
**More Features**
=================================================== -->
        <section class="more-features padding-lg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5">
                        <h2>API_Doc<br>Documentaci�n OnLine</h2>
                        <p>Gracias a nuestra herramienta de API-Doc podr� conocer y compartir la documentaci�n de cada una de las APIS de forma simple y sencilla sin requerir largos procesos de espera de documentaci�n y posterior an�lisis.</p>
                        <ul class="more-features-list">
                            <li>
                                <p>Gr�ficos de monitoreo y analisis de consumo por tipo de API y fechas de actividad.</p>
                            </li>
                            <li>
                                <p>Indicadores de tipo widget contadores indicando consumos y cr�ditos disponibles para la suscripci�n.</p>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-7">
                        <div class="img-holder">
                            <figure class="img"><img src="images/more-features-img.png" class="img-fluid" alt=""></figure>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<!-- ==============================================
**Awsome Design**
=================================================== -->
        <section class="awesome-design padding-lg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <h2>Integre nuestras APIS en 5 minutos.</h2>
                        <p>Gracias a nuestros portales API-Center, API-Doc y API-Test usted con tan solo registrarse podr� disponer en tan solo 5 minutos de la documentaci�n de las apis, las claves token de prueba y el sistema de testing online sin que tenga que escribir una sola linea de c�digo fuente.</p>
                    </div>
                </div>
                <div class="vertical-tab-outer clearfix">
                    <div class="tab-area">
                        <div class="tabs-vertical">
                            <!-- Start : tab menu -->
                            <ul>
                                <li class="active" rel="tab-1">
                                    <figure class="icon-div"><img src="images/link-building-ico.png" class="rounded-circle img-fluid" alt=""></figure>
                                    <div class="text-div">
                                        <h4>API-Center</h4>
                                        <p>Gracias a nuestro API-Center podr� obtener los tokens de las APIS y gestionar sus l�mites y consumos.</p>
                                    </div>
                                </li>
                                <li rel="tab-2" class="">
                                    <figure class="icon-div"><img src="images/seo-succes-ico.png" class="rounded-circle img-fluid" alt=""></figure>
                                    <div class="text-div">
                                        <h4>API-DOC</h4>
                                        <p>Mediente nuestro portal API-Doc acceder� a toda la documentaci�n en linea y ejemplos de c�digo fuente.</p>
                                    </div>
                                </li>
                                <li rel="tab-3">
                                    <figure class="icon-div"><img src="images/audience-ico.png" class="rounded-circle img-fluid" alt=""></figure>
                                    <div class="text-div">
                                        <h4>API-Testing</h4>
                                        <p>El portal API-Doc le permitir� proba todas las APIS que desee de forma online y sin escribir c�digo.</p>
                                    </div>
                                </li>
                            </ul>
                            <!-- End : tab menu -->
                        </div>
                        <!-- Start : accordion-container -->
                        <div class="tab-vertical-container">
                            <!-- Start : #accordion1 -->
                            <div class="tab-drawer-heading active-item" rel="tab-1">
                                <div class="text-div">
                                    <h4>Scalable Link Building Solutions</h4>
                                </div>
                            </div>
                            <div id="tab-1" class="tab-vertical-content" >
                                <figure><img src="images/awesome-design-img.png" alt=""></figure>
                            </div>
                            <!-- End : #accordion1 -->

                            <!-- Start : #accordion2 -->
                            <div class="tab-drawer-heading" rel="tab-2">
                                <div class="text-div">
                                    <h4>Clear And Ethical GuidanceTo SEO Success</h4>
                                </div>
                            </div>
                            <div id="tab-2" class="tab-vertical-content" >
                                <figure><img src="images/awesome2.png" alt=""></figure>
                            </div>
                            <!-- End : #accordion2-->

                            <!-- Start : #accordion3 -->
                            <div class="tab-drawer-heading" rel="tab-3">
                                <div class="text-div">
                                    <h4>Audience First Content Marketing</h4>
                                </div>
                            </div>
                            <div id="tab-3" class="tab-vertical-content" >
                                <figure><img src="images/awesome3.png" alt=""></figure>
                            </div>
                            <!-- End : #accordion3 -->
                        </div>
                        <!-- End :accordion-container -->
                    </div>
                </div>
            </div>
        </section>

<!-- ==============================================
**Counters Sec**
=================================================== -->
        <section class="generate-forms padding-lg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <h2>Nuestro N�meros</h2>
                        <p class="padd-sm">Nuestros n�meros hablan por nosotros. Miles de clientes confian en nuestros servicios y nos lo hacen saber minuto a minuto generando trafico en nuestro cloud.</p>
                    </div>
                </div>
                <ul class="counter-listing">
                    <li>
                        <div class="couter-outer"><span class="counter">190</span><span>+</span></div>
                        <span class="sub-title">Paises</span> </li>
                    <li>
                        <div class="couter-outer"><span class="counter">1200</span><span>M</span></div>
                        <span class="sub-title">Transacciones</span> </li>
                    <li>
                        <div class="couter-outer"><span class="counter">200</span><span>B</span></div>
                        <span class="sub-title">TB Bandwith</span> </li>
                </ul>

                <!-- Start: Caurosel -->
                <div class="features-carousel-sec">
                    <div class="owl-carousel owl-feature">
                        <div class="item"><img src="images/funtion_01.png"  alt=""></div>
                        <div class="item"><img src="images/function_02.png"  alt=""></div>
                        <div class="item"><img src="images/function_03.png"  alt=""></div>
                    </div>
                </div>
                <!-- End: Caurosel -->
            </div>
        </section>

<!-- ==============================================
**Our Features opt2**
=================================================== -->
        <section class="client-speak our-features padding-lg">
            <div class="container">
                <div class="row justify-content-center head-block">
                    <div class="col-md-10"> <span>Our Features</span>
                        <h2>Nuestro Valor Agregado</h2>
                        <p class="hidden-xs">Un equipo de profesionales altamente capacitados trabaja d�a a d�a creyendo que lo que hacemos le simplifica la vida a cientos de miles de personas y cuida el medioambiente reduciendo la huella de carbono.</p>
                    </div>
                </div>
                <ul class="row features-listing ico-bg">
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><span class="icon-analytics"></span></span>
                            <h3>Analisis de Consumos</h3>
                            <p>Gracias a nuestro panel de control comercial podr� controlar y monitorear su actividad, sus consumos y sus l�mites por cada una de las Suscripciones y APIS contratadas.</p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><span class="icon-responsive"></span></span>
                            <h3>M�ltiples Lenguajes</h3>
                            <p>Gracias a que nuestros servicios de integraciones estan basados en standares internaciones, podr� consumir los mismos desde cualquier lenguaje de programaci�n.</p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><span class="icon-support"></span></span>
                            <h3>Soporte Especializado</h3>
                            <p>Un equipo de soporte especializado y orientado a diferentes industrias lo acompa�aran y asesorar�n en cada una de las evaluaciones y/o integraciones que desee afrontra.</p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><span class="icon-settings"></span></span>
                            <h3>Soluciones a Medida</h3>
                            <p>En caso que lo requiera y gracias a la flexibilidad de parametrizaci�n de nuestro cloud, tenemos la capacidad de disponibilizarle recursos a medida de sus procesos.</p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><span class="icon-file"></span></span>
                            <h3>Innovaci�n</h3>
                            <p>Gracias a que creemos y nos adaptamos a la velocidad con la que cambian las tecnolog�as y las formas de consumir, es que estamos siempre agregando nuevas soluciones.</p>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="inner"> <span class="icon"><span class="icon-graphic"></span></span>
                            <h3>Escalabilidad</h3>
                            <p>Motivo de una ingraestructura basada en  m�ltiples centros de datos, alta disponibilidad,y balances de carga es que logramos poder crecer a la medida de su necesidad.</p>
                        </div>
                    </li>
                </ul>
            </div>
        </section>

        <section class="still-hav-qtns-outer padding-lg">
            <div class="container">
                <h2>�Tiene Consultas?</h2>
                <ul class="row features-listing">
                    <li class="col-md-4 equal-hight">
                        <div class="info-content"> <span class="icon-holder"><img src="images/stil-hav-sec-icon-1.png" alt=""></span>
                            <h3>Sitio de Soporte</h3>
                            <p>Acceda a nuestro equip� de soporte especializado y evacue todas sus dudas y/o consultas.</p>
                            <a href="support.php" class="know-more">Contactenos</a> </div>
                    </li>
                    <li class="col-md-4 equal-hight">
                        <div class="info-content"> <span class="icon-holder"><img src="images/stil-hav-sec-icon-2.png" alt=""></span>
                            <h3>Preguntas Frecuentes</h3>
                            <p>Visite nuestro sitio de preguntas frecuentes y evacue todas sus dudas de forma r�pida y simple.</p>
                            <a href="faq.php" class="know-more">Visitar</a> </div>
                    </li>
                    <li class="col-md-4 equal-hight">
                        <div class="info-content"> <span class="icon-holder"><img src="images/stil-hav-sec-icon-3.png" alt=""></span>
                            <h3>Documentaci�n</h3>
                            <p>Conozca la documentaci�n t�cnica para acceder y comenzar a disfrutar de nuestro Cloud API-Center.</p>
                            <a href="apidoc/index.php" class="know-more">Acceder</a> </div>
                    </li>
                </ul>
            </div>
        </section>



	<?php include 'footer.php';?>



        <!-- Scroll to top -->
        <a href="#" class="scroll-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>

        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery.min.js"></script>
        <!-- Popper JS -->
        <script src="js/popper.min.js"></script>
        <!-- Bootsrap JS -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- Select2 JS -->
        <script src="assets/select2/js/select2.min.js"></script>
        <!-- Match Height JS -->
        <script src="assets/matchHeight/js/matchHeight-min.js"></script>
        <!-- Video Popup JS -->
        <script src="assets/magnific-popup/js/magnific-popup.min.js"></script>
        <!-- Owl Carousal JS -->
        <script src="assets/owl-carousel/js/owl.carousel.min.js"></script>
        <!-- Waypoints JS -->
        <script src="assets/waypoints/js/waypoints.min.js"></script>
        <!-- Counterup JS -->
        <script src="assets/counterup/js/counterup.min.js"></script>
        <!-- Custom JS -->
        <script src="js/custom.js"></script>
    </body>

<!-- Mirrored from protechtheme.com/saas/features.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:17:39 GMT -->
</html>