<!-- get_header('Page Name','Title')-->
<!doctype html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>CasillaPostal - El poder de las comunicaciones formales.</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<link href="https://fonts.googleapis.com/css?family=Karla:400,700,700i%7CRubik:300,400,500,700" rel="stylesheet">

		<link rel="icon" type="image/png" href="favicon.ico">
		<!-- Place favicon.ico in the root directory -->
		<link rel="apple-touch-icon" href="apple-touch-icon.png">
		<link rel="stylesheet" href="assets/css/font-awesome.min.css">

		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/css/animate.css">
		<link rel="stylesheet" href="assets/css/iconfont.css">
		<link rel="stylesheet" href="assets/css/magnific-popup.css">
		<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
		<link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
 <link rel="icon" type="image/png" href="Casilla.ico">		<link rel="stylesheet" href="assets/css/jquery-ui.structure.min.css">
		<link rel="stylesheet" href="assets/css/jquery-ui.theme.min.css">


		<!--For Plugins external css-->
		<link rel="stylesheet" href="assets/css/plugins.css" />

		<!--Theme custom css -->
		<link rel="stylesheet" href="assets/css/style.css">

		<!--Theme Responsive css-->
		<link rel="stylesheet" href="assets/css/responsive.css" />
	</head>
	<body>
	<!--[if lt IE 10]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->

	<!-- prelaoder -->
	<div id="preloader">
    <div class="preloader-wrapper">
        <div class="spinner"></div>
    </div>
    <div class="preloader-cancel-btn">
        <a href="#" class="btn btn-secondary prelaoder-btn">Cancelar PreCarga</a>
    </div>
</div>	<!-- END prelaoder -->
<div class="header-transparent">
    <!-- topBar section -->
    <div class="xs-top-bar topbar-v2">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <ul class="xs-top-bar-info">
                    <li>
                        <p><i class="icon icon-phone3"></i>+54 (11)5277-8900</p>
                    </li>
                    <li>
                        <a href="mailto:info@domain.com"><i class="icon icon-envelope4"></i>info@sysworld.com.ar</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-6">
                <ul class="top-menu">
                    <li><a href="Politica.php"><i class="icon icon-license"></i>Políticas de Privacidad</a></li>
                    <li><a href="login.php"><i class="icon icon-key2"></i> Login</a></li>
                    <!-- <li><a href="signup.html">Sign Up</a></li> -->
                    <li><a href="soporte.php"><i class="icon icon-hours-support"></i> Soporte</a></li>
                </ul>
            </div>
        </div><!-- .row END -->
    </div><!-- .container END -->
</div>    <!-- End topBar section -->

    <!-- header section -->
    <header class="xs-header header-v3">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <div class="xs-logo-wraper">
                    <a href="index.php" class="xs-logo">
                        <img src="assets/images/logo.png" alt="">
                    </a>
                </div>
            </div>
            <div class="col-lg-8">
                <nav class="xs-menus">
                    <div class="nav-header">
                        <a class="nav-brand" href="index.php">
                            <img src="assets/images/logo.png" alt="">
                        </a>
                        <div class="nav-toggle"></div>
                    </div>
                    <div class="nav-menus-wrapper">
						<!--#include file="menu.asp"-->
                    </div>
                </nav>
            </div>
            <div class="col-lg-2">
                <ul class="xs-menu-tools">
                    <li>
                        <a href="#modal-popup-1" class="languageSwitcher-button xs-modal-popup"><i class="icon icon-internet"></i></a>
                    </li>
                    <li>
                        <a href="#" class="offset-side-bar"><i class="icon icon-cart2"></i><span class="item-count">2</span></a>
                    </li>
                    <li>
                        <a href="#modal-popup-2" class="navsearch-button xs-modal-popup"><i class="icon icon-search"></i></a>
                    </li>
                    <li>
                        <a href="#" class="navSidebar-button"><i class="icon icon-burger-menu"></i></a>
                    </li>
                </ul>
            </div>
        </div><!-- .row END -->
    </div><!-- .container END -->
</header>    <!-- End header section -->
</div>

<!-- banner section -->
<section class="xs-banner banner-v4 contet-to-center">
    <div class="container">
        <div class="row">
            <div class="col-md-5 align-self-center">
                <div class="xs-banner-content">
                    <h2 class="banner-title wow fadeInLeft">El centro de las comunicaciones.</h2>
                    <p class="wow fadeInUp" data-wow-duration="1.5s">
						CasillaPostal es la puerta de entrada para que su empresa comience a relacionarse con sus clientes de una forma alternativa y proactiva. Nuestra plataforma no solo le brinda un portal multiplataforma de comunicación con sus clientes, sino una herramienta bidireccional donde puede mejorar sustancialmente la comunicación con sus clientes.
                    </p>
                    <div class="xs-btn-wraper wow fadeInUp" data-wow-duration="1.7s">
                        <a href="#" class="btn btn-secondary icon-right">Registre su Empresa<i class="icon icon-arrow-right"></i></a>
                    </div>
                </div><!-- .xs-banner-content END -->
            </div>
            <div class="col-md-7 align-self-center">
                <div class="home5-banner-image wow fadeIn">
                    <img src="assets/images/welcome/banner_image-4.png" data-parallax='{"y": 150}' alt="">
                </div>
            </div>
        </div><!-- .row END -->
    </div><!-- .container END -->
    <div class="wave_animation_wraper">
        <div class="wave_animation">
            <div class="layer wave one" data-depth="0.20"><img  src="assets/images/welcome/horizontal-shape.png" alt="horizontal shape"></div>
            <div class="layer wave two" data-depth="0.6"><img src="assets/images/welcome/vertical-shape-1.png" alt="vrtical shape"></div>
            <div class="layer wave three" data-depth="0.4"><img  src="assets/images/welcome/vertical-shape-2.png" alt="vrtical shape"></div>
        </div>
    </div>
</section><!-- End banner section -->

<!-- service section -->
<section class="service-section xs-section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-4 mx-auto wow fadeIn">
                <div class="xs-heading">
                    <h2 class="heading-sub-title">Una red de contenidos formales</h2>
                    <h3 class="heading-title"><span>SERVICIOS</span></h3>
                </div><!-- .xs-heading END -->
            </div>
        </div><!-- .row END -->
        <div class="row">
            <div class="col-lg-4 col-md-6 xs-mb-8">
                <div class="xs-service-block wow fadeInUp">
                    <div class="service-img">
                        <img src="assets/images/service/service-7.png" alt="hosting service icon">
                    </div>
                    <h4 class="xs-title"><a href="shared-hosting.html">Comparta Documentos</a></h4>
                    <p>Puesta a disposición de documentación y comprobantes digitales a sus clientes.</p>
                    <a href="shared-hosting.html" class="simple-btn icon-right">Ver Más<i class="icon icon-play2"></i></a>
                </div><!-- .xs-service-block END -->
            </div>
            <div class="col-lg-4 col-md-6 xs-mb-8">
                <div class="xs-service-block wow fadeInUp" data-wow-duration="1.5s">
                    <div class="service-img">
                        <img src="assets/images/service/service-8.png" alt="hosting service icon">
                    </div>
                    <h4 class="xs-title"><a href="reseller-hosting.html">Notificaciones</a></h4>
                    <p>Envie notificaciones push a los celulares o computadoras de sus clientes.</p>
                    <a href="reseller-hosting.html" class="simple-btn icon-right">Ver Más<i class="icon icon-play2"></i></a>
                </div><!-- .xs-service-block END -->
            </div>
            <div class="col-lg-4 col-md-6 xs-mb-8">
                <div class="xs-service-block wow fadeInUp" data-wow-duration="2s">
                    <div class="service-img">
                        <img src="assets/images/service/service-9.png" alt="hosting service icon">
                    </div>
                    <h4 class="xs-title"><a href="vps-hosting.html">Mensajeria Instantanea</a></h4>
                    <p>Permita abrir un canal bidireccional para chatear y responder a sus clientes.</p>
                    <a href="vps-hosting.html" class="simple-btn icon-right">Ver Más<i class="icon icon-play2"></i></a>
                </div><!-- .xs-service-block END -->
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="xs-service-block wow fadeInUp" data-wow-duration="2.5s">
                    <div class="service-img">
                        <img src="assets/images/service/service-10.png" alt="hosting service icon">
                    </div>
                    <h4 class="xs-title"><a href="cloud-hosting.html">Novedades</a></h4>
                    <p>Disponga de un sector desestructurado para circularizar novedades comerciales.</p>
                    <a href="cloud-hosting.html" class="simple-btn icon-right">Ver Más<i class="icon icon-play2"></i></a>
                </div><!-- .xs-service-block END -->
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="xs-service-block wow fadeInUp" data-wow-duration="3s">
                    <div class="service-img">
                        <img src="assets/images/service/service-11.png" alt="hosting service icon">
                    </div>
                    <h4 class="xs-title"><a href="dedicated-hosting.html">Vencimientos</a></h4>
                    <p>Disponibilice en el calendario de sus clientes los vencimientos más importantes de sus comunicaciones.</p>
                    <a href="dedicated-hosting.html" class="simple-btn icon-right">Ver Más<i class="icon icon-play2"></i></a>
                </div><!-- .xs-service-block END -->
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="xs-service-block wow fadeInUp" data-wow-duration="3.5s">
                    <div class="service-img">
                        <img src="assets/images/service/service-12.png" alt="hosting service icon">
                    </div>
                    <h4 class="xs-title"><a href="domain.html">Panel de Control</a></h4>
                    <p>Disponga de un panel de control completo e intuitivo de sus operaciones.</p>
                    <a href="domain.html" class="simple-btn icon-right">Ver Más<i class="icon icon-play2"></i></a>
                </div><!-- .xs-service-block END -->
            </div>
        </div>
    </div><!-- .container END -->
</section><!-- END service section -->

<!-- service section -->
<section class="xs-section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-8 mx-auto">
                <div class="xs-heading heading-v2">
                    <h3 class="heading-title">Unirse a la red de <span>comunicaciones formales</span> más grande de la region.</h3>
                </div><!-- .xs-heading END -->
            </div>
        </div><!-- .row END -->
        <div class="row style-boxed">
            <div class="col-lg-4 col-md-6">
                <div class="xs-service-block">
                    <div class="service-img">
                        <img src="assets/images/service/service-13.png" alt="hosting service icon">
                    </div>
                    <h4 class="xs-title"><a href="shared-hosting.html">99.9% Uptime</a></h4>
                    <p>Toda nuestra infraestructura se encuentra gestada para garantizar la disponibilidad de los recursos pase lo que pase.</p>
                </div><!-- .xs-service-block END -->
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="xs-service-block active">
                    <div class="service-img">
                        <img src="assets/images/service/service-14.png" alt="hosting service icon">
                    </div>
                    <h4 class="xs-title"><a href="shared-hosting.html">Invitaciones Proactivas</a></h4>
                    <p>En caso que existan clientes no registrados, sistemas proactivos de invitación se encargarán de suscribirlos.</p>
                </div><!-- .xs-service-block END -->
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="xs-service-block">
                    <div class="service-img">
                        <img src="assets/images/service/service-15.png" alt="hosting service icon">
                    </div>
                    <h4 class="xs-title"><a href="shared-hosting.html">Servidores Distribuidores</a></h4>
                    <p>Una red globalmente distribuida garantizan un rápido acceso a los contenidos distribuidos en nuestros sistemas.</p>
                </div><!-- .xs-service-block END -->
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="xs-service-block">
                    <div class="service-img">
                        <img src="assets/images/service/service-16.png" alt="hosting service icon">
                    </div>
                    <h4 class="xs-title"><a href="shared-hosting.html">Web Disponible</a></h4>
                    <p>Un panel de control WEB y API le permite seguir de cerca sus operaciones en nuestra comunidad de usuarios.</p>
                </div><!-- .xs-service-block END -->
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="xs-service-block">
                    <div class="service-img">
                        <img src="assets/images/service/service-17.png" alt="hosting service icon">
                    </div>
                    <h4 class="xs-title"><a href="shared-hosting.html">Soporte 7x24</a></h4>
                    <p>Profesionales altamente entrenados en gestión de comunicaciones están para ayudarlo 7x24.</p>
                </div><!-- .xs-service-block END -->
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="xs-service-block">
                    <div class="service-img">
                        <img src="assets/images/service/service-18.png" alt="hosting service icon">
                    </div>
                    <h4 class="xs-title"><a href="shared-hosting.html">Seguridad Garantizada</a></h4>
                    <p>Un enfoque con principal atención en seguridad de la información y continuidad del negocio garantizan sus operaciones.</p>
                </div><!-- .xs-service-block END -->
            </div>
        </div>
    </div><!-- .container END -->
</section><!-- END service section -->

<!-- pricing section -->
<section class="pricing-section pricing-v3 xs-section-padding xs-bg-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-4 mx-auto">
                <div class="xs-heading wow fadeIn">
                    <h2 class="heading-sub-title">Plan de Precios</h2>
                    <h3 class="heading-title">NUESTRO <span>MEJOR PRECIO</span></h3>
                </div><!-- .xs-heading END -->
            </div>
        </div><!-- .row END -->
        <div class="row">
            <div class="col-md-12 xs-pricing-group">
                <div class="tab-content">
                    <div class="tab-pane fadeIn animated show active" id="monthly" role="tabpanel" aria-labelledby="monthly-tab">
                        <div class="row">
                            <div class="col-lg-4 col-md-6">
                                <div class="xs-single-pricing">
                                    <div class="pricing-header">
                                        <div class="pricing-img-block">
                                            <img src="assets/images/pricing/pricing.svg" alt="hostinza pricing image">
                                        </div>
                                        <h4 class="xs-title">Personal</h4>
                                    </div><!-- .pricing-header END -->
                                    <div class="pricing-body">
                                        <p>Documentos Link, Notificaciones, Alertas y Vencimientos.</p><br>
                                        <div class="pricing-price">
                                            <p>Desde </p>
                                            <h2><sup>$</sup>0</h2>
                                            <h6 class="discount-price">&nbsp;</h6>
                                        </div>
                                    </div><!-- .pricing-body END -->
                                    <div class="pricing-footer">
                                        <a href="precios.php" class="btn btn-primary">Ver Detalles</a>
                                    </div><!-- .pricing-footer END -->
                                </div><!-- .xs-single-pricing END -->
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="xs-single-pricing active">
                                    <div class="pricing-header">
                                        <div class="pricing-img-block">
                                            <img src="assets/images/pricing/pricing.svg" alt="hostinza pricing image">
                                        </div>
                                        <h4 class="xs-title">Standard</h4>
                                    </div><!-- .pricing-header END -->
                                    <div class="pricing-body">
                                        <p>Documentos Link, Notificaciones, Alertas, Vencimientos, Novedades.</p><br>
                                        <div class="pricing-price">
                                            <p>Desde</p>
                                            <h2><sup>$</sup>0</h2>
                                            <h6 class="discount-price">Antes <del>$890.99</del></h6>
                                        </div>
                                    </div><!-- .pricing-body END -->
                                    <div class="pricing-footer">
                                        <a href="precios.php" class="btn btn-primary">Ver Detalles</a>
                                    </div><!-- .pricing-footer END -->
                                </div><!-- .xs-single-pricing END -->
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="xs-single-pricing">
                                    <div class="pricing-header">
                                        <div class="pricing-img-block">
                                            <img src="assets/images/pricing/pricing.svg" alt="hostinza pricing image">
                                        </div>
                                        <h4 class="xs-title">Gold</h4>
                                    </div><!-- .pricing-header END -->
                                    <div class="pricing-body">
                                        <p>Documentos Host, Notificaciones, Alertas, Vencimientos, Novedades, Chat, Bot.</p>
                                        <div class="pricing-price">
                                            <p>Desde</p>
                                            <h2><sup>$</sup>3500</h2>
                                            <h6 class="discount-price">Antes <del>$5900</del></h6>
                                        </div>
                                    </div><!-- .pricing-body END -->
                                    <div class="pricing-footer">
                                        <a href="precios.php" class="btn btn-primary">Ver Detalles</a>
                                    </div><!-- .pricing-footer END -->
                                </div><!-- .xs-single-pricing END -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 mx-auto">
                <div class="pricing-info wow fadeIn">
                    <h5><a href="precios.php">Ver detalle de planes </a>y precios para mayor información</h5>
                    <ul class="xs-list list-inline">
                        <li><i class="icon icon-back_up"></i>Seguridad</li>
                        <li><i class="icon icon-migration"></i>Integraciones</li>
                        <li><i class="icon icon-checkmark"></i>Comprobación de Usuarios</li>
                    </ul>
                </div>
            </div>
        </div><!-- .row END -->
    </div><!-- .container END -->
</section><!-- END pricing section -->


<!-- call to action section -->
<section class="call-to-action-section call-to-action-v3 wow fadeIn">
    <div class="container">
        <div class="row">
            <div class="col-md-8 mx-auto">
                <div class="call-to-action-content">
                    <h2>Renovamos nuestro compromiso de asegurarle efectividad en sus procesos de comunicaciones</h2>
                </div>
            </div>
        </div><!-- .row END -->
    </div><!-- .container END -->
    <div class="icon-bg" style="background-image: url(assets/images/icon-bg.png)"></div>
</section><!-- END call to action section -->

<!-- feature list section -->
<div class="xs-feature-section feature-v3">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="xs-feature-group wow fadeInLeft">
                    <div class="media">
                        <div class="feature-img d-flex">
                            <img src="assets/images/sheild.png" alt="hosting feature img">
                        </div>
                        <div class="media-body feature-content">
                            <h4 class="xs-title">30 Días de Prueba</h4>
                            <p>Acceda a nuestro sistema de prueba gratuita de 30 días antes de comenzar un plan pago.</p>
                            <div class="xs-btn-wraper">
                                <a href="#" class="btn btn-secondary">Conocer Más</a>
                            </div>
                        </div>
                    </div>
                    <span class="icon icon-dollar watermark-icon"></span>
                </div><!-- .xs-feature-group END -->
            </div>
            <div class="col-lg-6">
                <div class="xs-feature-group wow fadeInRight">
                    <div class="media">
                        <div class="feature-img d-flex">
                            <img src="assets/images/cloud-lock.png" alt="hosting feature img">
                        </div>
                        <div class="media-body feature-content">
                            <h4 class="xs-title">Infraestructura Dedicada</h4>
                            <p>Tenemos capacidad para generar ambientes dedicados y exclusivos para clientes premium.</p>
                            <div class="xs-btn-wraper">
                                <a href="#" class="btn btn-secondary">Conocer Más</a>
                            </div>
                        </div>
                    </div>
                    <span class="icon icon-key3 watermark-icon"></span>
                </div><!-- .xs-feature-group END -->
            </div>
        </div><!-- .row END -->
    </div><!-- .container END -->
</div><!-- END feature list section -->

<!-- client section -->
<div class="xs-client-section">
    <div class="container">
        <div class="xs-client-slider owl-carousel">
            <div class="single-client">
                <img src="assets/images/client/client-1.png" alt="hosting client image">
            </div><!-- .single-client END -->
            <div class="single-client">
                <img src="assets/images/client/client-2.png" alt="hosting client image">
            </div><!-- .single-client END -->
            <div class="single-client">
                <img src="assets/images/client/client-3.png" alt="hosting client image">
            </div><!-- .single-client END -->
            <div class="single-client">
                <img src="assets/images/client/client-4.png" alt="hosting client image">
            </div><!-- .single-client END -->
            <div class="single-client">
                <img src="assets/images/client/client-5.png" alt="hosting client image">
            </div><!-- .single-client END -->
        </div>
    </div><!-- .container END -->
</div><!-- END client section -->

<!-- hosting info section -->
<section class="xs-section-padding xs-hosting-info">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="hosting-info-img">
                    <img src="assets/images/info/info-img-1.png" alt="Hosting info image">
                    <img src="assets/images/info/icon-1.png" class="info-icon icon-1" alt="hosting icon">
                    <img src="assets/images/info/icon-2.png" class="info-icon icon-2" alt="hosting icon">
                    <img src="assets/images/info/icon-3.png" class="info-icon icon-3" alt="hosting icon">
                    <img src="assets/images/info/icon-4.png" class="info-icon icon-4" alt="hosting icon">
                </div>
            </div>
            <div class="col-lg-6 align-self-center">
                <div class="hosting-info-wraper">
                    <div class="spinner-icon wow">
                        <img src="assets/images/info/electric-wave.png" alt="hosting info info">
                    </div>
                    <div class="wow fadeInUp">
                        <h2 class="content-title">Ventajas de integrar CasillaPostal</h2>
                        <p>CasillaPostal le permitirá a su empresa incrementar la efectividad de sus comunicaciones electronicas y aumentar la satisfacción de sus clientes reduciendo notablemente los costos operativos.</p>
                        <ul class="xs-list check">
                            <li>Comunique en cuestion de minutos a toda su cartera.</li>
                            <li>Acceda a cientos de miles de usuarios ya registrados.</li>
                            <li>Sea pionero en comunicaciones formales integradas.</li>
                        </ul>
                        <a href="#" class="btn btn-primary">Crear su cuenta</a>
                    </div>
                </div><!-- .hosting-info-wraper END -->
            </div>
        </div><!-- .row END -->
        <div class="row">
            <div class="col-lg-6 align-self-center">
                <div class="hosting-info-wraper-2 wow fadeInLeft" data-wow-duration="1.5s">
                    <h2 class="content-title">El poder de las comunicaciones formales</h2>
                    <div class="media hosting-info-list">
                        <div class="d-flex info-icon">
                            <img src="assets/images/info/gaurd-icon.png" alt="hosting info icon">
                        </div>
                        <div class="media-body">
                            <h4 class="xs-title small">Evite no comunicar en tiempo y forma:</h4>
                            <p>Obtenga directamente la atención de sus clientes mediante un canal desarrollado para comunicaciones formales.</p>
                        </div>
                    </div><!-- .hosting-info-list END -->
                    <div class="media hosting-info-list">
                        <div class="d-flex info-icon">
                            <img src="assets/images/info/cloud-icon.png" alt="hosting info icon">
                        </div>
                        <div class="media-body">
                            <h4 class="xs-title small">Acelere la distribución de contenidos:</h4>
                            <p>Optimice la distribución de contenidos bajando sus costos operativos y mejorando la disponibilidad.</p>
                        </div>
                    </div><!-- .hosting-info-list END -->
                    <div class="media hosting-info-list">
                        <div class="d-flex info-icon">
                            <img src="assets/images/info/clock-icon.png" alt="hosting info icon">
                        </div>
                        <div class="media-body">
                            <h4 class="xs-title small">Integrese en pocos minutos:</h4>
                            <p>Cree su cuenta e integre su empresa mediante diferentes metodologías de programación y uso.</p>
                        </div>
                    </div><!-- .hosting-info-list END -->
                    <a href="#" class="btn btn-primary">Crear su cuenta</a>
                </div><!-- .hosting-info-wraper-2 END -->
            </div>
            <div class="col-lg-6">
                <div class="hosting-info-img wow fadeInRight" data-wow-duration="2s">
                    <img src="assets/images/info/info-img-2.png" alt="Hosting info image">
                </div>
            </div>
        </div><!-- .row END -->
    </div><!-- .container END -->
</section><!-- END hosting info section -->

<!-- testimonial section -->
<section class="xs-testimonial-section wow fadeIn testimonial-v4">
    <div class="container">
        <div class="xs-testimonial-slider owl-carousel">
            <div class="xs-testimonial-item">
                <div class="testimonial-content">
                    <p>Our best-in-class WordPress solution with additio nal optiz ation to make an running a WooCommerce</p>
                    <div class="commentor-bio media">
                        <div class="d-flex round-avatar">
                            <img src="assets/images/avatar/avatar-1.png" alt="avatar image">
                        </div>
                        <div class="media-body align-self-center">
                            <h4 class="commentor-title">RichardÂ IÂ Jones</h4>
                            <p class="commentor-info">WWW Realty</p>
                            <i class="icon icon-quote"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="xs-testimonial-item">
                <div class="testimonial-content">
                    <p>Our best-in-class WordPress solution with additio nal optiz ation to make an running a WooCommerce</p>
                    <div class="commentor-bio media">
                        <div class="d-flex round-avatar">
                            <img src="assets/images/avatar/avatar-2.png" alt="avatar image">
                        </div>
                        <div class="media-body align-self-center">
                            <h4 class="commentor-title">CeciliaÂ HÂ King</h4>
                            <p class="commentor-info">The White Rabbit</p>
                            <i class="icon icon-quote"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="xs-testimonial-item">
                <div class="testimonial-content">
                    <p>Our best-in-class WordPress solution with additio nal optiz ation to make an running a WooCommerce</p>
                    <div class="commentor-bio media">
                        <div class="d-flex round-avatar">
                            <img src="assets/images/avatar/avatar-3.png" alt="avatar image">
                        </div>
                        <div class="media-body align-self-center">
                            <h4 class="commentor-title">JoeÂ DÂ Walczak</h4>
                            <p class="commentor-info">Hechinger</p>
                            <i class="icon icon-quote"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .container END -->
</section><!-- END testimonial section -->

<!-- blog section -->
<section class="xs-section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-4 mx-auto">
                <div class="xs-heading wow fadeIn">
                    <h2 class="heading-sub-title">Latest Info</h2>
                    <h3 class="heading-title">GET <span>LATEST INFO</span></h3>
                </div><!-- .xs-heading END -->
            </div>
        </div><!-- .row END -->
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="xs-blog-post wow fadeInUp">
                    <div class="post-image">
                        <img src="assets/images/blogs/blogs-1.jpg" alt="hosting blog images">
                    </div><!-- .post-image END -->
                    <div class="entry-header">
                        <div class="post-meta">
                            <span class="post-cat"><a href="#">Cloud Hosting</a></span>
                        </div>
                        <h2 class="entry-title"><a href="blog-single.html">Best domain name for Business</a></h2>
                        <div class="entry-meta">
                            <span class="meta-date">7 March, 2018</span>
                        </div>
                    </div><!-- .entry-header END -->
                </div><!-- .xs-blog-post END -->
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="xs-blog-post wow fadeInUp" data-wow-duration="1.5s">
                    <div class="post-image">
                        <img src="assets/images/blogs/blogs-2.jpg" alt="hosting blog images">
                    </div><!-- .post-image END -->
                    <div class="entry-header">
                        <div class="post-meta">
                            <span class="post-cat"><a href="#">Reseller Hosting</a></span>
                        </div>
                        <h2 class="entry-title"><a href="blog-single.html">Best reseller hosting for start business</a></h2>
                        <div class="entry-meta">
                            <span class="meta-date">1 June, 2017</span>
                        </div>
                    </div><!-- .entry-header END -->
                </div><!-- .xs-blog-post END -->
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="xs-blog-post wow fadeInUp" data-wow-duration="2s">
                    <div class="post-image">
                        <img src="assets/images/blogs/blogs-3.jpg" alt="hosting blog images">
                    </div><!-- .post-image END -->
                    <div class="entry-header">
                        <div class="post-meta">
                            <span class="post-cat"><a href="#">Shared Hosting</a></span>
                        </div>
                        <h2 class="entry-title"><a href="blog-single.html">Domain name for student</a></h2>
                        <div class="entry-meta">
                            <span class="meta-date">2 April, 2017</span>
                        </div>
                    </div><!-- .entry-header END -->
                </div><!-- .xs-blog-post END -->
            </div>
        </div><!-- .row END -->
    </div><!-- .container END -->
</section><!-- END blog section -->

<!-- language switcher strart -->
<!-- xs modal -->
<div class="zoom-anim-dialog mfp-hide modal-language" id="modal-popup-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="language-content">
                <p>Switch The Language</p>
                <ul class="flag-lists">
                    <li><a href="#"><img src="assets/images/flags/006-united-states.svg" alt=""><span>English</span></a></li>
                    <li><a href="#"><img src="assets/images/flags/002-canada.svg" alt=""><span>English</span></a></li>
                    <li><a href="#"><img src="assets/images/flags/003-vietnam.svg" alt=""><span>Vietnamese</span></a></li>
                    <li><a href="#"><img src="assets/images/flags/004-france.svg" alt=""><span>French</span></a></li>
                    <li><a href="#"><img src="assets/images/flags/005-germany.svg" alt=""><span>German</span></a></li>
                </ul>
            </div>
        </div>
    </div>
</div><!-- End xs modal --><!-- end language switcher strart -->

<!-- language switcher strart -->
<!-- xs modal -->
<div class="zoom-anim-dialog mfp-hide modal-searchPanel" id="modal-popup-2">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="xs-search-panel">
                <form action="#" method="POST" class="xs-search-group">
                    <input type="search" class="form-control" name="search" id="search" placeholder="Buscar">
                    <button type="submit" class="search-button"><i class="icon icon-search"></i></button>
                </form>
            </div>
        </div>
    </div>
</div><!-- End xs modal --><!-- end language switcher strart -->

<!-- offset cart strart -->
<!-- sidebar cart item -->
<div class="xs-sidebar-group cart-group">
    <div class="xs-overlay xs-bg-black"></div>
    <div class="xs-sidebar-widget">
        <div class="sidebar-widget-container">
            <div class="widget-heading media">
                <div class="media-body">
                    <a href="#" class="close-side-widget">
                        X
                    </a>
                </div>
            </div>
            <div class="xs-empty-content">
                <h3 class="widget-title">Shopping cart</h3>
                <h4 class="xs-title">No products in the cart.</h4>
                <p class="empty-cart-icon">
                    <i class="icon icon-shopping-cart"></i>
                </p>
                <p class="xs-btn-wraper">
                    <a class="btn btn-primary" href="#">Return To Shop</a>
                </p>
            </div>
        </div>
    </div>
</div>    <!-- END sidebar cart item -->    <!-- END offset cart strart -->

<!-- offset cart strart -->
<!-- sidebar cart item -->
<div class="xs-sidebar-group info-group">
    <div class="xs-overlay xs-bg-black"></div>
    <div class="xs-sidebar-widget">
        <div class="sidebar-widget-container">
            <div class="widget-heading">
                <a href="#" class="close-side-widget">
                    X
                </a>
            </div>
            <div class="sidebar-textwidget">
                <div class="sidebar-logo-wraper">
                    <img src="assets/images/blue_logo.png" alt="sidebar logo">
                </div>
                <p>Far far away, behind the word moun tains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of  </p>
                <ul class="sideabr-list-widget">
                    <li>
                        <div class="media">
                            <div class="d-flex">
                                <img src="assets/images/location.png" alt="">
                            </div>
                            <div class="media-body">
                                <p>759 Pinewood Avenue</p>
                                <span>Marquette, Michigan</span>
                            </div>
                        </div><!-- address 1 -->
                    </li>
                    <li>
                        <div class="media">
                            <div class="d-flex">
                                <img src="assets/images/mail.png" alt="">
                            </div>
                            <div class="media-body">
                                <a href="mailto:info@domain.com">info@domain.com</a>
                                <span>Online Support</span>
                            </div>
                        </div><!-- address 1 -->
                    </li>
                    <li>
                        <div class="media">
                            <div class="d-flex">
                                <img src="assets/images/phone.png" alt="">
                            </div>
                            <div class="media-body">
                                <p>906-624-2565</p>
                                <span>Mon-Fri 8am-5pm</span>
                            </div>
                        </div><!-- address 1 -->
                    </li>
                </ul><!-- .sideabr-list-widget -->
                <div class="subscribe-from">
                    <p>Get Subscribed!</p>
                    <form action="#" method="POST" id="subscribe-form" class="subscribe-form">
                        <label for="sub-input"></label>
                        <div class="form-group">
                            <input type="email" name="email" id="sub-input" placeholder="Enter your mail here" class="form-control">
                            <button class="sub-btn" type="submit"><i class="icon icon-arrow-right"></i></button>
                        </div>
                    </form>
                </div>
                <ul class="social-list version-2">
                    <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#" class="vimeo"><i class="fa fa-vimeo"></i></a></li>
                </ul><!-- .social-list -->
                <div class="text-center">
                    <a href="#" class="btn btn-primary">Purchase Now</a>
                </div>
            </div>
        </div>
    </div>
</div>    <!-- END sidebar widget item -->    <!-- END offset cart strart -->

		<!-- footer section start -->
		<footer class="xs-footer-section">
			<div class="footer-group" style="background-image: url(assets/images/footer-bg.png);">
				<div class="footer-main">
					<div class="container">
						<div class="row">
							<div class="col-lg-3 col-md-6">
								<div class="footer-widget wow fadeInUp">
									<h3 class="widget-title">CasillaPostal</h3>
									<ul class="xs-list">
										<li><a href="index.php">Inicio</a></li>
										<li><a href="precios.php">Precios</a></li>
										<li><a href="contacto.php">Contacto</a></li>
										<li><a href="login.php">Login</a></li>
										<li><a href="api.php">Api Desarrolladores</a></li>
										<li><a href="Politica.php" target="_blank">Legales</a></li>
									</ul><!-- .xs-list END -->
								</div><!-- .footer-widget END -->
							</div>
							<div class="col-lg-3 col-md-6">
								<div class="footer-widget wow fadeInUp" data-wow-duration="1s">
									<h3 class="widget-title">Empresa</h3>
									<ul class="xs-list">
										<li><a href="empresa.php">Nosotros</a></li>
										<li><a href="http://www.sysworld.com.ar" target="_blank">Sitio Corporativo</a></li>
										<li><a href="contacto.php">Contactenos</a></li>
										<li><a href="http://www.sysworld.com.ar/v2/sw/empresa_dist.php" target="_blank">Distribuidores</a></li>
										<li><a href="http://www.sysworld.com.ar/" target="_blank">Politica de Privacidad</a></li>
										<li><a href="http://www.sysworld.com.ar/" target="_blank">Datos Personales</a></li>
									</ul><!-- .xs-list END -->
								</div><!-- .footer-widget END -->
							</div>
							<div class="col-lg-3 col-md-6">
								<div class="footer-widget wow fadeInUp" data-wow-duration="1.3s">
									<h3 class="widget-title">Soporte</h3>
									<ul class="xs-list">
										<li><a href="who-is.html">Tutoriales</a></li>
										<li><a href="http://cloud.sysworld.com.ar/" target="_blank">Cargar Ticket</a></li>
										<li><a href="http://estado.sysworld.com.ar/" target="_blank">Estado de Servicios</a></li>
										<li><a href="faq.php">FAQ</a></li>
									</ul><!-- .xs-list END -->
								</div><!-- .footer-widget END -->
							</div>
							<div class="col-lg-3 col-md-6">
								<div class="footer-widget wow fadeInUp" data-wow-duration="1.5s">
									<h3 class="widget-title">Contacto</h3>
									<ul class="contact-info-widget">
										<li class="media">
											<img src="assets/images/address-pin.png" class="d-flex" alt="contact icon">
											<span class="media-body">Bucarelli 2480, CABA, Argentina</span>
										</li><!-- .media END -->
										<li class="media">
											<img src="assets/images/phone-pin.png" class="d-flex" alt="contact icon">
											<span class="media-body">+54 (011) 5269-2919</span>
										</li><!-- .media END -->
										<li class="media">
											<img src="assets/images/email-icon.png" class="d-flex" alt="contact icon">
											<span class="media-body">info@sysworld.com.ar</span>
										</li><!-- .media END -->
									</ul><!-- .contact-info-widget -->
								</div><!-- .footer-widget END -->
							</div>
						</div><!-- .row END -->
					</div><!-- .container END -->
				</div><!-- .footer-main END -->
				<div class="container">
					<div class="footer-bottom">
						<div class="row">
							<div class="col-md-6">
								<div class="footer-bottom-info wow fadeInUp">
									<p>Un aliado estratégico para su negocio. Desde nuestra empresa creemos en crear soluciones que le apliquen valor a su negocio. <a href="http://www.sysworld.com.ar">Sysworld Servicios S.A.</a></p>
								</div>
							</div>
							<div class="col-md-6">
								<ul class="xs-list list-inline wow fadeInUp" data-wow-duration="1s">
									<li><img src="assets/images/security/security-company-images-1.png" alt="security company images"></li>
									<li><img src="assets/images/security/security-company-images-2.png" alt="security company images"></li>
									<li><img src="assets/images/security/security-company-images-3.png" alt="security company images"></li>
									<li><img src="assets/images/security/security-company-images-4.png" alt="security company images"></li>
								</ul>
							</div>
						</div><!-- .row END -->
					</div><!-- .footer-bottom end -->
				</div><!-- .container end -->
			</div><!-- .footer-group END -->
			<div class="footer-copyright">
				<div class="container">
					<div class="row">
						<div class="col-md-4">
							<div class="copyright-text wow fadeInUp">
								<p>&copy; 2018 - <%=year(date)%> Powered by <a href="http://www.sysworld.com.ar">Sysworld Servicios S.A. </a></p>
							</div><!-- .copyright-text END -->
						</div>
						<div class="col-md-4">
							<div class="footer-logo-wraper wow fadeInUp" data-wow-duration="1s">
								<a href="index.php" class="footer-logo"><img src="assets/images/logo.png" alt="footer logo" width="180" height="45"></a>
							</div><!-- .footer-logo-wraper END -->
						</div>
						<div class="col-md-4">
							<div class="social-list-wraper wow fadeInUp" data-wow-duration="1.3s">
								<ul class="social-list">
									<li><a href="https://www.facebook.com/SysWorld-Servicios-SA-323344231089747" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a></li>
									<li><a href="https://twitter.com/Sysworldsa" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a></li>
								</ul>
							</div><!-- .social-list-wraper END -->
						</div>
					</div><!-- .row END -->
				</div><!-- .container END -->
			</div><!-- .footer-copyright END -->
		</footer>
		<!-- footer section end -->
		<!-- js file start -->
		<script src="assets/js/jquery-3.2.1.min.js"></script>
		<script src="assets/js/jquery-ui.min.js"></script>
		<script src="assets/js/plugins.js"></script>
		<script src="assets/js/Popper.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/jquery.magnific-popup.min.js"></script>
		<script src="assets/js/owl.carousel.min.js"></script>
		<script src="assets/js/tweetie.js"></script>
		<script src="assets/js/jquery.parallax-scroll.js"></script>
		<script src="assets/js/jquery.ajaxchimp.min.js"></script>
		<script src="assets/js/jquery.parallax.js"></script>
		<script src="assets/js/hostslide.js"></script>
		<script src="assets/js/vps-slider.js"></script>
		<script src="assets/js/vps-slider-settings.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyCy7becgYuLwns3uumNm6WdBYkBpLfy44k"></script>
		<script src="assets/js/main.js"></script>		<!-- End js file -->
	</body>
</html>