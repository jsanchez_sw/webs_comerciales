<section class="signup-outer gradient-bg padding-lg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <ul class="clearfix">
                        <li> <span class="icon-men"></span>
                            <h4><?php echo $lang['GRAL_section_prueba_item1'];?></h4>
                        </li>
                        <li> <span class="icon-chat"></span>
                            <h4><?php echo $lang['GRAL_section_prueba_item2'];?></h4>
                        </li>
                        <li> <span class="icon-lap"></span>
                            <h4><?php echo $lang['GRAL_section_prueba_item3'];?></h4>
                        </li>
                    </ul>
                    <div class="signup-form">
                        <form action="register.php" method="post">
                            <div class="email">
                                <input name="email" type="text" placeholder="email">
                            </div>
                            <div class="password">
                                <input name="password" type="password" placeholder="password">
                            </div>
                            <button class="signup-btn"><?php echo $lang['GRAL_section_prueba_button'];?></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
<footer class="footer">
        <div class="top">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-lg-3 mob-acco">
                        <div class="quick-links">
                            <h4><?php echo $lang['FOOTER_qlinks'];?></h4>
                            <ul>
                                <li><a href="index.php"><?php echo $lang['FOOTER_link1'];?></a></li>
                                <li><a href="contact.php"><?php echo $lang['FOOTER_link2'];?></a></li>
                                <li><a href="quality.php"><?php echo $lang['FOOTER_link3'];?></a></li>
                                <li><a href="legal.php"><?php echo $lang['FOOTER_link4'];?></a></li>
                                <li><a href="https://cloud.sysworld.com.ar/clients/login"><?php echo $lang['FOOTER_link5'];?></a></li>
                                <li><a href="features.php"><?php echo $lang['FOOTER_link6'];?></a></li>
                                <li><a href="pricing.php"><?php echo $lang['FOOTER_link7'];?></a></li>
                                <li><a href="network.php"><?php echo $lang['FOOTER_link8'];?></a></li>
                                <li><a href="http://www.sysworld.com.ar/v2/sw/empresa_clientes.asp"><?php echo $lang['FOOTER_link9'];?></a></li>
                                <li><a href="https://apicloud.certisend.com:9443/store/"><?php echo $lang['FOOTER_link10'];?></a></li>
                                <li><a href="faq.php"><?php echo $lang['FOOTER_link11'];?></a></li>
                                <li><a href="support.php"><?php echo $lang['FOOTER_link12'];?></a></li>
                            </ul>
                        </div>
                        <div class="connect-outer">
                            <h4><?php echo $lang['FOOTER_social_red'];?></h4>
                            <ul class="connect-us">
                                <li><a href="https://www.facebook.com/SysWorld.ar/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="https://twitter.com/sysworldsa"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="https://ar.linkedin.com/company/sysworld"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 mob-acco">
                        <div class="recent-post">
                            <h4><?php echo $lang['FOOTER_post_tittle'];?></h4>
                            <ul class="list-unstyled">
                                <li class="media">
                                    <figure class="d-flex"><img src="images/rp-thumb1.jpg" class="img-fluid" alt=""></figure>
                                    <div class="media-body">
                                        <h5><?php echo $lang['FOOTER_post1'];?></h5>
                                    </div>
                                </li>
                                <li class="media">
                                    <figure class="d-flex"><img src="images/rp-thumb2.jpg" class="img-fluid" alt=""></figure>
                                    <div class="media-body">
                                        <h5><?php echo $lang['FOOTER_post2'];?></h5>
                                    </div>
                                </li>
                                <li class="media">
                                    <figure class="d-flex"><img src="images/rp-thumb3.jpg" class="img-fluid" alt=""></figure>
                                    <div class="media-body">
                                        <h5><?php echo $lang['FOOTER_post3'];?></h5>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-5">
                        <div class="subscribe">
                            <h4><?php echo $lang['FOOTER_sub_tittle'];?></h4>
                            <p class="hidden-xs"><?php echo $lang['FOOTER_sub_subtittle'];?></p>
                            <div class="input-outer clearfix">
                                <div id="mc_embed_signup">
                                    <form action="http://protechtheme.us16.list-manage.com/subscribe/post?u=cd5f66d2922f9e808f57e7d42&amp;id=ec6767feee" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                        <div id="mc_embed_signup_scroll">
                                            <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="<?php echo $lang['FOOTER_sub_email'];?>" required>
                                            <div style="position: absolute; left: -5000px;" aria-hidden="true">
                                                <input type="text" name="b_cd5f66d2922f9e808f57e7d42_ec6767feee" tabindex="-1" value="">
                                            </div>
                                            <div class="clear">
                                                <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!--End mc_embed_signup-->
                            </div>
                        </div>
                        <div class="tweet clearfix"> <span class="icon"><i class="fa fa-twitter" aria-hidden="true"></i></span>
                            <div class="right-cnt">
                                <p><?php echo $lang['FOOTER_lastpost'];?></p>
                                <div class="sourse"><?php echo $lang['FOOTER_twitter_profile'];?> <span><?php echo $lang['FOOTER_twitter_profile2'];?>,</span>
                                    <ul>
                                        <li><a href="#"><i class="fa fa-reply" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-retweet" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom">
            <div class="container">
                <?php echo $lang['FOOTER_Copyright'];?> <br>
                <?php echo $lang['FOOTER_Powered'];?>
            </div>
        </div>
    </footer>
