<!-- get_header('Page Name','Title')-->
<!doctype html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>CasillaPostal - El poder de las comunicaciones formales.</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<link href="https://fonts.googleapis.com/css?family=Karla:400,700,700i%7CRubik:300,400,500,700" rel="stylesheet">

		<link rel="icon" type="image/png" href="favicon.ico">
		<!-- Place favicon.ico in the root directory -->
		<link rel="apple-touch-icon" href="apple-touch-icon.png">
		<link rel="stylesheet" href="assets/css/font-awesome.min.css">

		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/css/animate.css">
		<link rel="stylesheet" href="assets/css/iconfont.css">
		<link rel="stylesheet" href="assets/css/magnific-popup.css">
		<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
		<link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
 <link rel="icon" type="image/png" href="Casilla.ico">		<link rel="stylesheet" href="assets/css/jquery-ui.structure.min.css">
		<link rel="stylesheet" href="assets/css/jquery-ui.theme.min.css">


		<!--For Plugins external css-->
		<link rel="stylesheet" href="assets/css/plugins.css" />

		<!--Theme custom css -->
		<link rel="stylesheet" href="assets/css/style.css">

		<!--Theme Responsive css-->
		<link rel="stylesheet" href="assets/css/responsive.css" />
	</head>
	<body>
	<!--[if lt IE 10]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->

	<!-- prelaoder -->
	<div id="preloader">
    <div class="preloader-wrapper">
        <div class="spinner"></div>
    </div>
    <div class="preloader-cancel-btn">
        <a href="#" class="btn btn-secondary prelaoder-btn">Cancel Preloader</a>
    </div>
</div>	<!-- END prelaoder -->
<div class="header-transparent">
    <!-- topBar section -->
    <div class="xs-top-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <ul class="xs-top-bar-info">
                    <li>
                        <p><i class="icon icon-phone3"></i>009-215-5596</p>
                    </li>
                    <li>
                        <a href="mailto:info@domain.com"><i class="icon icon-envelope4"></i>info@domain.com</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-6">
                <ul class="top-menu">
                                  <li><a href="Politica.php"><i class="icon icon-license"></i>Políticas de Privacidad</a></li>
      <li><a href="login.php"><i class="icon icon-key2"></i> Login</a></li>
                    <!-- <li><a href="signup.html">Sign Up</a></li> -->
                    <li><a href="soporte.php"><i class="icon icon-hours-support"></i> Support</a></li>
                </ul>
            </div>
        </div><!-- .row END -->
    </div><!-- .container END -->
</div>    <!-- End topBar section -->

    <!-- header section -->
    <header class="xs-header">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <div class="xs-logo-wraper">
                    <a href="index.php" class="xs-logo">
                        <img src="assets/images/logo.png" alt="">
                    </a>
                </div>
            </div>
            <div class="col-lg-8">
                <nav class="xs-menus">
                    <div class="nav-header">
                        <a class="nav-brand" href="index.php">
                            <img src="assets/images/logo.png" alt="">
                        </a>
                        <div class="nav-toggle"></div>
                    </div>
                    <div class="nav-menus-wrapper">
						<!--#include file="menu.asp"-->
                    </div>
                </nav>
            </div>
            <div class="col-lg-2">
                <ul class="xs-menu-tools">
                    <li>
                        <a href="#modal-popup-1" class="languageSwitcher-button xs-modal-popup"><i class="icon icon-internet"></i></a>
                    </li>
                    <li>
                        <a href="#" class="offset-side-bar"><i class="icon icon-cart2"></i><span class="item-count">2</span></a>
                    </li>
                    <li>
                        <a href="#modal-popup-2" class="navsearch-button xs-modal-popup"><i class="icon icon-search"></i></a>
                    </li>
                    <li>
                        <a href="#" class="navSidebar-button"><i class="icon icon-burger-menu"></i></a>
                    </li>
                </ul>
            </div>
        </div><!-- .row END -->
    </div><!-- .container END -->
</header>    <!-- End header section -->
</div>

<!-- banner section -->
<section class="xs-banner service-banner contet-to-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 align-self-center">
                <div class="xs-banner-content">
                    <div class="xs-banner-group">
                        <h2 class="banner-title wow fadeInLeft" data-wow-duration="1s">Documentos Digitales</h2>
                        <p class="wow fadeInLeft" data-wow-duration="1.5s">
                        Ponga a disposición de sus clientes documentos de forma digital y aumente la eficacia de sus procesos y la satisfacción de sus clientes.
                        </p>
                    </div>
                </div><!-- .xs-banner-content END -->
            </div>
            <div class="col-lg-5 align-self-center">
                <div class="inner-welcome-image-group wow fadeIn">
                    <img src="assets/images/innerWelcome/shared-hosting.png" data-parallax='{"y": 150}' alt="hosting image">
                </div>
            </div>
        </div><!-- .row END -->
    </div><!-- .container END -->
</section><!-- End banner section -->

<!-- shared hosting details section -->
<section class="xs-section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mx-auto">
                <div class="xs-heading wow fadeIn">
                    <h2 class="heading-sub-title">Ventajas</h2>
                    <h3 class="heading-title">NUESTRO <span>VALOR AGREGADO</span></h3>
                </div><!-- .xs-heading END -->
            </div>
        </div><!-- .row END -->
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="why-choose-us-block wow fadeInUp">
                    <div class="choose-us-img">
                        <img src="assets/images/chooseUs/choose-us-1.png" alt="hostinza hosting image">
                    </div>
                    <h4 class="xs-title">99.9% Tiempo de actividad Garantizado</h4>
                    <p>Alta disponibilidad y continuidad de negocio garantizado para disponibilizar sus documentos.</p>
                </div><!-- .why-choose-us-block END -->
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="why-choose-us-block wow fadeInUp" data-wow-duration="1.5s">
                    <div class="choose-us-img">
                        <img src="assets/images/chooseUs/choose-us-2.png" alt="hostinza hosting image">
                    </div>
                    <h4 class="xs-title">Seguridad de Información</h4>
                    <p>Procesos gestados para garantizar la seguridad de los datos almacenados y distribuidos.</p>
                </div><!-- .why-choose-us-block END -->
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="why-choose-us-block wow fadeInUp" data-wow-duration="2s">
                    <div class="choose-us-img">
                        <img src="assets/images/chooseUs/choose-us-3.png" alt="hostinza hosting image">
                    </div>
                    <h4 class="xs-title">Reducción de Costos</h4>
                    <p>Disponibilice con herramientas modernas y reduzca sus costos operativos y logísticos.</p>
                </div><!-- .why-choose-us-block END -->
            </div>
        </div>
    </div><!-- .container END -->
</section><!-- END shared hosting details section -->


<!-- include feature section -->
<section class="xs-section-padding included-feature-list bg-gradient">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mx-auto">
                <div class="xs-heading wow fadeIn">
                    <h2 class="heading-sub-title">¿Porque Integrarse?</h2>
                    <h3 class="heading-title">Generación y Hosting de <span>Documentos</span></h3>
                </div><!-- .xs-heading END -->
            </div>
        </div><!-- .row END -->
        <div class="row">
            <div class="col-lg-6">
                <div class="include-feature-list">
                    <div class="media hosting-info-list">
                        <div class="d-flex info-icon">
                            <img src="assets/images/included-feature/included-feature-1.png" alt="included feature icon">
                        </div>
                        <div class="media-body">
                            <h4 class="xs-title">Puesta a Disposición Multiplataforma</h4>
                            <p>Disponibilice a sus clientes la documentación digital en una APP Movil, una APP Web y hasta mediante un portal parametrizado con su marca, estetica y hasta dominio.</p>
                        </div>
                    </div><!-- .hosting-info-list END -->
                    <div class="media hosting-info-list">
                        <div class="d-flex info-icon">
                            <img src="assets/images/included-feature/included-feature-2.png" alt="included feature icon">
                        </div>
                        <div class="media-body">
                            <h4 class="xs-title">API de Integración</h4>
                            <p>Obtenga una API de integración para integrar sus procesos a nuestros sistemas y enviar y recibir información en tiempo real. Mejore la efectividad de sus procesos.</p>
                        </div>
                    </div><!-- .hosting-info-list END -->
                    <div class="media hosting-info-list">
                        <div class="d-flex info-icon">
                            <img src="assets/images/included-feature/included-feature-3.png" alt="included feature icon">
                        </div>
                        <div class="media-body">
                            <h4 class="xs-title">Documentos Prediseñados</h4>
                            <p>Genere con nuestros sistemas de generación de documentos archivos digitales prediseñados para cada uno de sus clientes. Genere reglas de negocio condicionales para la generación.</p>
                        </div>
                    </div><!-- .hosting-info-list END -->
                </div><!-- .include-feature-list END -->
            </div>
            <div class="col-lg-6">
                <div class="include-feature-list">
                    <div class="media hosting-info-list">
                        <div class="d-flex info-icon">
                            <img src="assets/images/included-feature/included-feature-4.png" alt="included feature icon">
                        </div>
                        <div class="media-body">
                            <h4 class="xs-title">Generación Masiva Optimizada</h4>
                            <p>Genere cientos de miles de documentos en cuestion de minutos, reduzca los tiempos de procesamiento de sus servidores y genere la puesta a disposición en tiempos record.</p>
                        </div>
                    </div><!-- .hosting-info-list END -->
                    <div class="media hosting-info-list">
                        <div class="d-flex info-icon">
                            <img src="assets/images/included-feature/included-feature-5.png" alt="included feature icon">
                        </div>
                        <div class="media-body">
                            <h4 class="xs-title">Interfaces Parametrizables</h4>
                            <p>Olvidese de generar archivos a la medida de cada proveedor. Usted nos propone su diseño y el sistema se adapta para capturar y/o generar los documentos digitales.</p>
                        </div>
                    </div><!-- .hosting-info-list END -->
                    <div class="media hosting-info-list">
                        <div class="d-flex info-icon">
                            <img src="assets/images/included-feature/included-feature-6.png" alt="included feature icon">
                        </div>
                        <div class="media-body">
                            <h4 class="xs-title">Multiples Tipos de DocumentoPush </h4>
                            <p>Genere documentos a la medida de su negocio en extenciones del tipo PDF, DOCX, TXT, CSV, XLSS.</p>
                        </div>
                    </div><!-- .hosting-info-list END -->
                </div><!-- .include-feature-list END -->
            </div>
        </div>
    </div><!-- .container END -->
</section><!-- END include feature section -->

<!-- testimonial section -->
<section class="xs-testimonial-section testimonial-v3 wow fadeIn">
    <div class="container">
        <div class="xs-testimonial-slider owl-carousel">
            <div class="xs-testimonial-item">
                <div class="testimonial-content">
                    <p>Our best-in-class WordPress solution with additio nal optiz ation to make an running a WooCommerce</p>
                    <div class="commentor-bio media">
                        <div class="d-flex round-avatar">
                            <img src="assets/images/avatar/avatar-1.png" alt="avatar image">
                        </div>
                        <div class="media-body align-self-center">
                            <h4 class="commentor-title">RichardÂ IÂ Jones</h4>
                            <p class="commentor-info">WWW Realty</p>
                            <i class="icon icon-quote"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="xs-testimonial-item">
                <div class="testimonial-content">
                    <p>Our best-in-class WordPress solution with additio nal optiz ation to make an running a WooCommerce</p>
                    <div class="commentor-bio media">
                        <div class="d-flex round-avatar">
                            <img src="assets/images/avatar/avatar-2.png" alt="avatar image">
                        </div>
                        <div class="media-body align-self-center">
                            <h4 class="commentor-title">CeciliaÂ HÂ King</h4>
                            <p class="commentor-info">The White Rabbit</p>
                            <i class="icon icon-quote"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="xs-testimonial-item">
                <div class="testimonial-content">
                    <p>Our best-in-class WordPress solution with additio nal optiz ation to make an running a WooCommerce</p>
                    <div class="commentor-bio media">
                        <div class="d-flex round-avatar">
                            <img src="assets/images/avatar/avatar-3.png" alt="avatar image">
                        </div>
                        <div class="media-body align-self-center">
                            <h4 class="commentor-title">JoeÂ DÂ Walczak</h4>
                            <p class="commentor-info">Hechinger</p>
                            <i class="icon icon-quote"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .container END -->
</section><!-- END testimonial section -->

<!-- client section -->
<div class="xs-client-section xs-section-padding-bottom">
    <div class="container">
        <div class="xs-client-slider owl-carousel">
            <div class="single-client">
                <img src="assets/images/client/client-1.png" alt="hosting client image">
            </div><!-- .single-client END -->
            <div class="single-client">
                <img src="assets/images/client/client-2.png" alt="hosting client image">
            </div><!-- .single-client END -->
            <div class="single-client">
                <img src="assets/images/client/client-3.png" alt="hosting client image">
            </div><!-- .single-client END -->
            <div class="single-client">
                <img src="assets/images/client/client-4.png" alt="hosting client image">
            </div><!-- .single-client END -->
            <div class="single-client">
                <img src="assets/images/client/client-5.png" alt="hosting client image">
            </div><!-- .single-client END -->
        </div>
    </div><!-- .container END -->
</div><!-- END client section -->

<!-- language switcher strart -->
<!-- xs modal -->
<div class="zoom-anim-dialog mfp-hide modal-language" id="modal-popup-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="language-content">
                <p>Switch The Language</p>
                <ul class="flag-lists">
                    <li><a href="#"><img src="assets/images/flags/006-united-states.svg" alt=""><span>English</span></a></li>
                    <li><a href="#"><img src="assets/images/flags/002-canada.svg" alt=""><span>English</span></a></li>
                    <li><a href="#"><img src="assets/images/flags/003-vietnam.svg" alt=""><span>Vietnamese</span></a></li>
                    <li><a href="#"><img src="assets/images/flags/004-france.svg" alt=""><span>French</span></a></li>
                    <li><a href="#"><img src="assets/images/flags/005-germany.svg" alt=""><span>German</span></a></li>
                </ul>
            </div>
        </div>
    </div>
</div><!-- End xs modal --><!-- end language switcher strart -->

<!-- language switcher strart -->
<!-- xs modal -->
<div class="zoom-anim-dialog mfp-hide modal-searchPanel" id="modal-popup-2">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="xs-search-panel">
                <form action="#" method="POST" class="xs-search-group">
                    <input type="search" class="form-control" name="search" id="search" placeholder="Buscar">
                    <button type="submit" class="search-button"><i class="icon icon-search"></i></button>
                </form>
            </div>
        </div>
    </div>
</div><!-- End xs modal --><!-- end language switcher strart -->

<!-- offset cart strart -->
<!-- sidebar cart item -->
<div class="xs-sidebar-group cart-group">
    <div class="xs-overlay xs-bg-black"></div>
    <div class="xs-sidebar-widget">
        <div class="sidebar-widget-container">
            <div class="widget-heading media">
                <div class="media-body">
                    <a href="#" class="close-side-widget">
                        X
                    </a>
                </div>
            </div>
            <div class="xs-empty-content">
                <h3 class="widget-title">Shopping cart</h3>
                <h4 class="xs-title">No products in the cart.</h4>
                <p class="empty-cart-icon">
                    <i class="icon icon-shopping-cart"></i>
                </p>
                <p class="xs-btn-wraper">
                    <a class="btn btn-primary" href="#">Return To Shop</a>
                </p>
            </div>
        </div>
    </div>
</div>    <!-- END sidebar cart item -->    <!-- END offset cart strart -->

<!-- offset cart strart -->
<!-- sidebar cart item -->
<div class="xs-sidebar-group info-group">
    <div class="xs-overlay xs-bg-black"></div>
    <div class="xs-sidebar-widget">
        <div class="sidebar-widget-container">
            <div class="widget-heading">
                <a href="#" class="close-side-widget">
                    X
                </a>
            </div>
            <div class="sidebar-textwidget">
                <div class="sidebar-logo-wraper">
                    <img src="assets/images/blue_logo.png" alt="sidebar logo">
                </div>
                <p>Far far away, behind the word moun tains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of  </p>
                <ul class="sideabr-list-widget">
                    <li>
                        <div class="media">
                            <div class="d-flex">
                                <img src="assets/images/location.png" alt="">
                            </div>
                            <div class="media-body">
                                <p>759 Pinewood Avenue</p>
                                <span>Marquette, Michigan</span>
                            </div>
                        </div><!-- address 1 -->
                    </li>
                    <li>
                        <div class="media">
                            <div class="d-flex">
                                <img src="assets/images/mail.png" alt="">
                            </div>
                            <div class="media-body">
                                <a href="mailto:info@domain.com">info@domain.com</a>
                                <span>Online Support</span>
                            </div>
                        </div><!-- address 1 -->
                    </li>
                    <li>
                        <div class="media">
                            <div class="d-flex">
                                <img src="assets/images/phone.png" alt="">
                            </div>
                            <div class="media-body">
                                <p>906-624-2565</p>
                                <span>Mon-Fri 8am-5pm</span>
                            </div>
                        </div><!-- address 1 -->
                    </li>
                </ul><!-- .sideabr-list-widget -->
                <div class="subscribe-from">
                    <p>Get Subscribed!</p>
                    <form action="#" method="POST" id="subscribe-form" class="subscribe-form">
                        <label for="sub-input"></label>
                        <div class="form-group">
                            <input type="email" name="email" id="sub-input" placeholder="Enter your mail here" class="form-control">
                            <button class="sub-btn" type="submit"><i class="icon icon-arrow-right"></i></button>
                        </div>
                    </form>
                </div>
                <ul class="social-list version-2">
                    <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#" class="vimeo"><i class="fa fa-vimeo"></i></a></li>
                </ul><!-- .social-list -->
                <div class="text-center">
                    <a href="#" class="btn btn-primary">Purchase Now</a>
                </div>
            </div>
        </div>
    </div>
</div>    <!-- END sidebar widget item -->    <!-- END offset cart strart -->

		<!-- footer section start -->
		<footer class="xs-footer-section">
			<div class="footer-group" style="background-image: url(assets/images/footer-bg.png);">
				<div class="footer-main">
					<div class="container">
						<div class="row">
							<div class="col-lg-3 col-md-6">
								<div class="footer-widget wow fadeInUp">
									<h3 class="widget-title">CasillaPostal</h3>
									<ul class="xs-list">
										<li><a href="index.php">Inicio</a></li>
										<li><a href="precios.php">Precios</a></li>
										<li><a href="contacto.php">Contacto</a></li>
										<li><a href="login.php">Login</a></li>
										<li><a href="api.php">Api Desarrolladores</a></li>
										<li><a href="Politica.php" target="_blank">Legales</a></li>
									</ul><!-- .xs-list END -->
								</div><!-- .footer-widget END -->
							</div>
							<div class="col-lg-3 col-md-6">
								<div class="footer-widget wow fadeInUp" data-wow-duration="1s">
									<h3 class="widget-title">Empresa</h3>
									<ul class="xs-list">
										<li><a href="empresa.php">Nosotros</a></li>
										<li><a href="http://www.sysworld.com.ar" target="_blank">Sitio Corporativo</a></li>
										<li><a href="contacto.php">Contactenos</a></li>
										<li><a href="http://www.sysworld.com.ar/v2/sw/empresa_dist.php" target="_blank">Distribuidores</a></li>
										<li><a href="http://www.sysworld.com.ar/" target="_blank">Politica de Privacidad</a></li>
										<li><a href="http://www.sysworld.com.ar/" target="_blank">Datos Personales</a></li>
									</ul><!-- .xs-list END -->
								</div><!-- .footer-widget END -->
							</div>
							<div class="col-lg-3 col-md-6">
								<div class="footer-widget wow fadeInUp" data-wow-duration="1.3s">
									<h3 class="widget-title">Soporte</h3>
									<ul class="xs-list">
										<li><a href="who-is.html">Tutoriales</a></li>
										<li><a href="http://cloud.sysworld.com.ar/" target="_blank">Cargar Ticket</a></li>
										<li><a href="http://estado.sysworld.com.ar/" target="_blank">Estado de Servicios</a></li>
										<li><a href="faq.php">FAQ</a></li>
									</ul><!-- .xs-list END -->
								</div><!-- .footer-widget END -->
							</div>
							<div class="col-lg-3 col-md-6">
								<div class="footer-widget wow fadeInUp" data-wow-duration="1.5s">
									<h3 class="widget-title">Contacto</h3>
									<ul class="contact-info-widget">
										<li class="media">
											<img src="assets/images/address-pin.png" class="d-flex" alt="contact icon">
											<span class="media-body">Bucarelli 2480, CABA, Argentina</span>
										</li><!-- .media END -->
										<li class="media">
											<img src="assets/images/phone-pin.png" class="d-flex" alt="contact icon">
											<span class="media-body">+54 (011) 5269-2919</span>
										</li><!-- .media END -->
										<li class="media">
											<img src="assets/images/email-icon.png" class="d-flex" alt="contact icon">
											<span class="media-body">info@sysworld.com.ar</span>
										</li><!-- .media END -->
									</ul><!-- .contact-info-widget -->
								</div><!-- .footer-widget END -->
							</div>
						</div><!-- .row END -->
					</div><!-- .container END -->
				</div><!-- .footer-main END -->
				<div class="container">
					<div class="footer-bottom">
						<div class="row">
							<div class="col-md-6">
								<div class="footer-bottom-info wow fadeInUp">
									<p>Un aliado estratègico para su negocio. Desde nuestra empresa creemos en crear soluciones que le apliquen valor a su negocio. <a href="http://www.sysworld.com.ar">Sysworld Servicios S.A.</a></p>
								</div>
							</div>
							<div class="col-md-6">
								<ul class="xs-list list-inline wow fadeInUp" data-wow-duration="1s">
									<li><img src="assets/images/security/security-company-images-1.png" alt="security company images"></li>
									<li><img src="assets/images/security/security-company-images-2.png" alt="security company images"></li>
									<li><img src="assets/images/security/security-company-images-3.png" alt="security company images"></li>
									<li><img src="assets/images/security/security-company-images-4.png" alt="security company images"></li>
								</ul>
							</div>
						</div><!-- .row END -->
					</div><!-- .footer-bottom end -->
				</div><!-- .container end -->
			</div><!-- .footer-group END -->
			<div class="footer-copyright">
				<div class="container">
					<div class="row">
						<div class="col-md-4">
							<div class="copyright-text wow fadeInUp">
								<p>&copy; 2018 - <%=year(date)%> Powered by <a href="http://www.sysworld.com.ar">Sysworld Servicios S.A. </a></p>
							</div><!-- .copyright-text END -->
						</div>
						<div class="col-md-4">
							<div class="footer-logo-wraper wow fadeInUp" data-wow-duration="1s">
								<a href="index.php" class="footer-logo"><img src="assets/images/logo.png" alt="footer logo" width="180" height="45"></a>
							</div><!-- .footer-logo-wraper END -->
						</div>
						<div class="col-md-4">
							<div class="social-list-wraper wow fadeInUp" data-wow-duration="1.3s">
								<ul class="social-list">
									<li><a href="https://www.facebook.com/SysWorld-Servicios-SA-323344231089747" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a></li>
									<li><a href="https://twitter.com/Sysworldsa" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a></li>
								</ul>
							</div><!-- .social-list-wraper END -->
						</div>
					</div><!-- .row END -->
				</div><!-- .container END -->
			</div><!-- .footer-copyright END -->
		</footer>
		<!-- footer section end -->
		<!-- js file start -->
		<script src="assets/js/jquery-3.2.1.min.js"></script>
		<script src="assets/js/jquery-ui.min.js"></script>
		<script src="assets/js/plugins.js"></script>
		<script src="assets/js/Popper.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/jquery.magnific-popup.min.js"></script>
		<script src="assets/js/owl.carousel.min.js"></script>
		<script src="assets/js/tweetie.js"></script>
		<script src="assets/js/jquery.parallax-scroll.js"></script>
		<script src="assets/js/jquery.ajaxchimp.min.js"></script>
		<script src="assets/js/jquery.parallax.js"></script>
		<script src="assets/js/hostslide.js"></script>
		<script src="assets/js/vps-slider.js"></script>
		<script src="assets/js/vps-slider-settings.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyCy7becgYuLwns3uumNm6WdBYkBpLfy44k"></script>
		<script src="assets/js/main.js"></script>		<!-- End js file -->
	</body>
</html>