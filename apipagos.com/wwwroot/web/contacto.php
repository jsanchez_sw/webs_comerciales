<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="iso-8859-1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
<link href="images/favicon.png" rel="icon" />
<title>APIPAGOS - El centro de los Pagos Online.</title>
<meta name="description" content="This professional design html template is for build a Money Transfer and online payments website.">
<meta name="author" content="harnishdesign.net">

<!-- Web Fonts
============================================= -->
<link rel='stylesheet' href='../../../fonts.googleapis.com/cssab2a.css?family=Rubik:300,300i,400,400i,500,500i,700,700i,900,900i' type='text/css'>

<!-- Stylesheet
============================================= -->
<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="vendor/font-awesome/css/all.min.css" />
<link rel="stylesheet" type="text/css" href="vendor/owl.carousel/assets/owl.carousel.min.css" />
<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
</head>
<body>

<!-- Preloader -->
<div id="preloader">
  <div data-loader="dual-ring"></div>
</div>
<!-- Preloader End -->

<!-- Document Wrapper
============================================= -->
<div id="main-wrapper">

  <!-- Header
  ============================================= -->
  <header id="header">
    <div class="container">
      <div class="header-row">
        <div class="header-column justify-content-start">
          <!-- Logo
          ============================= -->
          <div class="logo"> <a class="d-flex" href="index.php" title="Inicio"><img src="images/logo.png" alt="APIPagos" /></a> </div>
          <!-- Logo end -->
          <!-- Collapse Button
          ============================== -->
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header-nav"> <span></span> <span></span> <span></span> </button>
          <!-- Collapse Button end -->

          <!-- Primary Navigation
          ============================== -->
          <nav class="primary-menu navbar navbar-expand-lg">
            <div id="header-nav" class="collapse navbar-collapse">
              <ul class="navbar-nav mr-auto">
                <li><a href="../pays/paysimple.php">Pagar</a></li>
                <li><a href="cobrar.php">Cobrar</a></li>
                <li><a href="nosotros.php">Nosotros</a></li>
                <li><a href="contacto.php">Contacto</a></li>
                <li class="dropdown"> <a class="dropdown-toggle" href="#">API</a>
				  <ul class="dropdown-menu">
					<li><a class="dropdown-item" href="http://www.apilanding.com">Documentaci�n</a></li>
				  </ul>
                </li>
              </ul>
            </div>
          </nav>
          <!-- Primary Navigation end -->
        </div>
        <div class="header-column justify-content-end">
          <!-- Login & Signup Link
          ============================== -->
          <nav class="login-signup navbar navbar-expand">
            <ul class="navbar-nav">
              <li><a href="login.php" target="_blank">Login</a> </li>
              <li class="align-items-center h-auto ml-sm-3"><a class="btn btn-primary d-none d-sm-block" href="registro.php" target="_blank">Contratar</a></li>
            </ul>
          </nav>
          <!-- Login & Signup Link end -->
        </div>
      </div>
    </div>
  </header>
  <!-- Header End -->

<!-- Page Header
============================================= -->
<section class="page-header page-header-text-light bg-dark-3 py-5">
  <div class="container">
    <div class="row text-center">
      <div class="col-12">
        <ul class="breadcrumb mb-0">
          <li><a href="index.php">Inicio</a></li>
          <li class="active">Contacto</li>
        </ul>
      </div>
      <div class="col-12">
        <h1>Contactenos</h1>
      </div>
    </div>
  </div>
</section>
<!-- Page Header End -->

  <!-- Content
  ============================================= -->
  <div id="content">
  <div class="container">
      <div class="row">

      <div class="col-md-4 mb-4">
      <div class="bg-white shadow-md rounded h-100 p-3">
      	<div class="featured-box text-center">
              <div class="featured-box-icon text-primary mt-4"> <i class="fas fa-map-marker-alt"></i></div>
              <h3>Oficinas Buenos Aires.</h3>
              <p>Bucarelli 2480<br>
                Ciudad de Buenos Aires.<br>
                (C1431DRB)<br>
                Argentina</p>
            </div>
      </div>
      </div>

      <div class="col-md-4 mb-4">
      <div class="bg-white shadow-md rounded h-100 p-3">
      	<div class="featured-box text-center">
              <div class="featured-box-icon text-primary mt-4"> <i class="fas fa-phone"></i> </div>
              <h3>Telefono</h3>
              <p class="mb-0">(+054) 011 5263-2919</p>
              <p>&nbsp;</p>
            </div>
      </div>
      </div>

      <div class="col-md-4 mb-4">
      <div class="bg-white shadow-md rounded h-100 p-3">
      	<div class="featured-box text-center">
              <div class="featured-box-icon text-primary mt-4"> <i class="fas fa-envelope"></i> </div>
              <h3>Email de Contacto</h3>
              <p>info@sysworld.com.ar</p>
            </div>
      </div>
      </div>


      <div class="col-12 mb-4">
      <div class="text-center py-5 px-2">
      <h2 class="text-8">Dejenos un Mensaje</h2>
      <p class="lead">
	      <iframe width="600" height="600" src="https://cloud.sysworld.com.ar/forms/wtl/20cc38c01fe29ce640581ae67433c250" frameborder="0" allowfullscreen=""></iframe>
      </p>


      </div>
      </div>

      </div>
    </div>



    <!-- Customer Support
    ============================================= -->
    <section class="hero-wrap section shadow-md">
      <div class="hero-mask opacity-9 bg-primary"></div>
      <div class="hero-bg" style="background-image:url('images/bg/image-2.jpg');"></div>
      <div class="hero-content py-5">
        <div class="container text-center">
          <h2 class="text-9 text-white">Contacte un Asesor en Cobranzas</h2>
          <p class="text-4 text-white mb-4">�Tiene consultas del servicio? Contacte a nuestro asesor de cobranzas online y conozca porque somos la mejor alternativa en el mercado.</p>
          <a href="#" class="btn btn-light">Contactar Ahora</a> </div>
      </div>
    </section>
    <!-- Customer Support end -->

  <!-- Footer
  ============================================= -->
  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg d-lg-flex align-items-center">
          <ul class="nav justify-content-center justify-content-lg-start text-3">
            <li class="nav-item"> <a class="nav-link active" href="nosotros.php">Nosotros</a></li>
            <li class="nav-item"> <a class="nav-link" href="soporte.php">Soporte</a></li>
            <li class="nav-item"> <a class="nav-link" href="ayuda.php">Ayuda</a></li>
            <li class="nav-item"> <a class="nav-link" href="contacto.php">Contacto</a></li>
            <li class="nav-item"> <a class="nav-link" href="nosotros.php?#partners">Partners</a></li>
            <li class="nav-item"> <a class="nav-link" href="precios.php">Precios</a></li>
          </ul>
        </div>
        <div class="col-lg d-lg-flex justify-content-lg-end mt-3 mt-lg-0">
          <ul class="social-icons justify-content-center">
            <li class="social-icons-facebook"><a data-toggle="tooltip" href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
            <li class="social-icons-twitter"><a data-toggle="tooltip" href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
            <li class="social-icons-google"><a data-toggle="tooltip" href="http://www.google.com/" target="_blank" title="Google"><i class="fab fa-google"></i></a></li>
            <li class="social-icons-youtube"><a data-toggle="tooltip" href="http://www.youtube.com/" target="_blank" title="Youtube"><i class="fab fa-youtube"></i></a></li>
          </ul>
        </div>
      </div>
      <div class="footer-copyright pt-3 pt-lg-2 mt-2">
        <div class="row">
          <div class="col-lg">
            <p class="text-center text-lg-left mb-2 mb-lg-0">Copyright &copy; 2019 <a href="index.php">APIPagos</a>. All Rights Reserved.</p>
          </div>
          <div class="col-lg d-lg-flex align-items-center justify-content-lg-end">
            <ul class="nav justify-content-center">
              <li class="nav-item"> <a class="nav-link active" href="seguridad.php">Seguridad</a></li>
              <li class="nav-item"> <a class="nav-link" href="terminos.php">T�rminos y Condiciones</a></li>
              <li class="nav-item"> <a class="nav-link" href="privacidad.php">Privacidad</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- Footer end -->


</div>
<!-- Document Wrapper end -->

<!-- Back to Top
============================================= -->
<a id="back-to-top" data-toggle="tooltip" title="Back to Top" href="javascript:void(0)"><i class="fa fa-chevron-up"></i></a>

<!-- Video Modal
============================================= -->
<div class="modal fade" id="videoModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content bg-transparent border-0">
      <button type="button" class="close text-white opacity-10 ml-auto mr-n3 font-weight-400" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <div class="modal-body p-0">
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" id="video" allow="autoplay"></iframe>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Video Modal end -->

<!-- Script -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="js/theme.js"></script>
</body>
</html>