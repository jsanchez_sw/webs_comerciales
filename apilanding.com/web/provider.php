<?php
    include 'functions/db.php';

    include_once 'functions/language.php';


	$sql = "SELECT * from web_apis_proveedores WHERE codigo = '" . $_GET['cod'] . "'";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$id = $_GET['id'];
			$codigo = $row["codigo"];
			$descripcion = $row["descripcion"];
			$descripcioncomercial = $row["descripcioncomercial"];
			$descripciontecnica = $row["descripciontecnica"];
			$cantsusc = $row["cantsusc"];
			$cantapis = $row["cantapis"];
			$fechavigencia = $row["fechavigencia"];
			$url = $row["url"];
		}
	}else{
		header('Location: providers.php');
		die();
	}


?>




<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from protechtheme.com/saas/blog-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:17:29 GMT -->
<head>
        <!-- Required meta tags -->
        <meta charset="iso-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="keywords" content="<?php echo $lang['SITE_keywords'];?>">
		<meta name="description" content="<?php echo $lang['SITE_desc'];?>">
		<meta name="author" content="Sysworld Servicios S.A.">
        <title><?php echo $lang['SITE_title'];?></title>
        <!-- Bootstrap core CSS -->
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Select2 -->
        <link href="assets/select2/css/select2.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- Iconmoon -->
        <link href="assets/iconmoon/css/iconmoon.css" rel="stylesheet">
        <!-- Magnific Popup -->
        <link href="assets/magnific-popup/css/magnific-popup.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="css/custom.css" rel="stylesheet">
        <link rel="icon" type="image/x-icon" href="images/favicon.ico" />
    </head>

<!-- ==============================================
**Preloader**
=================================================== -->
        <div id="loader">
            <div id="element">
                <div class="circ-one"></div>
                <div class="circ-two"></div>
            </div>
        </div>

<!-- ==============================================
**Header**
=================================================== -->
        <header>
			<?php include 'menu.php';?>
        </header>

<!-- ==============================================
**Inner Banner**
=================================================== -->
        <section class="inner-banner">
            <div class="container">
                <div class="contents">
                    <h1>Fuentes de APIS</h1>
                    <p>A continuaci�n se listan nuestros proveedores y fuentes que hacen nuestro Catalogo de APIS.<span></span></p>
                </div>
            </div>
        </section>

<!-- ==============================================
**Blog section**
=================================================== -->
        <section class="blog-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-lg-9">
                        <div class="blog-content blog-detailed">
                            <p class="time"><?php echo substr($fechavigencia,0,10);?> <span></span></p>
                            <h5><?php echo $descripcion;?></h5>
                            <div class="box">
                                <ul class="blog-info">
                                    <li class="comment"><?php echo $cantsusc;?> Suscripciones</li>
                                    <li class="comment"><?php echo $cantapis;?> APIS</li>
                                </ul>
                            </div>
                            <p><?php echo $descripcioncomercial;?></p>
                            <p><?php echo $descripciontecnica;?></p>
                            <ul class="blog-tag">
								<li><a href="<?php echo $url;?>" target="_blank">Web</a></li>
								<li><a href="apis.php?p=<?php echo $codigo;?>">Ver APIS</a></li>
                            </ul>
                        </div>
                    </div>

<!-- ==============================================
**Blog Sidebar**
=================================================== -->
                    <aside class="col-md-4 col-lg-3">
                        <div class="blog-sidebar">

                            <!--Start Categories-->
                            <div class="cmn-box">
								<h4><?php echo $lang['APIS_category_title'];?></h4>
								<ul>
									<li><a href="apis.php?c=fintech"><?php echo $lang['MENU_APIS_FintechFinance'];?></a></li>
									<li><a href="apis.php?c=identity"><?php echo $lang['MENU_APIS_Identity'];?></a></li>
									<li><a href="apis.php?c=notifications"><?php echo $lang['MENU_APIS_EmailNotification'];?></a></li>
									<li><a href="apis.php?c=security"><?php echo $lang['MENU_APIS_Security'];?></a></li>
									<li><a href="apis.php?c=developers"><?php echo $lang['MENU_APIS_DeveloperTools'];?></a></li>
									<li><a href="apis.php?c=locations"><?php echo $lang['MENU_APIS_Localisation'];?></a></li>
									<li><a href="apis.php?c=productivity"><?php echo $lang['MENU_APIS_Productivity'];?></a></li>
									<li><a href="apis.php?c=socialnetworks"><?php echo $lang['MENU_APIS_SocialNetworks'];?></a></li>
									<li><a href="apis.php?c=utilitys"><?php echo $lang['MENU_APIS_Utilities'];?></a></li>
								</ul>
                            </div>
                            <!--End Categories-->
                            <!--Start Archieves-->
                            <div class="cmn-box archive">
								<h4><?php echo $lang['APIS_category_industries'];?></h4>
								<ul>
									<li><a href="apis.php?i=pymes"><?php echo $lang['APIS_search_category_industries_pymes'];?></a></li>
									<li><a href="apis.php?i=coorp"><?php echo $lang['APIS_search_category_industries_coorp'];?></a></li>
									<li><a href="apis.php?i=automotive"><?php echo $lang['APIS_search_category_industries_automotive'];?></a></li>
									<li><a href="apis.php?i=fintech"><?php echo $lang['APIS_search_category_industries_finance'];?></a></li>
									<li><a href="apis.php?i=insurance"><?php echo $lang['APIS_search_category_insurance'];?></a></li>
									<li><a href="apis.php?i=retail"><?php echo $lang['APIS_search_category_retail'];?></a></li>
									<li><a href="apis.php?i=tech"><?php echo $lang['APIS_search_category_tech'];?></a></li>
									<li><a href="apis.php?i=gov"><?php echo $lang['APIS_search_category_government'];?></a></li>
								</ul>
                            </div>
                            <!--End Archieves-->

                        </div>
                    </aside>
                </div>
            </div>
        </section>


<!-- ==============================================
**Footer opt1**
=================================================== -->
		<?php include 'footer.php';?>



        <!-- Scroll to top -->
        <a href="#" class="scroll-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>

        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery.min.js"></script>
        <!-- Popper JS -->
        <script src="js/popper.min.js"></script>
        <!-- Bootsrap JS -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- Select2 JS -->
        <script src="assets/select2/js/select2.min.js"></script>
        <!-- Display Comment Count JS -->
        <script id="dsq-count-scr" src="../../http-protechtheme-com.disqus.com/count.js" async></script>
        <!-- Custom JS -->
        <script src="js/custom.js"></script>
    </body>

<!-- Mirrored from protechtheme.com/saas/blog-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:17:32 GMT -->
</html>