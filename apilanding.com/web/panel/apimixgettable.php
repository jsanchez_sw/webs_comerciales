<?php
    include '../functions/db.php';
    include_once '../functions/language.php';
	include 'session.php';

	$usuario = $_SESSION['apilanding_user_email'];


	$idapi = $_POST['api'];
	$idfile = $_POST['file'];

	$table = '';

    $sql = "SELECT * FROM web_apis WHERE id = " . $idfile;
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$tabla = 'api_data_sw_';
			if (isset($row['propietario'])){
				if ($row['propietario'] != ''){
					$tabla = 'api_data_cli_';
				}
			}
			$tabla = $tabla . $row['id'] . "_" . $row['codigo'];
			$contexto = $row['contexto'];
		}
	}else{
		echo "Invalid API." . $sql;
		die();
	}



    $sql = "SELECT A.contexto, B.apitoken, C.susctoken FROM web_apis A "
    . " INNER JOIN web_suscripciones_keys B "
    . " ON A.id = B.idapi "
    . " INNER JOIN web_suscripciones C "
    . " ON B.idsuscripcion = C.id "
    . " WHERE A.id = " . $idapi
    . " AND B.usuario = '" . $usuario . "'";


	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$contexto = $row['contexto'];
			$susctoken = $row['susctoken'];
			$apitoken = $row['apitoken'];
		}
	}else{
		echo "Invalid API." . $sql;
		die();
	}

	$urlapi = 'http://cont1-virtual1.certisend.com/web/container/api/v1/' . $contexto . "?token-susc=" . $susctoken . "&token-api=" . $apitoken;

	//Parametros api
	$parametrosapi = '';
	$sql = "SELECT * from web_apis_parametros WHERE idapi = " . $idapi;
	$result2 = $conn->query($sql);
	if ($result2->num_rows > 0) {
		while($row2 = $result2->fetch_assoc()) {
			$parametrosapi = $parametrosapi . $row2['parametro'] . ';';
		}
	}
	$arrparametrosapi = explode(";", $parametrosapi);

	//campos respuestas api
	$camposrtaapi = '';
	$sql = "SELECT * from web_apis_campos_respuesta WHERE idapi = " . $idapi;
	$result2 = $conn->query($sql);
	if ($result2->num_rows > 0) {
		while($row2 = $result2->fetch_assoc()) {
			$camposrtaapi = $camposrtaapi . $row2['campo'] . ';';
		}
	}
	$arrcamposrtaapi = explode(";", $camposrtaapi);



	//Campos file
	$campos = '';
	$sql = "SELECT * from web_apis_data_campos WHERE idapi = " . $idfile . " order by orden ";
	$result2 = $conn->query($sql);
	if ($result2->num_rows > 0) {
		while($row2 = $result2->fetch_assoc()) {
			$campos = $campos . $row2['campo'] . ';';
		}
	}
	$arrcampos = explode(";", $campos);


	$sql = "SELECT * FROM " . $tabla . " limit 10";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$table = $table . '<tr>';
			foreach ($arrcampos as $campo) {
				if (strlen($campo) > 0){
					$table = $table . '<td>' . $row[$campo] . '</td>';
				}
			}

			$urltempapi = $urlapi;
			foreach ($arrparametrosapi as $parametro){
				if (strlen($parametro) > 0){
					$urltempapi = $urltempapi . "&" . $parametro . "=" . $row[$_POST[$parametro]];
				}
			}
			$jsonrta = file_get_contents($urltempapi);
			$arrrta = json_decode($jsonrta, true);

			foreach ($arrcamposrtaapi as $campo) {
				if (strlen($campo) > 0){
					$table = $table . '<td>' . $arrrta[$row[$campo]] . '</td>';
				}
			}



			$table = $table . '</tr>';
		}
	}


	echo $table;

?>
