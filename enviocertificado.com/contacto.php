<html lang="en">
	<head>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<title>EnvioCertificado.com - Envio con Respaldo Notarial.</title>
		<meta name="description" content="Cloudhub is a modern, responsive and easy to customize HTML template, perfectly suited for hosting and technology companies.">
		<meta name="keywords" content="html template, responsive, retina, cloud hosting, technology, startup">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<!-- Icons -->
		<link rel="apple-touch-icon-precomposed" href="img/icons/apple-touch-icon.png">
		<link rel="icon" href="img/icons/favicon.ico">
		<!-- Stylesheets -->
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="css/main.min.css">
	</head>
	<body class="footer-dark">
		<!-- Header -->
		<header id="header" class="header-dynamic header-light header-shadow-scroll">
			<div class="container">
				<a class="logo" href="index.html">
					<img src="img/logos/logo_ern.jpg" alt="">
				</a>
				<nav>
					<ul class="nav-primary">
					<li>
							<a href="index.php">Inicio</a>
						</li>
						<li>
							<a href="empresa.php">Nuestra Empresa</a>
						</li>
						<li>
							<a href="funcionalidades.php">Funcionalidades</a>
						</li>
						<li>
							<a href="faq.php">FAQ</a>
						</li>
						<li>
							<a href="contacto.php">Contacto</a>
						</li>
						<li>
							<a class="button button-secondary" href="panel.php">
								<i class="fa fa-lock icon-left"></i>Acceso Sistema
							</a>
						</li>
					</ul>
					<ul class="nav-secondary">
						<li>
							<a href="contacto.php"><i class="fa fa-phone icon-left"></i>+54 (011) 5263-2919</a>
						</li>
						<li>
							<a href="contacto.php"><i class="fa fa-comment icon-left"></i>Live Chat</a>
						</li>
						<li>
							<a href="http://help.enviocertificado.com"><i class="fa fa-question-circle icon-left"></i>Tutoriales</a>
						</li>
						<li>
							<a href="http://estado.sysworld.com.ar"><i class="fa fa-check icon-left"></i>Estado de Servicios</a>
						</li>
					</ul>
				</nav>
			</div>
		</header>
		<!-- Content -->
		<section id="content">
			<!-- Content Row -->
			<section class="content-row content-row-color content-row-clouds">
				<div class="container">
					<header class="content-header content-header-small content-header-uppercase">
						<h1>
							Contactenos
						</h1>
						<p>
							Envíenos un mensaje y a la brevedad le responderemos.
						</p>
					</header>
				</div>
			</section>
			<!-- Content Row -->
			<section class="content-row">
				<div class="container">
					<center><iframe width="600" height="850" src="http://cloud.sysworld.com.ar/forms/wtl/20cc38c01fe29ce640581ae67433c250" frameborder="0" allowfullscreen></iframe></center>
				</div>
			</section>
			<!-- Content Row -->
			<section class="content-row content-row-gray">
				<div class="container">
					<div class="column-row align-center text-align-center">
						<div class="column-33">
							<i class="fa fa-support icon-feature"></i>
							<h3>
								Soporte a Clientes
							</h3>
							<p>
								Acceda al sistema de soporte de clientes.
							</p>
							<p>
								<a href="http://cloud.sysworld.com.ar/clients/login" target="_blank">Soporte dedicado<i class="fa fa-angle-right icon-right"></i></a>
							</p>
						</div>
						<div class="column-33">
							<i class="fa fa-comments icon-feature"></i>
							<h3>
								Tutoriales en Línea
							</h3>
							<p>
								Visite nuestro centro de ayuda en línea.
							</p>
							<p>
								<a href="http://cloud.sysworld.com.ar/knowledge-base" target="_blank">Ayuda y Tutoriales<i class="fa fa-angle-right icon-right"></i></a>
							</p>
						</div>
						<div class="column-33">
							<i class="fa fa-exclamation-triangle icon-feature"></i>
							<h3>
								Estado de Servicios
							</h3>
							<p>
								Controle el estado de nuestros servicios.
							</p>
							<p>
								<a href="http://estado.sysworld.com.ar" target="_blank">Estatus del Sistema<i class="fa fa-angle-right icon-right"></i></a>
							</p>
						</div>
						<div class="column-33">
													<a href="http://qr.afip.gob.ar/?qr=3O7M4FgVqm6jjYGyB0IMcA,," target="_F960AFIPInfo"> <img src="http://www.afip.gob.ar/images/f960/DATAWEB.jpg" border="0" width="150" height="150">
													<h3>

													</h3>
													<p>

													</p>
													<p>
													  	</a>
													</p>
						</div>
					</div>
				</div>
			</section>
			<!-- Content Row -->
			<section class="content-row content-row-color">
				<div class="container">
					<header class="content-header">
						<h2>
							¿Interesado en nuestra empresa?
						</h2>
						<p>
							Acceda al sitio web corporativo para conocer mas detalles de nuestra empresa.<br><br>
							<a class="button button-secondary" href="http://www.sysworld.com.ar" target="_blank">
								<i class="fa fa-globe icon-left"></i>Sitio Web Corporativo
							</a>
						</p>
					</header>
				</div>
			</section>
		</section>
		<!-- Footer -->
		<footer id="footer">
			<section class="footer-primary">
				<div class="container">
					<div class="column-row">
						<div class="column-33">
							<h5>
								Sysworld Servicios S.A.
							</h5>
							<p>
								EnvioCertificado.com | EnvioLegal.com | CertiSend.com |Envio de Email Certificados | Envios con Respaldo Notarial | Certificaciones en la BlockChain | SMS Certificados | Transacciones Certificadas | Auditoria de Transacciones
							</p>
						</div>
						<div class="column-66">
							<div class="column-row align-right-top">
								<div class="column-25">
									<h5>
										Conectarse
									</h5>
									<ul class="list-style-icon">
										<li>
											<a href="https://www.facebook.com/SysWorld-Servicios-SA-323344231089747" target="_blank"><i class="fa fa-facebook"></i>Facebook</a>
										</li>
										<li>
											<a href="https://twitter.com/Sysworldsa" target="_blank"><i class="fa fa-twitter"></i>Twitter</a>
										</li>
										<li>
											<a href="contacto.php"><i class="fa fa-github"></i>Contacto</a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar/v2/sw/contacto.php" target="_blank"><i class="fa fa-xing"></i>Live Chat</a>
										</li>
									</ul>
								</div>
								<div class="column-25">
									<h5>
										Productos
									</h5>
									<ul>
										<li>
											<a href="prodenviocertificado.php">EnvioCertificado</a>
										</li>
										<li>
											<a href="prodern.php">ERN</a>
										</li>
										<li>
											<a href="prodtrn.php">TRN</a>
										</li>
										<li>
											<a href="http://www.centrodevalidaciones.com" target="_blank">Validadores</a>
										</li>
										<li>
											<a href="http://www.omnicrm.cloud" target="_blank">OmniCRM.cloud</a>
										</li>
										<li>
											<a href="http://cloud.sysworld.com.ar/" target="_blank">Customer Portal</a>
										</li>
									</ul>
								</div>
								<div class="column-25">
									<h5>
										Recursos
									</h5>
									<ul>
										<li>
											<a href="funcionalidades.php">Funcionalidades</a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar/v2/sw/empresa.php?f=network" target="_blank">Network</a>
										</li>
										<li>
											<a href="http://sysworld.com.ar/v2/sw/empresa.php?f=politicas">Respaldo Legales</a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar/v2/sw/empresa.php?f=partners" target="_blank">Partners</a>
										</li>
									</ul>
								</div>
								<div class="column-flex">
									<h5>
										Empresa
									</h5>
									<ul>
										<li>
											<a href="empresa.php">Nosotros</a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar" target="_blank">Sitio Corporativo</a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar/v2/sw/empresa_dist.php" target="_blank">Distribuidores</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="footer-secondary">
				<div class="container">
					<p>
						Copyright 2006 -<?php echo date("Y"); ?> &copy; Sysworld Servicios S.A. Todos los derechos reservados.<br>
						Powered by <a href="http://www.sysworld.com.ar">Sysworld Servicios S.A.</a>
					</p>
				</div>
			</section>
		</footer>
		<!-- Scripts -->

		<script type="text/javascript">
				var LHCChatOptions = {};
				LHCChatOptions.opt = {widget_height:340,widget_width:300,popup_height:520,popup_width:500};
				(function() {
				var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
				var referrer = (document.referrer) ? encodeURIComponent(document.referrer.substr(document.referrer.indexOf('://')+1)) : '';
				var location  = (document.location) ? encodeURIComponent(window.location.href.substring(window.location.protocol.length)) : '';
				po.src = '//livechat.enviocertificado.com/panel/index.php/esp/chat/getstatus/(click)/internal/(position)/bottom_right/(ma)/br/(top)/350/(units)/pixels/(leaveamessage)/true/(department)/2/5/(theme)/4?r='+referrer+'&l='+location;
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
				})();
</script>

		<script src="js/jquery.min.js"></script>
		<script src="js/headroom.min.js"></script>
		<script src="js/js.cookie.min.js"></script>
		<script src="js/imagesloaded.min.js"></script>
		<script src="js/bricks.min.js"></script>
		<script src="js/main.min.js"></script>
	</body>
</html>
