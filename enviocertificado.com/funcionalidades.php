<html>
	<head>
			<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<title>EnvioCertificado.com - Envio con Respaldo Notarial.</title>
		<meta name="description" content="Cloudhub is a modern, responsive and easy to customize HTML template, perfectly suited for hosting and technology companies.">
		<meta name="keywords" content="html template, responsive, retina, cloud hosting, technology, startup">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<!-- Icons -->
		<link rel="apple-touch-icon-precomposed" href="img/icons/apple-touch-icon.png">
		<link rel="icon" href="img/icons/favicon.ico">
		<!-- Stylesheets -->
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="css/main.min.css">
	</head>
	<body class="footer-dark">
		<!-- Header -->
		<header id="header" class="header-dynamic header-light header-shadow-scroll">
			<div class="container">
				<a class="logo" href="index.html">
					<img src="img/logos/logo_ern.jpg" alt="">
				</a>
				<nav>
					<ul class="nav-primary">
						<li>
							<a href="index.php">Inicio</a>
						</li>
						<li>
							<a href="empresa.php">Nuestra Empresa</a>
						</li>
						<li>
							<a href="funcionalidades.php">Funcionalidades</a>
						</li>
						<li>
							<a href="faq.php">FAQ</a>
						</li>
						<li>
							<a href="contacto.php">Contacto</a>
						</li>
						<li>
							<a class="button button-secondary" href="panel.php">
								<i class="fa fa-lock icon-left"></i>Acceso al Sistema
							</a>
						</li>
					</ul>
					<ul class="nav-secondary">
						<li>
							<a href="contacto.php"><i class="fa fa-phone icon-left"></i>+54 (011) 5263-2919</a>
						</li>
						<li>
							<a href="contacto.php"><i class="fa fa-comment icon-left"></i>Live Chat</a>
						</li>
						<li>
							<a href="http://help.enviocertificado.com"><i class="fa fa-question-circle icon-left"></i>Tutoriales</a>
						</li>
						<li>
							<a href="http://estado.sysworld.com.ar"><i class="fa fa-check icon-left"></i>Estado de Servicios</a>
						</li>
					</ul>
				</nav>
			</div>
		</header>

		<!-- Content -->
		<section id="content">
			<!-- Content Row -->
			<section class="content-row content-row-color content-row-clouds">
				<div class="content-slider animate-container-slide" data-nav="true" data-rotation="5">
					<div class="slide">
						<div class="container">
							<div class="column-row align-center">
								<div class="column-33 text-align-center">
									<i class="fa fa-rocket icon-feature-large text-color-light"></i>
								</div>
								<div class="column-50">
									<header class="content-header content-header-large content-header-align-left">
										<h3>
											Deliverabilidad Garantizada
										</h3>

										<p>
											Gracias a la reputación de nuestra red y a certificaciones anti-spam el 99.9 % de nuestras comunicaciones gozan de llegar a bandeja de entrada.<br><br>
											<a class="button button-secondary" href="funcionalidades.php">Ver funcionalidades<i class="fa fa-chevron-right icon-right"></i></a>
										</p>

									</header>
								</div>
							</div>
						</div>
					</div>
					<div class="slide">
						<div class="container">
							<div class="column-row align-center">
								<div class="column-33 text-align-center">
									<i class="fa fa-cloud icon-feature-large text-color-light"></i>
								</div>
								<div class="column-50">
									<header class="content-header content-header-large content-header-align-left">
										<h3>
											Alta Disponibilidad
										</h3>
										<p>
											Una infraestructura globalmente distribuida basada en continuidad del negocio permita brindar alta disponibilidad a nuestros servicios más críticos.<br><br>
											<a class="button button-secondary" href="funcionalidades.php">Ver funcionalidades<i class="fa fa-chevron-right icon-right"></i></a>
										</p>
									</header>
								</div>
							</div>
						</div>
					</div>
					<div class="slide">
						<div class="container">
							<div class="column-row align-center">
								<div class="column-33 text-align-center">
									<i class="fa fa-shield icon-feature-large text-color-light"></i>
								</div>
								<div class="column-50">
									<header class="content-header content-header-large content-header-align-left">
										<h3>
											Seguridad de la Información
										</h3>
										<p>
											Todos nuestros procesos, productos y servicios son gestados bajo estrictas normas de seguridad de la información para garantizar la confidencialidad y protección de los datos.<br><br>
											<a class="button button-secondary" href="funcionalidades.php">Ver funcionalidades<i class="fa fa-chevron-right icon-right"></i></a>
										</p>
									</header>
								</div>
							</div>
						</div>
					</div>
					<div class="slide">
						<div class="container">
							<div class="column-row align-center">
								<div class="column-33 text-align-center">
									<i class="fa fa-refresh icon-feature-large text-color-light"></i>
								</div>
								<div class="column-50">
									<header class="content-header content-header-large content-header-align-left">
										<h3>
											Startup inmediato
										</h3>
										<p>
											Gracias a la disponibilidad inmediata de recursos y a que nuestros sistemas son 100% paramétricos, el alta de un nuevo cliente se realiza pocas horas.<br><br>
											<a class="button button-secondary" href="funcionalidades.php">Ver funcionalidades<i class="fa fa-chevron-right icon-right"></i></a>
										</p>
									</header>
								</div>
							</div>
						</div>
					</div>
					<div class="slide">
						<div class="container">
							<div class="column-row align-center">
								<div class="column-33 text-align-center">
									<i class="fa fa-support icon-feature-large text-color-light"></i>
								</div>
								<div class="column-50">
									<header class="content-header content-header-large content-header-align-left">
										<h3>
											Soporte Dedicado
										</h3>
										<p>
											Diferentes planes de soporte le garantizan a nuestros clientes una atención a la medida de cada necesidad.<br><br>
											<a class="button button-secondary" href="funcionalidades.php">Ver funcionalidades<i class="fa fa-chevron-right icon-right"></i></a>
										</p>
									</header>
								</div>
							</div>
						</div>
					</div>
					<div class="slide">
						<div class="container">
							<div class="column-row align-center">
								<div class="column-33 text-align-center">
									<i class="fa fa-gear icon-feature-large text-color-light"></i>
								</div>
								<div class="column-50">
									<header class="content-header content-header-large content-header-align-left">
										<h3>
											Automatización Poderosa
										</h3>
										<p>
											Gracias a nuestra API y nuestro workflow automatizado podemos crear reglas de negocio a la medida de cada uno de nuestros clientes.<br><br>
											<a class="button button-secondary" href="funcionalidades.php">Ver funcionalidades<i class="fa fa-chevron-right icon-right"></i></a>
										</p>
									</header>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- Content Row -->
			<section class="content-row">
				<div class="container">
					<div class="column-row">
						<div class="column-50">
							<font color="#0594F7">
							<h3>

								<i class="material-icons">mail_outline</i>	Emails certificados

							</h3>
							</font>

							<p align="justify">
							EnvioLegal le permite enviar sus comunicaciones por e-mail con un respaldo notarial. Además de esta importante característica que le brinda fuerza legal a sus comunicaciones, cada uno de los envíos cuenta con certificaciones internacionales que permiten un alto porcentaje de llegada a los destinatarios.
							</p>

									<ul class="list-style-icon">
																	<li>
																		<i class="fa fa-check"></i>Respaldo notarial de envíos
																	</li>
																	<li>
																		<i class="fa fa-check"></i>Respaldo notarial de estadísticas
																	</li>
																	<li>
																		<i class="fa fa-check"></i>Certificaciones internacionales Anti-SPAM
																	</li>
																	<li>
																		<i class="fa fa-check"></i>Certificaciones internacionales Optin.CertiStamp
																	</li>
									</ul>
									<br>
	</div>
							<div class="column-50">
							<font color="#0594F7">
										<h3>
										<i class="material-icons">chat</i>	SMS certificado
											</h3>
							</font>


									<p align="justify">
									Organismos públicos, entidades administrativas y judiciales que necesiten enviar, tener constancia y seguridad en la entrega de sus comunicaciones.
								</p>
								<p align="justify">
									ERN (Envío con Respaldo Notarial) permite el envío de comunicaciones electrónicas generando seguridad en los envíos y manteniendo resguardo de la documentación por 10 años. Incluye los servicios añadidos de prueba de entrega certificada como garantía de recepción, mediante prueba de entrega electrónica y confirmación de recepción.
							</p>

								</div>
							</div>
						</div>

			</section>
			<!-- Content Row -->
			<section class="content-row content-row-gray">
				<div class="container">
					<div class="column-row align-center">
						<div class="column-50">
							<div class="network-map">
								<ul>
									<li style="top: 42%; left: 12.5%;">
										<a href="network.html"></a>
									</li>
									<li style="top: 44%; left: 20%;">
										<a href="network.html"></a>
									</li>
									<li style="top: 40%; left: 26%;">
										<a href="network.html"></a>
									</li>
									<li style="top: 78%; left: 34%;">
										<a href="network.html"></a>
									</li>
									<li style="top: 41%; left: 44.5%;">
										<a href="network.html"></a>
									</li>
									<li style="top: 32%; left: 46.5%;">
										<a href="network.html"></a>
									</li>
									<li style="top: 34%; left: 49.5%;">
										<a href="network.html"></a>
									</li>
									<li style="top: 64%; left: 75%;">
										<a href="network.html"></a>
									</li>
									<li style="top: 44%; left: 85%;">
										<a href="network.html"></a>
									</li>
									<li style="top: 83%; left: 89%;">
										<a href="network.html"></a>
									</li>
								</ul>
								<img src="img/layout/map-dark.svg" alt="">
							</div>
						</div>
						<div class="column-50">

							<font color="#0594F7">
							<h3>
							<i class="material-icons">data_usage</i>DataCleaning
							</h3>
							</font>
							<p align="justify">
							 El servicio de DataCleaning le permite el descubrimiento, corrección o eliminación de datos erróneos de sus bases de datos permitiendo así una mejor efectividad en las campañas de comunicaciones.							</p>
							<font color="#0594F7"><br>
							<h3>
		<i class="material-icons">picture_as_pdf</i> Generación de PDFs
							</h3>
							</font>
							<p align="justify">
						EnvioLegal mediante el servicio de Generación de PDFs le posibilita enviar de forma plana los datos de sus comunicaciones y nuestros servidores harán el proceso de dibujo y personalización de cada uno de los archivos.
								</p>
						</div>
					</div>
				</div>
			</section>
			<!-- Content Row -->
			<section class="content-row">
				<div class="container">
					<div class="column-row">
						<div class="column-50">
							<font color="#0594F7">
							<h3>

								<i class="material-icons">verified_user</i> Validación de Emails
							</h3>
							</font>
							<p align="justify">
							Cada vez ganan más terreno las comunicaciones electrónicas, es por ello que para poder cursarlas se necesita tener los contactos actualizados y validados correctamente para evitar malas reputaciones en servidores de destino. Otras formas de comprobación de correo generan confusión y hasta complicaciones en el suscriptor, además de que requieren que el mismo descargue sus correos lo cual lleva tiempo.							</p>
						</div>
						<div class="column-50">

							<font color="#0594F7">
							<h3>
								<i class="material-icons">perm_identity</i> Validación de Emails	Validación de Identidad
							</h3>
							</font>
							<p align="justify">
							Cada vez son más frecuentes los envíos de comunicaciones sensibles de forma electrónica. Es por ello que los remitentes requieren de la certeza de que las comunicaciones sea enviadas a la entidad correcta. Gracias a diferentes alianzas con empresas de informes comerciales muy prestigiosas y mediante formularios multiple choice de verificación podemos garantizar con un scoring la validación de identidad de un destinatario.
							</p>
						</div>
					</div>
				</div>
			</section>
			<!-- Content Row -->
			<section class="content-row content-row-gray">
				<div class="container">
					<div class="column-row">
						<div class="column-50">
						<font color="#0594F7">
						<h3>
						<i class="material-icons">thumb_up</i>	Normalización
						</h3>
						</font>
						<p align="justify">
							Cada vez es mayor la necesidad de contener en sus bases de datos registros normalizados y verificados. Mediante este servicio EnvioLegal le permite normalizar sus datos para garantizar la correcta confección de las comunicaciones y su posterior envío.
						</p>
						</div>
						<div class="column-50">
						<font color="#0594F7">
						<h3>
						<i class="material-icons">question_answer</i>		Procesamiento de Respuestas
						</h3>
						</font>

								<p align="justify">
							El proceso de respuestas y su administración es un punto muy importante en la gestión de comunicaciones. Es por eso que ERN le brinda la posibilidad de tener un email reply-to (responder a) para procesar y poder generar acciones de autorespuestas automáticas dentro de los servidores de ERN.								</p>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- Content Row -->
			<section class="content-row content-row-color">
				<div class="container">
					<header class="content-header">
						<h2>
						Buscando una solución personalizada?
						</h2>
						<p>
						Nuestros técnicos pueden proporcionarle las mejores soluciones personalizadas en el mercado, sin importar si es una pequeña empresa o una gran empresa.<br>
						<br><a class="button button-secondary" href="contacto.php">
								<i class="fa fa-envelope icon-left"></i>Contáctanos
							</a>
						</p>
					</header>
				</div>
			</section>
		</section>
		<!-- Footer -->
		<footer id="footer">
			<section class="footer-primary">
				<div class="container">
					<div class="column-row">
						<div class="column-33">
							<h5>
								Sysworld Servicios S.A.
							</h5>
							<p>
								EnvioCertificado.com | EnvioLegal.com | CertiSend.com |Envio de Email Certificados | Envios con Respaldo Notarial | Certificaciones en la BlockChain | SMS Certificados | Transacciones Certificadas | Auditoria de Transacciones
							</p>
						</div>
						<div class="column-66">
							<div class="column-row align-right-top">
								<div class="column-25">
									<h5>
										Conectarse
									</h5>
									<ul class="list-style-icon">
										<li>
											<a href="https://www.facebook.com/SysWorld-Servicios-SA-323344231089747" target="_blank"><i class="fa fa-facebook"></i>Facebook</a>
										</li>
										<li>
											<a href="https://twitter.com/Sysworldsa" target="_blank"><i class="fa fa-twitter"></i>Twitter</a>
										</li>
										<li>
											<a href="contacto.php"><i class="fa fa-github"></i>Contacto</a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar/v2/sw/contacto.php" target="_blank"><i class="fa fa-xing"></i>Live Chat</a>
										</li>
									</ul>
								</div>
								<div class="column-25">
									<h5>
																			Productos
																		</h5>
																		<ul>
																			<li>
																				<a href="http://sysworld.com.ar/v2/sw/index.php">EnvioCertificado</a>
																			</li>
																			<li>
																				<a href="">ERN</a>
																			</li>
																			<li>
																				<a href="">TRN</a>
																			</li>
																			<li>
																				<a href="http://www.centrodevalidaciones.com" target="_blank">Validadores</a>
																			</li>
																			<li>
																				<a href="http://www.omnicrm.cloud" target="_blank">OmniCRM.cloud</a>
																			</li>
																			<li>
																				<a href="http://cloud.sysworld.com.ar/" target="_blank">Portal</a>
										</li>
									</ul>
								</div>
								<div class="column-25">
									<h5>
										Recursos
									</h5>
									<ul>
										<li>
											<a href="funcionalidades.php">Funcionalidades</a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar/v2/sw/empresa.php?f=network" target="_blank">Network</a>
										</li>
										<li>
											<a href="http://sysworld.com.ar/v2/sw/empresa.php?f=politicas">Respaldo Legales</a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar/v2/sw/empresa.php?f=partners" target="_blank">Partners</a>
										</li>
									</ul>
								</div>
								<div class="column-flex">
									<h5>
										Empresa
									</h5>
									<ul>
										<li>
											<a href="empresa.php">Nosotros</a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar" target="_blank">Sitio Corporativo</a>
										</li>
										<li>
											<a href="http://www.sysworld.com.ar/v2/sw/empresa_dist.php" target="_blank">Distribuidores</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="footer-secondary">
				<div class="container">
					<p>
						Copyright 2006 -<?php echo date("Y"); ?> &copy; Sysworld Servicios S.A. Todos los derechos reservados.<br>
						Powered by <a href="http://www.sysworld.com.ar">Sysworld Servicios S.A.</a>
					</p>
				</div>
			</section>
		</footer>
		<!-- Scripts -->
		<script type="text/javascript">
				var LHCChatOptions = {};
				LHCChatOptions.opt = {widget_height:340,widget_width:300,popup_height:520,popup_width:500};
				(function() {
				var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
				var referrer = (document.referrer) ? encodeURIComponent(document.referrer.substr(document.referrer.indexOf('://')+1)) : '';
				var location  = (document.location) ? encodeURIComponent(window.location.href.substring(window.location.protocol.length)) : '';
				po.src = '//livechat.enviocertificado.com/panel/index.php/esp/chat/getstatus/(click)/internal/(position)/bottom_right/(ma)/br/(top)/350/(units)/pixels/(leaveamessage)/true/(department)/2/5/(theme)/4?r='+referrer+'&l='+location;
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
				})();
</script>


		<script src="js/jquery.min.js"></script>
		<script src="js/headroom.min.js"></script>
		<script src="js/js.cookie.min.js"></script>
		<script src="js/imagesloaded.min.js"></script>
		<script src="js/bricks.min.js"></script>
		<script src="js/main.min.js"></script>
	</body>
</html>
