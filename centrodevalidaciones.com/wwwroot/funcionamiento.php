<!DOCTYPE html>
<html lang="zxx" dir="ltr">

    <head>
        <!-- Standard Meta -->
        <meta charset="utf-8">
        <meta name="format-detection" content="telephone=no" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- Site Properties -->
        <title>Centro de Validaciones - Funcionamiento</title>
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon-precomposed" href="images/apple-touch-icon.png">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Rajdhani:300,400,500,600,700%7CRoboto:300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">

        <!-- CSS -->
        <link rel="stylesheet" href="css/uikit.css">
        <link rel="stylesheet" href="css/components/slider.css">
        <link rel="stylesheet" href="css/components/slideshow.css">
        <link rel="stylesheet" href="css/components/slidenav.css">
        <link rel="stylesheet" href="css/components/dotnav.css">
        <link rel="stylesheet" href="css/small-icon.css">
        <link rel="stylesheet" href="css/elements.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/mediaquery.css">
    </head>

    <body>

        <!-- Header Section -->
        <header id="idz-header">
            <!-- Mobile / Offcanvas Menu -->
        	<div id="mobile-nav" class="uk-offcanvas">
				<div class="uk-offcanvas-bar">
					<ul class="uk-nav uk-nav-offcanvas uk-nav-parent-icon" data-uk-nav>
						<li class="uk-parent"><a href="#">home</a>
							<ul class="uk-nav-sub">
								<li><a href="index.html">Homepage 1</a></li>
						        <li><a href="index2.html">Homepage 2</a></li>
						        <li><a href="index3.html">Homepage 3</a></li>
						        <li><a href="index4.html">Homepage 4</a></li>
					        </ul>
						</li>
			            <li><a href="shared.html">shared</a></li>
			            <li><a href="vps.html">vps</a></li>
			            <li><a href="dedicated.html">Dedicated</a></li>
			            <li><a href="other-services.html">other services </a></li>
			            <li class="uk-parent"><a href="#">blog</a>
						    <ul class="uk-nav-sub">
						    	<li><a href="blog.html">Blog Page</a></li>
						        <li><a href="single.html">Single Post</a></li>
					        </ul>
			            </li>
			            <li><a href="contact.html">Meet Us</a></li>
						<li class="uk-parent uk-active"><a href="#">Pages</a>
						    <ul class="uk-nav-sub">
						        <li><a href="domain-pricing.html">Domain Pricing</a></li>
						        <li><a href="features.html">Features</a></li>
						        <li><a href="about.html">About</a></li>
						        <li><a href="testimonial.html">Testimonial</a></li>
						        <li><a href="team.html">Team</a></li>
						        <li><a href="login.html">Login</a></li>
						        <li><a href="client-area.html">Client Area</a></li>
						        <li><a href="portfolio.html">Portfolio</a></li>
						        <li><a href="faq.html">FAQ</a></li>
						        <li><a href="sitemap.html">Sitemap</a></li>
						        <li><a href="404.html">404 Error</a></li>
						    </ul>
						</li>
						<li class="uk-parent"><a href="#">Elements</a>
						    <ul class="uk-nav-sub">
						        <li><a href="column.html">Column</a></li>
						        <li><a href="table.html">Table</a></li>
						        <li><a href="button-list.html">Button &amp; List</a></li>
						        <li><a href="icon-list.html">Icon List</a></li>
						        <li><a href="icon-use.html">Icon Use</a></li>
						        <li><a href="typography.html">Typography</a></li>
						        <li><a href="panel-promo.html">Panel &amp; Promo Box</a></li>
						        <li><a href="tab-accordion.html">Tab &amp; Accordion</a></li>
						        <li><a href="alert-progress.html">Alert &amp; Progress</a></li>
						    </ul>
						</li>
					</ul>
				</div>
			</div>
			<!-- Mobile / Offcanvas Menu End -->

			<div class="uk-container uk-container-center">
				<div class="uk-grid">

					<!-- Logo -->
					<div class="uk-width-large-1-2 uk-width-medium-1-1 uk-width-small-1-1">
						<div class="idz-logo">
							<a href="index.php"><img src="images/logo.png" alt="logo" class=""></a>
						</div>
					</div>
					<!-- Logo End -->

					<!-- Domain Search Form -->
					<div class="uk-width-large-1-2  uk-width-medium-1-1 uk-width-small-1-1">
						<div class="idz-search-domain-block">
							<div class="idz-header-search-domain">

								<form class="uk-form idz-search-domain-form">
									<fieldset>
									<input type="text" class="uk-form-large uk-width-1-1 " placeholder="Prueba de Validacion de Email/Telefono/Domicilio/DNI">
									<button class="uk-button-large idz-button blue uppercase">Probar Validador</button>
									</fieldset>
								</form>
							</div>
						</div>
					</div>
					<!-- Domain Search Form End -->

					<!-- Header Info -->
					<div class="uk-width-large-1-1 uk-width-medium-1-1 uk-width-small-1-1">
						<ul class="uk-grid uk-grid-large uk-grid-with-medium-1-4 idz-header-contact">
							<li><i class="uk-icon-globe"></i>Español</li>
							<li><i class="uk-icon-phone"></i>(+54 11) 5263-2919</li>
							<li><i class="uk-icon-headphones"></i><a target="blank" href="http://www.sysworld.com.ar/soporte.php">Live Chat</li></a>
							<li><i class="uk-icon-life-bouy "></i><a href="#client-area" data-uk-modal>Acceso Clientes</a>
						</ul>
					</div>
					<!-- Header End -->

				</div>
			</div>

			<!-- Header Nav -->
			<div class="idz-header-nav" data-uk-sticky="{top:-400}">

				<div class="uk-container uk-container-center">
					<div class="uk-grid">
						<div class="uk-width-medium-1-1">

							<div class="idz-menu-wrapper">
								<div class="uk-grid">
									<div class="uk-width-large-8-10 uk-width-medium-3-10 uk-width-small-3-10">

										<!-- Menu Items -->
										<div class="idz-nav">
											<nav class="uk-navbar">
											    <div class="uk-navbar-flip">
											        <ul class="uk-navbar-nav uk-hidden-medium  uk-hidden-small">
											            <li class="uk-parent uk-active" data-uk-dropdown><a href="index.php">Inicio</a></li>
											            <li><a href="empresa.php">Nuestra Empresa</a></li>
											            <li class="uk-parent" data-uk-dropdown="{justify:'.idz-menu-wrapper'}"><a href="funcionamiento.php">Validadores</a>
					                                        <div class="uk-dropdown uk-dropdown-navbar idz-megamenu">
					                                            <ul class="uk-grid" data-uk-grid-margin>
					                                                <li class="uk-width-large-3-10 uk-width-medium-4-10">
					                                                    <ul class="uk-list uk-list-space">
					                                                        <li>
					                                                            <a href="validadoremail.php">
					                                                                <div class="icon-wrap circle left small blue">
					                                                                    <i class="uk-icon-cubes"></i>
					                                                                </div>
					                                                                <h5>Email</h5>
					                                                                <p class="uk-nbfc">Verifique si una cuenta de email existe o no.</p>
					                                                            </a>
					                                                        </li>
					                                                        <li>
					                                                            <a href="validadortelefono.php">
					                                                                <div class="icon-wrap circle left small blue">
					                                                                    <i class="uk-icon-cube"></i>
					                                                                </div>
					                                                                <h5>Teléfono</h5>
					                                                                <p class="uk-nbfc">Verifique y Normalice un número de teléfono fijo y/o movil.</p>
					                                                            </a>
					                                                        </li>
					                                                        <li>
					                                                            <a href="validadordomicilio.php">
					                                                                <div class="icon-wrap circle left small blue">
					                                                                    <i class="uk-icon-tasks"></i>
					                                                                </div>
					                                                                <h5>Domicilio</h5>
					                                                                <p class="uk-nbfc">Valide, Normalice y Enriquezca un Domicilio.</p>
					                                                            </a>
					                                                        </li>
					                                                        <li>
					                                                            <a href="validadoridentidad.php">
					                                                                <div class="icon-wrap circle left small blue">
					                                                                    <i class="uk-icon-tasks"></i>
					                                                                </div>
					                                                                <h5>Identidad</h5>
					                                                                <p class="uk-nbfc">Valide la identidad de una persona física.</p>
					                                                            </a>
					                                                        </li>					                                                    </ul>
					                                                </li>
					                                                <li class="uk-width-large-2-10 uk-width-medium-3-10">
					                                                	<h5>Otros Servicios</h5>
					                                                    <ul class="uk-list uk-list-space idz-list-check">
					                                                        <li><a href="http://www.enviocertificado.com" target="_blank">EnvíoCertificado.com</a></li>
					                                                        <li><a href="http://www.enviocertificado.com" target="_blank">ContactoCertificado.com</a></li>
					                                                        <li><a href="http://www.tuscomprobantes.com" target="_blank">TusComprobantes.com</a></li>
					                                                    </ul>
					                                                </li>
					                                                <li class="uk-width-large-2-10 uk-width-medium-3-10">
					                                                	<h5>Soporte</h5>
					                                                    <ul class="uk-list  uk-list-space idz-list-custom">
					                                                        <li><a href="contacto.php"><i class="uk-icon-envelope-o"></i>Contactenos</a></li>
					                                                        <li><a href="http://help.enviocertificado.com" target="_blank"><i class="uk-icon-book"></i>Tutoriales</a></li>
					                                                        <li><a href="http://help.enviocertificado.com" target="_blank"><i class="uk-icon-comments-o"></i>Foro de Debate</a></li>
					                                                        <li><a href="http://www.sysworld.com.ar/portal" target="_blank"><i class="uk-icon-exclamation-triangle"></i>Portal de Clientes</a></li>
					                                                    </ul>
					                                                </li>
					                                                <li class="uk-width-large-3-10 uk-hidden-medium">
					                                                    <h5>¿Porqué usar validadores?</h5>
					                                                    <p>Integrar sistemas automáticos de validacion en sus procesos le garantiza poder contactar a sus clientes y reducir costos en la comunicación.</p>
					                                                </li>
					                                            </ul>
					                                        </div>
					                                    </li>
											            <li><a href="funcionamiento.php">Funcionamiento</a></li>
											            <li><a href="contacto.php">Contactenos</a></li>
											        </ul>
											    </div>
											</nav>
											<!-- Mobile Menu Toggle  -->
                                            <a href="#mobile-nav" class="uk-navbar-toggle uk-visible-small uk-visible-medium" data-uk-offcanvas></a>
                                            <!-- Mobile Menu Toggle End-->
										</div>
										<!-- Menu Items End -->
									</div>


								</div>
							</div>

						</div>
					</div>
				</div>

			</div>
			<!-- Header Nav End -->

		</header>
		<!-- Header Section End -->

        <!-- Page Header -->
        <section id="idz-header-inner">
            <div class="uk-container uk-container-center">

                <div class="idz-page-title idz-padding-medium">
                    <div class="uk-grid">
						<!-- Page Header Title -->
                        <div class="uk-width-large-1-2 uk-width-medium-1-3">
                            <div class="idz-title">
                                <h1 class="">Funcionamiento<span>De los validadores</span></h1>
                                <i class="uk-icon-server"></i>
                            </div>
                        </div>
                        <!-- Page Header Title End-->

						<!-- Page Description -->
                        <div class="uk-width-large-1-2 uk-width-medium-2-3">
                            <div class="uk-grid">
                                <div class="uk-width-medium-3-4 uk-push-1-4">
                                    <div class="idz-page-promo uk-margin-top">
                                        <p></p>
                                    </div>
                                </div>
                                <div class="uk-width-medium-1-4 uk-pull-3-4"></div>
                          </div>
                       </div>
                       <!-- Page Description End -->

                    </div>
                </div>

            </div>
        </section>
        <!-- Page Header End -->

       	<!-- Content Section -->
        <section class="idz-padding">
            <div class="uk-container uk-container-center">
                <div class="uk-grid">
                    <!-- feature 7 begin -->
                    <div class="uk-width-large-1-1 uk-margin-large-bottom">
                        <h2 class="uk-margin-small-bottom uk-text-center">Integre validaciones en sus procesos.</h2>
                        <p class="uk-text-large uk-margin-small-top uk-text-center">Email, Teléfono, Domicilio, Identidad</p>
                        <div class="uk-grid uk-margin-medium-top">
                            <div class="uk-width-large-1-2 uk-width-medium-1-1 uk-width-small-1-1">
                                <img src="images/sample/sample_object11.png" alt="" class="uk-align-center idz-tablet-pt-center idz-tablet-pt-margin">
                            </div>
                            <div class="uk-width-large-1-2 uk-width-medium-1-1 uk-width-small-1-1">
                                <p>Integrar validadores dentro de sus procesos o por única vez, mejora sustancialmente la calidad de su base y permite tener contacto permanente con sus clientes. De está forma puede reducir notablemente los costos de comunicación y gestión interna.</p>
                                <p>Nuestros sistemas de validación y normalización se pueden integrar de forma on-line o batch a sus sistemas y devolverle los resultados que espera para lograr sus objetivos.</p>
                                <p>Un equipo de asistentes esta a disposición para recomendarle cual es la mejor solución para su necesidad.</p>
                            </div>
                        </div>
                    </div>
                    <!-- feature 7 end -->
                    <div class="uk-width-large-1-1">
                        <hr class="uk-margin-large-top uk-margin-large-bottom" />
                    </div>

                    <!-- feature 1 begin -->
                    <div class="uk-width-large-1-10 uk-width-medium-2-10 uk-hidden-small">
                        <div class="icon-wrap center idz-bottom-adjustment5">
                            <i class="smico-shopping-bag"></i>
                        </div>
                    </div>
                    <div class="uk-width-large-9-10 uk-width-medium-8-10 uk-width-small-1-1">
                        <h2>Fácil Integración. <span class="uk-text-thin">Integrable bajo cualquier Lenguaje/Plataforma.</span></h2>
                        <p>Todos los validadores son fácilmente integrables desde cualquier lenguaje de programación y/o plataforma que pueda generar una llamada de tipo HTTP GET y pueda leer un formato xml.</p>
                        <div class="uk-grid uk-margin-medium-top">
                            <div class="uk-width-large-4-10">
                                <img src="images/sample/sample_object5.png" alt="" class="uk-align-left idz-bottom-adjustment5 idz-tablet-potrait-hidden uk-hidden-small idz-tablet-pt-margin">
                            </div>
                            <div class="uk-width-large-6-10 uk-width-medium-1-1 uk-width-small-1-1">
                                <h5>Plataforma populares.</h5>
                                <ul class="hosting-apps">
                                    <li>
                                        <img src="images/sample/logo_wordpress.svg" alt="" /> Wordpress
                                    </li>
                                    <li>
                                        <img src="images/sample/logo_joomla.svg" alt="" /> Joomla
                                    </li>
                                    <li>
                                        <img src="images/sample/logo_drupal.svg" alt="" /> Drupal
                                    </li>
                                    <li>
                                        <img src="images/sample/logo_mysql.svg" alt="" /> MySQL Database
                                    </li>
                                    <li>
                                        <img src="images/sample/logo_magento.svg" alt="" /> Magento
                                    </li>
                                    <li>
                                        <img src="images/sample/logo_windows.svg" alt="" /> Windows
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- feature 1 end -->
                    <div class="uk-width-large-1-1">
                        <hr class="uk-margin-large-top uk-margin-large-bottom" />
                    </div>
                    <!-- feature 2 begin -->
                    <div class="uk-width-large-1-10 uk-width-medium-2-10 uk-hidden-small">
                        <div class="icon-wrap center idz-bottom-adjustment5">
                            <i class="smico-repeat"></i>
                        </div>
                    </div>
                    <div class="uk-width-large-9-10 uk-width-medium-8-10 uk-width-small-1-1">
                        <h2>Ventajas Ilimitadas. <span class="uk-text-thin">El limite esta en su imaginación.</span></h2>
                        <p>Los validadores pueden agregar valor a sus procesos tanto como su imaginación llegue. La flexibilidad de configuración y el dinamismo de implementación permitirán mejorar la calidad de su base a niveles inimaginables.</p>
                        <div class="uk-grid uk-margin-medium-top">
                            <div class="uk-width-large-1-2 uk-width-small-1-1 uk-margin-medium-bottom">
                                <div class="icon-wrap small left">
                                    <i class="smico-graph-2"></i>
                                </div>
                                <h5 class="uk-margin-small-bottom">Integraciones dinámicas</h5>
                                <p class="uk-nbfc uk-margin-small-top">Los validadores pueden ser consumidos tanto de forma online por medio de un webservices o de forma automatizada mediantes interfaces de entrada/salida con el diseño de archivo propuesto por el cliente.</p>
                            </div>
                            <div class="uk-width-large-1-2 uk-width-small-1-1 uk-margin-medium-bottom">
                                <div class="icon-wrap small left">
                                    <i class="smico-chart"></i>
                                </div>
                                <h5 class="uk-margin-small-bottom">Estadísticas en tiempo real</h5>
                                <p class="uk-nbfc uk-margin-small-top">Todas las estadísticas que el sistema vaya recolectando tras las validaciones pueden ser visualizadas y exportadas en tiempo real desde la plataforma web.</p>
                            </div>
                            <div class="uk-width-large-1-2 uk-width-small-1-1 idz-margin-bottom-ml">
                                <div class="icon-wrap small left">
                                    <i class="smico-www"></i>
                                </div>
                                <h5 class="uk-margin-small-bottom">Portal web de administración</h5>
                                <p class="uk-nbfc uk-margin-small-top">Podrá desde un portal web controlar, monitorear y administrar su cuenta. Todos los consumos, validaciones, usuarios y accesos serán auditados para su posterior control y auditoría interna.</p>
                            </div>
                            <div class="uk-width-large-1-2 uk-width-small-1-1">
                                <div class="icon-wrap small left">
                                    <i class="smico-open-letter"></i>
                                </div>
                                <h5 class="uk-margin-small-bottom">Integración con envíos de email</h5>
                                <p class="uk-nbfc uk-margin-small-top">Los validadores pueden ser integrados en un workflow con el servicio de EnvioCertificado con el fin de generar un proceso de validación, autorización y/o confirmación automatizado de mayor seguridad.</p>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-large-1-1">
                        <hr class="uk-margin-large-top uk-margin-large-bottom" />
                    </div>
                    <!-- feature 4 begin -->
                    <div class="uk-width-large-1-10 uk-width-medium-2-10 uk-hidden-small">
                        <div class="icon-wrap center idz-bottom-adjustment5">
                            <i class="smico-paint"></i>
                        </div>
                    </div>
                    <div class="uk-width-large-9-10 uk-width-medium-8-10 uk-width-small-1-1">
                        <h2>Intalación Fácil. <span class="uk-text-thin">Asistencia en la implementación.</span></h2>
                        <div class="uk-grid uk-margin-top">
                            <div class="uk-width-large-6-10 uk-width-medium-1-1 uk-width-small-1-1">
                                <p>El proceso de implementación es fácil y rápido. Nuestros asistentes y tutoriales en línea lo acompañaran en todo el proceso.</p>
                                <div class="uk-grid uk-margin-medium-top" data-uk-grid-margin>
                                    <div class="uk-width-large-1-4 uk-width-medium-1-2 uk-width-small-1-1">
                                        <div class="uk-panel uk-panel-box idz-panel grey feature-ic-section">
                                            <i class="uk-icon-check-circle"></i>
                                            <p>Fácil implentación</p>
                                        </div>
                                    </div>
                                    <div class="uk-width-large-1-4 uk-width-medium-1-2 uk-width-small-1-1">
                                        <div class="uk-panel uk-panel-box idz-panel grey feature-ic-section">
                                            <i class="uk-icon-hand-o-up"></i>
                                            <p>Desarrollo y Testing 1-Click</p>
                                        </div>
                                    </div>
                                    <div class="uk-width-large-1-4 uk-width-medium-1-2 uk-width-small-1-1">
                                        <div class="uk-panel uk-panel-box idz-panel grey feature-ic-section">
                                            <i class="uk-icon-user"></i>
                                            <p>Asistentes de Soporte</p>
                                        </div>
                                    </div>
                                    <div class="uk-width-large-1-4 uk-width-medium-1-2 uk-width-small-1-1">
                                        <div class="uk-panel uk-panel-box idz-panel grey feature-ic-section">
                                            <i class="uk-icon-life-ring"></i>
                                            <p>Tutoriales de ejemplo</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-large-4-10 idz-tablet-potrait-hidden uk-hidden-small">
                                <img src="images/sample/sample_object9.png" alt="" class="uk-align-right uk-margin-top  idz-tablet-pt-center idz-tablet-pt-margin">
                            </div>
                        </div>
                    </div>
                    <!-- feature 4 end -->
                    <div class="uk-width-large-1-1">
                        <hr class="uk-margin-large-top uk-margin-large-bottom" />
                    </div>
                    <!-- feature 5 begin -->
                    <div class="uk-width-large-1-10 uk-width-medium-2-10 uk-hidden-small">
                        <div class="icon-wrap center idz-bottom-adjustment5">
                            <i class="smico-help"></i>
                        </div>
                    </div>
                    <div class="uk-width-large-9-10 uk-width-medium-8-10 uk-width-small-1-1">
                        <h2>Servicio de Soporte. <span class="uk-text-thin">Todo en cualquier momento.</span></h2>
                        <p>Los procesos de atención al cliente de nuestra empresa se encuentran desarrollados bajo normas de calidad de servicio con el fin de garantizar el mejor nivel de satisfacción de clientes.</p>
                        <div class="uk-grid uk-margin-medium-top uk-text-center" data-uk-grid-margin>
                            <div class="uk-width-large-1-4 uk-width-medium-1-2 uk-width-small-1-1">
                                <div class="icon-wrap center square green uk-margin-bottom">
                                    <i class="uk-icon-phone"></i>
                                </div>
                                <h4 class="uk-margin-bottom-remove">Soporte Comercial.</h4>
                                <p class="uk-margin-top-remove">(+54 11) 5263-2919</p>
                            </div>
                            <div class="uk-width-large-1-4 uk-width-medium-1-2 uk-width-small-1-1">
                                <div class="icon-wrap center square red uk-margin-bottom">
                                    <i class="uk-icon-ticket"></i>
                                </div>
                                <h4 class="uk-margin-bottom-remove">Sistema de Tickets</h4>
                                <p class="uk-margin-top-remove">cloud.sysworld.com.ar</p>
                            </div>
                            <div class="uk-width-large-1-4 uk-width-medium-1-2 uk-width-small-1-1">
                                <div class="icon-wrap center square orange uk-margin-bottom">
                                    <i class="uk-icon-envelope"></i>
                                </div>
                                <h4 class="uk-margin-bottom-remove">Ticket por Email</h4>
                                <p class="uk-margin-top-remove">soporte@sysworld.com.ar</p>
                            </div>
                            <div class="uk-width-large-1-4 uk-width-medium-1-2 uk-width-small-1-1">
                                <div class="icon-wrap center square blue uk-margin-bottom">
                                    <i class="uk-icon-comments"></i>
                                </div>
                                <h4 class="uk-margin-bottom-remove">Live Chat</h4>
                                <p class="uk-margin-top-remove">sysworld.com.ar/chat</p>
                            </div>
                        </div>
                    </div>
                    <!-- feature 5 end -->
                    <div class="uk-width-large-1-1">
                        <hr class="uk-margin-large-top uk-margin-large-bottom" />
                    </div>
                    <!-- feature 6 begin -->
                    <div class="uk-width-large-1-10 uk-width-medium-2-10 uk-hidden-small">
                        <div class="icon-wrap center idz-bottom-adjustment5">
                            <i class="smico-shield"></i>
                        </div>
                    </div>
                    <div class="uk-width-large-9-10 uk-width-medium-8-10 uk-width-small-1-1">
                        <h2>Seguridad de Información. <span class="uk-text-thin">Políticas de Confidencialidad.</span></h2>
                        <p>Todos nuestros productos y servicios se encuentran desarrollados y hosteados bajo estrictas normas de seguridad de la información. Así mismo todos nuestros clientes cuentan con la posibilidad de firmar documentación de confidencialidad y políticas de privacidad.</p>
                        <div class="uk-grid uk-margin-medium-top">
                            <div class="uk-width-large-1-3 uk-width-medium-1-2 uk-width-small-1-1">
                                <ul class="uk-list idz-list-check green big">
                                    <li>Transferencias de Datos Seguras</li>
                                    <li>Protocolos SSL</li>
                                    <li>Políticas ISO 27001</li>
                                    <li>Personal Capacitado</li>
                                    <li>Procesos DRP</li>
                                </ul>
                            </div>
                            <div class="uk-width-large-1-3 uk-width-medium-1-2 uk-width-small-1-1">
                                <ul class="uk-list idz-list-check green big">
                                    <li>Datacenter Seguro</li>
                                    <li>Políticas de Backup</li>
                                    <li>Continuidad de Negocio</li>
                                    <li>Uptime Transparente</li>
                                    <li>Certificaciones Internacionales</li>
                                </ul>
                            </div>
                            <div class="uk-width-large-1-3 uk-hidden-small">
                                <img src="images/sample/sample_object10.png" alt="" class="uk-align-right idz-top-adjustment20 idz-tablet-pt-center idz-tablet-pt-margin">
                            </div>
                        </div>
                    </div>
                    <!-- feature 6 end -->
                    <div class="uk-width-large-1-1">
                        <hr class="uk-margin-large-top uk-margin-large-bottom" />
                    </div>
                </div>
            </div>
        </section>
        <!-- Content Section End -->

		<!-- Footer -->
		<footer id="idz-footer" class="idz-padding">
			<div class="uk-container uk-container-center">
				<div class="uk-grid">

					<!-- Footer Column 1 -->
					<div class="uk-width-large-1-4 uk-width-medium-1-4 uk-width-small-1-2">
						<div class="idz-footer-widget">
							<h3 class="uppercase idz-text-white">Compañía</h3>
							<ul class="uk-list uk-list-space idz-footer-menu">
								<li><a href="contacto.php">Contactenos</a></li>
								<li><a href="http://www.sysworld.com.ar/legal.php" target="_blank">Términos y Condiciones</a></li>
								<li><a href="http://www.sysworld.com.ar/legal.php" target="_blank">Politica de Privacidad</a></li>
								<li><a href="http://www.sysworld.com.ar/" target="_blank">Sitio Coorporativo</a></li>
							</ul>
						</div>
					</div>
					<!-- Footer Column 1 End -->

					<!-- Footer Column 2 -->
					<div class="uk-width-large-1-4 uk-width-medium-1-4 uk-width-small-1-2">
						<div class="idz-footer-widget">
							<h3 class="uppercase idz-text-white">Soporte</h3>
							<ul class="uk-list uk-list-space idz-footer-menu">
								<li><a href="http://help.enviocertificado.com" target="_blank">Tutoriales</a></li>
								<li><a href="http://cloud.sysworld.com.ar" target="_blank">Portal de Tickets</a></li>
								<li><a href="http://erp.sysworld.com.ar/app" target="_blank">Portal Administrativo</a></li>
								<li><a href="contacto.php" target="_blank">Contacto</a></li>
							</ul>
						</div>
					</div>
					<!-- Footer Column 2 End -->

					<!-- Footer Column 3 -->
					<div class="uk-width-large-1-4 uk-width-medium-1-4 uk-width-small-1-2">
						<div class="idz-footer-widget">
							<h3 class="uppercase idz-text-white">Validadores</h3>
							<ul class="uk-list uk-list-space idz-footer-menu">
								<li><a href="validadoremail.php">Email</a></li>
								<li><a href="validadortelefono.php">Teléfono</a></li>
								<li><a href="validadordomicilio.php">Domicilio</a></li>
								<li><a href="validadoridentidad.php">Identidad</a></li>
							</ul>
						</div>
					</div>
					<!-- Footer Column 3 End -->

					<!-- Footer Column 4 (Social Profile Links) -->
					<div class="uk-width-large-1-4 uk-width-medium-1-4 uk-width-small-1-2">
						<div class="idz-footer-widget idz-footer-social">
							<ul class="uk-list">
								<li>
									<div class="idz-button-group">
										<a target="blank" href="https://www.facebook.com/SysWorld.ar/"><i class="uk-icon-facebook-official"></i>seguinos en nuestro<span class="uppercase">facebook</span></a>
									</div>
								</li>
								<li>
									<div class="idz-button-group">
										<a target="blank" href="https://twitter.com/sysworldsa"><i class="uk-icon-twitter"></i>seguinos en nuestro<span class="uppercase">Twitter</span></a>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<!-- Footer Column 4 (Social Profile Links) -->


				</div>

				<!-- Footer Text -->
				<div class="uk-grid">
					<div class="uk-width-medium-1-1 uk-container-center uk-text-center">
						<p class="idz-footer-note idz-thin idz-text-22px uk-margin-remove">Ya validamos <span><%=FormatNumber(DateDiff("s", "11/08/2006 15:22:12", now),0)%> </span>transacciones</p>
						<p class="idz-site-copyright uk-margin-remove">COPYRIGHT  &copy; © 2006-<%=year(date)%> EnvioCertificado.com - Sysworld Servicios S.A.</p>
					</div>
				</div>
				<!-- Footer Text End -->
			</div>

			<!-- Scroll to Top -->
			<a href="#top" class="to-top uk-icon-chevron-up" data-uk-smooth-scroll></a>
			<!-- Scroll to Top End -->
        </footer>
		<!-- Footer End -->

		<!-- client area modal begin -->
        <div id="client-area" class="uk-modal">
            <div class="uk-modal-dialog">

                <ul id="client-area-tab" class="uk-tab uk-tab-grid" data-uk-tab="{connect:'#account'}">
                    <li class="uk-active uk-width-large-1-2"><a href="">Crear Cuenta</a></li>
                    <li class="uk-width-large-1-2"><a href="#">Registrarse</a></li>
                </ul>

                <ul id="account" class="uk-switcher uk-margin">
                	<!-- Modal Login Form -->
                    <li>
                        <form id="signin-account" class="uk-form">
                            <fieldset>
                                <div class="uk-form-icon">
                                    <i class="uk-icon-envelope"></i>
                                    <input type="text" placeholder="Email">
                                </div>
                                <div class="uk-form-icon">
                                    <i class="uk-icon-key"></i>
                                    <input type="text" placeholder="Password">
                                </div>
                                <label>
                                    <input type="checkbox">Remember me</label>
                                	<a href="http://web.enviocertificado.com" target="_blank" class="uk-button uk-button-large idz-button blue uk-width-large-1-1">Ingresar</a>
                            </fieldset>
                        </form>
                    </li>
                	<!-- Modal Login Form End -->

					<!-- Modal Register Form -->
                    <li>
                        <form id="create-account" class="uk-form" action="http://cloud.sysworld.com.ar/clients/register" target="_blank">
                            <fieldset>
                                <div class="uk-form-icon">
                                    <i class="uk-icon-user"></i>
                                    <input type="text" placeholder="Usuario">
                                </div>
                                <div class="uk-form-icon">
                                    <i class="uk-icon-envelope"></i>
                                    <input type="text" placeholder="Email">
                                </div>
                                <div class="uk-form-icon">
                                    <i class="uk-icon-key"></i>
                                    <input type="text" placeholder="Password">
                                </div>
                                <label>
                                    <input type="checkbox">Acepto los términos y condiciones.</label>
                                <a href="http://cloud.sysworld.com.ar/clients/register" target="_blank" class="uk-button uk-button-large idz-button blue uk-width-large-1-1">Registrarse</a>
                            </fieldset>
                        </form>
                    </li>
                    <!-- Modal Register Form End -->
                </ul>
                <div class="uk-modal-caption">
                    <a href="#">Forgot your password?</a>
                </div>
            </div>
        </div>
        <!-- client area modal end -->

        <!-- Javascript -->
        <script src="js/jquery.js"></script>
        <script src="js/uikit.min.js"></script>
        <script src="js/components/slider.js"></script>
        <script src="js/components/slideshow.js"></script>
        <script src="js/components/slideshow-fx.js"></script>
        <script src="js/components/parallax.js"></script>
        <script src="js/components/sticky.js"></script>
        <script src="js/jquery.countTo.js"></script>
        <script src="js/template-config.js"></script>
    </body>
</html>