<?php
    include '../functions/db.php';
	include 'session.php';

	$usuario = $_SESSION['apilanding_user_email'];
	$idapi = $_GET['id'];
	$codigo = $_GET['codigo'];

    $sql = "SELECT * FROM web_apis "
	. " WHERE id = " . $idapi . " AND codigo = '" . $codigo . "' ";

	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$nombre = $row['nombre'];
			$tipoapi = $row['tipoapi'];
			$version = $row['version'];
			$idestado = $row['idestado'];
			$descripcion = $row['descripcion'];
			$codigo = $row['codigo'];


			$tabla = 'api_data_sw_';
			if (isset($row['propietario'])){
				if ($row['propietario'] != ''){
					$tabla = 'api_data_cli_';
				}
			}
			$tabla = $tabla . $row['id'] . "_" . $row['codigo'];

		}
	}else{
		echo "Invalid API.";
		die();
	}


	$rndtemp = md5(date('dmYhis') . rand()) . '_' . date('dmY');

	header('Content-Type: text/csv; charset=utf-8');
	header('Content-Disposition: attachment; filename=' . $rndtemp . '.csv');
	$sql = "SELECT * FROM " . $tabla . " ORDER BY idreg desc";
	$result = $conn->query($sql);
	$output = fopen("php://output", "w");
	while($row = $result->fetch_assoc()) {
		fputcsv($output, $row, ';');
	}
	fclose($output);
?>