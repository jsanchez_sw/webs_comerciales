<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="iso-8859-1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
<link href="images/favicon.png" rel="icon" />
<title>APIPAGOS - El centro de los Pagos Online.</title>
<meta name="description" content="">
<meta name="author" content="">

<!-- Web Fonts
============================================= -->
<link rel='stylesheet' href='../../../fonts.googleapis.com/cssab2a.css?family=Rubik:300,300i,400,400i,500,500i,700,700i,900,900i' type='text/css'>

<!-- Stylesheet
============================================= -->
<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="vendor/font-awesome/css/all.min.css" />
<link rel="stylesheet" type="text/css" href="vendor/bootstrap-select/css/bootstrap-select.css" />
<link rel="stylesheet" type="text/css" href="vendor/currency-flags/css/currency-flags.min.css" />
<link rel="stylesheet" type="text/css" href="vendor/owl.carousel/assets/owl.carousel.min.css" />
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
</head>
<body>

<!-- Preloader -->
<div id="preloader">
  <div data-loader="dual-ring"></div>
</div>
<!-- Preloader End -->

<!-- Document Wrapper
============================================= -->
<div id="main-wrapper">

  <!-- Header
  ============================================= -->
  <header id="header">
    <div class="container">
      <div class="header-row">
        <div class="header-column justify-content-start">
          <!-- Logo
          ============================= -->
          <div class="logo"> <a class="d-flex" href="index.php" title="Inicio"><img src="images/logo.png" alt="APIPagos" /></a> </div>
          <!-- Logo end -->
          <!-- Collapse Button
          ============================== -->
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header-nav"> <span></span> <span></span> <span></span> </button>
          <!-- Collapse Button end -->

          <!-- Primary Navigation
          ============================== -->
          <nav class="primary-menu navbar navbar-expand-lg">
            <div id="header-nav" class="collapse navbar-collapse">
              <ul class="navbar-nav mr-auto">
                <li><a href="../pays/paysimple.php">Pagar</a></li>
                <li><a href="cobrar.php">Cobrar</a></li>
                <li><a href="nosotros.php">Nosotros</a></li>
                <li><a href="contacto.php">Contacto</a></li>
                <li class="dropdown"> <a class="dropdown-toggle" href="#">API</a>
				  <ul class="dropdown-menu">
					<li><a class="dropdown-item" href="http://www.apilanding.com">Documentaci�n</a></li>
				  </ul>
                </li>
              </ul>
            </div>
          </nav>
          <!-- Primary Navigation end -->
        </div>
        <div class="header-column justify-content-end">
          <!-- Login & Signup Link
          ============================== -->
          <nav class="login-signup navbar navbar-expand">
            <ul class="navbar-nav">
              <li><a href="login.html">Login</a> </li>
              <li class="align-items-center h-auto ml-sm-3"><a class="btn btn-primary d-none d-sm-block" href="signup.html">Sign Up</a></li>
            </ul>
          </nav>
          <!-- Login & Signup Link end -->
        </div>
      </div>
    </div>
  </header>
  <!-- Header End -->

  <!-- Content
  ============================================= -->
  <div id="content">

    <!-- Send Money
    ============================================= -->
    <section class="hero-wrap section">
      <div class="hero-mask opacity-9 bg-primary"></div>
      <div class="hero-bg" style="background-image:url('images/bg/image-5.jpg');"></div>
      <div class="hero-content py-2 py-lg-5">
        <div class="container text-center">
          <h2 class="text-14 text-white">Cobre Digitalmente sin Costos Extra</h2>
          <p class="text-5 text-white mb-4">
          	Hemos desarrollado una plataforma para dar servicios a grandes empresas <br class="d-none d-lg-block">
          	sin pagar costos de pagos transaccionales basados en escalas de comisi�n o fees fijos.</p>
        </div>
      </div>
    </section>
    <!-- Send Money End -->

    <!-- How it works
    ============================================= -->
    <section class="section bg-white">
      <div class="container">
        <div class="row">
          <div class="col-xl-10 mx-auto">
            <div class="row">
              <div class="col-md-5 col-lg-6 text-center my-auto order-2 order-md-1"> <img class="img-fluid shadow" src="images/request-money.png" alt=""> </div>
              <div class="col-md-7 col-lg-6 order-1 order-md-2">
                <h2 class="text-9"> Tan Simple como Eficiente</h2>
                <p class="text-3 mb-4">Tenemos una herramienta para cada necesidad de cobranzas digitales.</p>
                <div class="row">
                  <div class="col-12 mb-4">
                    <div class="featured-box style-3">
                      <div class="featured-box-icon text-light"><span class="w-100 text-20 font-weight-500">1</span></div>
                      <h3>Se llega a su cliente</h3>
                      <p>Con diferentes herramientas de automatizaci�n, logramos llegar a su cliente.</p>
                    </div>
                  </div>
                  <div class="col-12 mb-4">
                    <div class="featured-box style-3">
                      <div class="featured-box-icon text-light"><span class="w-100 text-20 font-weight-500">2</span></div>
                      <h3>Su cliente Paga Online</h3>
                      <p>El cliente, una vez contactado elije la forma de pago habilitada y realiza el pago.</p>
                    </div>
                  </div>
                  <div class="col-12 mb-4 mb-sm-0">
                    <div class="featured-box style-3">
                      <div class="featured-box-icon text-light"><span class="w-100 text-20 font-weight-500">3</span></div>
                      <h3>Fondos Acreditados</h3>
                      <p>Nuestra plataforma se encarga de recibir los fondos y remitirlos a su cuenta bancaria.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- How it works End -->

    <!-- Why choose us
    ============================================= -->
    <section class="section">
    <div class="container">
      <h2 class="text-9 text-center">Nuestro Diferencial</h2>
      <p class="text-4 text-center mb-4">Hemos desarrollado un producto disruptivo para resolver problemas que nadie atiende.</p>
      <div class="row">
        <div class="col-xl-10 mx-auto">
          <div class="row">
            <div class="col-sm-6">
              <div class="featured-box style-1">
                <div class="featured-box-icon text-primary"> <i class="far fa-check-circle"></i> </div>
                <h3>+ 20 Medios de Pago</h3>
                <p>Integramos los medio de pago m�s utilizados tanto nacionales como internacionales.</p>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="featured-box style-1">
                <div class="featured-box-icon text-primary"> <i class="far fa-check-circle"></i> </div>
                <h3>Sin Fees Ocultos</h3>
                <p>Nuestra plataforma no agrega costos a las transacciones, solamente tiene los costos nativos.</p>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="featured-box style-1">
                <div class="featured-box-icon text-primary"> <i class="far fa-check-circle"></i> </div>
                <h3>F�cil de Integrar</h3>
                <p>Desarrollamos herramientas para todas las tecnolog�as y procesos especiales de las empresas.</p>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="featured-box style-1">
                <div class="featured-box-icon text-primary"> <i class="far fa-check-circle"></i> </div>
                <h3>Ciclo Automatizado</h3>
                <p>Todo el cliclo de vida de la cobranza esta automatizada por un workflow parametrizable.</p>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="featured-box style-1">
                <div class="featured-box-icon text-primary"> <i class="far fa-check-circle"></i> </div>
                <h3>100% Seguro</h3>
                <p>Nuestra politica de calidad y seguridad garantizan una infraestructura estable y segura.</p>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="featured-box style-1">
                <div class="featured-box-icon text-primary"> <i class="far fa-check-circle"></i> </div>
                <h3>Soporte 24/7</h3>
                <p>Un equipo de soporte esta para ayudar en las transacciones 7x24, garantizando as� el �xito.</p>
              </div>
            </div>
          </div>
          <div class="text-center mt-4"><a href="contacto.php" class="btn btn-outline-primary shadow-none text-uppercase">Contacte un Asesor</a></div>
        </div>
      </div>
    </div>
  </div>
  </section>
  <!-- Why choose us End -->

  <!-- How work
    ============================================= -->
  <section class="section bg-white">
    <div class="container">
      <div class="row">
        <div class="col-lg-7 order-2 order-lg-1">
          <div class="hero-wrap section h-100 p-5 mx-4 rounded-pill">
            <div class="hero-mask rounded-pill opacity-7 bg-dark"></div>
            <div class="hero-bg rounded-pill" style="background-image:url('images/bg/image-6.jpg');"></div>
            <div class="hero-content text-center py-5 my-5"> </div>
          </div>
        </div>
        <div class="col-lg-5 my-auto order-1 order-lg-2">
          <h2 class="text-9">�C�mo es que lo Hacemos?</h2>
          <p class="text-3">Gracias a nuestro equipo de profesionales y nuestra trayectoria en el mercado, hemos generado alianzas durante a�os para poder llevarle a nuestros clientes la soluci�n r�pida, segura y m�s economica del mercado.</p>
          <a href="#" class="btn-link text-4">Conocer las Herramientas<i class="fas fa-chevron-right text-2 ml-2"></i></a> </div>
      </div>
    </div>
  </section>
  <!-- How work End -->

  <!-- Testimonial
    =============================================
  <section class="section">
    <div class="container">
      <h2 class="text-9 text-center">What people say about Payyed</h2>
      <p class="text-4 text-center mb-5">A payments experience people love to talk about</p>
      <div class="row">
        <div class="col-lg-6 mb-4">
          <div class="testimonial rounded text-center p-4">
            <p class="text-4">�Easy to use, reasonably priced simply dummy text of the printing and typesetting industry. Quidam lisque persius interesset his et, in quot quidam possim iriure.�</p>
            <strong class="d-block font-weight-500">Jay Shah</strong> <span class="text-muted">Founder at Icomatic Pvt Ltd</span> </div>
        </div>
        <div class="col-lg-6 mb-4">
          <div class="testimonial rounded text-center p-4">
            <p class="text-4">�I am happy Working with printing and typesetting industry. Quidam lisque persius interesset his et, in quot quidam persequeris essent possim iriure.�</p>
            <strong class="d-block font-weight-500">Patrick Cary</strong> <span class="text-muted">Freelancer from USA</span> </div>
        </div>
        <div class="col-lg-6 mb-4">
          <div class="testimonial rounded text-center p-4">
            <p class="text-4">�Only trying it out since a few days. But up to now excellent. Seems to work flawlessly. I'm only using it for sending money to friends at the moment.�</p>
            <strong class="d-block font-weight-500">Dennis Jacques</strong> <span class="text-muted">User from USA</span> </div>
        </div>
        <div class="col-lg-6 mb-4">
          <div class="testimonial rounded text-center p-4">
            <p class="text-4">�I have used them twice now. Good rates, very efficient service and it denies high street banks an undeserved windfall. Excellent.�</p>
            <strong class="d-block font-weight-500">Chris Tom</strong> <span class="text-muted">User from UK</span> </div>
        </div>
      </div>
      <div class="text-center mt-4"><a href="#" class="btn-link text-4">See more people review<i class="fas fa-chevron-right text-2 ml-2"></i></a></div>
    </div>
  </section>
  <!-- Testimonial end -->

  <!-- Frequently asked questions
    =============================================
  <section class="section bg-white">
    <div class="container">
      <h2 class="text-9 text-center">Frequently Asked Questions</h2>
      <p class="text-4 text-center mb-4 mb-sm-5">Can't find it here? Check out our <a href="help.html">Help center</a></p>
      <div class="row">
        <div class="col-md-10 col-lg-8 mx-auto">
          <hr class="mb-0">
          <div class="accordion accordion-alternate arrow-right" id="popularTopics">
              <div class="card">
                <div class="card-header" id="heading1">
                  <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse1" aria-expanded="false" aria-controls="collapse1">What is Payyed?</a> </h5>
                </div>
                <div id="collapse1" class="collapse" aria-labelledby="heading1" data-parent="#popularTopics">
                  <div class="card-body"> Lisque persius interesset his et, in quot quidam persequeris vim, ad mea essent possim iriure. Mutat tacimates id sit. Ridens mediocritatem ius an, eu nec magna imperdiet. </div>
                </div>
              </div>
              <div class="card">
                <div class="card-header" id="heading2">
                  <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">How to receive money online?</a> </h5>
                </div>
                <div id="collapse2" class="collapse" aria-labelledby="heading2" data-parent="#popularTopics">
                  <div class="card-body"> Iisque Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. </div>
                </div>
              </div>
              <div class="card">
                <div class="card-header" id="heading3">
                  <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">Is my money safe with Payyed?</a> </h5>
                </div>
                <div id="collapse3" class="collapse" aria-labelledby="heading3" data-parent="#popularTopics">
                  <div class="card-body"> Iisque persius interesset his et, in quot quidam persequeris vim, ad mea essent possim iriure. Mutat tacimates id sit. Ridens mediocritatem ius an, eu nec magna imperdiet. </div>
                </div>
              </div>
              <div class="card">
                <div class="card-header" id="heading4">
                  <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">How much fees does Payyed charge?</a> </h5>
                </div>
                <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#popularTopics">
                  <div class="card-body"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. </div>
                </div>
              </div>
              <div class="card">
                <div class="card-header" id="heading5">
                  <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">What is the fastest way to receive money from abroad?</a> </h5>
                </div>
                <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#popularTopics">
                  <div class="card-body"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. </div>
                </div>
              </div>
              <div class="card">
                <div class="card-header" id="heading6">
                  <h5 class="mb-0"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">Can I open an Payyed account for business?</a> </h5>
                </div>
                <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#popularTopics">
                  <div class="card-body"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. </div>
                </div>
              </div>
            </div>
          <hr class="mt-0">
        </div>
      </div>
      <div class="text-center mt-4"><a href="#" class="btn-link text-4">See more FAQ<i class="fas fa-chevron-right text-2 ml-2"></i></a></div>
    </div>
  </section>
  <!-- Frequently asked questions end -->

  <!-- Special Offer
    ============================================= -->
  <section class="hero-wrap py-5">
    <div class="hero-mask opacity-8 bg-dark"></div>
    <div class="hero-bg" style="background-image:url('images/bg/image-2.jpg');"></div>
    <div class="hero-content">
      <div class="container d-md-flex text-center text-md-left align-items-center justify-content-center">
        <h2 class="text-6 font-weight-400 text-white mb-3 mb-md-0">Contacte a nuestros asesores y solicite una demo completa de su proceso!</h2>
        <a href="contacto.php" class="btn btn-outline-light text-nowrap ml-4">Contactar Ahora</a> </div>
    </div>
  </section>
  <!-- Special Offer end -->
</div>
<!-- Content end -->


  <!-- Footer
  ============================================= -->
  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg d-lg-flex align-items-center">
          <ul class="nav justify-content-center justify-content-lg-start text-3">
            <li class="nav-item"> <a class="nav-link active" href="nosotros.php">Nosotros</a></li>
            <li class="nav-item"> <a class="nav-link" href="soporte.php">Soporte</a></li>
            <li class="nav-item"> <a class="nav-link" href="ayuda.php">Ayuda</a></li>
            <li class="nav-item"> <a class="nav-link" href="contacto.php">Contacto</a></li>
            <li class="nav-item"> <a class="nav-link" href="nosotros.php?#partners">Partners</a></li>
            <li class="nav-item"> <a class="nav-link" href="precios.php">Precios</a></li>
          </ul>
        </div>
        <div class="col-lg d-lg-flex justify-content-lg-end mt-3 mt-lg-0">
          <ul class="social-icons justify-content-center">
            <li class="social-icons-facebook"><a data-toggle="tooltip" href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
            <li class="social-icons-twitter"><a data-toggle="tooltip" href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
            <li class="social-icons-google"><a data-toggle="tooltip" href="http://www.google.com/" target="_blank" title="Google"><i class="fab fa-google"></i></a></li>
            <li class="social-icons-youtube"><a data-toggle="tooltip" href="http://www.youtube.com/" target="_blank" title="Youtube"><i class="fab fa-youtube"></i></a></li>
          </ul>
        </div>
      </div>
      <div class="footer-copyright pt-3 pt-lg-2 mt-2">
        <div class="row">
          <div class="col-lg">
            <p class="text-center text-lg-left mb-2 mb-lg-0">Copyright &copy; 2019 <a href="index.php">APIPagos</a>. All Rights Reserved.</p>
          </div>
          <div class="col-lg d-lg-flex align-items-center justify-content-lg-end">
            <ul class="nav justify-content-center">
              <li class="nav-item"> <a class="nav-link active" href="seguridad.php">Seguridad</a></li>
              <li class="nav-item"> <a class="nav-link" href="terminos.php">T�rminos y Condiciones</a></li>
              <li class="nav-item"> <a class="nav-link" href="privacidad.php">Privacidad</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- Footer end -->


</div>
<!-- Document Wrapper end -->

<!-- Back to Top
============================================= -->
<a id="back-to-top" data-toggle="tooltip" title="Back to Top" href="javascript:void(0)"><i class="fa fa-chevron-up"></i></a>

<!-- Video Modal
============================================= -->
<div class="modal fade" id="videoModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content bg-transparent border-0">
      <button type="button" class="close text-white opacity-10 ml-auto mr-n3 font-weight-400" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <div class="modal-body p-0">
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" id="video" allow="autoplay"></iframe>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Video Modal end -->

<!-- Script -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="vendor/bootstrap-select/js/bootstrap-select.min.js"></script>
<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="js/theme.js"></script>
</body>
</html>