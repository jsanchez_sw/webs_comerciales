﻿<!-- get_header('Page Name','Title')-->
<!doctype html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>CasillaPostal - El poder de las comunicaciones formales.</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<link href="https://fonts.googleapis.com/css?family=Karla:400,700,700i%7CRubik:300,400,500,700" rel="stylesheet">

		<link rel="icon" type="image/png" href="favicon.ico">
		<!-- Place favicon.ico in the root directory -->
		<link rel="apple-touch-icon" href="apple-touch-icon.png">
		<link rel="stylesheet" href="assets/css/font-awesome.min.css">

		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/css/animate.css">
		<link rel="stylesheet" href="assets/css/iconfont.css">
		<link rel="stylesheet" href="assets/css/magnific-popup.css">
		<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
		<link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
 <link rel="icon" type="image/png" href="Casilla.ico">		<link rel="stylesheet" href="assets/css/jquery-ui.structure.min.css">
		<link rel="stylesheet" href="assets/css/jquery-ui.theme.min.css">


		<!--For Plugins external css-->
		<link rel="stylesheet" href="assets/css/plugins.css" />

		<!--Theme custom css -->
		<link rel="stylesheet" href="assets/css/style.css">

		<!--Theme Responsive css-->
		<link rel="stylesheet" href="assets/css/responsive.css" />
	</head>
	<body>
	<!--[if lt IE 10]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->

	<!-- prelaoder -->
	<div id="preloader">
    <div class="preloader-wrapper">
        <div class="spinner"></div>
    </div>
    <div class="preloader-cancel-btn">
        <a href="#" class="btn btn-secondary prelaoder-btn">Cancel Preloader</a>
    </div>
</div>	<!-- END prelaoder -->
<div class="header-transparent">
    <!-- topBar section -->
    <div class="xs-top-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <ul class="xs-top-bar-info">
                    <li>
                        <p><i class="icon icon-phone3"></i>009-215-5596</p>
                    </li>
                    <li>
                        <a href="mailto:info@domain.com"><i class="icon icon-envelope4"></i>info@domain.com</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-6">
                <ul class="top-menu">
                                   <li><a href="Politica.asp"><i class="icon icon-license"></i>Políticas de Privacidad</a></li>
     <li><a href="login.asp"><i class="icon icon-key2"></i> Login</a></li>
                    <!-- <li><a href="signup.html">Sign Up</a></li> -->
                    <li><a href="soporte.asp"><i class="icon icon-hours-support"></i> Support</a></li>
                </ul>
            </div>
        </div><!-- .row END -->
    </div><!-- .container END -->
</div>    <!-- End topBar section -->

    <!-- header section -->
    <header class="xs-header">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <div class="xs-logo-wraper">
                    <a href="index.html" class="xs-logo">
                        <img src="assets/images/logo.png" alt="">
                    </a>
                </div>
            </div>
            <div class="col-lg-8">
                <nav class="xs-menus">
                    <div class="nav-header">
                        <a class="nav-brand" href="index.html">
                            <img src="assets/images/logo.png" alt="">
                        </a>
                        <div class="nav-toggle"></div>
                    </div>
                    <div class="nav-menus-wrapper">
						<!--#include file="menu.asp"-->
                    </div>
                </nav>
            </div>
            <div class="col-lg-2">
                <ul class="xs-menu-tools">
                    <li>
                        <a href="#modal-popup-1" class="languageSwitcher-button xs-modal-popup"><i class="icon icon-internet"></i></a>
                    </li>
                    <li>
                        <a href="#" class="offset-side-bar"><i class="icon icon-cart2"></i><span class="item-count">2</span></a>
                    </li>
                    <li>
                        <a href="#modal-popup-2" class="navsearch-button xs-modal-popup"><i class="icon icon-search"></i></a>
                    </li>
                    <li>
                        <a href="#" class="navSidebar-button"><i class="icon icon-burger-menu"></i></a>
                    </li>
                </ul>
            </div>
        </div><!-- .row END -->
    </div><!-- .container END -->
</header>    <!-- End header section -->
</div>

<!-- banner section -->
<section class="xs-banner inner-banner contet-to-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 align-self-center">
                <div class="xs-banner-content">
                    <h1 class="banner-sub-title wow fadeInLeft">Sysworld Servicios S.A.</h1>
                    <h2 class="banner-title wow fadeInLeft" data-wow-duration="1.5s">Nuestra Empresa</h2>
                </div><!-- .xs-banner-content END -->
            </div>
            <div class="col-lg-6 align-self-end">
                <div class="inner-welcome-image-group">
                    <img src="assets/images/innerWelcome/blog.png" alt="hosting image">
                    <img src="assets/images/innerWelcome/man.png" class="banner-ico banner-ico-1" alt="phone icon">
                    <img src="assets/images/innerWelcome/love.png" class="banner-ico banner-ico-2" alt="phone icon">
                </div>
            </div>
        </div><!-- .row END -->
    </div><!-- .container END -->
</section><!-- End banner section -->

<!-- blog post section -->
<section class="xs-section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-lg-8">
                <div class="blog-post-group">
                    <div class="post post-details">
                        <div class="post-media">
                            <img src="assets/images/blog-list/blog-list-image-1.jpg" alt="blog list image">
                        </div><!-- .post-media END -->
                        <div class="post-body">
                            <div class="entry-header">
                                <h2 class="entry-title">
                                    Nosotros:
                                </h2><!-- .entry-title END -->
                                <div class="entry-content">
                                    <p>
										SysWorld Servicios S.A - es una empresa Argentina dedicada a servicios de tecnología, comunicaciones y datacenter. Forma parte de un grupo de negocios conjuntamente con Worldsys S.A., una empresa dedicada al desarrollo de software de aplicación, implantación y servicios de consultoría relacionados con las áreas administrativas y contables de entidades financieras, industriales, comerciales y de servicios líder en el mercado nacional.<br><br>
										SysWorld Servicios S.A - cuenta con profesionales de amplia experiencia en el segmento de servicios informáticos, redes y telecomunicaciones. Alianzas estratégicas con proveedores de hardware y software líderes en el mundo le permiten a nuestra compañía poder dar un servicio de calidad garantizada.
                                    </p>
                                    <blockquote>
                                        Un aliado estratégico para su negocio. Desde nuestra empresa creemos en crear soluciones que le apliquen valor agregado a su negocio.
                                        <cite>Sebastián Piñeiro (CEO)</cite>
                                    </blockquote>
                                    <h4>Nuestros Valores</h4>
                                    <p>
                                    	Trabajamos día a día para ser una de las empresas líderes en la prestación de servicios profesionales de comunicaciones y certificaciones electrónicas de valor agregado gracias al compromiso y talento de nuestros profesionales. En cada proyecto, nos ponemos a prueba, entregamos lo mejor de nosotros para comprender los desafíos que enfrentan nuestros clientes para desarrollar soluciones que le agreguen valor a su negocio.<br><br>
                                    	Actuamos con integridad porque somos coherentes entre lo que pensamos, decimos y emprendemos dentro de los ámbitos personales y empresariales.
                                    </p>
                                    <ul class="xs-list check">
                                        <li>
	                                        <b>Creencias</b><br>
											Un equipo de profesionales altamente capacitados trabaja día a día creyendo que lo que hacemos le simplifica la vida a cientos de miles de personas y cuida el medioambiente reduciendo la huella de carbono.
                                        </li>

                                        <li>
	                                        <b>¿Con Qué?</b><br>
											Simplemente desarrollamos software como servicios para gestión, securización y automatización de procesos de comunicaciones entre una organización y sus clientes.
                                        </li>

                                        <li>
	                                        <b>¿Porqué?</b><br>
											Innovación: En todo lo que hacemos estamos pensando 4 años más adelante con el fin de anticiparnos a los cambios y poder acompañar a nuestros clientes en los cambios culturales.
                                        </li>

                                    </ul>
                                </div><!-- .entry-content END -->
                            </div><!-- .entry-header END -->
                        </div><!-- .post-body END -->
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-lg-4">
                <!-- sidebar -->
                <div class="sidebar-widget sidebar-right">
    <div class="widget widget-search">
        <form action="#" class="xs-serach" method="post">
            <div class="input-group">
                <input type="search" name="search" placeholder="Buscar">
                <button class="search-btn"><i class="icon icon-search"></i></button>
            </div>
        </form>
    </div><!-- .widget .widget-search END -->
    <div class="widget widget-categories">
        <h4 class="xs-title">Accesos</h4>
        <ul class="list-group">
            <li>
                <a href="http://www.sysworld.com.ar/v2/sw/empresa.asp?f=network">Network</a>
            </li>
            <li>
                <a href="http://www.sysworld.com.ar/v2/sw/empresa.asp?f=politicas">Politicas y Certificaciones</a>
            </li>
            <li>
                <a href="http://www.sysworld.com.ar/v2/sw/empresa_clientes.asp">Clientes</a>
            </li>
            <li>
                <a href="http://www.sysworld.com.ar/v2/sw/empresa_dist.asp">Distribuidores y Agentes</a>
            </li>
            <li>
                <a href="http://www.sysworld.com.ar/v2/sw/empresa.asp?f=partners">Partners</a>
            </li>
            <li>
                <a href="http://www.sysworld.com.ar/">Productos</a>
            </li>
        </ul>
    </div><!-- .widget .widget-categories END -->
    <div class="widget widget-tag">
        <h4 class="xs-title">Casos de Éxito</h4>
        <div class="tag-lists">
            <a target="_blank" href="http://www.sysworld.com.ar/v2/sw/caso_validaciones.asp">Validadores y Normalizadores</a>
            <a target="_blank" href="http://www.sysworld.com.ar/v2/sw/caso_suscriba.asp">Suscripciones</a>
            <a target="_blank" href="http://www.sysworld.com.ar/v2/sw/caso_enriquezca.asp">Enriquecimientos</a>
            <a target="_blank" href="http://www.sysworld.com.ar/v2/sw/caso_alertas.asp">Alertas Preventivas</a>
            <a target="_blank" href="http://www.sysworld.com.ar/v2/sw/caso_portalweb.asp">Portal Web</a>
            <a target="_blank" href="http://www.sysworld.com.ar/v2/sw/caso_ern.asp">Envíos Certificados</a>
            <a target="_blank" href="http://www.sysworld.com.ar/v2/sw/caso_sms.asp">Envíos SMS</a>
            <a target="_blank" href="http://www.sysworld.com.ar/v2/sw/caso_whatsapp.asp">Envíos WhatsApp</a>
            <a target="_blank" href="http://www.sysworld.com.ar/v2/sw/caso_sociales.asp">Envíos Redes Sociales</a>
            <a target="_blank" href="http://www.sysworld.com.ar/v2/sw/caso_documentos.asp">Gestor de Documentos</a>
            <a target="_blank" href="http://www.sysworld.com.ar/v2/sw/caso_multichannel.asp">Multichannel</a>
        </div>
    </div><!-- .widget .widget-categories END -->
    <div class="widget widget-twitter">
        <h4 class="xs-title">Twitter</h4>
        <div class="xs-tweet"></div>
    </div><!-- .widget .widget-twitter END -->
</div>                <!-- END sidebar -->
            </div>
        </div><!-- .row END -->
    </div><!-- .container END -->
</section><!-- END blog post section -->

<!-- language switcher strart -->
<!-- xs modal -->
<div class="zoom-anim-dialog mfp-hide modal-language" id="modal-popup-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="language-content">
                <p>Switch The Language</p>
                <ul class="flag-lists">
                    <li><a href="#"><img src="assets/images/flags/006-united-states.svg" alt=""><span>English</span></a></li>
                    <li><a href="#"><img src="assets/images/flags/002-canada.svg" alt=""><span>English</span></a></li>
                    <li><a href="#"><img src="assets/images/flags/003-vietnam.svg" alt=""><span>Vietnamese</span></a></li>
                    <li><a href="#"><img src="assets/images/flags/004-france.svg" alt=""><span>French</span></a></li>
                    <li><a href="#"><img src="assets/images/flags/005-germany.svg" alt=""><span>German</span></a></li>
                </ul>
            </div>
        </div>
    </div>
</div><!-- End xs modal --><!-- end language switcher strart -->

<!-- language switcher strart -->
<!-- xs modal -->
<div class="zoom-anim-dialog mfp-hide modal-searchPanel" id="modal-popup-2">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="xs-search-panel">
                <form action="#" method="POST" class="xs-search-group">
                    <input type="search" class="form-control" name="search" id="search" placeholder="Buscar">
                    <button type="submit" class="search-button"><i class="icon icon-search"></i></button>
                </form>
            </div>
        </div>
    </div>
</div><!-- End xs modal --><!-- end language switcher strart -->

<!-- offset cart strart -->
<!-- sidebar cart item -->
<div class="xs-sidebar-group cart-group">
    <div class="xs-overlay xs-bg-black"></div>
    <div class="xs-sidebar-widget">
        <div class="sidebar-widget-container">
            <div class="widget-heading media">
                <div class="media-body">
                    <a href="#" class="close-side-widget">
                        X
                    </a>
                </div>
            </div>
            <div class="xs-empty-content">
                <h3 class="widget-title">Shopping cart</h3>
                <h4 class="xs-title">No products in the cart.</h4>
                <p class="empty-cart-icon">
                    <i class="icon icon-shopping-cart"></i>
                </p>
                <p class="xs-btn-wraper">
                    <a class="btn btn-primary" href="#">Return To Shop</a>
                </p>
            </div>
        </div>
    </div>
</div>    <!-- END sidebar cart item -->    <!-- END offset cart strart -->

<!-- offset cart strart -->
<!-- sidebar cart item -->
<div class="xs-sidebar-group info-group">
    <div class="xs-overlay xs-bg-black"></div>
    <div class="xs-sidebar-widget">
        <div class="sidebar-widget-container">
            <div class="widget-heading">
                <a href="#" class="close-side-widget">
                    X
                </a>
            </div>
            <div class="sidebar-textwidget">
                <div class="sidebar-logo-wraper">
                    <img src="assets/images/blue_logo.png" alt="sidebar logo">
                </div>
                <p>Far far away, behind the word moun tains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of  </p>
                <ul class="sideabr-list-widget">
                    <li>
                        <div class="media">
                            <div class="d-flex">
                                <img src="assets/images/location.png" alt="">
                            </div>
                            <div class="media-body">
                                <p>759 Pinewood Avenue</p>
                                <span>Marquette, Michigan</span>
                            </div>
                        </div><!-- address 1 -->
                    </li>
                    <li>
                        <div class="media">
                            <div class="d-flex">
                                <img src="assets/images/mail.png" alt="">
                            </div>
                            <div class="media-body">
                                <a href="mailto:info@domain.com">info@domain.com</a>
                                <span>Online Support</span>
                            </div>
                        </div><!-- address 1 -->
                    </li>
                    <li>
                        <div class="media">
                            <div class="d-flex">
                                <img src="assets/images/phone.png" alt="">
                            </div>
                            <div class="media-body">
                                <p>906-624-2565</p>
                                <span>Mon-Fri 8am-5pm</span>
                            </div>
                        </div><!-- address 1 -->
                    </li>
                </ul><!-- .sideabr-list-widget -->
                <div class="subscribe-from">
                    <p>Get Subscribed!</p>
                    <form action="#" method="POST" id="subscribe-form" class="subscribe-form">
                        <label for="sub-input"></label>
                        <div class="form-group">
                            <input type="email" name="email" id="sub-input" placeholder="Enter your mail here" class="form-control">
                            <button class="sub-btn" type="submit"><i class="icon icon-arrow-right"></i></button>
                        </div>
                    </form>
                </div>
                <ul class="social-list version-2">
                    <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#" class="f"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#" class="vimeo"><i class="fa fa-vimeo"></i></a></li>
                </ul><!-- .social-list -->
                <div class="text-center">
                    <a href="#" class="btn btn-primary">Purchase Now</a>
                </div>
            </div>
        </div>
    </div>
</div>    <!-- END sidebar widget item -->    <!-- END offset cart strart -->

		<!-- footer section start -->
		<footer class="xs-footer-section">
			<div class="footer-group" style="background-image: url(assets/images/footer-bg.png);">
				<div class="footer-main">
					<div class="container">
						<div class="row">
							<div class="col-lg-3 col-md-6">
								<div class="footer-widget wow fadeInUp">
									<h3 class="widget-title">CasillaPostal</h3>
									<ul class="xs-list">
										<li><a href="index.asp">Inicio</a></li>
										<li><a href="precios.asp">Precios</a></li>
										<li><a href="contacto.asp">Contacto</a></li>
										<li><a href="login.asp">Login</a></li>
										<li><a href="api.asp">Api Desarrolladores</a></li>
										<li><a href="Politica.asp" target="_blank">Legales</a></li>
									</ul><!-- .xs-list END -->
								</div><!-- .footer-widget END -->
							</div>
							<div class="col-lg-3 col-md-6">
								<div class="footer-widget wow fadeInUp" data-wow-duration="1s">
									<h3 class="widget-title">Empresa</h3>
									<ul class="xs-list">
										<li><a href="empresa.asp">Nosotros</a></li>
										<li><a href="http://www.sysworld.com.ar" target="_blank">Sitio Corporativo</a></li>
										<li><a href="contacto.asp">Contactenos</a></li>
										<li><a href="http://www.sysworld.com.ar/v2/sw/empresa_dist.asp" target="_blank">Distribuidores</a></li>
										<li><a href="http://www.sysworld.com.ar/" target="_blank">Politica de Privacidad</a></li>
										<li><a href="http://www.sysworld.com.ar/" target="_blank">Datos Personales</a></li>
									</ul><!-- .xs-list END -->
								</div><!-- .footer-widget END -->
							</div>
							<div class="col-lg-3 col-md-6">
								<div class="footer-widget wow fadeInUp" data-wow-duration="1.3s">
									<h3 class="widget-title">Soporte</h3>
									<ul class="xs-list">
										<li><a href="who-is.html">Tutoriales</a></li>
										<li><a href="http://cloud.sysworld.com.ar/" target="_blank">Cargar Ticket</a></li>
										<li><a href="http://estado.sysworld.com.ar/" target="_blank">Estado de Servicios</a></li>
										<li><a href="faq.asp">FAQ</a></li>
									</ul><!-- .xs-list END -->
								</div><!-- .footer-widget END -->
							</div>
							<div class="col-lg-3 col-md-6">
								<div class="footer-widget wow fadeInUp" data-wow-duration="1.5s">
									<h3 class="widget-title">Contacto</h3>
									<ul class="contact-info-widget">
										<li class="media">
											<img src="assets/images/address-pin.png" class="d-flex" alt="contact icon">
											<span class="media-body">Bucarelli 2480, CABA, Argentina</span>
										</li><!-- .media END -->
										<li class="media">
											<img src="assets/images/phone-pin.png" class="d-flex" alt="contact icon">
											<span class="media-body">+54 (011) 5269-2919</span>
										</li><!-- .media END -->
										<li class="media">
											<img src="assets/images/email-icon.png" class="d-flex" alt="contact icon">
											<span class="media-body">info@sysworld.com.ar</span>
										</li><!-- .media END -->
									</ul><!-- .contact-info-widget -->
								</div><!-- .footer-widget END -->
							</div>
						</div><!-- .row END -->
					</div><!-- .container END -->
				</div><!-- .footer-main END -->
				<div class="container">
					<div class="footer-bottom">
						<div class="row">
							<div class="col-md-6">
								<div class="footer-bottom-info wow fadeInUp">
									<p>Un aliado estratégico para su negocio. Desde nuestra empresa creemos en crear soluciones que le apliquen valor a su negocio. <a href="http://www.sysworld.com.ar">Sysworld Servicios S.A.</a></p>
								</div>
							</div>
							<div class="col-md-6">
								<ul class="xs-list list-inline wow fadeInUp" data-wow-duration="1s">
									<li><img src="assets/images/security/security-company-images-1.png" alt="security company images"></li>
									<li><img src="assets/images/security/security-company-images-2.png" alt="security company images"></li>
									<li><img src="assets/images/security/security-company-images-3.png" alt="security company images"></li>
									<li><img src="assets/images/security/security-company-images-4.png" alt="security company images"></li>
								</ul>
							</div>
						</div><!-- .row END -->
					</div><!-- .footer-bottom end -->
				</div><!-- .container end -->
			</div><!-- .footer-group END -->
			<div class="footer-copyright">
				<div class="container">
					<div class="row">
						<div class="col-md-4">
							<div class="copyright-text wow fadeInUp">
								<p>&copy; 2018 - <%=year(date)%> Powered by <a href="http://www.sysworld.com.ar">Sysworld Servicios S.A. </a></p>
							</div><!-- .copyright-text END -->
						</div>
						<div class="col-md-4">
							<div class="footer-logo-wraper wow fadeInUp" data-wow-duration="1s">
								<a href="index.asp" class="footer-logo"><img src="assets/images/logo.png" alt="footer logo" width="180" height="45"></a>
							</div><!-- .footer-logo-wraper END -->
						</div>
						<div class="col-md-4">
							<div class="social-list-wraper wow fadeInUp" data-wow-duration="1.3s">
								<ul class="social-list">
									<li><a href="https://www.facebook.com/SysWorld-Servicios-SA-323344231089747" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a></li>
									<li><a href="https://twitter.com/Sysworldsa" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a></li>
								</ul>
							</div><!-- .social-list-wraper END -->
						</div>
					</div><!-- .row END -->
				</div><!-- .container END -->
			</div><!-- .footer-copyright END -->
		</footer>
		<!-- footer section end -->
		<!-- js file start -->
		<script src="assets/js/jquery-3.2.1.min.js"></script>
		<script src="assets/js/jquery-ui.min.js"></script>
		<script src="assets/js/plugins.js"></script>
		<script src="assets/js/Popper.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/jquery.magnific-popup.min.js"></script>
		<script src="assets/js/owl.carousel.min.js"></script>
		<script src="assets/js/tweetie.js"></script>
		<script src="assets/js/jquery.parallax-scroll.js"></script>
		<script src="assets/js/jquery.ajaxchimp.min.js"></script>
		<script src="assets/js/jquery.parallax.js"></script>
		<script src="assets/js/hostslide.js"></script>
		<script src="assets/js/vps-slider.js"></script>
		<script src="assets/js/vps-slider-settings.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyCy7becgYuLwns3uumNm6WdBYkBpLfy44k"></script>
		<script src="assets/js/main.js"></script>		<!-- End js file -->
	</body>
</html>