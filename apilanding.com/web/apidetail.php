<?php
    include 'functions/db.php';

    include_once 'functions/language.php';


	$sql = "SELECT * from web_apis A left join web_apis_desc B ON A.id = B.idapi WHERE A.id = " . $_GET['id'];
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$id = $_GET['id'];
			$nombre = $row["nombre"];
			$descripcion = $row["descripcion"];
			$proveedor = $row["proveedor"];
			$contexto = $row["contexto"];
			$linkapicenter = $row["linkapicenter"];
			$linkdoc = $row["linkdoc"];
			$imagen1 = $row["imagen1"];
			$imagen2 = $row["imagen2"];
			$tiposuscripcion = $row["tiposuscripcion"];
			$idestado = $row["idestado"];
			$tipoapi = $row["tipoapi"];
			$categorias = str_replace(';', ', ', $row["categorias"]);
			$industrias = str_replace(';', ', ', $row["industrias"]);
			$fechavigencia = $row["fechavigencia"];
			$auditor = $row["auditor"];
			$fechabaja = $row["fechabaja"];
			$idioma = $row["idioma"];


			if ($idestado == "0"){
				$estado = "Desarrollo";
			}
			if ($idestado == "1"){
				$estado = "Activa";
			}
			if ($idestado == "2"){
				$estado = "Pausada";
			}
			if ($idestado == "3"){
				$estado = "Discontinua";
			}
			if ($row["restriccion"] == "0"){
				$restriccion = '';
			}
			if ($row["restriccion"] == "1"){
				$restriccion = 'Restringida';
			}
			if ($row["restriccion"] == "2"){
				$restriccion = 'Restringida';
			}


		}
	}else{
		header('Location: 404.php');
		die();
	}






	$sql = "SELECT * from web_apis_desc WHERE idapi = " . $_GET['id'] . " AND idioma = 'es'";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$descripcioncomercial = $row["descripcioncomercial"];
			$descripciontecnica = $row["descripciontecnica"];
			$casoexito = $row["casoexito"];
		}
	}



	function obtenerprecioapi($idapitemp, $conntemp){
		$precio = '<a href="contact.php">Info</a>';

		$sql = "SELECT precio from web_apis_precios WHERE idapi = " . $idapitemp . " AND tipocontador = 1 AND contador = 1 and moneda = 'usd'";
		$result = $conntemp->query($sql);
		if ($result->num_rows > 0){
			while($row = $result->fetch_assoc()){
				$precio = $row['precio'];
			}
		}

		return $precio;

	}







?>


<!DOCTYPE html>
<html lang="<?php echo $lang['SITE_lang'];?>">

<!-- Mirrored from protechtheme.com/saas/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:17:32 GMT -->
<head>
        <!-- Required meta tags -->
        <meta charset="iso-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="keywords" content="<?php echo $lang['SITE_keywords'];?>">
		<meta name="description" content="<?php echo $lang['SITE_desc'];?>">
		<meta name="author" content="Sysworld Servicios S.A.">
        <title><?php echo $lang['SITE_title'];?></title>
        <!-- Bootstrap core CSS -->
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Select2 -->
        <link href="assets/select2/css/select2.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- Iconmoon -->
        <link href="assets/iconmoon/css/iconmoon.css" rel="stylesheet">
        <!-- Owl Carousel -->
        <link href="assets/owl-carousel/css/owl.carousel.min.css" rel="stylesheet">
        <!-- jQuery UI -->
        <link href="assets/jquery-ui/css/jquery-ui.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="css/custom.css" rel="stylesheet">
        <link rel="icon" type="image/x-icon" href="images/favicon.ico" />
    </head>

    <body>

<!-- ==============================================
**Preloader**
=================================================== -->
        <div id="loader">
            <div id="element">
                <div class="circ-one"></div>
                <div class="circ-two"></div>
            </div>
        </div>

<!-- ==============================================
**Header**
=================================================== -->
        <header>
			<?php include 'menu.php';?>
        </header>

<!-- ==============================================
**Inner Banner**
=================================================== -->
        <section class="inner-banner">
            <div class="container">
                <div class="contents">
                    <h1>Detalle de la API</h1>
                    <p>A continuaci�n podra consultar la informaci�n, comercial, t�cnica y funcional de la API seleccionada.</span></p>
                </div>
            </div>
        </section>

<!-- ==============================================
**Product Listing**
=================================================== -->
        <section class="shop-single padding-lg">
            <div class="container">
                <div class="row product-outer">
                    <div class="col-md-7">
                        <div class="product-gallery d-flex">
                            <div class="left">
                                <div class="product-carousel">
                                    <figure><img src="http://webmaster.apilanding.com/scriptcase/file/img/<?php echo $imagen2;?>" class="img-fluid" alt=""></figure>
                                </div>
                            </div>
                            <div class="thub-outer">
                                <div id="bx-pager">
                                    <a href="#" data-slide-index="0">
                                        <figure><img src="http://webmaster.apilanding.com/scriptcase/file/img/<?php echo $imagen2;?>" class="img-fluid" alt=""></figure>
                                    </a>
                                    <a href="#" data-slide-index="1">
                                        <figure><img src="http://webmaster.apilanding.com/scriptcase/file/img/<?php echo $imagen1;?>" class="img-fluid" alt=""></figure>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 product-detail">
                        <h2><b><?php echo $nombre;?></b></h2>
                        <ul class="rating">
                            <li class="color"><i class="fa fa-star" aria-hidden="true"></i></li>
                            <li class="color"><i class="fa fa-star" aria-hidden="true"></i></li>
                            <li class="color"><i class="fa fa-star" aria-hidden="true"></i></li>
                            <li class="color"><i class="fa fa-star-half" aria-hidden="true"></i></li>
                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                        </ul>
                        <p><?php echo $descripcion;?></p>
                        <p><b>Proveedor: </b><?php echo $proveedor;?></p>
                        <p><b>Fecha: </b><?php echo $fechavigencia;?></p>
                        <div class="row select-spec">
                            <div class="col-6">
                                <label><b>Categorias</b></label><br>
                                <?php echo $categorias;?>
                            </div>
                            <div class="col-6">
                                <label><b>Industrias</b></label><br>
                                <?php echo $industrias;?>
                            </div>
                        </div>
                        <div class="cart-outer align-items-center">
                            <div class="price"><sup>$</sup>
							<?php if ($tiposuscripcion == "1"){?>
								Incluida.
							<?php }?>
							<?php
								if ($tiposuscripcion == "2"){
									echo "<small>" . obtenerprecioapi($id, $conn) . "</small>";
								}
							?>

                            </div>
                            <a href="apiswagger.php?idapi=<?php echo $id;?>" target="_blank"><button class="btn add-cart">API-Test</button></a>
                            &nbsp;&nbsp;&nbsp;
                            <a href="apidoc/index.php?idapi=<?php echo $id;?>" target="_blank"><button class="btn add-cart">API-Doc</button></a>
                            &nbsp;&nbsp;&nbsp;
                            <a href="panel/apislist.php?t=cart"><button class="btn add-cart">API-Center</button></a>
                        </div>
                        <div class="sku-outer">
                            <ul>
                                <li><strong>ID : </strong> <?php echo md5($contexto);?></li>
                                <li><strong>Contexto :</strong> <?php echo $contexto;?></li>
                                <li>
                                	<strong>Estado :</strong> <span class="label"> <?php echo $estado;?></span>
                                	<?php if (strlen($restriccion) > 0){ ?>
                                		<span class="label"> <?php echo $restriccion;?></span>
                                	<?php }?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="product-info-tab">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item"> <a class="nav-link active" id="desc-tab" data-toggle="tab" href="#desc" role="tab" aria-controls="desc" aria-selected="true">Descripcion</a> </li>
                        <li class="nav-item"> <a class="nav-link" id="info-tab" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="false">Informaci�n Tecnica</a> </li>
                        <li class="nav-item"> <a class="nav-link" id="reviews-tab" data-toggle="tab" href="#reviews" role="tab" aria-controls="reviews" aria-selected="false">Caso de Exito</a> </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="desc" role="tabpanel" aria-labelledby="desc-tab">
                            <p><?php echo $descripcioncomercial;?></p>
                        </div>
                        <div class="tab-pane fade" id="info" role="tabpanel" aria-labelledby="info-tab">
							<p><?php echo $descripciontecnica;?></p>
							<p>Se encuentra a disposici�n para consultar la documentaci�n t�cnica, revisar los parametros de entrada/salida y evaluar las respuestas de la api accediendo a nuestro <a href="apidoc/index.php?idapi=<?php echo $id;?>">API-Doc</a>. As� mismo si se <a href="register.php">registra gratuitamente</a> en muestro
							sistema, puede acceder a generar sus Token de Prueba e ingresar a nuestro portal <a href="apiswagger.php?idapi=<?php echo $id;?>">API-Test</a>
							</p>
							<hr><br>
							<h3>Definici�n T�cnica de la API:<br></h3>
							<iframe width="100%" height="2000" src="apiswagger.php?idapi=<?php echo $id;?>" frameborder="0" allowfullscreen=""></iframe>
                        </div>
                        <div class="tab-pane fade" id="reviews" role="tabpanel" aria-labelledby="reviews-tab">
                            <p><?php echo $casoexito;?></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

     <?php include 'footer.php';?>


        <!-- Scroll to top -->
        <a href="#" class="scroll-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>

        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery.min.js"></script>
        <!-- Popper JS -->
        <script src="js/popper.min.js"></script>
        <!-- Bootsrap JS -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- Select2 JS -->
        <script src="assets/select2/js/select2.min.js"></script>
        <!-- Bxslider JS -->
        <script src="assets/bxslider/js/bxslider.min.js"></script>
        <!-- Owl Carousal JS -->
        <script src="assets/owl-carousel/js/owl.carousel.min.js"></script>
        <!-- Custom JS -->
        <script src="js/custom.js"></script>
    </body>


<!-- Mirrored from protechtheme.com/saas/shop-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:18:25 GMT -->
</html>
