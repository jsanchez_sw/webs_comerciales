<?php
    include 'functions/db.php';

    include_once 'functions/language.php';

	function obtenerprecioapi($idapitemp, $conntemp){
		$precio = '<a href="contact.php">Info</a>';

		$sql = "SELECT precio from web_apis_precios WHERE idapi = " . $idapitemp . " AND tipocontador = 1 AND contador = 1 and moneda = 'usd'";
		$result = $conntemp->query($sql);
		if ($result->num_rows > 0){
			while($row = $result->fetch_assoc()){
				$precio = $row['precio'];
			}
		}

		return $precio;

	}


?>

<!DOCTYPE html>
<html lang="<?php echo $lang['SITE_lang'];?>">

<!-- Mirrored from protechtheme.com/saas/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:17:32 GMT -->
<head>
        <!-- Required meta tags -->
        <meta charset="iso-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="keywords" content="<?php echo $lang['SITE_keywords'];?>">
		<meta name="description" content="<?php echo $lang['SITE_desc'];?>">
		<meta name="author" content="Sysworld Servicios S.A.">
        <title><?php echo $lang['SITE_title'];?></title>

    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="assets/select2/css/select2.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Iconmoon -->
    <link href="assets/iconmoon/css/iconmoon.css" rel="stylesheet">
    <!-- jQuery UI -->
    <link href="assets/jquery-ui/css/jquery-ui.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/custom.css" rel="stylesheet">
    <link rel="icon" type="image/x-icon" href="images/favicon.ico" />
</head>

<body>

<!-- ==============================================
**Preloader**
=================================================== -->
    <div id="loader">
        <div id="element">
            <div class="circ-one"></div>
            <div class="circ-two"></div>
        </div>
    </div>

<!-- ==============================================
**Header**
=================================================== -->
    <header>
			<?php include 'menu.php';?>
    </header>

<!-- ==============================================
**Inner Banner**
=================================================== -->
    <section class="inner-banner">
        <div class="container">
            <div class="contents">
                <h1><?php echo $lang['APIS_header_title'];?></h1>
                <p><?php echo $lang['APIS_header_desc'];?></p>
            </div>
        </div>
    </section>

<!-- ==============================================
**Product Listing**
=================================================== -->
    <section class="shop-grid padding-lg">
        <div class="container">
            <div class="row">
                <!-- Start Sidebar -->
                <div class="col-md-4 col-lg-3 shop-sidebar">
                    <form action="apis.php" method="get" class="search-outer d-flex">
                        <input name="s" placeholder="<?php echo $lang['APIS_search_title'];?>" type="text">
                        <button class="go-btn"><span class="icon-search"></span></button>
                    </form>
					<?php if (isset($_GET['s']) or isset($_GET['f']) or isset($_GET['c']) or isset($_GET['i'])){ ?>
						<center><b><a href="apis.php">Quitar Filtros</a></b><br></center><hr><br><br><br>
					<?php }?>
                    <div class="product-categories">
                        <h3><?php echo $lang['APIS_search_category_title'];?></h3>
                        <ul>
							<li><a href="apis.php?c=fintech"><?php echo $lang['MENU_APIS_FintechFinance'];?></a></li>
							<li><a href="apis.php?c=identity"><?php echo $lang['MENU_APIS_Identity'];?></a></li>
							<li><a href="apis.php?c=notifications"><?php echo $lang['MENU_APIS_EmailNotification'];?></a></li>
							<li><a href="apis.php?c=security"><?php echo $lang['MENU_APIS_Security'];?></a></li>
							<li><a href="apis.php?c=developers"><?php echo $lang['MENU_APIS_DeveloperTools'];?></a></li>
							<li><a href="apis.php?c=locations"><?php echo $lang['MENU_APIS_Localisation'];?></a></li>
							<li><a href="apis.php?c=productivity"><?php echo $lang['MENU_APIS_Productivity'];?></a></li>
							<li><a href="apis.php?c=socialnetworks"><?php echo $lang['MENU_APIS_SocialNetworks'];?></a></li>
							<li><a href="apis.php?c=utilitys"><?php echo $lang['MENU_APIS_Utilities'];?></a></li>
                        </ul>
                    </div>
                    <div class="product-categories archieves">
                        <h3><?php echo $lang['APIS_search_category_industries'];?></h3>
                        <ul>
                            <li><a href="apis.php?i=pymes"><?php echo $lang['APIS_search_category_industries_pymes'];?></a></li>
                            <li><a href="apis.php?i=coorp"><?php echo $lang['APIS_search_category_industries_coorp'];?></a></li>
                            <li><a href="apis.php?i=automotive"><?php echo $lang['APIS_search_category_industries_automotive'];?></a></li>
							<li><a href="apis.php?i=fintech"><?php echo $lang['APIS_search_category_industries_finance'];?></a></li>
							<li><a href="apis.php?i=insurance"><?php echo $lang['APIS_search_category_insurance'];?></a></li>
							<li><a href="apis.php?i=retail"><?php echo $lang['APIS_search_category_retail'];?></a></li>
							<li><a href="apis.php?i=tech"><?php echo $lang['APIS_search_category_tech'];?></a></li>
							<li><a href="apis.php?i=gov"><?php echo $lang['APIS_search_category_government'];?></a></li>
                        </ul>
                    </div>
                </div>
                <!-- End Sidebar -->

                <!-- Start Product Listing -->
                <div class="col-md-8 col-lg-9">

                    <ul class="row Product-listing right-sec">

						<?php
						$sql = "SELECT A.id, A.nombre, A.descripcion, A.proveedor, A.contexto, A.imagen1, A.tiposuscripcion from web_apis A left join web_apis_desc B ON A.id = B.idapi WHERE A.idestado = 1 AND A.tiposuscripcion > 0 ";

						if (isset($_GET['s'])){
							$sql = $sql . " AND (A.nombre like '%" . $_GET['s'] . "%' OR A.descripcion like '%" . $_GET['s'] . "%')";
						}
						if (isset($_GET['f'])){
							if ($_GET['f'] == "susc"){
								$sql = $sql . " AND (A.tiposuscripcion = 1)";
							}
						}
						if (isset($_GET['c'])){
							$sql = $sql . " AND (A.categorias like '%" . $_GET['c'] . "%')";
						}
						if (isset($_GET['i'])){
							$sql = $sql . " AND (A.industrias like '%" . $_GET['i'] . "%')";
						}

						$result = $conn->query($sql);
						if ($result->num_rows > 0) {
							while($row = $result->fetch_assoc()) {

							if (strlen($row["contexto"]) > 36){
								$contexto = substr($row["contexto"], 0,36) . '...';
							}else{
								$contexto = $row["contexto"];
							}
							if (strlen($row["descripcion"]) < 30){
								$desc = $row["descripcion"] . "<br><br>";
							}else{
								$desc = $row["descripcion"];
							}




						?>

								<li class="col-6 col-lg-4">
									<div class="inner">
										<div class="product-block">
											<a href="apidetail.php?id=<?php echo $row["id"];?>"><figure><img src="http://webmaster.apilanding.com/scriptcase/file/img/<?=$row["imagen1"];?>" class="img-fluid" alt=""></figure></a>
										</div>
										<div class="product-detail">
											<a href="apidetail.php?id=<?php echo $row["id"];?>">
												<b><?=$row["nombre"];?></b>
												<p><?=$desc;?></p>
											</a>
											<hr>
											<p><small><?=$contexto;?></small></p>
											<br>
											<ul class="rating">
												<li class="color"><i class="fa fa-star" aria-hidden="true"></i></li>
												<li class="color"><i class="fa fa-star" aria-hidden="true"></i></li>
												<li class="color"><i class="fa fa-star" aria-hidden="true"></i></li>
												<li><i class="fa fa-star" aria-hidden="true"></i></li>
												<li><i class="fa fa-star" aria-hidden="true"></i></li>
											</ul>
											<div class="bottom">
												<div class="price"><sup>$</sup>
													<?php if ($row["tiposuscripcion"] == "1"){?>
														Inc.
													<?php }?>
													<?php
														if ($row["tiposuscripcion"] == "2"){
															echo "<small>" . obtenerprecioapi($row['id'], $conn) . "</small>";
														}
													?>



												</div>
												<a href="apidetail.php?id=<?php echo $row["id"];?>"><button class="btn add-cart"><?php echo $lang['APIS_list_link_button'];?></button></a>
											</div>

											<p><u><a href="provider.php?cod=<?=$row["proveedor"];?>"><?=$row["proveedor"];?></a></u></p>
										</div>
									</div>
								</li>

						<?php
							}
						}
						?>


                    </ul>

                </div>
                <!-- End Product Listing -->
            </div>
        </div>
    </section>

	<?php include 'footer.php';?>


    <!-- Scroll to top -->
    <a href="#" class="scroll-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="js/popper.min.js"></script>
    <!-- Bootsrap JS -->
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- Select2 JS -->
    <script src="assets/select2/js/select2.min.js"></script>
    <!-- jQuery UI JS -->
    <script src="assets/jquery-ui/js/jquery-ui.min.js"></script>
    <!-- Custom JS -->
    <script src="js/custom.js"></script>
</body>


<!-- Mirrored from protechtheme.com/saas/shop-grid-sidebar.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:18:22 GMT -->
</html>
