<?php
    include '../functions/db.php';
    include_once '../functions/language.php';
	include 'session.php';

	$usuario = $_SESSION['apilanding_user_email'];
	$idapi = $_GET['id'];
	$codigo = $_GET['codigo'];

    $sql = "SELECT * FROM web_apis "
	. " WHERE id = " . $idapi . " AND codigo = '" . $codigo . "' ";

	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$nombre = $row['nombre'];
			$tipoapi = $row['tipoapi'];
			$version = $row['version'];
			$idestado = $row['idestado'];
			$descripcion = $row['descripcion'];
			$codigo = $row['codigo'];

			if ($row['idestado'] == "0"){
				$idestado0 = 'selected';
			}
			if ($row['idestado'] == "1"){
				$idestado1 = 'selected';
			}
			if ($row['idestado'] == "2"){
				$idestado2 = 'selected';
			}
			if ($row['idestado'] == "3"){
				$idestado3 = 'selected';
			}

			$apidataseparador = $row['apidataseparador'];
			if ($row['apidataseparador'] == ","){
				$separadorcoma = 'selected';
			}
			if ($row['apidataseparador'] == ";"){
				$separadorpuntoycoma = 'selected';
			}

			$apidataomitirencabezado = $row['apidataomitirencabezado'];
			if ($row['apidataomitirencabezado'] == "1"){
				$encabezado1 = 'selected';
			}
			if ($row['apidataomitirencabezado'] == "2"){
				$encabezado2 = 'selected';
			}
			if ($row['apidataomitirencabezado'] == "3"){
				$encabezado3 = 'selected';
			}


			if ($tipoapi == "1"){
				$type = "file";
			}
			if ($tipoapi == "2"){
				$type = "db";
			}

		}
	}else{
		echo "Invalid API.";
		die();
	}


if (isset($_GET['section'])){
	if ($_GET['section'] == "fields"){
		$solapacampos = 'active';
	}
	if ($_GET['section'] == "config"){
		$solapaconfig = 'active';
	}
	if ($_GET['section'] == "files"){
		$solapaarchivos = 'active';
	}
	if ($_GET['section'] == "upload"){
		$solapaupload = 'active';
	}
}else{
	$solapaconfig = 'active';
}



$rndtemp = md5(date('dmYhis') . rand());



?>


<!DOCTYPE html>
<html lang="<?php echo $lang['SITE_lang'];?>">

<head>
    <meta charset="iso-8859-1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="<?php echo $lang['SITE_keywords'];?>">
    <meta name="description" content="<?php echo $lang['SITE_desc'];?>">
    <meta name="author" content="Sysworld Servicios S.A.">
    <link rel="icon" type="image/x-icon" href="../images/favicon.ico" />
    <title><?php echo $lang['SITE_title'];?></title>
    <!-- ===== Bootstrap CSS ===== -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- ===== Plugin CSS ===== -->
    <link href="../plugins/components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <!-- ===== Animation CSS ===== -->
    <link href="css/animate.css" rel="stylesheet">
    <!-- ===== Custom CSS ===== -->
    <link href="css/style.css" rel="stylesheet">
    <!-- ===== Color CSS ===== -->
    <link href="css/colors/default.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<link rel="stylesheet" href="../plugins/components/dropify/dist/css/dropify.min.css">

</head>

<body class="mini-sidebar">
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <!-- ===== Top-Navigation ===== -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
                <a class="navbar-toggle font-20 hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse">
                    <i class="fa fa-bars"></i>
                </a>
                <div class="top-left-part">
                    <a class="logo" href="index.php">
                        <span>&nbsp;</span>
                        <b>
                             <img src="../images/panel_api.png" width="240px" height="55px" />
                        </b>
                        <span>&nbsp;</span>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-left hidden-xs">
                    <li>
                        <a href="javascript:void(0)" class="sidebartoggler font-20 waves-effect waves-light"><i class="icon-arrow-left-circle"></i></a>
                    </li>
                    <li>
                        <form role="search" class="app-search hidden-xs" action="../apis.php" method="get">
                            <i class="icon-magnifier"></i>
                            <input type="text" placeholder="<?php echo $lang['APILIST_page_search'];?>" class="form-control">
                        </form>
                    </li>
                </ul>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle waves-effect waves-light font-20" data-toggle="dropdown" href="javascript:void(0);">
                            <i class="icon-speech"></i>
                            <span class="badge badge-xs badge-danger"></span>
                        </a>
                        <ul class="dropdown-menu mailbox animated bounceInDown">
                            <li>
                                <div class="drop-title"><?php echo $lang['INDEX_page_messages'];?></div>
                            </li>
                            <li>
                                <div class="message-center">
                                    <a href="javascript:void(0);">
                                        <div class="user-img">
                                            <img src="../plugins/images/users/1.jpg" alt="user" class="img-circle">
                                            <span class="profile-status online pull-right"></span>
                                        </div>
                                        <div class="mail-contnet">
                                            <h5>Pavan kumar</h5>
                                            <span class="mail-desc">Just see the my admin!</span>
                                            <span class="time">9:30 AM</span>
                                        </div>
                                    </a>
                                    <a href="javascript:void(0);">
                                        <div class="user-img">
                                            <img src="../plugins/images/users/2.jpg" alt="user" class="img-circle">
                                            <span class="profile-status busy pull-right"></span>
                                        </div>
                                        <div class="mail-contnet">
                                            <h5>Sonu Nigam</h5>
                                            <span class="mail-desc">I've sung a song! See you at</span>
                                            <span class="time">9:10 AM</span>
                                        </div>
                                    </a>
                                    <a href="javascript:void(0);">
                                        <div class="user-img">
                                            <img src="../plugins/images/users/3.jpg" alt="user" class="img-circle"><span class="profile-status away pull-right"></span>
                                        </div>
                                        <div class="mail-contnet">
                                            <h5>Arijit Sinh</h5>
                                            <span class="mail-desc">I am a singer!</span>
                                            <span class="time">9:08 AM</span>
                                        </div>
                                    </a>
                                    <a href="javascript:void(0);">
                                        <div class="user-img">
                                            <img src="../plugins/images/users/4.jpg" alt="user" class="img-circle">
                                            <span class="profile-status offline pull-right"></span>
                                        </div>
                                        <div class="mail-contnet">
                                            <h5>Pavan kumar</h5>
                                            <span class="mail-desc">Just see the my admin!</span>
                                            <span class="time">9:02 AM</span>
                                        </div>
                                    </a>
                                </div>
                            </li>
                            <li>
                                <a class="text-center" href="javascript:void(0);">
                                    <strong><?php echo $lang['INDEX_page_notifications'];?></strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle waves-effect waves-light font-20" data-toggle="dropdown" href="javascript:void(0);">
                            <i class="icon-calender"></i>
                            <span class="badge badge-xs badge-danger"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-tasks animated slideInUp">
                        </ul>
                    </li>
                    <li class="right-side-toggle">
                    </li>
                </ul>
            </div>
        </nav>
        <!-- ===== Top-Navigation-End ===== -->
        <!-- ===== Left-Sidebar ===== -->
        <aside class="sidebar" role="navigation">
            <div class="scroll-sidebar">
                <div class="user-profile">
                    <div class="dropdown user-pro-body">
                        <p class="profile-text m-t-15 font-16"><a href="javascript:void(0);"> Panel</a></p>
                    </div>
                </div>
                <nav class="sidebar-nav">
					<?php include 'menu.php';?>
                </nav>
                <div class="p-30">
                    <span class="hide-menu">
                        <a href="suscriptions.php" class="btn btn-success"><?php echo $lang['MENU_menu_bottom1'];?></a>
                        <a href="../index.php" class="btn btn-default m-t-15"><?php echo $lang['MENU_menu_bottom2'];?></a>
                    </span>
                </div>
            </div>
        </aside>
        <!-- ===== Left-Sidebar-End ===== -->
        <!-- Page Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                <!-- /row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">


							<!-- sample modal content -->
                            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                                            <h4 class="modal-title" id="myLargeModalLabel"><div id="divtitlekeys">&nbsp;</div></h4> </div>
                                        <div class="modal-body" id="divkeys">

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

							<div class="row">



								<div class="white-box">
									<h3 class="box-title m-b-30">Administrar API: <u><?php echo $nombre;?></u></h3>
									<div data-example-id="togglable-tabs" class="bs-example bs-example-tabs">
										<ul role="tablist" class="nav nav-tabs" id="myTabs">
											<li class="<?php echo $solapaconfig;?>" role="presentation"><a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="home-tab" href="#home5">Configuraci�n</a></li>
											<li role="presentation" class="<?php echo $solapacampos;?>"><a aria-controls="profile" data-toggle="tab" id="profile-tab" role="tab" href="#profile5" aria-expanded="false">Campos</a></li>
											<?php if ($tipoapi == "2"){?>
												<li role="presentation" class="<?php echo $solapadb;?>"><a aria-controls="profile" data-toggle="tab" id="profile-tab" role="tab" href="#profile5" aria-expanded="false">DB</a></li>
											<?php }?>
											<?php if ($tipoapi == "1"){?>
												<li class="dropdown <?php echo $solapaarchivos . $solapaupload;?> " role="presentation"> <a aria-controls="myTabDrop1-contents" data-toggle="dropdown" class="dropdown-toggle" id="myTabDrop1" href="#" aria-expanded="false">Archivos<span class="caret"></span></a>
													<ul id="myTabDrop1-contents" aria-labelledby="myTabDrop1" class="dropdown-menu">
														<li class=""><a aria-controls="dropdown1" data-toggle="tab" id="dropdown1-tab" role="tab" href="#dropdown1" aria-expanded="true">Ver Archivos</a></li>
														<li class=""><a aria-controls="dropdown2" data-toggle="tab" id="dropdown2-tab" role="tab" href="#dropdown2" aria-expanded="false">Subir Archivo</a></li>
														<li class=""><a aria-controls="dropdown3" data-toggle="tab" id="dropdown3-tab" role="tab" href="#dropdown3" aria-expanded="false">Descargar Base</a></li>
													</ul>
												</li>
											<?php }?>
										</ul>
										<div class="tab-content" id="myTabContent">
											<div aria-labelledby="home-tab" id="home5" class="tab-pane fade <?php echo $solapaconfig;?> in" role="tabpanel">
												<h3>Editar Configuraci�n:</h3>

												<form action="apiabmpost.php" method="post">
													<input type="hidden" id="abm" name="abm" value="m">
													<input type="hidden" id="tipoapi" name="tipoapi" value="<?php echo $tipoapi;?>">
													<input type="hidden" id="codigo" name="codigo" value="<?php echo $codigo;?>">
													<input type="hidden" id="id" name="id" value="<?php echo $idapi;?>">


													<div class="form-body">
														<h3 class="box-title">Datos Generales</h3>
														<hr>
														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	<label class="control-label">Nombre</label>
																	<input type="text" id="nombre" name="nombre" class="form-control" placeholder="API Name" value="<?php echo $nombre;?>">
																	<span class="help-block"> Nombre para la API.</span> </div>
															</div>
															<!--/span-->
															<div class="col-md-6">
																<div class="form-group has-error">
																	<label class="control-label">C�digo</label>
																	<input type="text" id="code" name="code" class="form-control" value="<?php echo $codigo;?>" disabled></div>
															</div>
															<!--/span-->
														</div>
														<!--/row-->
														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	<label class="control-label">Versi�n</label>
																	<select class="form-control" id="version" name="version">
																		<option value="v1.0.0">v1.0.0</option>
																		<option value="v2.0.0">v2.0.0</option>
																	</select> <span class="help-block"> Seleccione la versi�n de su API.</span> </div>
															</div>
															<!--/span-->
															<div class="col-md-6">
																<div class="form-group">
																	<label class="control-label">Estado</label>
																	<select class="form-control" id="idestado" name="idestado">
																		<option value="1" <?php echo $idestado1;?>>Activa</option>
																		<option value="0" <?php echo $idestado0;?>>Desarrollo</option>
																		<option value="2" <?php echo $idestado2;?>>Pausada</option>
																		<option value="3" <?php echo $idestado3;?>>Discontinua</option>
																	</select> <span class="help-block"> Seleccione el estado de su API.</span> </div>
															</div>
															<!--/span-->
														</div>
														<!--/row-->
														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	<label class="control-label">Separador Archivo</label>
																	<select class="form-control" id="separador" name="separador">
																		<option value="coma" <?php echo $separadorcoma;?>>Coma (,)</option>
																		<option value="puntoycoma" <?php echo $separadorpuntoycoma;?>>Punto y Coma (;)</option>
																	</select>
																	<span class="help-block"> Seleccione de que forma separa su archivo txt o csv los campos.</span>
																</div>
															</div>
															<!--/span-->
															<div class="col-md-6">
																<div class="form-group">
																	<label class="control-label">Omitir Encabezados</label>
																	<select class="form-control" id="encabezado" name="encabezado">
																		<option value="">NO</option>
																		<option value="1" <?php echo $encabezado1;?>>1 Linea</option>
																		<option value="2" <?php echo $encabezado2;?>>2 Lineas</option>
																		<option value="3" <?php echo $encabezado3;?>>3 Lineas</option>
																	</select>
																	<span class="help-block"> Indique la cantidad de lineas que se omitir�n del archivo.</span> </div>
															</div>
															<!--/span-->
														</div>
														<!--/row-->

														<h3 class="box-title m-t-40">Descripci�n</h3>
														<hr>
														<div class="row">
															<div class="col-md-12 ">
																<div class="form-group">
																	<label>Descripci�n</label>
																	<input type="text" class="form-control" id="descripcion" name="descripcion" value="<?php echo $descripcion;?>">  </div>
															</div>
														</div>
													</div>
													<div class="form-actions">
														<button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Guardar</button>
														<a href="apilist.php?t=<?php echo $_GET['t'];?>"><button type="button" class="btn btn-default">Cancelar</button></a>
														<a href="apiabmpost.php?abm=b&id=<?php echo $idapi;?>&codigo=<?php echo $codigo;?>&type=<?php echo $type;?>"><button type="button" class="btn btn-danger">Eliminar</button></a>

													</div>
												</form>


											</div>
											<div aria-labelledby="profile-tab" id="profile5" class="tab-pane fade <?php echo $solapacampos;?> in" role="tabpanel">
												<h3>Configuraci�n de campos:</h3>
												<br>
												<button data-toggle="modal" data-target="#responsive-modal" style="width:150px;" class="btn btn-block btn-success">Crear Campo</button>
												<br>
					                            <div class="table-responsive">
													<table id="example23" class="display nowrap" cellspacing="0" width="100%">
														<thead>
															<tr>
																<th>ID</th>
																<th>Orden</th>
																<th>Campo</th>
																<th>Formato</th>
																<th>Requerido</th>
																<th>Tipo</th>
																<th>Longitud</th>
																<th>Descripcion</th>
															</tr>
														</thead>
														<?php
															$sql = "SELECT * FROM web_apis_data_campos WHERE idapi = " . $idapi . " order by orden";
															$result = $conn->query($sql);

															$_SESSION['apilanding_apidata_campos_archivos_csv'] = '';
															$campostemp = '';
															if ($apidataomitirencabezado == "1"){
																$campostemp = $campostemp . '//HEADER LINE 1//<br>';
															}
															if ($apidataomitirencabezado == "2"){
																$campostemp = $campostemp . '//HEADER LINE 1//<br>';
																$campostemp = $campostemp . '//HEADER LINE 2//<br>';
															}
															if ($apidataomitirencabezado == "3"){
																$campostemp = $campostemp . '//HEADER LINE 1//<br>';
																$campostemp = $campostemp . '//HEADER LINE 2//<br>';
																$campostemp = $campostemp . '//HEADER LINE 3//<br>';
															}

															if ($result->num_rows > 0) {
																while($row = $result->fetch_assoc()) {
																	$campostemp = $campostemp . $row['campo'] . $apidataseparador;
														?>
																	<tbody>
																		<tr>
																			<td><?php echo $row['id'];?></td>
																			<td><?php echo $row['orden'];?></td>
																			<td><?php echo $row['campo'];?></td>
																			<td><?php echo $row['formato'];?></td>
																			<td><?php echo $row['requerido'];?></td>
																			<td><?php echo $row['tipo'];?></td>
																			<td><?php echo $row['longitud'];?></td>
																			<td><?php echo $row['descripcion'];?></td>
																		</tr>
																	</tbody>
														<?php
																}
																$campostemp = substr($campostemp, 0, strlen($campostemp) - 1);
																$_SESSION['apilanding_apidata_campos_archivos_csv'] = $campostemp;
															}
														?>
	                                				</table>
                            					</div>
                            					<center><a href="apidatagetfileex.php?id=<?php echo $idapi;?>&codigo=<?php echo $codigo;?>" target="_blank"><button type="button" class="btn btn-warning"> <i class="fa fa-check"></i> Descargar Archivo Ejemplo para Upload</button></a></center>
											</div>
											<div aria-labelledby="dropdown1-tab" id="dropdown1" class="tab-pane fade <?php echo $solapaarchivos;?> in" role="tabpanel">
												<h3>Historial de Archivos:</h3>
					                            <div class="table-responsive">
													<table id="example24" class="display nowrap" cellspacing="0" width="100%">
														<thead>
															<tr>
																<th>ID</th>
																<th>Nombre</th>
																<th>Tamano</th>
																<th>Fecha</th>
																<th>&nbsp</th>
																<th>&nbsp</th>
															</tr>
														</thead>
														<?php
															$sql = "SELECT * FROM web_apis_data_archivos WHERE idapi = " . $idapi . " order by id desc";
															$result = $conn->query($sql);
															if ($result->num_rows > 0) {
																while($row = $result->fetch_assoc()) {
														?>
																	<tbody>
																		<tr>
																			<td><?php echo $row['id'];?></td>
																			<td><?php echo $row['nombrearchivo'];?></td>
																			<td><?php echo $row['tamanoarchivo'];?></td>
																			<td><?php echo $row['fechavigencia'];?></td>
																			<td>
																				<?php
																					if ($row['idestado'] == "0"){
																						$vars = "'" . $row['idapi'] . "','" . $codigo . "','" . $row['id'] . "'";
																						echo '<a href="javascript:procesararchivo(' . $vars . ');">Procesar</a>';
																					}
																					if ($row['idestado'] == "1"){
																						echo "Procesado";
																					}
																					if ($row['idestado'] == "2"){
																						echo "Error";
																					}
																				?>

																			</td>
																			<td>
																				<a href="http://webmaster.apilanding.com/panel/grid_web_apis_data_archivos_descargar/index.php?idapi=<?php echo $idapi;?>&idarchivo=<?php echo $row['id'];?>&codigoapi=<?php echo $codigo;?>&s=<?php echo $rndtemp;?>" target="_blank" title="Descargar" style="background-color:#FFFFFF;color:#000000;text-decoration:none"><i class="fa fa-cloud-download"></i></a>
																			</td>
																		</tr>
																	</tbody>
														<?php
																}
															}
														?>
	                                				</table>
	                                				<div id="divresultado"></div>
	                                			</div>
											</div>
											<div aria-labelledby="dropdown2-tab" id="dropdown2" class="tab-pane fade <?php echo $solapaupload;?> in" role="tabpanel">
												<h3>Upload de Archivos:</h3>
												<form action="apiabmpost.php" method="post" enctype="multipart/form-data" >
													<input type="hidden" id="abm" name="abm" value="u_files">
													<input type="hidden" id="id" name="id" value="<?php echo $idapi;?>">
													<input type="hidden" id="tipoapi" name="tipoapi" value="<?php echo $tipoapi;?>">
													<input type="hidden" id="codigo" name="codigo" value="<?php echo $codigo;?>">
													<h3 class="box-title">Subir Archivo</h3>
													<div class="white-box">
														<label for="input-file-max-fs">Mediante la siguiente zona de upload podr� subir un archivo el cual se procesar� en su API.</label>
														<input type="file" name="file" id="input-file-max-fs" class="dropify" data-max-file-size="200M" />
														<span class="help-block"><u>Nota:</u> El archivo subido, pisar� el contenido actual. </span>
													</div>
		                                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
												</form>
											</div>
											<div aria-labelledby="dropdown3-tab" id="dropdown3" class="tab-pane fade" role="tabpanel">
		                                        <h3>Descarga de Archivos:</h3>
		                                        <p>Mediante el siguiente bot�n podr� descargar la base actualizada de su API.</p>
		                                        <a href="apidatagetcsv.php?id=<?php echo $idapi;?>&codigo=<?php echo $codigo;?>" target="_blank"><button type="button" class="btn btn-success"> <i class="fa fa-check"></i> Descargar Archivo CSV</button></a>
		                                        &nbsp;&nbsp;&nbsp;
		                                        <a href="apidatagetjson.php?id=<?php echo $idapi;?>&codigo=<?php echo $codigo;?>" target="_blank"><button type="button" class="btn btn-success"> <i class="fa fa-check"></i> Descargar Archivo JSON</button></a>

											</div>

										</div>
									</div>
								</div>



							</div>

                        </div>
                    </div>
                </div>
                <!-- /.row -->


				<!-- /.modal Crear Campos-->
						<div id="responsive-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
							<form action="apiabmpost.php" method="post">
								<input type="hidden" id="abm" name="abm" value="a_fields">
								<input type="hidden" id="id" name="id" value="<?php echo $idapi;?>">
								<input type="hidden" id="tipoapi" name="tipoapi" value="<?php echo $tipoapi;?>">
								<input type="hidden" id="codigo" name="codigo" value="<?php echo $codigo;?>">

                            	<div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                                            <h4 class="modal-title">Crear Campo</h4>
                                        </div>
                                        <div class="modal-body">
											<div class="form-body">
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Campo</label>
															<div class="col-md-9">
																<input type="text" id="campo" name="campo" class="form-control" placeholder="Field Name"> <span class="help-block"> Nombre del campo.</span>
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Orden</label>
															<div class="col-md-9">
																<input type="text" id="orden" name="orden" class="form-control"> <span class="help-block"> Orden del campo en el archivo de entrada. </span>
															</div>
														</div>
													</div>
												</div>
												<br>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Tipo</label>
															<div class="col-md-9">
																<select class="form-control" id="type" name="type">
																	<option value="entrada">Campo Clave</option>
																	<option value="salida">Campo Salida</option>
																</select>

															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Requerido</label>
															<div class="col-md-9">
																<select class="form-control" id="requerido" name="requerido">
																	<option value="true">True</option>
																	<option value="false">False</option>
																</select>
															</div>
														</div>
													</div>
												</div>
												<br>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Formato</label>
															<div class="col-md-9">
																	<select class="form-control" id="formato" name="formato">
																		<option value="integer">Integer</option>
																		<option value="string">String</option>
																	</select>
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Longitud</label>
															<div class="col-md-9">
																<input type="text" id="longitud" name="longitud" class="form-control"> <span class="help-block"> Cantidad de caracteres / longitud del campo. </span> </div>
														</div>
													</div>
												</div>
												<br>
												<div class="row">
													<div class="col-md-12">
														<div class="form-group">
															<label>Descripci�n:</label>
															<div class="col-md-12">
																<input type="text" id="descripcion" name="descripcion" class="form-control" placeholder="John doe"> <span class="help-block">Descripci�n / Ayuda del Campo.</span>
															</div>
														</div>
													</div>
												</div>
											</div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

						</form>
					</div>
				<!-- /.modal Crear Campos-->


                <!-- ===== Right-Sidebar ===== -->
                <div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"><?php echo $lang['INDEX_page_panel_service'];?> <span><i class="icon-close right-side-toggler"></i></span> </div>
                        <div class="r-panel-body">
                            <ul class="hidden-xs">
                                <li><b><?php echo $lang['INDEX_page_panel_service_item1'];?></b></li>
                                <li>
                                    <div class="checkbox checkbox-danger">
                                        <input id="headcheck" type="checkbox" class="fxhdr">
                                        <label for="headcheck"> Fix Header </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox checkbox-warning">
                                        <input id="sidecheck" type="checkbox" class="fxsdr">
                                        <label for="sidecheck"> Fix Sidebar </label>
                                    </div>
                                </li>
                            </ul>
                            <ul id="themecolors" class="m-t-20">
                                <li><b><?php echo $lang['INDEX_page_panel_service_item2'];?></b></li>
                                <li><a href="javascript:void(0)" data-theme="default" class="default-theme working">1</a></li>
                                <li><a href="javascript:void(0)" data-theme="green" class="green-theme">2</a></li>
                                <li><a href="javascript:void(0)" data-theme="yellow" class="yellow-theme">3</a></li>
                                <li><a href="javascript:void(0)" data-theme="red" class="red-theme">4</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple" class="purple-theme">5</a></li>
                                <li><a href="javascript:void(0)" data-theme="black" class="black-theme">6</a></li>
                                <li class="db"><b><?php echo $lang['INDEX_page_panel_service_item3'];?></b></li>
                                <li><a href="javascript:void(0)" data-theme="default-dark" class="default-dark-theme">7</a></li>
                                <li><a href="javascript:void(0)" data-theme="green-dark" class="green-dark-theme">8</a></li>
                                <li><a href="javascript:void(0)" data-theme="yellow-dark" class="yellow-dark-theme">9</a></li>
                                <li><a href="javascript:void(0)" data-theme="red-dark" class="red-dark-theme">10</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple-dark" class="purple-dark-theme">11</a></li>
                                <li><a href="javascript:void(0)" data-theme="black-dark" class="black-dark-theme">12</a></li>
                            </ul>
                            <ul class="m-t-20 chatonline">
                                <li><b><?php echo $lang['INDEX_page_panel_service_item4'];?></b></li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/1.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/2.jpg" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/3.jpg" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/4.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/5.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/6.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- ===== Right-Sidebar-End ===== -->
            </div>
            <!-- /.container-fluid -->
            <footer class="footer t-a-c">
                APILanding.com - The Center of the APIS.<br>Powered by <a href="http://www.sysworld.com.ar">Sysworld Servicios S.A.</a>
            </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="../plugins/components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="js/sidebarmenu.js"></script>
    <!--slimscroll JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="js/custom.js"></script>
    <script src="../plugins/components/datatables/jquery.dataTables.min.js"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <!-- end - This is for export functionality only -->

    <script>
    	function procesararchivo(idapi, codigoapi, idarchivo){
			document.getElementById("divresultado").innerHTML = "<b>Procesando, por favor espere...</b>";

			var xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					window.location.replace("apiedit.php?id=" + idapi + "&codigo=" + codigoapi + "&section=files");
				}
			};
			xhttp.open("GET", "http://webmaster.apilanding.com/panel/grid_web_apis_data_archivos_procesar/index.php?idapi=" + idapi + "&idarchivo=" + idarchivo + "&codigoapi=" + codigoapi + "&s=<?php echo $rndtemp;?>", true);
			xhttp.send();

    	}
    </script>


    <script>
    $(function() {
        $('#myTable').DataTable();

        var table = $('#example').DataTable({
            "columnDefs": [{
                "visible": false,
                "targets": 2
            }],
            "order": [
                [2, 'asc']
            ],
            "displayLength": 25,
            "drawCallback": function(settings) {
                var api = this.api();
                var rows = api.rows({
                    page: 'current'
                }).nodes();
                var last = null;
                api.column(2, {
                    page: 'current'
                }).data().each(function(group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                        last = group;
                    }
                });
            }
        });
        // Order by the grouping
        $('#example tbody').on('click', 'tr.group', function() {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                table.order([2, 'desc']).draw();
            } else {
                table.order([2, 'asc']).draw();
            }
        });
    });
    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    $('#example24').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>

    <script>
    	function obtenerapikeys(context, id){
			$(".bs-example-modal-lg").modal()
    		var divkeys = document.getElementById('divtitlekeys');
    		divkeys.innerHTML = "API: " + context;

			document.getElementById("divkeys").innerHTML = '<h4>Obteniendo Claves... Por favor espere...</h4>';

			var req = new XMLHttpRequest();
			req.open('GET', 'getapikeys.php?idapi=' + id, true);
			req.onreadystatechange = function (aEvt) {
			  if (req.readyState == 4) {
				 if(req.status == 200)
				  document.getElementById("divkeys").innerHTML = req.responseText;
				 else
				  document.getElementById("divkeys").innerHTML = "api error";
			  }
			};
			req.send(null);


    	}
    </script>


<script src="../plugins/components/dropify/dist/js/dropify.min.js"></script>
    <script>
    $(function() {
        // Basic
        $('.dropify').dropify();
        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-d�posez un fichier ici ou cliquez',
                replace: 'Glissez-d�posez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'D�sol�, le fichier trop volumineux'
            }
        });
        // Used events
        var drEvent = $('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
    </script>



    <!--Style Switcher -->
    <script src="../plugins/components/styleswitcher/jQuery.style.switcher.js"></script>


	<script src="../plugins/components/session-timeout/jquery.sessionTimeout.min.js"></script>
	<script>
		var SessionTimeout=function(){var i=function(){$.sessionTimeout({title:"Session Timeout",message:"Su sesi�n ha llegado a un tiempo de inactividad que se cerrar�.",redirUrl:"logout.php",logoutUrl:"logout.php",warnAfter:900000,redirAfter:902000,ignoreUserActivity:!0,countdownMessage:"Cerrando en {timer} segundos.",countdownBar:!0})};return{init:function(){i()}}}();jQuery(document).ready(function(){SessionTimeout.init()});
	</script>

</body>

</html>
