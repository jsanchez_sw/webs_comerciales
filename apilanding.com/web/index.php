<?php
    include 'functions/db.php';
	include_once 'functions/language.php';

?>


<!DOCTYPE html>
<html lang="<?php echo $lang['SITE_lang'];?>">

<!-- Mirrored from protechtheme.com/saas/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:17:32 GMT -->
<head>
        <!-- Required meta tags -->
        <meta charset="iso-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="keywords" content="<?php echo $lang['SITE_keywords'];?>">
		<meta name="description" content="<?php echo $lang['SITE_desc'];?>">
		<meta name="author" content="Sysworld Servicios S.A.">
        <title><?php echo $lang['SITE_title'];?></title>

        <!-- Bootstrap core CSS -->
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Select2 -->
        <link href="assets/select2/css/select2.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- Iconmoon -->
        <link href="assets/iconmoon/css/iconmoon.css" rel="stylesheet">
        <!-- Owl Carousel -->
        <link href="assets/owl-carousel/css/owl.carousel.min.css" rel="stylesheet">
        <!-- Video Popup -->
        <link href="assets/magnific-popup/css/magnific-popup.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="css/custom.css" rel="stylesheet">
        <link rel="icon" type="image/x-icon" href="images/favicon.ico" />
    </head>
    <body>

<!-- ==============================================
**Preloader**
=================================================== -->
        <div id="loader">
            <div id="element">
                <div class="circ-one"></div>
                <div class="circ-two"></div>
            </div>
        </div>

<!-- ==============================================
**Header**
=================================================== -->
        <header>
			<?php include 'menu.php';?>
        </header>

<!-- ==============================================
**Banner Gradient**
=================================================== -->
        <div class="banner gradient-bg">
            <div class="container">
                <div class="row cnt-block">
                    <div class="col-md-7">
                        <div class="left">
                            <h1><?php echo $lang['INDEX_header_tittle'];?></h1>
                            <p><?php echo $lang['INDEX_header_subtittle'];?></p>
                        </div>
                    </div>
                    <div class="col-md-4 right-sec"> <a href="apis.php" class="get-started"><?php echo $lang['INDEX_header_bottom'];?></a>
                        <p><?php echo $lang['INDEX_header_bottom_info'];?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="pop-box clearfix">
                            <figure class="icon"><img src="images/manage-seo-ico.png" alt=""></figure>
                            <div class="right-cnt">
                                <h3>Administre sus claves y accesos.</h3>
                                <p>Con tan solo crear su cuenta gratuita obtenga los accesos a las APIS.</p>
                            </div>
                        </div>
                        <div class="tab-screen">
                            <figure><img src="images/banner-tab-screen.png" class="img-fluid" alt=""></figure>
                        </div>
                        <div class="pop-box right clearfix">
                            <figure class="icon"><img src="images/seo-reports-ico.png" alt=""></figure>
                            <div class="right-cnt">
                                <h3>Panel de Control On-Line</h3>
                                <p>Monitoree sus consumos y las actividades de sus integraciones.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<!-- ==============================================
**Content Marketing opt1**
=================================================== -->
        <section class="content-marketing padding-lg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <div class="head-block">
                            <h2><?php echo $lang['INDEX_section2_tittle'];?></h2>
                            <p><?php echo $lang['INDEX_section2_desc'];?></p>
                        </div>
                    </div>
                </div>
                <ul class="row marketing-list">
                    <li class="col-md">
                        <div class="inner">
                            <figure><img src="images/scalable-link-ico.png" class="img-fluid" alt=""></figure>
                            <h3><?php echo $lang['INDEX_section2_item1_tittle'];?></h3>
                            <p><?php echo $lang['INDEX_section2_item1_desc'];?></p>
                            <a href="features.php" class="know-more"><?php echo $lang['INDEX_section2_bottom_items'];?></a> </div>
                    </li>
                    <li class="col-md">
                        <div class="inner">
                            <figure><img src="images/seo-success-ico.png" class="img-fluid" alt=""></figure>
                            <h3><?php echo $lang['INDEX_section2_item2_tittle'];?></h3>
                            <p><?php echo $lang['INDEX_section2_item2_desc'];?></p>
                            <a href="features.php" class="know-more"><?php echo $lang['INDEX_section2_bottom_items'];?></a> </div>
                    </li>
                    <li class="col-md">
                        <div class="inner">
                            <figure><img src="images/content-marketing-ico.png" class="img-fluid" alt=""></figure>
                            <h3><?php echo $lang['INDEX_section2_item3_tittle'];?></h3>
                            <p><?php echo $lang['INDEX_section2_item3_desc'];?></p>
                            <a href="features.php" class="know-more"><?php echo $lang['INDEX_section2_bottom_items'];?></a> </div>
                    </li>
                </ul>
            </div>
        </section>

<!-- ==============================================
**Create SEO Reports**
=================================================== -->
        <section class="seo-reports">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 cnt-block">
                        <h2><?php echo $lang['INDEX_section3_tittle'];?></h2>
                        <p><?php echo $lang['INDEX_section3_desc'];?></p>
                        <a href="features.php" class="know-more"><?php echo $lang['INDEX_section3_bottom'];?></a> </div>
                    <div class="col-lg-8">
                        <figure class="img"><img src="images/screen2.png" class="img-fluid" style="margin-top:63px;" alt=""></figure>
                    </div>
                </div>
            </div>
        </section>

<!-- ==============================================
**Generate Forms**
=================================================== -->
        <section class="generate-forms padding-lg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <h2><?php echo $lang['INDEX_section4_tittle'];?></h2>
                        <p><?php echo $lang['INDEX_section4_desc'];?></p>
                        <figure class="img"><img src="images/screen3.png" class="img-fluid" alt=""></figure>
                    </div>
                </div>
            </div>
        </section>

<!-- ==============================================
**Simple Editor**
=================================================== -->
        <section class="simple-editor padding-lg">
            <div class="container">
                <div class="row">
                </div>
            </div>
        </section>

<!-- ==============================================
**Choose Pack**
=================================================== -->


<!-- ==============================================
**Client Speak opt1**
=================================================== -->
        <section class="client-speak carousel1 padding-lg">
            <div class="container">
                <div class="row justify-content-center head-block">
                    <div class="col-md-10">
                        <h2><?php echo $lang['INDEX_section6_tittle'];?></h2>
                        <p class="hidden-xs"><?php echo $lang['INDEX_section6_desc'];?></p>
                    </div>
                </div>
                <ul class="speak-listing opt1 owl-carousel">
                    <li>
                        <div class="inner">
                            <figure><img src="images/vw.png" style="width:32%!important; height:32%!important;" class="" alt=""></figure>
                            <span class="icon-quote"></span>
                            <div class="client-detail">
                                <h4><?php echo $lang['INDEX_section6_item1_name'];?></h4>
                                <span class="designation"><?php echo $lang['INDEX_section6_item1_tittle'];?></span> </div>
                            <p><?php echo $lang['INDEX_section6_item1_desc'];?></p>
                        </div>
                    </li>
                    <li>
                        <div class="inner">
                            <figure><img src="images/bf.png" style="width:40%!important; height:40%!important;" class="" alt=""></figure>
                            <span class="icon-quote"></span>
                            <div class="client-detail">
                                <h4><?php echo $lang['INDEX_section6_item2_name'];?></h4>
                                <span class="designation"><?php echo $lang['INDEX_section6_item2_tittle'];?></span> </div>
                            <p><?php echo $lang['INDEX_section6_item2_desc'];?></p>
                        </div>
                    </li>
                    <li>
                        <div class="inner">
                            <figure><img src="images/bfor.png" style="width:40%!important; height:40%!important;" class="" alt=""></figure>
                            <span class="icon-quote"></span>
                            <div class="client-detail">
                                <h4><?php echo $lang['INDEX_section6_item3_name'];?></h4>
                                <span class="designation"><?php echo $lang['INDEX_section6_item3_tittle'];?></span> </div>
                            <p><?php echo $lang['INDEX_section6_item3_desc'];?></p>
                        </div>
                    </li>
                    <li>
                        <div class="inner">
                            <figure><img src="images/fiat.png" style="width:25%!important; height:25%!important;" class="" alt=""></figure>
                            <span class="icon-quote"></span>
                            <div class="client-detail">
                                <h4><?php echo $lang['INDEX_section6_item4_name'];?></h4>
                                <span class="designation"><?php echo $lang['INDEX_section6_item4_tittle'];?></span> </div>
                            <p><?php echo $lang['INDEX_section6_item4_desc'];?></p>
                        </div>
                    </li>
                </ul>
            </div>
        </section>

<!-- ==============================================
**Latest Stories**
=================================================== -->

<!-- ==============================================
**Partners**
=================================================== -->


<!-- ==============================================
**Signup Section**
=================================================== -->

<!-- ==============================================
**Footer opt1**
=================================================== -->
     <?php
    include 'footer.php';
     ?>

        <!-- Scroll to top -->
        <a href="#" class="scroll-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>

        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery.min.js"></script>
        <!-- Popper JS -->
        <script src="js/popper.min.js"></script>
        <!-- Bootsrap JS -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- Select2 JS -->
        <script src="assets/select2/js/select2.min.js"></script>
        <!-- Bxslider JS -->
        <script src="assets/bxslider/js/bxslider.min.js"></script>
        <!-- Owl Carousal JS -->
        <script src="assets/owl-carousel/js/owl.carousel.min.js"></script>
        <!-- Video Popup JS -->
        <script src="assets/magnific-popup/js/magnific-popup.min.js"></script>
        <!-- Custom JS -->
        <script src="js/custom.js"></script>
    </body>

<!-- Mirrored from protechtheme.com/saas/index1.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:16:20 GMT -->
</html>