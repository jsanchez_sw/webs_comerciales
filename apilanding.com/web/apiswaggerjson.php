<?php
    include 'functions/db.php';

    include_once 'functions/language.php';


	$sql = "SELECT * from web_apis A left join web_apis_desc B ON A.id = B.idapi WHERE A.id = " . $_GET['idapi'];
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$id = $_GET['id'];
			$codigo = $row["codigo"];
			$nombre = $row["nombre"];
			$nombre2 = $row["nombre"];
			$descripcion = $row["descripcion"];
			$proveedor = $row["proveedor"];
			$contexto = $row["contexto"];
			$linkapicenter = $row["linkapicenter"];
			$linkdoc = $row["linkdoc"];
			$imagen1 = $row["imagen1"];
			$imagen2 = $row["imagen2"];
			$tiposuscripcion = $row["tiposuscripcion"];
			$idestado = $row["idestado"];
			$categorias = $row["categorias"];
			$industrias = $row["industrias"];
			$fechavigencia = $row["fechavigencia"];
			$auditor = $row["auditor"];
			$fechabaja = $row["fechabaja"];
			$idioma = $row["idioma"];
			$descripcioncomercial = $row["descripcioncomercial"];
			$descripciontecnica = $row["descripciontecnica"];
			$casoexito = $row["casoexito"];
			$tipoapi = $row["tipoapi"];

			if ($tipoapi == "1"){
				$contexto = "database/apidata/get";
				$descripcion = $descripcion . " (Return Data)";
				$nombre2 = $nombre . " (Return Data)";
				$contextoupdate = "database/apidata/update";
				$descripcionupdate = $descripcion . " (Update Data)";
				$nombreupdate = $nombre . " (Update Data)";
				$contextoinsert = "database/apidata/insert";
				$descripcioninsert = $descripcion . " (Insert Data)";
				$nombreinsert = $nombre . " (Insert Data)";
				$contextodelete = "database/apidata/delete";
				$descripciondelete = $descripcion . " (Delete Data)";
				$nombredelete = $nombre . " (Delete Data)";

			}

		}
	}





?>




{
  "swagger": "2.0",
  "info": {
    "description": "<?php echo $descripcion;?>",
    "version": "1.0.0",
    "title": "<?php echo $nombre;?>",
    "termsOfService": "http://swagger.io/terms/",
    "contact": {
      "email": "soporte1@sysworld.com.ar",
      "url": "http://www.apilanding.com"
    },
    "license": {
      "name": "Apache 2.0",
      "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
    }
  },
  "host": "cont1-virtual1.certisend.com",
  "basePath": "/web/container/api/v1",
  "schemes": [
    "https",
    "http"
  ],
  "paths": {
    "/<?php echo $contexto;?>": {
      "get": {
        "tags": [
          "<?php echo $nombre;?>"
        ],
        "summary": "<?php echo $nombre2;?>",
        "description": "<?php echo $descripcion;?>",
        "operationId": "<?php echo $nombre;?>",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "token-susc",
            "in": "query",
            "description": "Token of suscription user. View Comercial Portal -> API List",
            "required": true,
            "type": "string"
          },
          {
            "name": "token-api",
            "in": "query",
            "description": "Token of API List. View Comercial Portal -> API List",
            "required": true,
            "type": "string"
          },

<?php
	if ($tipoapi == "1"){
?>
		  {
			"name": "apicode",
			"in": "query",
			"description": "Code of your API Data.",
			"required": true,
			"type": "string"
		  },
<?php
	}
?>

<?php
	$sql = "SELECT * from web_apis_parametros WHERE idapi = " . $_GET['idapi'];
	$resultparametros = $conn->query($sql);

	if ($resultparametros->num_rows > 0) {
		while($row = $resultparametros->fetch_assoc()) {
?>
		  {
			"name": "<?php echo $row['parametro'];?>",
			"in": "<?php echo $row['formato'];?>",
			"description": "<?php echo $row['descripcion'];?>",
			"required": <?php echo $row['requerido'];?>,
			"type": "<?php echo $row['tipo'];?>"
		  },
<?php
		}
	}
?>

          {
            "name": "internalid",
            "in": "query",
            "description": "Transaction identity.",
            "required": false,
            "type": "string"
          }


        ],
        "responses": {
          "200": {
            "description": "successful operation",
			"schema": {
              "$ref": "#/definitions/Def"
            }
          },
          "401": {
            "description": "autentication token suscription or token api error."
          },
          "409": {
            "description": "api or suscription canceled by administrator."
          },
          "429": {
            "description": "api limit exceeded."
          }
        }
      }
    }
    <?php if ($tipoapi == "1"){?>
    ,"/<?php echo $contextoupdate;?>": {
      "post": {
        "tags": [
          "<?php echo $nombre;?>_Update"
        ],
        "summary": "<?php echo $nombreupdate;?>",
        "description": "<?php echo $$descripcionupdate;?>",
        "operationId": "<?php echo $nombreupdate;?>",
        "produces": [
          "application/json"
        ],
        "parameters": [
		  {
            "name": "token-susc",
            "in": "query",
            "description": "Token of suscription user. View Comercial Portal -> API List",
            "required": true,
            "type": "string"
          },
          {
            "name": "token-api",
            "in": "query",
            "description": "Token of API List. View Comercial Portal -> API List",
            "required": true,
            "type": "string"
          },

<?php
	if ($tipoapi == "1"){
?>
		  {
			"name": "apicode",
			"in": "query",
			"description": "Code of your API Data.",
			"required": true,
			"type": "string"
		  },
<?php
	}
?>

<?php
	$sql = "SELECT * from web_apis_parametros WHERE idapi = " . $_GET['idapi'];
	$resultparametros = $conn->query($sql);

	if ($resultparametros->num_rows > 0) {
		while($row = $resultparametros->fetch_assoc()) {
?>
		  {
			"name": "<?php echo $row['parametro'];?>",
			"in": "<?php echo $row['formato'];?>",
			"description": "<?php echo $row['descripcion'];?>",
			"required": <?php echo $row['requerido'];?>,
			"type": "<?php echo $row['tipo'];?>"
		  },
<?php
		}
	}
?>
		  {
            "in": "body",
            "name": "body",
            "description": "Data to update in database",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Def"
            }
          },
          {
            "name": "internalid",
            "in": "query",
            "description": "Transaction identity.",
            "required": false,
            "type": "string"
          }


        ],
        "responses": {
          "200": {
            "description": "successful operation",
			"schema": {
              "$ref": "#/definitions/Def"
            }
          },
          "401": {
            "description": "autentication token suscription or token api error."
          },
          "409": {
            "description": "api or suscription canceled by administrator."
          },
          "429": {
            "description": "api limit exceeded."
          }
        }
      }
    }

    ,"/<?php echo $contextoinsert;?>": {
      "post": {
        "tags": [
          "<?php echo $nombre;?>_Insert"
        ],
        "summary": "<?php echo $nombreinsert;?>",
        "description": "<?php echo $$descripcioninsert;?>",
        "operationId": "<?php echo $nombreinsert;?>",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "token-susc",
            "in": "query",
            "description": "Token of suscription user. View Comercial Portal -> API List",
            "required": true,
            "type": "string"
          },
          {
            "name": "token-api",
            "in": "query",
            "description": "Token of API List. View Comercial Portal -> API List",
            "required": true,
            "type": "string"
          },

<?php
	if ($tipoapi == "1"){
?>
		  {
			"name": "apicode",
			"in": "query",
			"description": "Code of your API Data.",
			"required": true,
			"type": "string"
		  },
<?php
	}
?>

<?php
	$sql = "SELECT * from web_apis_parametros WHERE idapi = " . $_GET['idapi'];
	$resultparametros = $conn->query($sql);

	if ($resultparametros->num_rows > 0) {
		while($row = $resultparametros->fetch_assoc()) {
?>
		  {
			"name": "<?php echo $row['parametro'];?>",
			"in": "<?php echo $row['formato'];?>",
			"description": "<?php echo $row['descripcion'];?>",
			"required": <?php echo $row['requerido'];?>,
			"type": "<?php echo $row['tipo'];?>"
		  },
<?php
		}
	}
?>
		  {
            "in": "body",
            "name": "body",
            "description": "Data to update in database",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Def"
            }
          },

          {
            "name": "internalid",
            "in": "query",
            "description": "Transaction identity.",
            "required": false,
            "type": "string"
          }


        ],
        "responses": {
          "200": {
            "description": "successful operation",
			"schema": {
              "$ref": "#/definitions/Def"
            }
          },
          "401": {
            "description": "autentication token suscription or token api error."
          },
          "409": {
            "description": "api or suscription canceled by administrator."
          },
          "429": {
            "description": "api limit exceeded."
          }
        }
      }
    }


    ,"/<?php echo $contextodelete;?>": {
      "get": {
        "tags": [
          "<?php echo $nombre;?>_Delete"
        ],
        "summary": "<?php echo $nombredelete;?>",
        "description": "<?php echo $$descripciondelete;?>",
        "operationId": "<?php echo $nombredelete;?>",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "token-susc",
            "in": "query",
            "description": "Token of suscription user. View Comercial Portal -> API List",
            "required": true,
            "type": "string"
          },
          {
            "name": "token-api",
            "in": "query",
            "description": "Token of API List. View Comercial Portal -> API List",
            "required": true,
            "type": "string"
          },

<?php
	if ($tipoapi == "1"){
?>
		  {
			"name": "apicode",
			"in": "query",
			"description": "Code of your API Data.",
			"required": true,
			"type": "string"
		  },
<?php
	}
?>

<?php
	$sql = "SELECT * from web_apis_parametros WHERE idapi = " . $_GET['idapi'];
	$resultparametros = $conn->query($sql);

	if ($resultparametros->num_rows > 0) {
		while($row = $resultparametros->fetch_assoc()) {
?>
		  {
			"name": "<?php echo $row['parametro'];?>",
			"in": "<?php echo $row['formato'];?>",
			"description": "<?php echo $row['descripcion'];?>",
			"required": <?php echo $row['requerido'];?>,
			"type": "<?php echo $row['tipo'];?>"
		  },
<?php
		}
	}
?>

          {
            "name": "internalid",
            "in": "query",
            "description": "Transaction identity.",
            "required": false,
            "type": "string"
          }


        ],
        "responses": {
          "200": {
            "description": "successful operation",
			"schema": {
              "$ref": "#/definitions/Def"
            }
          },
          "401": {
            "description": "autentication token suscription or token api error."
          },
          "409": {
            "description": "api or suscription canceled by administrator."
          },
          "429": {
            "description": "api limit exceeded."
          }
        }
      }
    }

    <?php }?>

  },
  "definitions": {
      "Def": {
        "type": "object",
        "properties": {
<?php
	$sql = "SELECT * from web_apis_campos_respuesta WHERE idapi = " . $_GET['idapi'];
	$resultrespuestas = $conn->query($sql);

	if ($resultrespuestas->num_rows > 0) {
		while($row = $resultrespuestas->fetch_assoc()) {
?>
          "<?php echo $row['campo'];?>": {
            "type": "<?php echo $row['tipo'];?>",
			  <?php if ($row['tipo'] == "integer"){?>
				"format": "int32",
			  <?php }?>
            "description": "<?php echo $row['descripcion'];?>"
          },
<?php
		}
	}
?>
          "id": {
            "type": "integer",
            "format": "int64",
            "description": "id of log internal."
          }
        },
        "xml": {
          "name": "User"
        }
      },
      "ApiResponse": {
        "type": "object",
        "properties": {
          "code": {
            "type": "integer",
            "format": "int32"
          },
          "type": {
            "type": "string"
          },
          "message": {
            "type": "string"
          }
        }
      }
    },
    "externalDocs": {
      "description": "Find out more about Swagger",
      "url": "http://swagger.io"
    }
}

