﻿<!DOCTYPE html>
<html lang="en-gb" dir="ltr">

    <head>
        <!-- Standard Meta -->
        <meta charset="utf-8">
        <meta name="format-detection" content="telephone=no" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- Site Properties -->
        <title>Centro de Validaciones - Validación de Identidad</title>
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon-precomposed" href="images/apple-touch-icon.png">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Rajdhani:300,400,500,600,700%7CRoboto:300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">

        <!-- CSS -->
        <link rel="stylesheet" href="css/uikit.css">
        <link rel="stylesheet" href="css/components/slider.css">
        <link rel="stylesheet" href="css/components/slideshow.css">
        <link rel="stylesheet" href="css/components/slidenav.css">
        <link rel="stylesheet" href="css/components/dotnav.css">
        <link rel="stylesheet" href="css/components/progress.css">
        <link rel="stylesheet" href="css/small-icon.css">
        <link rel="stylesheet" href="css/elements.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/mediaquery.css">
    </head>

    <body>

        <!-- Header Section -->
        <header id="idz-header">
            <!-- Mobile / Offcanvas Menu -->
        	<div id="mobile-nav" class="uk-offcanvas">
				<div class="uk-offcanvas-bar">
					<ul class="uk-nav uk-nav-offcanvas uk-nav-parent-icon" data-uk-nav>
						<li class="uk-parent"><a href="#">home</a>
							<ul class="uk-nav-sub">
								<li><a href="index.html">Homepage 1</a></li>
						        <li><a href="index2.html">Homepage 2</a></li>
						        <li><a href="index3.html">Homepage 3</a></li>
						        <li><a href="index4.html">Homepage 4</a></li>
					        </ul>
						</li>
			            <li><a href="shared.html">shared</a></li>
			            <li class="uk-active"><a href="vps.html">vps</a></li>
			            <li><a href="dedicated.html">Dedicated</a></li>
			            <li><a href="other-services.html">other services </a></li>
			            <li class="uk-parent"><a href="#">blog</a>
						    <ul class="uk-nav-sub">
						    	<li><a href="blog.html">Blog Page</a></li>
						        <li><a href="single.html">Single Post</a></li>
					        </ul>
			            </li>
			            <li><a href="contact.html">Meet Us</a></li>
						<li class="uk-parent"><a href="#">Pages</a>
						    <ul class="uk-nav-sub">
						        <li><a href="domain-pricing.html">Domain Pricing</a></li>
						        <li><a href="features.html">Features</a></li>
						        <li><a href="about.html">About</a></li>
						        <li><a href="testimonial.html">Testimonial</a></li>
						        <li><a href="team.html">Team</a></li>
						        <li><a href="login.html">Login</a></li>
						        <li><a href="client-area.html">Client Area</a></li>
						        <li><a href="portfolio.html">Portfolio</a></li>
						        <li><a href="faq.html">FAQ</a></li>
						        <li><a href="sitemap.html">Sitemap</a></li>
						        <li><a href="404.html">404 Error</a></li>
						    </ul>
						</li>
						<li class="uk-parent"><a href="#">Elements</a>
						    <ul class="uk-nav-sub">
						        <li><a href="column.html">Column</a></li>
						        <li><a href="table.html">Table</a></li>
						        <li><a href="button-list.html">Button &amp; List</a></li>
						        <li><a href="icon-list.html">Icon List</a></li>
						        <li><a href="icon-use.html">Icon Use</a></li>
						        <li><a href="typography.html">Typography</a></li>
						        <li><a href="panel-promo.html">Panel &amp; Promo Box</a></li>
						        <li><a href="tab-accordion.html">Tab &amp; Accordion</a></li>
						        <li><a href="alert-progress.html">Alert &amp; Progress</a></li>
						    </ul>
						</li>
					</ul>
				</div>
			</div>
			<!-- Mobile / Offcanvas Menu End -->

			<div class="uk-container uk-container-center">
				<div class="uk-grid">

					<!-- Logo -->
					<div class="uk-width-large-1-2 uk-width-medium-1-1 uk-width-small-1-1">
						<div class="idz-logo">
							<a href="index.asp"><img src="images/logo.png" alt="logo" class=""></a>
						</div>
					</div>
					<!-- Logo End -->

					<!-- Domain Search Form -->
					<div class="uk-width-large-1-2  uk-width-medium-1-1 uk-width-small-1-1">
						<div class="idz-search-domain-block">
							<div class="idz-header-search-domain">

								<form class="uk-form idz-search-domain-form">
									<fieldset>
									<input type="text" class="uk-form-large uk-width-1-1 " placeholder="Prueba de Validacion de Email/Telefono/Domicilio/DNI">
									<button class="uk-button-large idz-button blue uppercase">Probar Validador</button>
									</fieldset>
								</form>
							</div>
						</div>
					</div>
					<!-- Domain Search Form End -->

					<!-- Header Info -->
					<div class="uk-width-large-1-1 uk-width-medium-1-1 uk-width-small-1-1">
						<ul class="uk-grid uk-grid-large uk-grid-with-medium-1-4 idz-header-contact">
							<li><i class="uk-icon-globe"></i>Español</li>
							<li><i class="uk-icon-phone"></i>(+54 11) 5263-2919</li>
							<li><i class="uk-icon-headphones"></i><a target="blank" href="http://www.sysworld.com.ar/soporte.asp">Live Chat</li></a>
							<li><i class="uk-icon-life-bouy "></i><a href="#client-area" data-uk-modal>Acceso Clientes</a>
						</ul>
					</div>
					<!-- Header End -->

				</div>
			</div>

			<!-- Header Nav -->
			<div class="idz-header-nav" data-uk-sticky="{top:-400}">

				<div class="uk-container uk-container-center">
					<div class="uk-grid">
						<div class="uk-width-medium-1-1">

							<div class="idz-menu-wrapper">
								<div class="uk-grid">
									<div class="uk-width-large-8-10 uk-width-medium-3-10 uk-width-small-3-10">

										<!-- Menu Items -->
										<div class="idz-nav">
											<nav class="uk-navbar">
											    <div class="uk-navbar-flip">
											        <ul class="uk-navbar-nav uk-hidden-medium  uk-hidden-small">
											            <li class="uk-parent uk-active" data-uk-dropdown><a href="index.asp">Inicio</a></li>
											            <li><a href="empresa.asp">Nuestra Empresa</a></li>
											            <li class="uk-parent" data-uk-dropdown="{justify:'.idz-menu-wrapper'}"><a href="funcionamiento.asp">Validadores</a>
					                                        <div class="uk-dropdown uk-dropdown-navbar idz-megamenu">
					                                            <ul class="uk-grid" data-uk-grid-margin>
					                                                <li class="uk-width-large-3-10 uk-width-medium-4-10">
					                                                    <ul class="uk-list uk-list-space">
					                                                        <li>
					                                                            <a href="validadoremail.asp">
					                                                                <div class="icon-wrap circle left small blue">
					                                                                    <i class="uk-icon-cubes"></i>
					                                                                </div>
					                                                                <h5>Email</h5>
					                                                                <p class="uk-nbfc">Verifique si una cuenta de email existe o no.</p>
					                                                            </a>
					                                                        </li>
					                                                        <li>
					                                                            <a href="validadortelefono.asp">
					                                                                <div class="icon-wrap circle left small blue">
					                                                                    <i class="uk-icon-cube"></i>
					                                                                </div>
					                                                                <h5>Teléfono</h5>
					                                                                <p class="uk-nbfc">Verifique y Normalice un número de teléfono fijo y/o movil.</p>
					                                                            </a>
					                                                        </li>
					                                                        <li>
					                                                            <a href="validadordomicilio.asp">
					                                                                <div class="icon-wrap circle left small blue">
					                                                                    <i class="uk-icon-tasks"></i>
					                                                                </div>
					                                                                <h5>Domicilio</h5>
					                                                                <p class="uk-nbfc">Valide, Normalice y Enriquezca un Domicilio.</p>
					                                                            </a>
					                                                        </li>
					                                                        <li>
					                                                            <a href="validadoridentidad.asp">
					                                                                <div class="icon-wrap circle left small blue">
					                                                                    <i class="uk-icon-tasks"></i>
					                                                                </div>
					                                                                <h5>Identidad</h5>
					                                                                <p class="uk-nbfc">Valide la identidad de una persona física.</p>
					                                                            </a>
					                                                        </li>					                                                    </ul>
					                                                </li>
					                                                <li class="uk-width-large-2-10 uk-width-medium-3-10">
					                                                	<h5>Otros Servicios</h5>
					                                                    <ul class="uk-list uk-list-space idz-list-check">
					                                                        <li><a href="http://www.enviocertificado.com" target="_blank">EnvíoCertificado.com</a></li>
					                                                        <li><a href="http://www.enviocertificado.com" target="_blank">ContactoCertificado.com</a></li>
					                                                        <li><a href="http://www.tuscomprobantes.com" target="_blank">TusComprobantes.com</a></li>
					                                                    </ul>
					                                                </li>
					                                                <li class="uk-width-large-2-10 uk-width-medium-3-10">
					                                                	<h5>Soporte</h5>
					                                                    <ul class="uk-list  uk-list-space idz-list-custom">
					                                                        <li><a href="contacto.asp"><i class="uk-icon-envelope-o"></i>Contactenos</a></li>
					                                                        <li><a href="http://help.enviocertificado.com" target="_blank"><i class="uk-icon-book"></i>Tutoriales</a></li>
					                                                        <li><a href="http://help.enviocertificado.com" target="_blank"><i class="uk-icon-comments-o"></i>Foro de Debate</a></li>
					                                                        <li><a href="http://www.sysworld.com.ar/portal" target="_blank"><i class="uk-icon-exclamation-triangle"></i>Portal de Clientes</a></li>
					                                                    </ul>
					                                                </li>
					                                                <li class="uk-width-large-3-10 uk-hidden-medium">
					                                                    <h5>¿Porqué usar validadores?</h5>
					                                                    <p>Integrar sistemas automáticos de validacion en sus procesos le garantiza poder contactar a sus clientes y reducir costos en la comunicación.</p>
					                                                </li>
					                                            </ul>
					                                        </div>
					                                    </li>
											            <li><a href="funcionamiento.asp">Funcionamiento</a></li>
											            <li><a href="contacto.asp">Contactenos</a></li>
											        </ul>
											    </div>
											</nav>
											<!-- Mobile Menu Toggle  -->
                                            <a href="#mobile-nav" class="uk-navbar-toggle uk-visible-small uk-visible-medium" data-uk-offcanvas></a>
                                            <!-- Mobile Menu Toggle End-->
										</div>
										<!-- Menu Items End -->
									</div>


								</div>
							</div>

						</div>
					</div>
				</div>

			</div>
			<!-- Header Nav End -->

		</header>
		<!-- Header Section End -->

		<!-- Page Header -->
        <section id="idz-header-inner">
			<div class="uk-container uk-container-center">

				<div class="idz-page-title idz-padding-medium">
					<div class="uk-grid">
						<div class="uk-width-large-1-2 uk-width-medium-1-3">
							<div class="idz-title">
								<h1 class="">Validador de IDENTIDAD</h1>
								<i class="uk-icon-cube"></i>
							</div>
						</div>

						<div class="uk-width-large-1-2 uk-width-medium-2-3">
							<div class="uk-grid">
								<div class="uk-width-medium-3-4">
									<div class="idz-page-promo">
										<span>Verifique la identidad de sus usuarios.</span>
										<p>incremente la seguridad de sus transacciones.</p>
									</div>
								</div>
								<div class="uk-width-medium-1-4">
									<a href="#idz-pricing" class="link-plans uppercase" data-uk-smooth-scroll="">Ver Planes <i class="uk-icon-angle-double-down"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</section>

		<section class="idz-padding uk-padding-bottom-remove">
			<div class="uk-container uk-container-center">
				<div class="uk-grid">

					<div class="uk-width-large-2-3 uk-width-medium-1-1  uk-width-small-1-1">

						<ul class="uk-grid uk-grid-width-medium-1-3 idz-list-divider dark">
							<li>
								<div class="idz-list-item idz-padding-small">
									<div class="icon-wrap large center blue uk-margin-remove">
										<i class="uk-icon-sitemap"></i>
									</div>
									<div class="uk-display-block uk-text-center">
										<span class="idz-text-block uk-align-center">Potente Web API</span>
									</div>
								</div>
							</li>
							<li>
								<div class="idz-list-item idz-padding-small">
									<div class="icon-wrap large center blue uk-margin-remove">
										<i class="uk-icon-dashboard"></i>
									</div>
									<div class="uk-display-block uk-text-center">
										<span class="idz-text-block uk-align-center">Efectividad del 99,9 %</span>
									</div>
								</div>
							</li>
							<li>
								<div class="idz-list-item idz-padding-small">
									<div class="icon-wrap large center blue uk-margin-remove">
										<i class="uk-icon-download"></i>
									</div>
									<div class="uk-display-block uk-text-center">
										<span class="idz-text-block uk-align-center">Tutoriales de Integración</span>
									</div>
								</div>
							</li>
						</ul>

					</div>

					<div class="uk-width-large-1-3 uk-width-medium-1-1">
						<div class="idz-intro-text idz-margin-bottom-medium idz-resp-center-text">
							<h2 class="idz-text-28px ">Cartera Segura.</h2>
							<p class="idz-text-18px idz-lineheight-1-8">Poner el foco en la verificación de su carterá le permitirá mejorar sus transacciones y mitigar riesgos.</p>
						</div>
					</div>

				</div>
			</div>
		</section>

		<section class="idz-padding uk-padding-bottom-remove">
			<div class="uk-container uk-container-center">
				<div class="uk-panel uk-panel-box idz-panel">
					<div class="uk-grid">
						<div class="uk-width-medium-1-2">
							<p class="idz-text-18px uk-margin-remove idz-text-blue">La herramienta a medida de su necesidad.</p>
							<h1>Validador de Identidad.</h1>
							<div class="icon-wrap left">
								<i class="smico-layers-2"></i>
							</div>
							<p class="idz-text-15px idz-lineheight-1-8">El sistema de validacion de identidad de CentroDeValidaciones.com es una herramienta que permite integrarse de manera dinámica a la necesidad de cada uno de nuestros clientes.</p>
						</div>
						<div class="uk-width-medium-1-2">
							<ul class="uk-list idz-list-check idz-padding-top-small">
								<li>Validación contra datos personales.</li>
								<li>Validación webcam documentación respaldatoria.</li>
								<li>Validación y confirmación de Email y Teléfono.</li>
								<li>Validación con confirmación de Domicilios Postales.</li>
								<li>Validación contra Gateways de Pagos.</li>
								<li>Validación de Firmas Biométricas y Ológrafa.</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</section>


		<!-- Content Section -->
		<section class="idz-padding">
			<div class="uk-container uk-container-center">
				<div class="uk-grid">

					<div class="uk-width-medium-1-3 uk-container-center">
						<img src="images/sample/cloud-tasks.png" alt="cloud tasks" class="uk-img-preserve idz-fit-img uk-margin-bottom">
						<h1>Validaciones a medida de cada negocio.</h1>
						<p class="idz-text-18px idz-text-blue idz-margin-bottom-medium">Utilice los diferentes tipos de validadores de identidad según la necesidad de sus procesos. </p>
					</div>

					<div class="uk-width-large-2-3 uk-width-medium-2-3 uk-width-small-1-1 uk-container-center">
						<ul class="uk-grid uk-grid-width-medium-1-1 idz-services-list">
							<li>
								<div class="uk-grid">
									<div class="uk-width-large-1-5 uk-width-medium-1-5 uk-width-small-1-4">
										<div class="uk-margin-top"><div class="icon-wrap"><i class="smico-profile"></i></div></div>
									</div>
									<div class="uk-width-large-4-5 uk-width-medium-4-5 uk-width-small-3-4">
										<div class="idz-service-content">
											<h3 class="idz-text-22px idz-margin-bottom-tiny">Datos Personales</h3>
											<p class="idz-text-15px idz-lineheight-1-8 uk-margin-top-remove">Mediante una encuesta multiple-choice obtenga un score y verifique la coincidencia entre "QUIEN DICE SER" y "QUIEN REALMENTE ES"</p>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="uk-grid">
									<div class="uk-width-large-1-5 uk-width-medium-1-5 uk-width-small-1-4">
										<div class="uk-margin-top"><div class="icon-wrap"><span></span><i class="smico-photo"></i></div></div>
									</div>
									<div class="uk-width-large-4-5 uk-width-medium-4-5 uk-width-small-3-4">
										<div class="idz-service-content">
											<h3 class="idz-text-22px idz-margin-bottom-tiny">Validación Webcam</h3>
											<p class="idz-text-15px idz-lineheight-1-8 uk-margin-top-remove">Gracias a la integración webcam, el sistema permite capturar desde la foto del individuo hasta imagenes respaldatorias como cédulas, documentos, comprobantes de pago, etc.</p>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="uk-grid">
									<div class="uk-width-large-1-5 uk-width-medium-1-5 uk-width-small-1-4">
										<div class="uk-margin-top"><div class="icon-wrap"><span></span><i class="smico-paper-plane"></i></div></div>
									</div>
									<div class="uk-width-large-4-5 uk-width-medium-4-5 uk-width-small-3-4">
										<div class="idz-service-content">
											<h3 class="idz-text-22px idz-margin-bottom-tiny">Email y Teléfono</h3>
											<p class="idz-text-15px idz-lineheight-1-8 uk-margin-top-remove">Mediante links o códigos de seguridad podrá verificar realmente que su cliente está entregando correctamente el dato de contacto.</p>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="uk-grid">
									<div class="uk-width-large-1-5 uk-width-medium-1-5 uk-width-small-1-4">
										<div class="uk-margin-top"><div class="icon-wrap"><span></span><i class="smico-present"></i></div></div>
									</div>
									<div class="uk-width-large-4-5 uk-width-medium-4-5 uk-width-small-3-4">
										<div class="idz-service-content">
											<h3 class="idz-text-22px idz-margin-bottom-tiny">Domicilios Postales</h3>
											<p class="idz-text-15px idz-lineheight-1-8 uk-margin-top-remove">Gracias a la integración con correos postales, mediante el proceso de validación puede enviar una carta postal a un domicilio. El receptor al recibirla ingresará un código de seguridad en un link con el fin de verificar su domicilio.</p>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="uk-grid">
									<div class="uk-width-large-1-5 uk-width-medium-1-5 uk-width-small-1-4">
										<div class="uk-margin-top"><div class="icon-wrap"><span></span><i class="smico-wallet"></i></div></div>
									</div>
									<div class="uk-width-large-4-5 uk-width-medium-4-5 uk-width-small-3-4">
										<div class="idz-service-content">
											<h3 class="idz-text-22px idz-margin-bottom-tiny">Gateway de Pagos</h3>
											<p class="idz-text-15px idz-lineheight-1-8 uk-margin-top-remove">Al integrar el módulo de gateway de pagos, podrá generar un mínimo gasto de $1 en la tarjeta de crédito de su cliente y validar sus datos personales como Nombre, Apellido y DNI.</p>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="uk-grid">
									<div class="uk-width-large-1-5 uk-width-medium-1-5 uk-width-small-1-4">
										<div class="uk-margin-top"><div class="icon-wrap"><span></span><i class="smico-pencil"></i></div></div>
									</div>
									<div class="uk-width-large-4-5 uk-width-medium-4-5 uk-width-small-3-4">
										<div class="idz-service-content">
											<h3 class="idz-text-22px idz-margin-bottom-tiny">Firmas Biométricas y Ológrafas</h3>
											<p class="idz-text-15px idz-lineheight-1-8 uk-margin-top-remove">Incorporando tabletas digitales de firma a sus procesos, podrá validar las firmas de sus clientes ya sea mediante procesos standard o procesos de cálculos biométricos.</p>
										</div>
									</div>
								</div>
							</li>

						</ul>
					</div>

					<div class="uk-width-medium-1-1 idz-padding-small">
						<div class="idz-cta">
							<p>Para conocer nuestros planes de validación de identidad <a href="contacto.asp"><span class="idz-text-block idz-text-12px red">CONTACTENOS</span></a></p>
						</div>
					</div>

				</div>
			</div>
		</section>
		<!-- Content Section End -->


		<section class="uk-position-relative">
			<div class="uk-container uk-container-center">
				<div class="uk-grid">

					<div class="uk-width-large-1-1 uk-width-medium-1-1 uk-width-small-1-1 idz-padding-top idz-padding-bottom bg-parallax2"  data-uk-parallax="{bg: 0}">
						<div class="uk-grid">

							<div class="uk-width-large-2-3 uk-width-medium-1-1  uk-width-small-1-1">
								<div class="uk-grid" data-uk-margin="">

									<div class="uk-width-medium-1-1 uk-margin-bottom">
										<p class="idz-text-white uk-margin-remove">Infraestructura con balanceos de carga</p>
										<h1 class="idz-text-white">Capacidad de transacciones ilimitadas</h1>
									</div>

									<div class="uk-width-medium-1-2">
										<div class="icon-wrap small left">
					                        <i class="smico-processor idz-text-white"></i>
					                    </div>
					                    <h3 class="idz-text-22px uk-margin-small-bottom idz-text-white">Crecimiento Ilimitado</h3>
					                    <p class="uk-nbfc idz-text-15px  idz-lineheight-1-8  uk-margin-remove idz-text-white">Gracias a nuestra infraestructura dinámica y escalable sus consultas pueden crecer de forma ilimitada sin realizar cambios.</p>
									</div>

									<div class="uk-width-medium-1-2">
										<div class="icon-wrap small left">
					                        <i class="smico-settings-2 idz-text-white"></i>
					                    </div>
					                    <h3 class="idz-text-22px uk-margin-small-bottom idz-text-white">Facturación por Uso</h3>
					                   	<p class="uk-nbfc idz-text-15px  idz-lineheight-1-8  uk-margin-remove idz-text-white">La facturación del servicio se realiza en base al plan contratado y con la posibilidad de escalar dinamicamente los excedentes.</p>
									</div>

								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<div class="idz-color-overlay red"></div>
		</section>

		<section class="idz-padding">
			<div class="uk-container uk-container-center">
				<div class="uk-grid">

					<div class="uk-width-medium-1-1 uk-container-center">
						<h1 class="uk-text-center uk-margin-large">La Calidad y Seguridad del servicio son nuestras prioridades.</h1>
						<ul class="uk-grid uk-grid-collapse  uk-grid-width-large-1-4  uk-grid-width-medium-1-4 uk-grid-width-small-1-2 web-app-list uk-text-center">
							<li>
								<div class="icon-wrap center">
									<i class="smico-timer"></i>
								</div>
								<h4 class="uk-margin-remove">24 hs</h4>
								<p class="uk-margin-remove idz-text-12px uppercase">Servicio disponible 24x7</p>
							</li>
							<li>
								<div class="icon-wrap center">
									<i class="smico-browser"></i>
								</div>
								<h4 class="uk-margin-remove">Facturación Transparente</h4>
								<p class="uk-margin-remove idz-text-12px uppercase">Panel de control de transacciones.</p>
							</li>
							<li>
								<div class="icon-wrap center">
									<i class="smico-mailbox"></i>
								</div>
								<h4 class="uk-margin-remove">Efectividad de Respuesta</h4>
								<p class="uk-margin-remove idz-text-12px uppercase">99.9% de efectividad.</p>
							</li>
							<li>
								<div class="icon-wrap center">
									<i class="smico-graph"></i>
								</div>
								<h4 class="uk-margin-remove">Estadísticas</h4>
								<p class="uk-margin-remove idz-text-12px uppercase">Gráficos y Estadísticas.</p>
							</li>
							<li>
								<div class="icon-wrap center">
									<i class="smico-bank"></i>
								</div>
								<h4 class="uk-margin-remove">Seguridad de Información</h4>
								<p class="uk-margin-remove idz-text-12px uppercase">Confidencialidad y Privacidad.</p>
							</li>
							<li>
								<div class="icon-wrap center">
									<i class="smico-medal-gold"></i>
								</div>
								<h4 class="uk-margin-remove">Certificaciones</h4>
								<p class="uk-margin-remove idz-text-12px uppercase">ISO 9001, ISO 27001 y AntiSpam.</p>
							</li>
							<li>
								<div class="icon-wrap center">
									<i class="smico-sitemap-2"></i>
								</div>
								<h4 class="uk-margin-remove">Alta Disponibilidad</h4>
								<p class="uk-margin-remove idz-text-12px uppercase">Redundancia en Tiempo Real</p>
							</li>
							<li>
								<div class="icon-wrap center">
									<i class="smico-speed"></i>
								</div>
								<h4 class="uk-margin-remove">Performance Garantizada</h4>
								<p class="uk-margin-remove idz-text-12px uppercase">Promedio de Rta. 4,5 seg.</p>
							</li>
						</ul>
					</div>

				</div>
			</div>
		</section>

		<!-- Footer -->
		<footer id="idz-footer" class="idz-padding">
			<div class="uk-container uk-container-center">
				<div class="uk-grid">

					<!-- Footer Column 1 -->
					<div class="uk-width-large-1-4 uk-width-medium-1-4 uk-width-small-1-2">
						<div class="idz-footer-widget">
							<h3 class="uppercase idz-text-white">Compañía</h3>
							<ul class="uk-list uk-list-space idz-footer-menu">
								<li><a href="contacto.asp">Contactenos</a></li>
								<li><a href="http://www.sysworld.com.ar/legal.asp" target="_blank">Términos y Condiciones</a></li>
								<li><a href="http://www.sysworld.com.ar/legal.asp" target="_blank">Politica de Privacidad</a></li>
								<li><a href="http://www.sysworld.com.ar/" target="_blank">Sitio Coorporativo</a></li>
							</ul>
						</div>
					</div>
					<!-- Footer Column 1 End -->

					<!-- Footer Column 2 -->
					<div class="uk-width-large-1-4 uk-width-medium-1-4 uk-width-small-1-2">
						<div class="idz-footer-widget">
							<h3 class="uppercase idz-text-white">Soporte</h3>
							<ul class="uk-list uk-list-space idz-footer-menu">
								<li><a href="http://help.enviocertificado.com" target="_blank">Tutoriales</a></li>
								<li><a href="http://cloud.sysworld.com.ar" target="_blank">Portal de Tickets</a></li>
								<li><a href="http://erp.sysworld.com.ar/app" target="_blank">Portal Administrativo</a></li>
								<li><a href="contacto.asp" target="_blank">Contacto</a></li>
							</ul>
						</div>
					</div>
					<!-- Footer Column 2 End -->

					<!-- Footer Column 3 -->
					<div class="uk-width-large-1-4 uk-width-medium-1-4 uk-width-small-1-2">
						<div class="idz-footer-widget">
							<h3 class="uppercase idz-text-white">Validadores</h3>
							<ul class="uk-list uk-list-space idz-footer-menu">
								<li><a href="validadoremail.asp">Email</a></li>
								<li><a href="validadortelefono.asp">Teléfono</a></li>
								<li><a href="validadordomicilio.asp">Domicilio</a></li>
								<li><a href="validadoridentidad.asp">Identidad</a></li>
							</ul>
						</div>
					</div>
					<!-- Footer Column 3 End -->

					<!-- Footer Column 4 (Social Profile Links) -->
					<div class="uk-width-large-1-4 uk-width-medium-1-4 uk-width-small-1-2">
						<div class="idz-footer-widget idz-footer-social">
							<ul class="uk-list">
								<li>
									<div class="idz-button-group">
										<a target="blank" href="https://www.facebook.com/SysWorld.ar/"><i class="uk-icon-facebook-official"></i>seguinos en nuestro<span class="uppercase">facebook</span></a>
									</div>
								</li>
								<li>
									<div class="idz-button-group">
										<a target="blank" href="https://twitter.com/sysworldsa"><i class="uk-icon-twitter"></i>seguinos en nuestro<span class="uppercase">Twitter</span></a>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<!-- Footer Column 4 (Social Profile Links) -->


				</div>

				<!-- Footer Text -->
				<div class="uk-grid">
					<div class="uk-width-medium-1-1 uk-container-center uk-text-center">
						<p class="idz-footer-note idz-thin idz-text-22px uk-margin-remove">Ya validamos <span><%=FormatNumber(DateDiff("s", "11/08/2006 15:22:12", now),0)%> </span>transacciones</p>
						<p class="idz-site-copyright uk-margin-remove">COPYRIGHT  &copy; © 2006-<%=year(date)%> EnvioCertificado.com - Sysworld Servicios S.A.</p>
					</div>
				</div>
				<!-- Footer Text End -->
			</div>

			<!-- Scroll to Top -->
			<a href="#top" class="to-top uk-icon-chevron-up" data-uk-smooth-scroll></a>
			<!-- Scroll to Top End -->
        </footer>
		<!-- Footer End -->

		<!-- client area modal begin -->
        <div id="client-area" class="uk-modal">
            <div class="uk-modal-dialog">

                <ul id="client-area-tab" class="uk-tab uk-tab-grid" data-uk-tab="{connect:'#account'}">
                    <li class="uk-active uk-width-large-1-2"><a href="">Crear Cuenta</a></li>
                    <li class="uk-width-large-1-2"><a href="#">Registrarse</a></li>
                </ul>

                <ul id="account" class="uk-switcher uk-margin">
                	<!-- Modal Login Form -->
                    <li>
                        <form id="signin-account" class="uk-form">
                            <fieldset>
                                <div class="uk-form-icon">
                                    <i class="uk-icon-envelope"></i>
                                    <input type="text" placeholder="Email">
                                </div>
                                <div class="uk-form-icon">
                                    <i class="uk-icon-key"></i>
                                    <input type="text" placeholder="Password">
                                </div>
                                <label>
                                    <input type="checkbox">Remember me</label>
                                	<a href="http://web.enviocertificado.com" target="_blank" class="uk-button uk-button-large idz-button blue uk-width-large-1-1">Ingresar</a>
                            </fieldset>
                        </form>
                    </li>
                	<!-- Modal Login Form End -->

					<!-- Modal Register Form -->
                    <li>
                        <form id="create-account" class="uk-form" action="http://cloud.sysworld.com.ar/clients/register" target="_blank">
                            <fieldset>
                                <div class="uk-form-icon">
                                    <i class="uk-icon-user"></i>
                                    <input type="text" placeholder="Usuario">
                                </div>
                                <div class="uk-form-icon">
                                    <i class="uk-icon-envelope"></i>
                                    <input type="text" placeholder="Email">
                                </div>
                                <div class="uk-form-icon">
                                    <i class="uk-icon-key"></i>
                                    <input type="text" placeholder="Password">
                                </div>
                                <label>
                                    <input type="checkbox">Acepto los términos y condiciones.</label>
                                <a href="http://cloud.sysworld.com.ar/clients/register" target="_blank" class="uk-button uk-button-large idz-button blue uk-width-large-1-1">Registrarse</a>
                            </fieldset>
                        </form>
                    </li>
                    <!-- Modal Register Form End -->
                </ul>
                <div class="uk-modal-caption">
                    <a href="#">Forgot your password?</a>
                </div>
            </div>
        </div>
        <!-- client area modal end -->

        <!-- Javascript -->
        <script src="js/jquery.js"></script>
        <script src="js/uikit.min.js"></script>
		<script src="js/components/slider.js"></script>
        <script src="js/components/slideshow.js"></script>
        <script src="js/components/slideshow-fx.js"></script>
        <script src="js/components/parallax.js"></script>
        <script src="js/components/sticky.js"></script>
        <script src="js/jquery.countTo.js"></script>
        <script src="js/template-config.js"></script>
    </body>
</html>
