<?php
    include 'functions/db.php';

    include_once 'functions/language.php';




?>
<!DOCTYPE html>
<html lang="<?php echo $lang['SITE_lang'];?>">

<!-- Mirrored from protechtheme.com/saas/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:17:32 GMT -->
<head>
        <!-- Required meta tags -->
        <meta charset="iso-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="keywords" content="<?php echo $lang['SITE_keywords'];?>">
		<meta name="description" content="<?php echo $lang['SITE_desc'];?>">
		<meta name="author" content="Sysworld Servicios S.A.">
        <title><?php echo $lang['SITE_title'];?></title>

        <!-- Bootstrap core CSS -->
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <!-- Select2 -->
        <link href="assets/select2/css/select2.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- Iconmoon -->
        <link href="assets/iconmoon/css/iconmoon.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="css/custom.css" rel="stylesheet">
        <link rel="icon" type="image/x-icon" href="images/favicon.ico" />
    </head>
    <body>

<!-- ==============================================
**Preloader**
=================================================== -->
        <div id="loader">
            <div id="element">
                <div class="circ-one"></div>
                <div class="circ-two"></div>
            </div>
        </div>

<!-- ==============================================
**Header**
=================================================== -->
        <header>
			<?php include 'menu.php';?>
        </header>

<!-- ==============================================
**Inner Banner**
=================================================== -->
        <section class="inner-banner contact-banner">
            <div class="container">
                <div class="contents">
                    <h1><?php echo $lang['CONTACT_header_tittle'];?></h1>
                    <p><?php echo $lang['CONTACT_header_subtittle'];?></p>
                </div>
            </div>
        </section>

<!-- ==============================================
**Contact**
=================================================== -->
        <section class="contact-wrapper-outer">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 form-area">
                        <div class="contact-form-wrapper padding-lg">
                            <iframe width="600" height="600" src="http://cloud.sysworld.com.ar/forms/wtl/20cc38c01fe29ce640581ae67433c250" frameborder="0" allowfullscreen=""></iframe>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="contact-info-wrapper padding-lg">
                            <div class="contact-info">
                                <h3><?php echo $lang['CONTACT_info_tittle'];?></h3>
                                <ul class="info-contact-box">
                                    <li>
                                        <h6>Bucarelli 2480, Ciudad de Buenos Aires, Argentina</h6>
                                    </li>
                                    <li>
                                        <h6>(011) 5277-8900
                                            (011) 5263-2919 </h6>
                                    </li>
                                    <li> <a href="info@sysworld.com.ar">info@sysworld.com.ar</a> </li>
                                </ul>
                            </div>
                            <div class="social-media-box">
                                <h6><span><?php echo $lang['CONTACT_info_item_bottom'];?></span></h6>
                                <ul>
                                    <li><a href="https://www.facebook.com/SysWorld.ar/"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="https://twitter.com/sysworldsa"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="https://ar.linkedin.com/company/sysworld"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<!-- ==============================================
**Contact Map**
=================================================== -->
        <section class="contact-map">
            <div class="msg-box"><p><i class="fa fa-mouse-pointer" aria-hidden="true"></i> click and scroll to zoom the map</p></div>

			<div>
				<!-- Google map -->
				<div id="map" style="height: 600px"></div>
				<script>
					var map;
					function initMap() {
						map = new google.maps.Map(document.getElementById('map'), {
							center: {lat: -34.575097, lng: -58.489339},
							zoom: 16,
							scrollwheel: false//set to true to enable mouse scrolling while inside the map area
						});
					}
				</script>
				<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAccfiKin8RJkMkJ9FhZd_xLlTfB2SzIxU&callback=initMap"
						async defer>
				</script>

				<!-- End Google map -->
			</div>

        </section>

<!-- ==============================================
**Signup Section**
=================================================== -->

<!-- ==============================================
**Footer opt1**
=================================================== -->

	<?php include 'footer.php';?>



        <!-- Scroll to top -->
        <a href="#" class="scroll-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>

        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery.min.js"></script>
        <!-- Popper JS -->
        <script src="js/popper.min.js"></script>
        <!-- Bootsrap JS -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- Select2 JS -->
        <script src="assets/select2/js/select2.min.js"></script>
        <!-- Match Height JS -->
        <script src="assets/matchHeight/js/matchHeight-min.js"></script>
        <!-- Jquery Validate JS -->
        <script src="js/validate.min.js"></script>
        <!-- Custom JS -->
        <script src="js/custom.js"></script>
    </body>

<!-- Mirrored from protechtheme.com/saas/contact.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Apr 2019 13:18:10 GMT -->
</html>